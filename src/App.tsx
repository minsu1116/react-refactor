import React, {Suspense} from 'react';
import {CircularProgress} from '@mui/material';
import {Routes, Route, Navigate} from 'react-router-dom';
import {SWRConfig} from 'swr';
import './global';
import Spinner from 'components/atoms/spinner/Spinner';

const MainLayout = React.lazy(() => import('@page/layout/MainLayout'));
const UnauthorizedLayout = React.lazy(
    () => import('@page/authorized/UnauthorizedLayout'),
);

const App = () => {
    return (
        <React.Fragment>
            <SWRConfig value={{provider: () => new Map()}}>
                <Spinner />
                <Suspense fallback={<CircularProgress />}>
                    <Routes>
                        <Route path="/app" element={<MainLayout />} />
                        <Route
                            path="/auth/*"
                            element={<UnauthorizedLayout />}
                        />
                        <Route
                            path="*"
                            element={<Navigate replace to="/app" />}
                        />
                    </Routes>
                </Suspense>
            </SWRConfig>
        </React.Fragment>
    );
};

export default App;
