<<<<<<< HEAD
=======
/* eslint-disable prettier/prettier */
>>>>>>> dev_new
import React, {useEffect, useMemo, useState} from 'react';
import {RgbaStringColorPicker} from 'react-colorful';

import {colord, extend} from 'colord';
import namesPlugin from 'colord/plugins/names';

import color_src from '@img/ico_color.png';
import './style.scss';

extend([namesPlugin]);

const CustomPicker = ({color, tit, ...rest}) => {
    const [colorString, setColorString] = useState<string>();
    const rgbaString = useMemo(() => {
        return color.startsWith('rgba') ? color : colord(color).toRgbString();
    }, [color]);

    useEffect(() => {
        setColorString(colord(color).toHex());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [rgbaString]);

    const [isDetailOpen, setIsDetailOpen] = useState(false);

    const onClickDetailOpen = () => {
        const react_colorful_saturation = document.querySelectorAll<HTMLElement>(
                '.react-colorful__saturation',
            );

        const react_colorful =
            document.querySelectorAll<HTMLElement>('.react-colorful');

        if (isDetailOpen) {
            react_colorful_saturation.forEach((element) => {
                element.style.display = 'none';
                element.style.opacity = '0';
                element.style.marginBottom = '0px';
            });

            react_colorful.forEach((element) => {
                element.style.height = '32px';
            });
            // react_colorful_saturation[index].style.display = 'none';
            // react_colorful_saturation[index].style.opacity = '0';
            // react_colorful_saturation[index].style.marginBottom = '0px';
            // react_colorful[index].style.height = '32px';
        } else {
            react_colorful_saturation.forEach((element) => {
                element.style.display = 'block';
                element.style.opacity = '1';
                element.style.marginBottom = '5px';
            });

            react_colorful.forEach((element) => {
                element.style.height = '120px';
            });
            // react_colorful_saturation[index].style.display = 'block';
            // react_colorful_saturation[index].style.opacity = '1';
            // react_colorful_saturation[index].style.marginBottom = '5px';
            // react_colorful[index].style.height = '120px';
        }

        setIsDetailOpen(!isDetailOpen);
    };

    return (
        <div className="color_picker">
            <div className="tit">
                <div className="tit_img">
                    <img
                        className={`color_ico`}
                        src={color_src}
                        onClick={onClickDetailOpen}
                    />
                </div>
                <div className="tit_nm">
                    <div className="tit_nm_s" style={{backgroundColor: color}}>
                        {colorString}
                    </div>
                    <div className="tit_nm_m">{tit ? tit : '색상'}</div>
                </div>
            </div>
            <div className="color_picker_main">
                <RgbaStringColorPicker color={rgbaString} {...rest} />
            </div>
        </div>
    );
};

export default React.memo(CustomPicker);
