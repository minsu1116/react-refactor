import React, {useState} from 'react';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import moment from 'moment';
import './style.scss';
import {meg} from 'override/alert/Alert';

interface Param {
    label: string;
    id: string;
    inputFormat: string;
    /* eslint-disable @typescript-eslint/no-explicit-any */
    views?: any;
    mask: string;
    disabled: boolean;
    // eslint-disable-next-line @typescript-eslint/ban-types
    handleChange: Function;
    returnFormat?: string;
    defaultValue: string;
    width: number;
    validation?: boolean;
}

const DatePicker = (param: Param) => {
    const [dateValue, setDateValue] = useState(
        moment(param.defaultValue).format(param.inputFormat.toUpperCase()),
    );

    return (
        <>
            <div className="date_picker">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DesktopDatePicker
                        label={param.label}
                        value={dateValue}
                        mask={param.mask}
                        views={param.views}
                        inputFormat={param.inputFormat}
                        disabled={param.disabled}
                        onChange={async (newValue) => {
                            let setValue = newValue;
                            if (param.validation) {
                                if (
                                    moment().format(
                                        param.inputFormat.toUpperCase(),
                                    ) >
                                    moment(newValue).format(
                                        param.inputFormat.toUpperCase(),
                                    )
                                ) {
                                    await meg({
                                        title: `과거로 지정은 불가능 합니다.`,
                                        html: '',
                                    });

                                    setValue = dateValue;
                                }
                            }

                            setDateValue(setValue);
                            const returnValue = {
                                target: {
                                    id: param.id,
                                    value: moment(setValue).format(
                                        param.returnFormat
                                            ? param.returnFormat
                                            : param.inputFormat.toUpperCase(),
                                    ),
                                },
                            };
                            param.handleChange(returnValue);
                        }}
                        renderInput={(params) => {
                            // params.inputProps.value = moment(
                            //     params.inputProps.value,
                            // ).format('YYYY-MM-DD');
                            return (
                                <TextField
                                    sx={{width: param.width}}
                                    size="small"
                                    {...params}
                                />
                            );
                        }}
                    />
                </LocalizationProvider>
            </div>
        </>
    );
};
export default DatePicker;
