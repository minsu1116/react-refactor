import React, {useState} from 'react';
import TextField from '@mui/material/TextField';
import DateRangePicker, {DateRange} from '@mui/lab/DateRangePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import Box from '@mui/material/Box';
import moment from 'moment';
import './style.scss';
import { StaticDateRangePicker } from '@mui/lab';

interface DatePicker {
    startTit: string;
    endTit: string;
    fromDate: string;
    toDate: string;
    disabled: boolean;
    // eslint-disable-next-line @typescript-eslint/ban-types
    returnValueChange: Function;
}

const DatePickerRange = (param: DatePicker) => {
    const startText = param?.startTit || 'From';
    const endText = param?.endTit || 'To';
    const fromDate = param?.fromDate
        ? new Date(moment(param?.fromDate).format('YYYY-MM-DD'))
        : new Date(moment().format('YYYY-MM-DD'));
    const toDate = param?.toDate
        ? new Date(moment(param?.toDate).format('YYYY-MM-DD'))
        : new Date(moment().add(7, 'days').format('YYYY-MM-DD'));
    const returnValueChange = param?.returnValueChange;
    const disabled = param?.disabled;
    const [value, setValue] = useState<DateRange<Date>>([fromDate, toDate]);
    return (
        <>
<<<<<<< HEAD
            <LocalizationProvider dateAdapter={AdapterDateFns}>
                <StaticDateRangePicker
                    displayStaticWrapperAs="desktop"
                    value={value}
                    onChange={(newValue) => {
                        setValue(newValue);
                    }}
                    renderInput={(startProps, endProps) => (
                        <React.Fragment>
                            <TextField {...startProps} />
                            <Box sx={{mx: 2}}> to </Box>
                            <TextField {...endProps} />
                        </React.Fragment>
                    )}
                />
            </LocalizationProvider>
=======
            <div className="date_picker_range">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DateRangePicker
                        startText={startText}
                        endText={endText}
                        disabled={disabled}
                        mask={'____-__-__'}
                        inputFormat={'yyyy-MM-dd'}
                        value={value}
                        onChange={(newValue) => {
                            setValue(newValue);
                            if (returnValueChange) {
                                returnValueChange(newValue);
                            }
                        }}
                        renderInput={(startProps, endProps) => (
                            <React.Fragment>
                                <TextField size="small" {...startProps} />
                                <Box sx={{mx: 1}}> ~ </Box>
                                <TextField size="small" {...endProps} />
                            </React.Fragment>
                        )}
                    />
                </LocalizationProvider>
            </div>
>>>>>>> dev_new
        </>
    );
};
export default DatePickerRange;
