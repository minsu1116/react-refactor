<<<<<<< HEAD
import React from 'react';
=======
import React, {useEffect, useState} from 'react';
>>>>>>> dev_new
// Require Editor JS files.
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/js/plugins.pkgd.min.js';
import 'froala-editor/js/third_party/embedly.min.js';
// import "froala-editor/js/plugins/fullscreen.min.js"

// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'froala-editor/css/third_party/embedly.min.css';
import './style.scss';

import FroalaEditorView from 'react-froala-wysiwyg/FroalaEditorView';
import Froala from 'react-froala-wysiwyg';

interface Param {
    modal?: string;
    tit?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    readOnly: any;
    // eslint-disable-next-line @typescript-eslint/ban-types
    handleChange?: Function;
}

<<<<<<< HEAD
// eslint-disable-next-line react/display-name
export const Seditor = React.memo((param: Param) => {
    const readOnly: boolean = param?.readOnly || false;
=======
export const Seditor = (param: Param) => {
    const [readOnly, setReadOnly] = useState();
    const [renderReady, setRenderReady] = useState<boolean>(false);

>>>>>>> dev_new
    const handleModelChange = (model: string) => {
        param.handleChange(model);
    };

    useEffect(() => {
        setReadOnly(param.readOnly);
        setRenderReady(true);
    }, [param.readOnly]);

    const config = {
<<<<<<< HEAD
        // key: "IGLGTD1DMJf1BWLb1PO== ",
        // key: "IB2A6A1B1D3D1rD1H5G4A3B2C10A7E2E5D3eUh1QBRVCDLPAZMBQ==",
        key:
            'aLF3c1A7D6D6A2G3G2F2xROKLJKYHROLDXDRH1e1YYGRe1Bg1G3I3A2B5D6A3B2E4B2B2==',
=======
        key: 'aLF3c1A7D6D6A2G3G2F2xROKLJKYHROLDXDRH1e1YYGRe1Bg1G3I3A2B5D6A3B2E4B2B2==',
>>>>>>> dev_new
        height: '100%',
        attribution: false,
        charCounterCount: false,
        language: 'ko',
        quickInsertEnabled: false,
        tabSpaces: 4,

        //image
        imageDefaultWidth: 0, //auto
        imageDefaultAlign: 'left',
        imageUpload: false,

        listAdvancedTypes: true,
        toolbarButtons: [
            [
                'fontSize',
                'textColor',
                'backgroundColor',
                'emoticons',
                'specialCharacters',
            ],
            ['insertTable'],
            ['bold', 'italic', 'underline', 'strikeThrough'],
            [
                'alignLeft',
                'alignCenter',
                'alignRight',
                'formatOL',
                'formatUL',
                'outdent',
                'indent',
            ],
            ['undo', 'redo'],
        ],
        events: {
            // image가 파일 drag&drop으로 삽입될 경우 blob이 아닌 base64형태로 추가
            'image.beforeUpload': function (files) {
                // eslint-disable-next-line @typescript-eslint/no-this-alias
                const editor = this;
                if (files.length) {
                    for (let i = 0; i < files.length; i++) {
                        // Create a File Reader.
                        const reader = new FileReader();
                        // Set the reader to insert images when they are loaded.
                        reader.onload = function (e) {
                            const result = e.target.result;
                            editor.image.insert(
                                result,
                                null,
                                null,
                                editor.image.get(),
                            );
                        };
                        // Read image as base64.
                        reader.readAsDataURL(files[i]);
                    }
                }
                editor.popups.hideAll();
                // Stop default upload chain.
                return false;
            },
        },
    };

    return (
        <div className="seditor">
            {param.tit && <div className="tit">{param.tit}</div>}

            {renderReady && (
                <>
                    {!readOnly && (
                        <div className="seditor_main">
                            <Froala
                                model={param.modal}
                                onModelChange={handleModelChange}
                                tag="textarea"
                                config={config}
                            />
                        </div>
                    )}
                </>
            )}
            {readOnly && (
                <div className="seditor_read_only">
                    <FroalaEditorView model={param.modal} />
                </div>
<<<<<<< HEAD
            ) : (
                <Froala
                    model={param.modal}
                    onModelChange={handleModelChange}
                    tag="textarea"
                    config={config}
                />
=======
>>>>>>> dev_new
            )}
        </div>
    );
<<<<<<< HEAD
});
=======
};
>>>>>>> dev_new

export default Seditor;
