import React, {useCallback, useEffect, useState} from 'react';
import Dropzone from 'react-dropzone';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import {meg, confirm} from '@override/alert/Alert';
import API from 'override/api/API';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import './style.scss';

const URL = '/api/file';
const uploadConfig = {
    headers: {
        'Content-Type': 'multipart/form-data',
        Pragma: 'no-cache',
    },
};

export const saveFilesEditAll = async (saveList) => {
    if (saveList) {
        if (!saveList || saveList.filter((x) => x.newRow == 'Y').length == 0) {
            return;
        }
        try {
            const fileInfoItems = [];
            // let fileGrpId = "";
            saveList.map((item) => {
                if (item['fileInfo']) {
                    fileInfoItems.push(item['fileInfo']);
                    // if (!fileGrpId) {
                    //     // 대표로 fileGrpId 설정
                    //     fileGrpId = item[fileGrpIdField];
                    // }
                }
            });

            const fileGroupUpload = async (files, fileGrpId) => {
                if (files.length <= 0) return;

                let saveFileGrpId = fileGrpId;

                for (let i = 0; i < files.length; i++) {
                    const formData = new FormData();
                    formData.append('file', files[i]);
                    formData.append('fileGrpId', saveFileGrpId);

                    const result = await API.file.post(
                        `${URL}/groupupload`,
                        formData,
                        uploadConfig,
                    );
                    if (API.isLocal) {
                        console.log(
                            '#fileGroupUpload',
                            `${URL}/groupupload`,
                            {params: formData},
                            {result: result},
                        );
                    }
                    if (result.data.success) {
                        if (!saveFileGrpId || saveFileGrpId == '') {
                            saveFileGrpId = result.data.data.fileGrpId;
                        }
                    }
                }
                return saveFileGrpId;
            };

            const fileGrpId =
                saveList.filter((x) => x.fileGrpId)[0]?.fileGrpId || undefined;
            return await fileGroupUpload(fileInfoItems, fileGrpId);
        } catch (exception) {
            console.warn('그리드 파일 저장 시 에러 발생: ', exception);
            meg({
                title: `파일 저장 시 에러가 발생하였습니다.`,
                html: '',
            });
            return;
        }
    }
};

/**
 * @fileGrpId ** FILE_GRP_ID가 지정되는 Store 변수명
 * @showMessage 저장 항목 없음 알림, 저장 성공 알림 메시지 노출 옵션. [default: false]
 * @useFileAttach 파일첨부 버튼 사용여부. [default: true]
 * @useFileDelete 파일삭제 버튼 사용여부. [default: true]
 * @useDownload 파일다운로드 버튼 사용여부. [default: true]
 * @gridId 그리드 고유 ID
 */
const SFile = (param) => {
    const {useFileAttach, useFileDelete, gridId, fileGrpId, tit, isRefresh} =
        param;
    const {data} = Storage.getGridList();
    const [rowData, setRowData] = useState([]);
    const columnDefs = useCallback(() => {
        if (param?.useDownload) {
            // eslint-disable-next-line react-hooks/exhaustive-deps
            return [
                {headerName: '', field: 'check', width: 32, type: 'checkbox'},
                {
                    headerName: 'GROUP ID',
                    field: 'fileGroupId',
                    width: 60,
                    align: 'center',
                    hide: true,
                },
                {
                    headerName: 'FILE ID',
                    field: 'fileId',
                    width: 70,
                    align: 'center',
                    key: true,
                    hide: true,
                },
                {
                    headerName: '파일명',
                    field: 'orgFileNm',
                    width: 100,
                    hide: true,
                },
                {
                    headerName: '파일명',
                    field: 'fileId',
                    width: 300,
                    type: 'download',
                    fileNm: 'orgFileNm',
                    cellRendererParams: {byFileId: true},
                },
                {
                    headerName: '등록자',
                    field: 'inputEmpNm',
                    width: 80,
                    align: 'center',
                },
                {
                    headerName: '등록일',
                    field: 'inputDtm',
                    width: 150,
                    align: 'center',
                    type: 'dateTimeFull',
                },
            ];
        } else {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            return [
                {headerName: '', field: 'check', width: 32, type: 'checkbox'},
                {
                    headerName: 'GROUP ID',
                    field: 'fileGroupId',
                    width: 60,
                    align: 'center',
                    hide: true,
                },
                {
                    headerName: 'FILE ID',
                    field: 'fileId',
                    width: 70,
                    align: 'center',
                    key: true,
                    hide: true,
                },
                {headerName: '파일명', field: 'orgFileNm', width: 100},
                {
                    headerName: '등록자',
                    field: 'inputEmpNm',
                    width: 80,
                    align: 'center',
                },
                {
                    headerName: '등록일',
                    field: 'inputDtm',
                    width: 120,
                    align: 'center',
                    type: 'dateTimeFull',
                },
            ];
        }
    }, [param?.useDownload]);

    useEffect(() => {
        if (fileGrpId && fileGrpId != 0) {
            getFileInfo(fileGrpId);
        } else {
            setRowData([]);
        }
    }, [fileGrpId, isRefresh]);

    const getFileInfo = async (fileGrpId) => {
        if (fileGrpId) {
            const result = await API.request.get(
                `/api/file/getfileinfo?fileGrpId=${fileGrpId}`,
            );

            if (result.data.data) {
                setRowData(result.data.data);
            } else {
                setRowData([]);
            }
        } else {
            setRowData([]);
        }
    };

    const _handleFileAttach = async (event) => {
        const files = event.target.files;
        const tempSaveFileList = [];

        for (let i = 0; i < files.length; i++) {
            tempSaveFileList.push({
                fileInfo: files[i],
                orgFileNm: files[i].name,
                newRow: 'Y',
            });
        }

        const tempList = rowData.concat(tempSaveFileList);
        setRowData(tempList);
    };

    const _handleFileDelete = async () => {
        const fileGrid = data?.get(gridId);
        const rows = [];

        if (fileGrid) {
            fileGrid
                .getModel()
                .rowsToDisplay.map((item) => rows.push(item.data));

            const delList = rows.filter((x) => x.check == 'Y');
            if (delList.length == 0) {
                meg({
                    title: '삭제 할 항목이 없습니다.',
                    html: '',
                });
                return;
            }

            const deleteList = rows.filter((x) => x.check == 'Y' && !x.newRow);
            if (deleteList.length == 0) {
                const rowData = rows.filter((x) => x.check != 'Y');
                setRowData(rowData);
            } else {
                const confirmResult = await confirm({
                    title: '선택한 파일을 삭제하시겠습니까?',
                    html: '',
                });
                if (confirmResult) {
                    const apiDeleteList = rows.filter(
                        (x) => !x.newRow && x.check == 'Y',
                    );
                    apiDeleteList.forEach(async (element) => {
                        await API.request.get(
                            `/api/file/delete?fileId=${element.fileId}`,
                        );
                    });
                    const rowData = rows.filter((x) => x.check != 'Y');
                    setRowData(rowData);
                }
            }
        }
    };

    const _handleDrop = (files) => {
        if (files) {
            files.map((x) => {
                _addRow(x);
            });
        }
    };

    const _addRow = useCallback(
        (result) => {
            const fileGrid = data?.get(gridId);
            if (fileGrid) {
                if (fileGrid.destroyCalled) {
                    meg({
                        title: '첨부버튼으로 진행해 주십시오.',
                        html: '',
                    });
                    return;
                }

                const defaultValues = {
                    fileInfo: result,
                    orgFileNm: result.name,
                };
                if (result) {
                    const rowDataLength = rowData.length;
                    fileGrid.clearFocusedCell();
                    fileGrid.clearRangeSelection();
                    const addRow = {
                        editable: true,
                        newRow: 'Y',
                        check: 'Y',
                    };

                    columnDefs().map((item) => {
                        if (item.default && item.default != '') {
                            if (!addRow[item.field]) {
                                addRow[item.field] = item.default;
                            }
                        } else {
                            addRow[item.field] = '';
                        }
                        if (defaultValues) {
                            Object.keys(defaultValues).map((key) => {
                                if (key == item.field) {
                                    addRow[item.field] = defaultValues[key];
                                } else {
                                    addRow[key] = defaultValues[key];
                                }
                            });
                        }
                    });

                    rowData.push(addRow);
                    fileGrid.applyTransaction({
                        add: [addRow],
                        addIndex: rowDataLength,
                    });
                }
            }
        },
        [columnDefs, data, gridId, rowData],
    );

    return (
        <div className="file_upload">
            <div className="file_item">
                {tit && <div className="tit">{tit}</div>}
                {useFileAttach ? (
                    <Tooltip title="파일첨부">
                        <label htmlFor="file">
                            <input
                                id="file"
                                type="file"
                                multiple
                                style={{position: 'fixed', top: '-100em'}}
                                onChange={_handleFileAttach}
                            />
                            <IconButton component="span">
                                <AttachFileIcon className="btn_icon" />
                            </IconButton>
                        </label>
                    </Tooltip>
                ) : null}
                {useFileDelete ? (
                    <Tooltip title="파일 삭제">
                        <IconButton onClick={_handleFileDelete}>
                            <DeleteForeverIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                ) : null}
            </div>
            {useFileAttach ? (
                <Dropzone onDrop={_handleDrop} noClick={true}>
                    {({getRootProps}) => (
                        <div {...getRootProps({className: 'dropzone basic'})}>
                            <SGrid
                                grid={gridId}
                                rowData={rowData}
                                // height={200}
                                noRowsToShow={
                                    '업로드 할 파일을 드래그 해주십시오.'
                                }
                                columnDefs={columnDefs()}
                            />
                        </div>
                    )}
                </Dropzone>
            ) : (
                <SGrid
                    grid={gridId}
                    rowData={rowData}
                    // height={200}
                    noRowsToShow={'첨부 파일이 없습니다.'}
                    columnDefs={columnDefs()}
                />
            )}
        </div>
    );
};
export default SFile;
