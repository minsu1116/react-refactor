import React, {useEffect, useMemo, useState} from 'react';
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-enterprise';
import './style.scss';
import Storage from '@override/api/Storage';
import RowCellCheckbox from '@components/atoms/grid/editor/checkbox/RowCellCheckbox';
import {ColumnHeaderRenderer} from '@components/atoms/grid/render/ColumnHeaderRenderer';
import {RowCellIconButtonRender} from '@components/atoms/grid/render/popup/RowCellIconButtonRender';
import {RowCellButtonRender} from '@components/atoms/grid/render/btn/RowCellButtonRender';
import {RowCellFileDownloadRender} from '@components/atoms/grid/render/dowload/RowCellFileDownloadRender';
import {RowCellSelectRender} from '@components/atoms/grid/render/select/RowCellSelectRender';
import {meg, confirm} from '@override/alert/Alert';
import RowSpanCellRender from './render/rowSpan/RowSpanCellRender';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import LibraryAddIcon from '@mui/icons-material/LibraryAdd';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import {LicenseManager} from 'ag-grid-enterprise';
LicenseManager.setLicenseKey(
    'CompanyName=LS CNS,LicensedGroup=mshan@lscns.com,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=0,AssetReference=AG-028291,ExpiryDate=16_May_2023_[v2]_MTY4NDE5MTYwMDAwMA==5c936877cd7bf731ef822545e2a57890',
);

import {LicenseManager} from 'ag-grid-enterprise';
LicenseManager.setLicenseKey(
    'CompanyName=LS CNS,LicensedGroup=mshan@lscns.com,LicenseType=MultipleApplications,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=0,AssetReference=AG-028291,ExpiryDate=16_May_2023_[v2]_MTY4NDE5MTYwMDAwMA==5c936877cd7bf731ef822545e2a57890',
);

interface Param {
    grid: string;
    width?: string;
    height?: number;
    title?: string;
    isAddBtnOpen?: boolean;
    isModalOpen?: boolean;
    cellReadOnlyColor?: boolean;
    // eslint-disable-next-line @typescript-eslint/ban-types
    rowDoubleClick?: Function;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onModalOpen?: Function;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClickSave?: Function;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClickDelete?: Function;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    rowData: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    columnDefs: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    newRowDefault?: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onSelectionChanged?: any;
    noRowsToShow?: string;
    isForceDelete?: boolean;
}

export default function SGrid(param: Param) {
    let rowData = param.rowData;
    const grid = param.grid;
    const [gridApi, setGridApi] = useState(null);
    const [keyField, setKeyField] = useState([]);
    const [essentialField, setEssentialField] = useState([]);
    const {data, mutate} = Storage.getGridList();
    const width = useMemo(() => {
        return param.width;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const noRowsToShow = useMemo(() => {
        return param.noRowsToShow
            ? param.noRowsToShow
            : '조회된 데이터가 없습니다.';
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const height = useMemo(() => {
        return param.height;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.height]);
    const title = useMemo(() => {
        return param.title;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.title]);
    const isForceDelete = useMemo(() => {
        return param.isForceDelete;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.title]);
    const newRowDefault = useMemo(() => {
        return param.newRowDefault;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.newRowDefault]);
    const cellReadOnlyColor = useMemo(() => {
        return param.cellReadOnlyColor || false;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const rowDoubleClick = useMemo(() => {
        return param.rowDoubleClick;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.rowDoubleClick]);
    const commonCodeGroup = Storage.getGroupCode().data;
    const columns = useMemo(() => {
        return param.columnDefs;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param.columnDefs]);
    const gridOptions = useMemo(() => {
        return {
            suppressRowTransform: true, // span 사용자 필요
            stopEditingWhenCellsLoseFocus: true, // 그리드 외부 클릭 시 cell 편집 중지
            enterMovesDownAfterEdit: true, // enter 버튼 클릭 시 아래로 이동
            // , suppressContextMenu: true
            rowSelection: 'single',
            rowData: rowData,
            enableRangeSelection: true,
            columnTypes: {
                checkbox: {
                    cellRenderer: 'rowCellCheckbox',
                },
                iconButton: {
                    cellRenderer: 'rowCellIconButtonRender',
                },
                button: {
                    cellRenderer: 'rowCellButtonRender',
                },
                download: {
                    cellRenderer: 'rowCellFileDownloadRender',
                },
                select: {
                    cellRenderer: 'rowCellSelectRender',
                },
                dateTime: {
                    valueFormatter: function (params) {
                        const {value} = params;

                        try {
                            // nvarchar Date Case '20190101'
                            if (value.length == 8) {
                                return `${value.substring(
                                    0,
                                    4,
                                )}-${value.substring(4, 6)}-${value.substring(
                                    6,
                                    8,
                                )}`;
                            }
                            // DateTime Case
                            else if (value.length > 10) {
                                return value.substring(0, 10);
                            }
                            // nvarchar Time Case '0700'
                            else if (value.length == 4) {
                                return `${value.substring(
                                    0,
                                    2,
                                )}:${value.substring(2, 4)}`;
                            }
                        } catch {
                            return value;
                        }

                        return value;
                    },
                },
                dateTimeFull: {
                    valueFormatter: function (params) {
                        const {value} = params;
                        if (value) {
                            return value.substring(0, 19);
                        }
                        return;
                    },
                },
                number: {
                    cellClass: 'ag-numeric-cell',
                    valueFormatter: function (params) {
                        const decimalPoint = params.colDef.decimalPoint
                            ? params.colDef.decimalPoint
                            : 0;
                        const numberFormat = (value, decimalPoint = 0) => {
                            // 기존 ::  소수점 길이를 지정하지 않은 경우, DB에서 읽어온 대로 보여줬는데,
                            //          0 으로 처리하게 되면 소수점 이하 자리가 안 보임.
                            //          일단 기존에 나오던 대로 처리함.
                            if (!value) {
                                return value;
                                // params.value = 0; // 0
                            }

                            if (isNaN(value)) {
                                return value; // null
                            }
                            let data = String(Number(value));
                            const regx = new RegExp(/(-?\d+)(\d{3})/);
                            const bExists = data.indexOf('.', 0);
                            const strArr = data.split('.');
                            while (regx.test(strArr[0])) {
                                strArr[0] = strArr[0].replace(regx, '$1,$2');
                            }

                            // 소수점 자리수에 대해 고정되게 나오도록 처리
                            // decimalPoint = 2 인 경우 1.1 => 1.10
                            if (decimalPoint == 0) {
                                data = strArr[0];
                            } else {
                                let decimalValue = '';

                                if (bExists > -1) {
                                    decimalValue = strArr[1];
                                }

                                if (decimalValue.length > decimalPoint) {
                                    decimalValue = decimalValue.substring(
                                        0,
                                        decimalPoint,
                                    );
                                } else {
                                    decimalValue = decimalValue.padEnd(
                                        decimalPoint,
                                        '0',
                                    );
                                }

                                data = strArr[0] + '.' + decimalValue;
                            }

                            return data;
                        };
                        return numberFormat(params.value, decimalPoint);
                    },
                    valueParser: function (params) {
                        if (Number(params.newValue)) {
                            const decimalPoint = params.colDef.decimalPoint;
                            const strArr = params.newValue
                                ?.toString()
                                .split('.');
                            if (strArr.length == 1) {
                                return Number(params.newValue);
                            } else {
                                const point = strArr[1].substr(0, decimalPoint);
                                return Number(`${strArr[0]}.${point}`);
                            }
                        } else {
                            return 0;
                        }
                    },
                },
            },
            onRowSelected: _handleRowSelected,
            defaultColDef: {
                resizable: true,
                filter: true, // header filter
                editable: false,
                //, tooltipComponent: "gridTooltip"
                sortable: true,
                menuTabs: [],
                cellClass: _getCellClass,
            },
            autoGroupColumnDef: {
                width: 30,
            },
            // EVENTS
            // Add event handlers
            frameworkComponents: {
                rowCellCheckbox: RowCellCheckbox,
                rowCellIconButtonRender: RowCellIconButtonRender,
                rowCellFileDownloadRender: RowCellFileDownloadRender,
                rowCellSelectRender: RowCellSelectRender,
                rowCellButtonRender: RowCellButtonRender,
                agColumnHeader: ColumnHeaderRenderer,
                rowSpanCellRender: RowSpanCellRender,
            },
            suppressKeyboardEvent: _suppressKeyboardEvent,
            overlayLoadingTemplate:
                '<span className="ag-overlay-loading-center">잠시만 기다려주세요.</span>', // 로딩 내용
            // onRowEditingStarted: function (event) {
            //     // row 편집 시작
            // },
            // onRowEditingStopped: function (event) {
            //     // row 편집 종료
            // },
            onRowDoubleClicked: function (event) {
                // Row 더블 클릭 이벤트
                if (rowDoubleClick) {
                    //TODO 지우기
                    rowDoubleClick(event);
                }
            },
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        _getCellClass,
        _handleRowSelected,
        columns,
        rowData,
        rowDoubleClick,
        param,
    ]);

    useEffect(() => {
        _setColumn(columns);

        return function cleanup() {
            if (data) {
                data.delete(grid);
            }
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (gridApi) {
            gridApi.setRowData(rowData);
            setFocusByRowIndex(0);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [rowData, gridApi]);

    useEffect(() => {
        if (gridApi) {
            if (!data || !data.get(grid)) {
                mutate(grid, gridApi);
            }
        }
    });

    const onGridReady = (params) => {
        setGridApi(params.api);
    };

    if (rowData) {
        rowData.forEach((element) => {
            if (!element.check) {
                element.check = 'N';
            }
        });
    }

    const onCellValueChanged = (params) => {
        const colId = params.column.colId;
        if (keyField.filter((x) => x == colId).length > 0) {
            if (
                rowData.filter(
                    (x) =>
                        x[colId].toUpperCase() == params.newValue.toUpperCase(),
                ).length > 1 &&
                params.newValue != ''
            ) {
                const headerNm = params.colDef.headerName;
                params.data[colId] = '';
                meg({
                    title: `[${headerNm}]의 값이 중복 되었습니다. </br> 값을 변경 하십시오.`,
                    html: '',
                });
                gridApi.refreshCells({roNodes: [params.node]});
                gridApi.refreshCells({roNodes: [params.node]});
                return;
            }
        }

        if (params.oldValue != params.newValue) {
            params.data.edit = true;

            if (params.data.check != 'Y') {
                params.data.check = 'Y';
                gridApi.refreshCells({roNodes: [params.node]});
            }
        }
    };

    const addRow = () => {
        const rowDataLength = getRows().length;
        gridApi.clearFocusedCell();
        gridApi.clearRangeSelection();
        const addRow = {
            editable: true,
            newRow: true,
            check: 'Y',
        };

        columns.map((item) => {
            if (item.default && item.default != '') {
                if (!addRow[item.field]) {
                    addRow[item.field] = item.default;
                }
            } else {
                addRow[item.field] = '';
            }
        });
        if (newRowDefault) {
            const keys = Object.keys(newRowDefault);
            let validationChk = true;
            let result = undefined;
            keys.map((item) => {
                if (item === 'sort' && newRowDefault[item] === 'auto') {
                    const maxSort =
                        Math.max(...getRows().map((o) => o.sort)) + 1;
                    if (maxSort > 0) {
                        addRow[item] = maxSort;
                    } else {
                        addRow[item] = 1;
                    }
                } else {
                    addRow[item] = newRowDefault[item];
                }
                if (!newRowDefault[item]) {
                    result = columns.find((x) => x.field == item);
                    validationChk = false;
                    return;
                }
            });

            if (!validationChk) {
                meg({
                    title: `[${result.headerName}]의 값이 없습니다.`,
                    html: '',
                });
                return;
            }
        }
        rowData.push(addRow);
        gridApi.applyTransaction({
            add: [addRow],
            addIndex: rowDataLength,
        });

        setFocusByRowIndex(rowDataLength);
    };
    function keyEditable(params) {
        if (params.data.newRow) {
            return true;
        } else {
            return false;
        }
    }
    function _setColumn(columnDefs) {
        columnDefs.forEach((field) => {
            if (field.children) {
                _setColumn(field.children);
            }

            if (field.essential) {
                essentialField.push(field.field);
                setEssentialField(essentialField);
            }

            if (field.key == true) {
                keyField.push(field.field);
                setKeyField(keyField);
                if (field.editable == undefined) {
                    field.editable = keyEditable;
                }
                field.essential = true;

                // this.setState({ keyField: this.state.keyField.concat(field.field) })
                // const gridOptions = this.state.gridOptions;
                // gridOptions.getRowNodeId = this.getRowNodeId;
                // this.setState({ gridOptions: gridOptions })
            }

            if (
                field.codeGroup &&
                field.codeGroup != '' &&
                field.type != 'select'
            ) {
                const selectOption = function () {
                    const data = commonCodeGroup[field.codeGroup],
                        cd = 'cd',
                        cdNm = 'cdNm';
                    const result = new Object();

                    for (let i = 0; i < data.length; i++) {
                        const element = data[i];
                        result[element[cd]] = element[cdNm];
                    }
                    return result;
                };

                let optionList = [];

                if (field.include) {
                    const includeKeys = field.include.split(',');
                    includeKeys.map((key) => {
                        if (selectOption[key]) {
                            optionList[key] = selectOption[key];
                        }
                    });
                } else {
                    optionList = JSON.parse(JSON.stringify(selectOption()));
                }

                if (field.addOption) {
                    const emptyOption = {};
                    emptyOption[''] = field.addOption;
                    optionList = Object.assign(emptyOption, optionList);
                }

                field.singleClickEdit = true;
                field.cellEditor = 'agSelectCellEditor';
                field.cellEditorParams = function () {
                    return {values: Object.keys(optionList)};
                };
                field.valueFormatter = function (params) {
                    return optionList[params.value];
                };
                field.valueParser = function () {
                    for (const key in optionList) {
                        if (optionList.hasOwnProperty(key)) {
                            if (name === optionList[key]) {
                                return key;
                            }
                        }
                    }
                };
            }

            if (field.align && field.align != '') {
                if (field.align == 'center') {
                    field.customCellClass = 'cell_align_center';
                } else if (field.align == 'right') {
                    field.customCellClass = 'cell_align_right';
                }
            }

            // if (field.type && field.type.indexOf('checkbox') >= 0) {
            //     if (field.word == undefined && field.name == undefined) {
            //         field.headerName = '';
            //     }
            //     if (field.width == undefined) {
            //         field.width = 36;
            //     }
            //     field.customCellClass = 'cell_align_center';
            //     field.editable = false;
            // }

            if (field.type == 'download') {
                field.editable = false;
            }

            if (field.type == 'radio') {
                field.editable = false;
                field.customCellClass = 'cell_align_center';
            }

            field.customEditable = field.editable;
        });
    }

    async function _handleRowSelected(event) {
        if (event.node.selected) {
            const {onSelectionChanged} = param;

            // KEY_UP, KEY_DOWN 시에도 selectionChanged Event 발생을 위하여 포커스 조절할 때
            // unSelected일 때도 SelectionChanged Event가 중복으로 호출되어 rowSelected Event 이동
            if (onSelectionChanged) {
                await onSelectionChanged(event);
            }
        }
    }

    function _getCellClass(params) {
        const cellClass = [];
        if (!cellReadOnlyColor) {
            cellClass.push('general');

            if (params.colDef.type == 'numericColumn') {
                cellClass.push('ag-numeric-cell');
            }

            if (params.colDef.customCellClass) {
                cellClass.push(params.colDef.customCellClass);
            }

            return cellClass;
        }

        let editable;
        if (typeof params.colDef.editable === 'function') {
            editable = params.colDef.editable(params);
        } else {
            editable = params.colDef.editable;
        }

        if (!editable) {
            cellClass.push('general');

            if (params.colDef.type == 'numericColumn') {
                cellClass.push('ag-numeric-cell');
            }

            if (params.colDef.customCellClass) {
                cellClass.push(params.colDef.customCellClass);
            }

            cellClass.push('cell_readonly');

            return cellClass;
        }

        cellClass.push('general');
        return cellClass;
    }

    const _navigateToNextCell = (params) => {
        const suggestedNextCell = params.nextCellPosition;
        const key = params.key;

        // KEY_UP, KEY_DOWN 시에도 selectionChanged Event 발생을 위하여 포커스 조정
        if (suggestedNextCell != null) {
            // KEY_LEFT, KEY_RIGHT는 제외함
            if (key != '37' && key != '39') {
                setFocusByRowIndex(suggestedNextCell.rowIndex);
            }
        }

        return suggestedNextCell;
    };

    function getRowNodeByRowIndex(rowIndex) {
        const model = gridApi.getModel();

        if (model == null) {
            return null;
        }

        return model.rowsToDisplay[rowIndex];
    }

    const setFocusByRowIndex = (rowIndex) => {
        if (length(gridApi) == 0) return;
        const targetRow = getRowNodeByRowIndex(rowIndex);

        if (!targetRow) return;
        const row = gridApi.getRowNode(targetRow.id);
        // rowIndex에 행이 없을 경우
        if (!row) return;

        const visibleColumns = gridApi.columnModel.columnDefs
            ? gridApi.columnModel.columnDefs.filter((x) => x.visible == true)
            : undefined;

        if (!visibleColumns) return;
        gridApi.setFocusedCell(
            targetRow.rowIndex,
            gridApi.columnModel.columnDefs.filter((x) => x.field != 'check')[0]
                .field,
        );

        row.setSelected(true);
        gridApi.ensureIndexVisible(targetRow.rowIndex);
        gridApi.setFocused = true;
    };

    function length(grid) {
        return grid.getDisplayedRowCount();
    }

    async function onClickSave() {
        const saveList = getRows().filter((x) => x.check == 'Y');
        2;

        if (saveList.length == 0) {
            meg({
                title: '저장 할 항목이 없습니다.',
                html: '',
            });
            return;
        }

        if (await validation()) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                const result = {
                    data: saveList,
                };
                await param.onClickSave(result, setFocusByRowIndex);
            }
        } else {
            meg({
                title: '필수 항목 값이 없습니다.',
                html: '',
            });
            return;
        }
    }

    async function onClickDelete() {
        const delList = [];
        const deleteList = [];
        getRows().forEach((element) => {
            if (element && element.check == 'Y') {
                delList.push(element);
            }

            if (element && element.check == 'Y' && !element.newRow) {
                deleteList.push(element);
            }
        });
        if (delList.length == 0) {
            meg({
                title: '삭제 할 항목이 없습니다.',
                html: '',
            });
            return;
        }

        if (deleteList.length == 0) {
            rowData = getRows().filter((x) => x.check != 'Y');
            gridApi.setRowData(rowData);
            if (rowData.filter((x) => x.rowData).length > 0) {
                setFocusByRowIndex(rowData.length - 1);
            } else {
                setFocusByRowIndex(0);
            }

            if (isForceDelete) {
                const result = {
                    data: delList.filter((x) => !x.newRow),
                };
                await param.onClickDelete(result);
            }
        } else {
            const confirmResult = await confirm({
                title: '삭제 하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                const result = {
                    data: delList.filter((x) => !x.newRow),
                };

                await param.onClickDelete(result);
            }
        }
    }

    const validation = async () => {
        // let duplicationResult = '';

        let essentialResult = true;
        const rowData = getRows().filter((x) => x.check == 'Y');

        for (let i = rowData.length - 1; i >= 0; i--) {
            // 필수값 체크
            if (!_checkEmpty(rowData[i], essentialField)) {
                essentialResult = false;
                break;
            }

            function _checkEmpty(data, keys) {
                let check = true;
                for (let i = 0; i < keys.length; i++) {
                    if (!data[keys[i]] || data[keys[i]] == '') {
                        check = false;
                        break;
                    }
                }
                return check;
            }
        }

        return essentialResult;
    };

    function _suppressKeyboardEvent(params) {
        if (params.editing) {
            return false;
        }

        const event = params.event;
        const key = event.which;

        const KEY_C = 67;
        // let KEY_ENTER = 13;
        // const KEY_DELETE = 46;
        // const KEY_BACKSPACE = 8;
        // const KEY_SPACE = 32;

        // Ctrl+C 눌렀을 때 셀 하나만 선택되어 있으면 셀만 복사
        if (key == KEY_C && event.ctrlKey) {
            const rangeSelections = params.api.getCellRanges();

            if (rangeSelections.length == 1) {
                const rangeSelection = rangeSelections[0];
                if (
                    rangeSelection.startRow == rangeSelection.endRow &&
                    rangeSelection.columns.length == 1
                ) {
                    params.api.copySelectedRangeToClipboard();
                    return true;
                }
            }
        }
    }

    /**
     * 그리드에 바인딩되어 있는 모든 Data
     */
    const getRows = () => {
        const rows = [];
        gridApi.getModel().rowsToDisplay.map((item) => rows.push(item.data));
        return rows;
    };

    const onModalOpen = () => {
        if (param.onModalOpen) {
            param.onModalOpen();
        }
    };

    return (
        <>
            <div className="grid_contents">
                <div className="grid_top">
                    {title ? <div className="grid_tit">{title}</div> : null}
                    <div className="grid_btn_contents">
                        {param.isAddBtnOpen ? (
                            <Tooltip title="추가">
                                <IconButton
                                    onClick={addRow}
                                    className="btn_group">
                                    <PlaylistAddIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        ) : null}
                        {param.isModalOpen ? (
                            <Tooltip title="추가">
                                <IconButton
                                    onClick={onModalOpen}
                                    className="btn_group">
                                    <LibraryAddIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        ) : null}
                        {param.onClickSave ? (
                            <Tooltip title="저장">
                                <IconButton
                                    onClick={onClickSave}
                                    className="btn_group">
                                    <SaveAltIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        ) : null}
                        {param.onClickDelete ? (
                            <Tooltip title="삭제">
                                <IconButton
                                    onClick={onClickDelete}
                                    className="btn_group">
                                    <DeleteForeverIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        ) : null}
                    </div>
                </div>
                <div
                    className="ag-theme-balham"
                    style={{
                        width: width ? '100%' : width,
                        height: height ? height : '100%',
                    }}>
                    <AgGridReact
                        rowData={rowData}
                        rowHeight={30}
                        gridOptions={gridOptions}
                        onGridReady={onGridReady}
                        rowBuffer={15}
                        suppressKeyboardEvent={
                            gridOptions.suppressKeyboardEvent
                        }
                        suppressRowTransform={true}
                        autoGroupColumnDef={{minWidth: 300}}
                        onSelectionChanged={param.onSelectionChanged}
                        stopEditingWhenCellsLoseFocus={true}
                        localeText={{noRowsToShow: noRowsToShow}}
                        editType={'fullRow'}
                        animateRows={true}
                        onCellValueChanged={onCellValueChanged}
                        navigateToNextCell={_navigateToNextCell}
                        columnDefs={columns}
                        headerHeight={30}></AgGridReact>
                </div>
            </div>
        </>
    );
}
