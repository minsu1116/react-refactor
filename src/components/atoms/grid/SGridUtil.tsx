const gridTrigger = {
    editable: 'editable',
    onCellValueChanged: 'onCellValueChanged',
    rendererVisible: 'cellStyle',
    colSpan: 'colSpan',
    valueFormatter: 'valueFormatter',
    onCellClick: 'onCellClick',
    cellRendererParams: 'cellRendererParams',
    cellRenderer: 'cellRenderer',
    rowSpan: 'rowSpan',
};

/**
 * [SGrid] Row 병합
 * @columnDefs ** 컬럼 정의 집합.
 * @field ** 적용할 field. 단일 적용: "field", 다중 적용: ["field1", "field2"], 전체 적용: "ALL"
 * @refColumn 병합 시 참조할 columnField. 단일 적용: "field", 다중 적용: ["field1", "field2"]
 * @gridApi 그리드
 */
function setRowSpanning(columnDefs, field, refColumn = null, gridApi) {
    if (!columnDefs || columnDefs.length <= 0) return;

    let refTarget = undefined;
    if (refColumn) {
        refTarget = [];
        if (!Array.isArray(refColumn)) {
            refTarget.push(refColumn);
        } else {
            refTarget = refColumn;
        }
    }

    const func = function (params) {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        function rowSpanning(params, isOnlyTarget, refColumn) {
            function cellValueCombine(
                cols,
                row,
                toColunmnNm,
                isOnlyTo,
                refColumn,
            ) {
                let returnData = '';

                if (isOnlyTo == undefined || isOnlyTo == true) {
                    returnData = row.data[toColunmnNm];
                } else {
                    for (let i = 0; i < cols.length; i++) {
                        if (cols[i].visible == false) {
                            continue;
                        }

                        if (refColumn == undefined) {
                            returnData =
                                returnData + row.data[cols[i].colDef.field];
                        } else {
                            for (let j = 0; j < refColumn.length; j++) {
                                if (cols[i].colDef.field == refColumn[j]) {
                                    returnData =
                                        returnData +
                                        row.data[cols[i].colDef.field];
                                }
                            }
                        }

                        if (cols[i].colDef.field == toColunmnNm) {
                            // break;
                            continue;
                        }
                    }
                }

                return returnData;
            }

            const cols = params.columnApi.getAllColumns();
            const rows = params.api.getModel().rowsToDisplay;
            const columnNm = params.colDef.field;
            const rowIndex = params.node.rowIndex;
            let data = null;
            let prevData = null;
            let nextData = null;
            let result = 1;

            if (rows.length <= 0) {
                return result;
            }

            const valueCheck = rows[rowIndex].data[columnNm];
            if (!valueCheck || valueCheck == '') {
                return result;
            }

            if (refColumn) {
                if (refColumn.indexOf(columnNm) < 0) {
                    refColumn.push(columnNm);
                }
            }
            data = cellValueCombine(
                cols,
                rows[rowIndex],
                columnNm,
                isOnlyTarget,
                refColumn,
            );

            if (params.node.rowIndex > 0) {
                prevData = cellValueCombine(
                    cols,
                    rows[rowIndex - 1],
                    columnNm,
                    isOnlyTarget,
                    refColumn,
                );
            }

            if (data != prevData) {
                for (let i = 1; rowIndex + i < rows.length; i++) {
                    nextData = cellValueCombine(
                        cols,
                        rows[rowIndex + i],
                        columnNm,
                        isOnlyTarget,
                        refColumn,
                    );

                    if (data == nextData) {
                        result = result + 1;
                    } else {
                        break;
                    }
                }
            }

            params.colDef[`customRowSpan_${rowIndex}`] = result; //RowSpanCellRender에서 autoHeight일 때 height 계산 시에 사용
            params.node.customRowSpan = Math.max(
                params.node.customRowSpan ? params.node.customRowSpan : 0,
                result,
            ); //SGrid rowBuffer 설정에서 사용

            return result;
        }

        return rowSpanning(params, refColumn ? false : true, refTarget);
    };

    setOption(field, 'rowSpan', func, gridApi, columnDefs);
    setOption(field, 'valueFormatter', undefined, gridApi, columnDefs);
    setOption(field, 'cellRenderer', 'rowSpanCellRender', gridApi, columnDefs);
}

function setOption(field, trigger, func?, gridApi?, columns?) {
    // if (gridApi.gridCore.destroyed == true) return;

    let target = [];
    if (!Array.isArray(field)) {
        target.push(field);
    } else {
        target = field;
    }
    const tempColumnDefs = columns;
    if (field == 'ALL') {
        target = tempColumnDefs.map((x) => {
            return x.field;
        });
    }

    target.map((field) => {
        _setOption(tempColumnDefs, field, trigger, func);
    });

    columns = tempColumnDefs;
    gridApi.setColumnDefs(columns);
}

function setFocusByRowIndex(rowIndex, gridApi) {
    function length(grid) {
        return grid.getDisplayedRowCount();
    }

    function getRowNodeByRowIndex(rowIndex) {
        const model = gridApi.getModel();

        if (model == null) {
            return null;
        }

        return model.rowsToDisplay[rowIndex];
    }

    if (length(gridApi) == 0) return;
    const targetRow = getRowNodeByRowIndex(rowIndex);

    if (!targetRow) return;
    const row = gridApi.getRowNode(targetRow.id);
    // rowIndex에 행이 없을 경우
    if (!row) return;

    const visibleColumns = gridApi.columnModel.columnDefs
        ? gridApi.columnModel.columnDefs.filter((x) => x.visible == true)
        : undefined;

    if (!visibleColumns) return;
    gridApi.setFocusedCell(
        targetRow.rowIndex,
        gridApi.columnModel.columnDefs.filter((x) => x.field != 'check')[0]
            .field,
    );

    row.setSelected(true);
    gridApi.ensureIndexVisible(targetRow.rowIndex);
    gridApi.setFocused = true;
}

const _setOption = (columnDefs, field, trigger, func) => {
    columnDefs.forEach((col) => {
        if (col.children) {
            _setOption(col.children, field, trigger, func);
        }
        if (col.field == field) {
            col[gridTrigger[trigger]] = func;
        }
    });
};

export default {
    setOption,
    setFocusByRowIndex,
    setRowSpanning,
};
