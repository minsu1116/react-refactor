import React, {useEffect, useState} from 'react';
import Checkbox from '@mui/material/Checkbox';
import './style.scss';

export default function RowCellCheckbox(param) {
    const [check, setValue] = useState(Boolean);
    const {field, onChecked} = param.colDef;

    const data = param?.data;
    let value = 'N';

    useEffect(() => {
        if (field && data) {
            if (!data[field]) {
                data[field] = 'N';
            }
            // eslint-disable-next-line react-hooks/exhaustive-deps
            value = param.data[field];
            if (value != 'Y' && data.newRow != 'Y') {
                setValue(false);
            } else {
                data[field] = 'Y';
                setValue(true);
            }
        }
    }, [param?.data]);

    const _handleClick = (event) => {
        const {id, checked} = event.target;
        const value = checked == true ? 'Y' : 'N';
        if (param.data[field] != 'Y') {
            setValue(false);
        } else {
            setValue(true);
        }

        if (data[id]) {
            data[id] = value;
        } else {
            data[id] = 'N';
            setValue(false);
        }
        data.edit = true;

        if (onChecked) {
            onChecked(value, data);
        }

        if (
            id != 'check' &&
            param.data['check'] &&
            param.data['check'] === 'N'
        ) {
            param.data['check'] = 'Y';
            const value = data[id] != 'N' ? 'Y' : 'N';
            param.setValue(value);
            param.refreshCell({roNodes: [param.data]});
        }
    };

    return (
        <div
            className="check_box"
            style={{
                display: 'flex',
                justifyContent: 'space-around',
                position: 'relative',
                top: '-18%',
            }}>
            {field && data && (
                <Checkbox
                    id={field}
                    value={value}
                    checked={check}
                    size="small"
                    onClick={_handleClick}
                    onChange={_handleClick}
                    style={{
                        filter: 'drop-shadow(1px 1px 1px #ccc)',
                    }}
                    sx={{
                        color: '#5687f5',
                        '&.Mui-checked': {
                            color: '#5d80e7',
                        },
                    }}
                />
            )}
        </div>
    );
}
