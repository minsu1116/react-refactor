import React, {useEffect, useState} from 'react';

export function ColumnHeaderRenderer(param) {
    const [descSort, setDescSort] = useState(false);
    const [ascSort, setAscSort] = useState(false);

    const handleRowDataChanged = () => {
        setDescSort(false);
        setAscSort(false);
    };

    // const handleSortChanged = () => {
    //     setDescSort(false);
    //     setAscSort(false);

    //     const {column, api} = param;
    //     if (!column.colDef.sortable) {
    //         return;
    //     }

    //     const colId = column.colId;

    //     if (column.isSortAscending()) {
    //         setDescSort(true);
    //         const sort = [{colId: colId, sort: 'desc'}];
    //         api.setSortModel(api.getSortModel().concat(sort));
    //     } else if (column.isSortDescending()) {
    //         const sortModel = api.getSortModel();
    //         const currentIndex = sortModel.findIndex((x) => x.colId == colId);
    //         sortModel.splice(currentIndex, 1);
    //         api.setSortModel(sortModel);
    //     } else {
    //         setAscSort(true);
    //         const sort = [{colId: colId, sort: 'asc'}];
    //         api.setSortModel(api.getSortModel().concat(sort));
    //     }
    // };

    useEffect(() => {
        param.agGridReact?.gridOptions?.api.addEventListener(
            'rowDataChanged',
            handleRowDataChanged,
        );

        // eslint-disable-next-line react-hooks/exhaustive-deps
    });

    const essential = param.column.colDef.essential
        ? param.column.colDef.essential
        : false;

    let ascClassName;
    let descClassName;

    if (ascSort) {
        ascClassName = 'ag-header-icon ag-sort-order';
    } else {
        ascClassName = 'ag-header-icon ag-sort-order ag-hidden';
    }

    if (descSort) {
        descClassName = 'ag-header-icon ag-sort-descending-icon';
    } else {
        descClassName = 'ag-header-icon ag-sort-descending-icon ag-hidden';
    }

    return (
        <React.Fragment>
            <div
                className="ag-cell-label-container"
                // onClick={handleSortChanged}
                style={{
                    cursor: 'hand',
                    height: '100%',
                }}>
                <div className="ag-header-cell-label" unselectable="on">
                    {/* {editable == true ?
                            <span style={{ color: '#F15234' }}>*</span>
                            :
                            null
                        } */}
                    {essential == true ? (
                        <span style={{color: '#F15234'}}>*</span>
                    ) : null}
                    <span
                        className="ag-header-cell-text"
                        unselectable="on"
                        title={param.displayName}
                        style={{
                            height: '100%',
                            color: param.column.colDef.color
                                ? param.column.colDef.color
                                : '#4957b7',
                        }}>
                        {param.displayName == 'Check' ? '' : param.displayName}
                    </span>
                    <span className={ascClassName} aria-hidden={!ascSort}>
                        <span
                            className="ag-icon ag-icon-asc"
                            unselectable="on"></span>
                    </span>
                    <span className={descClassName} aria-hidden={!descSort}>
                        <span
                            className="ag-icon ag-icon-desc"
                            unselectable="on"></span>
                    </span>
                </div>
            </div>
        </React.Fragment>
    );
}
