import React from 'react';
import ReactTooltip from 'react-tooltip';
import Button from '@mui/material/Button';
import './style.scss';

export function RowCellButtonRender(param) {
    const id = `${param.colDef.field}_${param.rowIndex}`;
    const columInfo = param.column.colDef;
    return (
        <div
            key={id}
            data-tip=""
            data-for={id}
            style={{height: 'inherit', width: columInfo.width}}>
            {columInfo.tooltipDesc && !param.value ? (
                <ReactTooltip id={id} place={'right'}>
                    {columInfo.tooltipDesc}
                </ReactTooltip>
            ) : null}
            {columInfo.onButtonClick &&
            (param.value || !columInfo.validation) ? (
                <Button
                    id="save"
                    onClick={() => {
                        return columInfo.onButtonClick(param);
                    }}
                    style={{width: columInfo.width}}>
                    {columInfo.buttonNm
                        ? columInfo.buttonNm
                        : columInfo.headerName}
                </Button>
            ) : null}
        </div>
    );
}
