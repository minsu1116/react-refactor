import React from 'react';
import API from 'override/api/API';
import {meg} from '@override/alert/Alert';
import {AxiosRequestConfig} from 'axios';
import './style.scss';
import {Tooltip} from '@mui/material';

import ppt_icon from '@img/file/ppt_icon.png';
import pdf_icon from '@img/file/pdf_icon.png';
import img_icon from '@img/file/img_icon.png';
import excel_icon from '@img/file/excel_icon.png';
import zip_icon from '@img/file/zip_icon.png';
import exe_icon from '@img/file/exe_icon.png';
import docx_icon from '@img/file/docx_icon.png';
import etc_icon from '@img/file/etc_icon.png';
import upload_icon from '@img/file/upload_icon.png';

const URL_DOC_LOG = '/api/qc/doc/dataread';
const URL = '/api/file';

export function RowCellFileDownloadRender(param) {
    const {data} = param;
    const img = () => {
        switch (data.fileExt.toLowerCase()) {
            case '.pptx':
            case '.ppt':
                return ppt_icon;
            case '.pdf':
                return pdf_icon;
            case '.png':
            case '.gif':
            case '.svg':
            case '.jpg':
                return img_icon;
            case '.xlsx':
            case '.xls':
                return excel_icon;
            case '.7z':
            case '.zip':
                return zip_icon;
            case '.exe':
                return exe_icon;
            case '.docx':
                return docx_icon;
            default:
                return etc_icon;
        }
    };
    const _handleClick = async () => {
        const {byFileId} = param.column.colDef.cellRendererParams;
        const {value} = param;
        const {docReadLog} = param.column.colDef;
        let docId = undefined;
        if (docReadLog) {
            docId = param.data.docId;
        }

        if (byFileId) {
            await fileDownload(value, docId);
        } else {
            const result = await API.request.get(
                `/api/file/getfileinfo?fileGrpId=${value}`,
            );

            const {data} = result;
            if (data.success) {
                if (data.data.length == 1) {
                    await fileDownload(value, docId);
                } else if (data.data.length > 1) {
                    // helperList = data.data;
                    // setState({helperOpen: true});
                } else {
                    meg({
                        title: `다운로드 중 문제가 발생 하였습니다.`,
                        html: '',
                    });
                    return;
                }
            } else {
                meg({
                    title: `다운로드 중 문제가 발생 하였습니다.`,
                    html: '',
                });
                return;
            }
        }
    };

    const fileDownload = async (fileId, docId) => {
        const res = await _download(fileId);
        const disposition = res.headers['content-disposition'];
        let filename = '';

        function replaceAll(str, searchStr, replaceStr) {
            return str.split(searchStr).join(replaceStr);
        }

        try {
            filename = replaceAll(
                decodeURI(disposition.match(/fileName="(.*)"/)[1]),
                '+',
                ' ',
            );
            if (docId) {
                await _createDocLog(docId);
            }
        } catch (error) {
            meg({
                title: `다운로드 중 문제가 발생 하였습니다.`,
                html: '',
            });
            return;
        }

        if (navigator.appVersion.toString().indexOf('.NET') > 0) {
            window.navigator.msSaveBlob(new Blob([res.data]), filename);
        } else {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
        }
    };

    const _download = async (fileId) => {
        const config: AxiosRequestConfig = {
            headers: {
                Pragma: 'no-cache',
            },
            responseType: 'blob',
        };

        const result = await API.file.get(
            `${URL}/download?fileId=${fileId}`,
            config,
        );
        return result;
    };

    const _createDocLog = async (docId) => {
        const params = {
            docId: docId,
            gubn: 'D',
        };

        const result = await API.request.post(
            `${URL_DOC_LOG}/insertlog`,
            params,
        );

        return result.data.success;
    };

    return (
        <div className="download_render">
            {data ? (
                <>
                    {data.orgFileNm ? (
                        <Tooltip
                            title={data.orgFileNm || '없음'}
                            placement="left">
                            <div className={'download_file_tit'}>
                                {data.orgFileNm}
                            </div>
                        </Tooltip>
                    ) : null}
                    {data.fileId ? (
                        <div className="download_file_img">
                            <a
                                style={{cursor: 'pointer'}}
                                onClick={_handleClick}>
                                <img
                                    className="download_ico"
                                    src={img()}
                                    alt=""
                                />
                            </a>
                        </div>
                    ) : data.orgFileNm ? (
                        <div className="download_file_img" style={{opacity: 1}}>
                            <img
                                className="download_ico animate__animated animate__heartBeat animate__slower animate__infinite"
                                src={upload_icon}
                                alt=""
                            />
                        </div>
                    ) : null}
                </>
            ) : null}
        </div>
    );
}
