import React, {useState} from 'react';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import {meg} from 'override/alert/Alert';
import search_ico from '@img/ico_search.png';
import './style.scss';

export function RowCellIconButtonRender(param) {
    const [value, setValue] = useState(param.value || '');
    const [modalOpen, setModalOpen] = useState(false);
    const buttonIcon = param.colDef.buttonIcon;
    const PopupComponent = param.colDef.Modal;
    let width = 850;
    if (param.colDef.Modal) {
        width = param.colDef.Modal.props.width;
    }

    const handleClose = async (params) => {
        setModalOpen(false);

        if (params[param.colDef.field]) {
            const rows = [];
            param.api
                .getModel()
                .rowsToDisplay.map((item) => rows.push(item.data));

            if (param.colDef.key) {
                if (
                    rows.find(
                        (x) =>
                            x[param.colDef.field] == params[param.colDef.field],
                    )
                ) {
                    meg({
                        title: `[${param.colDef.headerName}]의 값이 중복 되었습니다.`,
                        html: '',
                    });
                    return;
                }
            }

            const result = Object.keys(params);
            result.forEach((element) => {
                if (element != 'sort' && params[element]) {
                    param.data[element] = params[element];
                }
            });

            if (param.data['check'] && param.data['check'] === 'N') {
                param.data['check'] = 'Y';
            }
            setValue(params[param.colDef.field]);
            param.setValue(params[param.colDef.field]);
            param.refreshCell({roNodes: [param.data]});
        }
    };

    const buttonClick = async () => {
        PopupComponent.props.onClose = handleClose;
        setModalOpen(true);
    };

    const style = {
        position: 'absolute' as const,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: width,
        bgcolor: 'background.paper',
        boxShadow: 24,
        border: '0px solid',
    };

    return (
        <React.Fragment>
            {PopupComponent ? (
                <Modal
                    open={modalOpen}
                    onClose={handleClose}
                    hideBackdrop={true}
                    // keepMounted={true}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={style} className="popup">
                        <div className="popup_top">
                            <span className="close_icon" onClick={handleClose}>
                                X
                            </span>
                        </div>
                        <div className="popup_contents">{PopupComponent}</div>
                    </Box>
                </Modal>
            ) : null}
            <div className="cell_contents">
                <div className="contents_value">{value}</div>
                <div
                    className="contents_icon"
                    onClick={buttonClick}
                    style={{cursor: 'pointer !important'}}>
                    {buttonIcon == 'icon-search' ? (
                        <span className="btn_ico cell_icon">
                            <a style={{cursor: 'pointer'}}>
                                <img className="ico" src={search_ico} alt="" />
                            </a>
                        </span>
                    ) : (
                        <i className={`${buttonIcon} cell_icon`} />
                    )}
                </div>
            </div>
        </React.Fragment>
    );
}
