import React from 'react';

const RowSpanCellRender = (props) => {
    const rowSpanCell = () => {
        const {value} = props;
        const cellData = value;

        if (value) {
            return cellData;
        } else {
            return null;
        }
    };

    const value = rowSpanCell();
    const {colDef, node, eGridCell, api} = props;
    if (value) {
        eGridCell.className = eGridCell.className + ' ag-row-span';
        eGridCell.style.background = 'inherit';
    }
    const rows = api.getModel().rowsToDisplay;
    const rowIndex = node.rowIndex;

    if (rows.length > 0) {
        let rowSpan;
        let totalRowHeight = 0;

        if (!colDef.rowSpan) {
            rowSpan = 1;
        } else if (typeof colDef.rowSpan != 'function') {
            rowSpan = colDef.rowSpan;
        } else {
            rowSpan = colDef[`customRowSpan_${node.rowIndex}`];
        }

        if (rowSpan && rowSpan != 1) {
            for (let i = rowIndex; i < rowIndex + rowSpan; i++) {
                if (rows.length <= rowIndex) {
                    continue;
                }

                const getNode = api.getRowNode(i);
                if (getNode) {
                    totalRowHeight = totalRowHeight + getNode.rowHeight;
                }
                eGridCell.style.height = `${totalRowHeight}px`;
                eGridCell.style.borderBottom = '1px solid #e3e3e3';
            }
        }
    }
    return <div className="span_container">{value}</div>;
};

export default RowSpanCellRender;
