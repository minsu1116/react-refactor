import React, {useEffect, useState} from 'react';

import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Tooltip from '@mui/material/Tooltip';
import Storage from '@override/api/Storage';
import './style.scss';

export function RowCellSelectRender(param) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [selectList, setSelectList] = useState(param?.selectList || []);
    const [value, setValue] = useState('');

    useEffect(() => {
        const tempList = [];
        if (param.colDef.codeGroup) {
            setSelectList(
                tempList.concat(commonCodeGroup[param.colDef.codeGroup]),
            );
        }
        const defaultValue = param?.value || '';
        setValue(defaultValue);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleChange = (event) => {
        setValue(event.target.value.toString());
        param.setValue(event.target.value);
    };

    return (
        <div className="grid_select">
            <FormControl>
                <Tooltip
                    title={
                        selectList.filter((x) => x.cd == value)[0]?.cdNm ||
                        '없음'
                    }
                    placement="right">
                    <Select
                        labelId="autowidth-label"
                        displayEmpty
                        id="target"
                        value={value ? value : ''}
                        // defaultValue={defaultValue}
                        disabled={
                            param.colDef.readOnly
                                ? param.colDef.readOnly
                                : false
                        }
                        error={param.error}
                        onChange={handleChange}>
                        <MenuItem value="">
                            <div>선택</div>
                        </MenuItem>
                        {selectList.map((x, i) => {
                            return (
                                <MenuItem
                                    sx={{m: 0.4}}
                                    key={`select_${i}`}
                                    value={x.cd}>
                                    {x.cdNm}
                                </MenuItem>
                            );
                        })}
                    </Select>
                </Tooltip>
            </FormControl>
        </div>
    );
}
