import React, {useRef} from 'react';
import TextField from '@mui/material/TextField';

export default function SInput(props) {
    const width = props.width;
    const id = props.id;
    const ref = props.ref;
    const className = props.className;
    const placeholderValue = props.placeholder;
    const label = props.label;
    const type = props.type;
    const value = props.value;
    let inpuRef = undefined;

    if (ref) {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        inpuRef = useRef();
        inpuRef.current.focus();
    }

    return (
        <div className="input_item">
            <TextField
                id={id}
                key={`input-${id}`}
                type={!type ? 'search' : type}
                className={className}
                onChange={props.onChange}
                label={label}
                value={value ? value : ''}
                style={{width: width}}
                size="small"
                placeholder={placeholderValue ? placeholderValue : ''}
            />
        </div>
    );
}

SInput.defaultProps = {
    readOnly: false,
    width: 370,
    title: '',
};
