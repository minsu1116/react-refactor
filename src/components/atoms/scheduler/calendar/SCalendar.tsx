import React, {useCallback, useEffect, useRef, useState} from 'react';

import {BryntumCalendar, BryntumProjectModel} from '@bryntum/calendar-react';
import {meg, confirm} from '@override/alert/Alert';
import {calendarConfig} from './config';
import './style.scss';
import {DateHelper, LocaleManager} from '@bryntum/calendar';
import localeKr from './localeKr';
import moment from 'moment';
import API from 'override/api/API';
import Storage from 'override/api/Storage';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
// SingleTon
LocaleManager.locale = localeKr;

const colorData = {
    '100': '#c3a1eb',
    '200': '#43b38f',
    '210': '#28f31a8f',
    '300': '#ccc',
    '400': '#ff9800',
};

async function getEquipOpenList() {
    const result = await API.request.post(
        `api/equip/rev/equip-open/list/get`,
        {},
    );

    if (result.data.success) {
        const resourcesList = result.data.data;
        const returnList = [];
        resourcesList.forEach((element) => {
            const openEquipResourece = {
                id: element.equipId,
                name: element.equipNm,
                eventColor: '#5bcb8d',
            };
            returnList.push(openEquipResourece);
        });
        return returnList;
    }

    return [];
}

async function getEventsList(userInfo) {
    const result = await API.request.post(
        `api/equip/rev/equip-rev/list/get`,
        {},
    );
    if (result.data.success) {
        const tmepEventList = [];
        result.data.data.forEach((element) => {
            const tempEventObj = {
                id: element.equipRevId,
                startDate: element.revStartDtm,
                endDate: element.revEndDtm,
                name: element.revTit,
                revEmpNm: element.revEmpNm,
                revDeptNm: element.revDeptNm,
                revState: element.revState,
                resourceId: element.equipId,
                eventColor: colorData[element.revState],
                readOnly:
                    userInfo.empNo == element.revEmpNo
                        ? element.revState > '100'
                            ? true
                            : false
                        : true,
            };
            tmepEventList.push(tempEventObj);
        });

        return tmepEventList;
    }

    return [];
}

// eslint-disable-next-line react/display-name
const SCalendar = React.memo(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const project: any = useRef(null);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const calendarRef: any = useRef(null);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [selectObj, setSelectObj] = useState<any>();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [mngList, setMngList] = useState<any>([]);
    const {data: userInfo} = Storage.getUserInfo();
    const {data: alarmCtn, mutate} = Storage.getAlaramCtn();

    const [resources, setResources] = useState([]);
    const [events, setEvents] = useState([]);

    useEffect(() => {
        initEquipList();
        getEquipMngList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        onSearchEvent();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [alarmCtn]);

    const initEquipList = useCallback(async () => {
        const result = await getEquipOpenList();
        setResources(result);
        onSearchEvent();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onSearchEvent = async () => {
        const eventsResult = await getEventsList(userInfo);
        setEvents(eventsResult);
    };

    const getEquipMngList = async () => {
        const param = {};
        const result = await API.request.post(
            `api/equip/equip-mt/mng-user/mt/get`,
            param,
        );

        if (result.data.success) {
            if (result.data.data) {
                setMngList(result.data.data);
            }
        } else {
            setMngList([]);
        }
    };

    const onSaveRest = async (params) => {
        const restResult = await API.request.post(
            `api/equip/rev/equip-rev/save`,
            params,
        );
        if (restResult.data.success) {
            const tmepEventList = [];
            restResult.data.data.forEach((element) => {
                const tempEventObj = {
                    id: element.equipRevId,
                    startDate: element.revStartDtm,
                    endDate: element.revEndDtm,
                    name: element.revTit,
                    revEmpNm: element.revEmpNm,
                    revDeptNm: element.revDeptNm,
                    revState: element.revState,
                    resourceId: element.equipId,
                    eventColor: colorData[element.revState],
                    readOnly: element.revState > '100' ? true : false,
                    equipNm: element.equipNm,
                };
                tmepEventList.push(tempEventObj);
            });

            setEvents(tmepEventList);
            if (!params.equipRevId) {
                if (
                    mngList.find(
                        (x) =>
                            x.equipId == params.equipId &&
                            x.equipMngEmpNo == userInfo.empNo,
                    )
                ) {
                    mutate(alarmCtn + 1);
                }
            }

            return true;
        } else {
            setEvents(events);
            return false;
        }
    };

    const deleteEvent = async (event) => {
        const param = {
            equipRevId: event.data.id,
            equipId: event.data.resourceId,
        };

        const restResult = await API.request.post(
            `api/equip/rev/equip-rev/delete`,
            param,
        );

        if (restResult.data.success) {
            const tmepEventList = [];
            restResult.data.data.forEach((element) => {
                const tempEventObj = {
                    id: element.equipRevId,
                    startDate: element.revStartDtm,
                    endDate: element.revEndDtm,
                    name: element.revTit,
                    revEmpNm: element.revEmpNm,
                    revDeptNm: element.revDeptNm,
                    revState: element.revState,
                    resourceId: element.equipId,
                    eventColor: colorData[element.revState],
                    readOnly: element.revState > '100' ? true : false,
                    equipNm: element.equipNm,
                };
                tmepEventList.push(tempEventObj);
            });

            setEvents(tmepEventList);
            if (param.equipRevId) {
                if (
                    mngList.find(
                        (x) =>
                            x.equipId == param.equipId &&
                            x.equipMngEmpNo == userInfo.empNo,
                    )
                ) {
                    mutate(alarmCtn - 1);
                }
            }
        } else {
            setEvents(events);
        }
    };

    const onSaveRev = async (param) => {
        if (param) {
            const saveParam = param.values;
            const apiData = param.eventRecord.data;
            const params = {
                equipRevId: isNaN(apiData.id) ? 0 : apiData.id,
                equipId: saveParam.resource.id,
                revTit: saveParam.name,
                revStartDtm: saveParam.allDay
                    ? moment(saveParam.startDate).format('YYYY-MM-DD 06:00:00')
                    : DateHelper.format(
                          saveParam.startDate,
                          'YYYY-MM-DD HH:mm:ss',
                      ),
                revEndDtm: saveParam.allDay
                    ? moment(saveParam.startDate).format('YYYY-MM-DD') ==
                      moment(saveParam.endDate).format('YYYY-MM-DD')
                        ? moment(saveParam.endDate).format(
                              'YYYY-MM-DD 22:00:00',
                          )
                        : moment(saveParam.endDate)
                              .add('-1', 'days')
                              .format('YYYY-MM-DD 22:00:00')
                    : DateHelper.format(
                          saveParam.endDate,
                          'YYYY-MM-DD HH:mm:ss',
                      ),

                revState: '100',
                allDay: saveParam.allDay ? 'Y' : 'N',
                apprYn: 'N',
            };
            const confirms = await confirm({
                title: '예약을 하시겠습니까?',
                html: `
                    <div>
                        <div class="rev_confirm">
                            <div class="equip_tit">
                               장비명
                            </div>
                            <div class="equip_nm">
                                ${saveParam.resource.name}
                            </div>
                        </div>
                        <div class="rev_tit">
                            <div class="rev_tit_conts">
                                ${params.revTit}
                            </div>
                        </div>
                        <div class="rev_day">
                            <div class="rev_day_cont">
                                <div class="day_cont1">
                                    사용일(<span class="force_tit">From</span>)
                                </div>
                                <div class="day_cont2">
                                    ${params.revStartDtm}
                                </div>
                            </div>
                            <div class="rev_day_cont">
                                <div class="day_cont1">
                                    사용일(<span class="force_tit">To</span>)
                                </div>
                                <div class="day_cont2">
                                    ${params.revEndDtm}
                                </div>
                            </div>
                        </div>
                    </div>
                        `,
            });
            if (confirms) {
                await onSaveRest(params);
            }
        }
    };

    const listeners = {
        // Async event listeners allowing you to veto drag operations
        beforeEventsave: async (eventRecords) => {
            eventRecords.context.async = true;
            const result = await validationDateCheck(
                eventRecords.eventRecord.startDate,
            );

            if (result) {
                onSaveRev(eventRecords);
            }

            eventRecords.context.finalize(false);
            return false;
        },
        // catchAll: (event) => {
        //     console.log(event, '??event');
        // },
        beforeEventDelete: (eventRecords) => {
            const data = eventRecords.eventRecords[0];
            // Show custom confirmation dialog (pseudo code)
            msgConfirm('삭제하시겠습니까?').then(async (result) => {
                if (result) {
                    eventRecords.context.finalize(true);
                    await deleteEvent(data);
                } else {
                    eventRecords.context.finalize(false);
                }
            });
            return false;
        },
        beforedragresizeend: async () => {
            let result = await validationDateCheck(selectObj.startDate);
            if (!result) {
                return false;
            }
            result = await msgConfirm('일정을 변경 하시겠습니까??');
            if (result) {
                const saveParam = selectObj[0].data;
                const params = {
                    equipRevId: saveParam.id,
                    equipId: saveParam.resourceId,
                    revTit: saveParam.name,
                    revStartDtm: DateHelper.format(
                        saveParam.startDate,
                        'YYYY-MM-DD HH:mm:ss',
                    ),
                    revEndDtm: DateHelper.format(
                        saveParam.endDate,
                        'YYYY-MM-DD HH:mm:ss',
                    ),
                    revState: '100',
                    allDay: saveParam.allDay ? 'Y' : 'N',
                    apprYn: 'N',
                };
                await onSaveRest(params);
            }
            return result;
        },
        beforeDragMoveEnd: async ({eventRecord}) => {
            let result = await validationDateCheck(eventRecord.startDate);
            if (!result) {
                return false;
            }

            if (!selectObj[0]) {
                meg({
                    title: `변경이 불가능 합니다.`,
                    html: '',
                });

                return false;
            }

            result = await msgConfirm('일정을 변경 하시겠습니까??');
            if (result) {
                const saveParam = selectObj[0].data;
                const params = {
                    equipRevId: saveParam.id,
                    equipId: saveParam.resourceId,
                    revTit: saveParam.name,
                    revStartDtm: DateHelper.format(
                        eventRecord.startDate,
                        'YYYY-MM-DD HH:mm:ss',
                    ),
                    revEndDtm: DateHelper.format(
                        eventRecord.endDate,
                        'YYYY-MM-DD HH:mm:ss',
                    ),
                    revState: '100',
                    allDay: saveParam.allDay ? 'Y' : 'N',
                    apprYn: 'N',
                };
                return onSaveRest(params);
            }
            return result;
        },
        beforeEventEdit: async (event) => {
            // event.eventRecord.data.allDay = true;\
            return event;
        },
    };

    async function validationDateCheck(targetDate) {
        const startDtm = moment(targetDate).format('YYYY-MM-DD');
        const validationDtm = moment().format('YYYY-MM-DD');

        if (validationDtm > startDtm) {
            meg({
                title: `${validationDtm} 현재보다 과거로 변경/예약이 불가능합니다.`,
                html: '',
            });
            return false;
        }

        return true;
    }
    async function msgConfirm(title: string) {
        const confirmResult = await confirm({
            title: title,
            html: '',
        });

        return confirmResult;
    }

    const selectionChangeHandler = ({selection}) => {
        setSelectObj(selection);
    };

    return (
        <div className="scalendar">
            <div className="scalendar_toolbar">
                <div className="scalendar_tit">{'예약(장비) 등록'}</div>
                <div className="scalendar_rev_desc">
                    <div className="desc_contents">
                        <div
                            className="desc_color"
                            style={{background: '#c3a1eb'}}>
                            C
                        </div>
                        <div className="desc_tit">결재대기</div>
                    </div>
                    <div className="desc_contents">
                        <div
                            className="desc_color"
                            style={{background: '#43b38f'}}>
                            C
                        </div>
                        <div className="desc_tit">예약완료</div>
                    </div>
                    <div className="desc_contents">
                        <div
                            className="desc_color"
                            style={{background: '#28f31a'}}>
                            C
                        </div>
                        <div className="desc_tit">사용중</div>
                    </div>
                    <div className="desc_contents">
                        <div
                            className="desc_color"
                            style={{background: '#ff9800'}}>
                            C
                        </div>
                        <div className="desc_tit">결재부결</div>
                    </div>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={initEquipList}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <BryntumProjectModel
                ref={project}
                events={events}
                resources={resources}
            />
            <BryntumCalendar
                ref={calendarRef}
                project={project}
                onSelectionChange={selectionChangeHandler}
                listeners={listeners}
                {...calendarConfig}></BryntumCalendar>
        </div>
    );
});

export default SCalendar;
