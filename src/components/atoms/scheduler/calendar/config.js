import {DateHelper, StringHelper} from '@bryntum/calendar';

const calendarConfig = {
    mode: 'month',
    sidebar: {
        collapsed: true,
        items: {
            eventFilter: null,
        },
    },
    features: {
        eventMenu: {
            items: {
                duplicate: null,
            },
        },
        eventTooltip: {
            // Configuration options are passed on to the tooltip instance.
            // Override the default which is to show on click.
            showOn: 'hover',

            // Create content for Tooltip header
            titleRenderer: (eventRecord) => {
                return `<div class="event_tit">${
                    eventRecord.data.name
                }</div><div class="event_rev_emp">${
                    eventRecord.data.revEmpNm +
                    '(' +
                    eventRecord.data.revDeptNm +
                    ')'
                }</div>`;
            },
        },
        eventEdit: {
            eventEditFeature: {
                // We like our editor to be modal and centered, not aligned to any event.
                editorConfig: {
                    titleRenderer(eventRecord) {
                        return StringHelper.xss`${DateHelper.format(
                            eventRecord.startDate,
                            'HH:mm',
                        )} - ${DateHelper.format(
                            eventRecord.endDate,
                            'HH:mm',
                        )} ${eventRecord.name}`;
                    },
                },

                items: {
                    recurrenceCombo: null,
                    // allDay: {
                    //     readOnly: true,
                    // },
                    // resourceField: {
                    //     weight: 5,
                    // },
                },
            },
        },
    },

    modeDefaults: {
        hideNonWorkingDays: true,
        eventRenderer({renderData, resourceRecord}) {
            const revStateNm = () => {
                switch (renderData.eventRecord.data.revState) {
                    case '100':
                        return '결재대기';
                    case '200':
                        return '예약완료';
                    case '210':
                        return '사용중';
                    case '300':
                        return '사용완료';
                    case '400':
                        return '결재부결';
                    default:
                        return '예약생성';
                }
            };

            return `<div class="equip_desc">${
                renderData.eventRecord.data.equipNm ||
                resourceRecord?.data?.name
            }</div><div class="tit_desc">${revStateNm()}</div>`;
        },
    },
    modes: {
        year: {
            // Show event presence as up to three event-coloured dots in the date cells
            showEvents: 'dots',
            hideNonWorkingDays: true,
        },
        week: {
            // Day columns start at 8am. No events shown before this
            dayStartTime: 6,
            dayEndTime: 22,
            hideNonWorkingDays: true,
        },
        day: {
            // Day columns start at 8am. No events shown before this
            dayStartTime: 6,
            dayEndTime: 22,
            hideNonWorkingDays: true,
        },
        month: {
            hideNonWorkingDays: true,
        },
    },
    datePicker: {
        showEvents: 'count',
    },
};

const highlightConfig = {
    placeholder: '제목을 입력하세요..',
    keyStrokeChangeDelay: 80,
    clearable: true,
    width: '200px',
    triggers: {
        filter: {
            align: 'start',
            cls: 'b-fa b-fa-search',
        },
    },
};

export {calendarConfig, highlightConfig};
