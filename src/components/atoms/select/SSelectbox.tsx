/* eslint-disable prettier/prettier */
/* eslint-disable react/jsx-no-undef */
import React, {useEffect, useState} from 'react';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Storage from '@override/api/Storage';
import InputLabel from '@mui/material/InputLabel';
// import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';

interface Param {
    label: string;
    isCodeGroupMode: boolean;
    codeGroup?: string;
    addRow?: [];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    selectList?: any;
    value?: string;
    disabled?: boolean;
    discription?: string;
    error?: boolean;
    id: string;
    // eslint-disable-next-line @typescript-eslint/ban-types
    handleChange: Function;
}

const SSelectbox = (param: Param) => {
    const [value, setValue] = useState('');

    const handleChange = (event) => {
        setValue(event.target.value.toString());
        const returnValue = {
            target: {
                id: param.id,
                value: event.target.value,
            },
        };
        param.handleChange(returnValue);
    };
    const commonCodeGroup = Storage.getGroupCode().data;
    const [selectList, setSelectList] = useState(param?.selectList || []);
    useEffect(() => {
        const tempList = [];
        if (param.addRow) {
            param.addRow.forEach((element) => {
                tempList.push(element);
            });
        }
        if (param.isCodeGroupMode && param.codeGroup) {
            setSelectList(tempList.concat(commonCodeGroup[param.codeGroup]));
        } else {
            if (param.selectList) {
                setSelectList(tempList.concat(param.selectList));
            } else {
                setSelectList([]);
            }
        }

        const defaultValue = param?.value || '';
        setValue(defaultValue);
    }, [
        commonCodeGroup,
        param.addRow,
        param.codeGroup,
        param?.value,
        param.isCodeGroupMode,
        param.selectList,
    ]);

    return (
        <>
            <div className="MuiInputLabel-sizeSmall">
                <FormControl sx={{minWidth: 176}} size="small">
                    <InputLabel
                        id="autowidth-label"
                        className="MuiInputLabel-sizeSmall">
                        {param.label}
                    </InputLabel>
                    <Select
                        labelId="autowidth-label"
                        id="target"
                        value={value ? value : ''}
                        label={param.label}
                        // defaultValue={defaultValue}
                        disabled={param.disabled ? param.disabled : false}
                        autoWidth
                        error={param.error}
                        onChange={handleChange}>
                        <MenuItem value=''>
                            <em>None</em>
                        </MenuItem>
                        {selectList.map((x, i) => {
                            return (
                                <MenuItem
                                    sx={{m: 0.4}}
                                    key={`select_${i}`}
                                    value={x.cd}>
                                    {x.cdNm}
                                </MenuItem>
                            );
                        })}
                    </Select>
                    {param.discription && (
                        <FormHelperText>{param.discription}</FormHelperText>
                    )}
                </FormControl>
            </div>
        </>
    );
};
export default SSelectbox;
