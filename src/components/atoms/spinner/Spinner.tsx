import React from 'react';
import './style.scss';

export function loadingLayer(isLayerPop) {
    const element = document.querySelector<HTMLElement>('#spinnersection');
    const layerOverlap = document.querySelector<HTMLElement>('#layerOverlap');

    let display1 = '';
    let display2 = '';

    if (element) {
        if (isLayerPop) {
            display1 = 'block';
        } else {
            display1 = 'none';
        }

        element.style.display = `${display1}`;
    }

    if (layerOverlap) {
        if (isLayerPop) {
            display2 = 'block';
        } else {
            display2 = 'none';
        }
        layerOverlap.style.display = `${display2}`;
    }
}

const Spinner = () => {
    return (
        <section
            id="spinnersection"
            className="section"
            style={{display: 'none'}}>
            <div className="loader loader-3">
                <div className="dot dot1"></div>
                <div className="dot dot2"></div>
                <div className="dot dot3"></div>
            </div>
        </section>
    );
};

export default Spinner;
