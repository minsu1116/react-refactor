import React, {useEffect, useState} from 'react';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';

import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight';

import './style.scss';

function not(a: readonly number[], b: readonly number[]) {
    return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a: readonly number[], b: readonly number[]) {
    return a.filter((value) => b.indexOf(value) !== -1);
}

export default function TransferList(props) {
    const [checked, setChecked] = useState<readonly number[]>([]);
    const [left, setLeft] = useState<readonly number[]>([]);
    const [right, setRight] = useState<readonly number[]>([]);
    const [displayNmMap, setdisplayNmMap] = useState(new Map());
    const [disable, setDisable] = useState(false);

    const leftChecked = intersection(checked, left);
    const rightChecked = intersection(checked, right);

    useEffect(() => {
        if (props.prop) {
            const left = [];
            const right = [];
            const tempMap = new Map();
            props.prop.leftInfo.list.forEach((element) => {
                left.push(element.equipId);
                tempMap.set(element.equipId, element.equipNm);
            });

            props.prop.rightInfo.list.forEach((element) => {
                right.push(element.equipId);
                tempMap.set(element.equipId, element.equipNm);
            });

            setDisable(props.prop.disable);
            setdisplayNmMap(tempMap);
            setLeft(left);
            setRight(right);
            setChecked([]);
            props.onChange(right);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.prop]);

    const handleToggle = (value: number) => () => {
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setChecked(newChecked);
    };

    const handleAllRight = () => {
        setRight(right.concat(left));
        props.onChange(right.concat(left));
        setLeft([]);
    };

    const handleCheckedRight = () => {
        setRight(right.concat(leftChecked));
        props.onChange(right.concat(leftChecked));
        setLeft(not(left, leftChecked));
        setChecked(not(checked, leftChecked));
    };

    const handleCheckedLeft = () => {
        setLeft(left.concat(rightChecked));
        setRight(not(right, rightChecked));
        props.onChange(not(right, rightChecked));
        setChecked(not(checked, rightChecked));
    };

    const handleAllLeft = () => {
        setLeft(left.concat(right));
        props.onChange([]);
        setRight([]);
    };

    const customList = (items: readonly number[], tit: string) => (
        <Paper sx={{width: 200, height: 230, overflow: 'auto'}}>
            <div className="tit">{tit}</div>
            <List className="transfer_list" dense component="div" role="list">
                {items.map((value: number) => {
                    const labelId = `transfer-list-item-${value}-label`;

                    return (
                        <ListItem
                            key={value}
                            role="listitem"
                            button
                            onClick={handleToggle(value)}>
                            <ListItemIcon>
                                <Checkbox
                                    checked={checked.indexOf(value) !== -1}
                                    tabIndex={-1}
                                    disableRipple
                                    inputProps={{
                                        'aria-labelledby': labelId,
                                    }}
                                />
                            </ListItemIcon>
                            <ListItemText
                                id={labelId}
                                primary={
                                    displayNmMap
                                        ? displayNmMap.get(value)
                                        : 'Undefined'
                                }
                            />
                        </ListItem>
                    );
                })}
                <ListItem />
            </List>
        </Paper>
    );

    return (
        <Grid className="transfer_list_main" container>
            <Grid className="transfer_list_left" item>
                {customList(left, '분석장비 리스트')}
            </Grid>
            <Grid className="transfer_btn_list" item>
                <Grid container direction="column">
                    <Tooltip title="모두 추가" placement="left">
                        <IconButton
                            onClick={handleAllRight}
                            disabled={left.length === 0 || disable}
                            className="btn_group">
                            <KeyboardDoubleArrowRightIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="선택항목 추가" placement="left">
                        <IconButton
                            onClick={handleCheckedRight}
                            disabled={leftChecked.length === 0 || disable}
                            className="btn_group">
                            <KeyboardArrowRightIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="선택항목 제거" placement="right">
                        <IconButton
                            onClick={handleCheckedLeft}
                            disabled={rightChecked.length === 0 || disable}
                            className="btn_group">
                            <KeyboardArrowLeftIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="모두 제거" placement="right">
                        <IconButton
                            onClick={handleAllLeft}
                            disabled={right.length === 0 || disable}
                            className="btn_group">
                            <KeyboardDoubleArrowLeftIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
            <Grid className="transfer_list_right" item>
                {customList(right, '추가된 분석장비 리스트')}
            </Grid>
        </Grid>
    );
}
