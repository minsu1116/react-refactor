import React, {useEffect, useState} from 'react';
import Tree from 'rc-tree';
import 'rc-tree/assets/index.less';
import './animaion.less';
/**
 * @treeData tree에 바인딩 할 데이터. [default: []]
 * @keyMember 데이터 중 key로 사용할 property 명칭. [default: cd]
 * @pIdMember 데이터 중 Parent Key로 사용할 property 명칭. [default: pCd]
 * @titleMember 데이터 중 display할 property 명칭. [default: cdNm]
 * @checkable 체크 가능여부. [default: false]
 * @disableMember e데이터 중 비활성화를 결정할 property 명칭. "Y"일 때 비활성화.
 */
export default function STree(params) {
    const treeData = params.treeData || [];
    const keyMember = params.keyMember || 'cd';
    const pIdMember = params.pIdMember || 'pCd';
    const titleMember = params.titleMember || 'cdNm';
    const checkable = params.checkable || false;
    const defaultExpandAll = params.defaultExpandAll || false;
    const height = params.height;
    const disableMember = undefined;
    const border = false;
    const [treeObj, setTreeNode] = useState({
        treeNode: params.treeData,
        expandedKeys: params.expandedKeysId,
        selectedKeys: params.selectedKeyId,
        checkedKeys: params.checkedKeys,
    });

    useEffect(() => {
        createTree();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    useEffect(() => {
        setTreeNode({
            ...treeObj,
            treeNode: params.treeData,
            expandedKeys: params.expandedKeysId,
            selectedKeys: params.selectedKeyId,
            checkedKeys: params.checkedKeys,
        });
        createTree();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params.treeData]);

    const createTree = () => {
        const data = setData();
        const defaultExpandedKey = [];
        if (defaultExpandAll) {
            expandedKeyFind(data);

            function expandedKeyFind(data) {
                if (data && data.length > 0) {
                    data.forEach((element) => {
                        if (element.children && element.children.length > 0) {
                            defaultExpandedKey.push(element.key);
                            expandedKeyFind(element.children);
                        }
                    });
                }
            }
        }

        if (defaultExpandedKey.length == 0 && params.expandedKeysId) {
            defaultExpandedKey.push(params.expandedKeysId);
        }

        setTreeNode({
            ...treeObj,
            treeNode: data,
            expandedKeys: defaultExpandedKey,
            checkedKeys: params.checkedKeysId,
        });
    };

    const setData = () => {
        return makeTreeNode(treeData);
    };

    const makeTreeNode = (data) => {
        let makeTree = [];

        for (let i = 0; i < data.length; i++) {
            const item = data[i];

            const temp = {key: item[keyMember], title: item[titleMember]};
            const target = Object.assign(temp, item);

            makeChildren(data, target);

            if (item[pIdMember] == null) {
                makeTree = makeTree.concat(target);
            }
        }

        function makeChildren(data, target) {
            // 나(target)를 부모로 가지고 있는 자식 가져오기

            const children = data.filter((x) => x[pIdMember] == target.key);

            // 자식이 있으면
            if (children) {
                // children 초기값 설정
                target.children = [];

                for (let i = 0; i < children.length; i++) {
                    const item = children[i];

                    // 자식 object 생성
                    const temp = {
                        key: item[keyMember],
                        title: item[titleMember],
                    };
                    const child = Object.assign(temp, item);

                    // 자식 key에도 자식이 있는지 확인하여 자식이 있으면 makeChildren 재귀호출
                    if (isExistChildren(data, child.key)) {
                        makeChildren(data, child);
                    }

                    // 자식 설정 후 target에 concat
                    target.children = target.children.concat(child);
                    if (disableMember) {
                        target.className = 'icon_tree_node';
                    }
                }
            }
        }

        function isExistChildren(data, pId) {
            // 자식이 있는지 확인
            return data.some((x) => x[pIdMember] == pId);
        }

        return makeTree;
    };

    const handleSelect = (selectedKeys, info) => {
        const selectedKey = selectedKeys.length > 0 ? selectedKeys[0] : '';

        if (!selectedKey) {
            return;
        }

        setTreeNode({
            ...treeObj,
            selectedKeys: selectedKeys,
        });

        if (info.node.children && info.node.children.length > 0) {
            if (treeObj.expandedKeys.indexOf(selectedKeys[0]) < 0) {
                setTreeNode({
                    ...treeObj,
                    expandedKeys: treeObj.expandedKeys.concat(selectedKeys),
                });
            }
        }

        if (params.onSelect) {
            params.onSelect(selectedKey, info.node);
        }
    };

    const handleCheck = (checkedKeys, info) => {
        setTreeNode({
            ...treeObj,
            checkedKeys: checkedKeys,
        });

        if (params.onCheck) {
            params.onCheck(checkedKeys, info);
        }
    };

    const handleExpand = (expandedKeys) => {
        setTreeNode({
            ...treeObj,
            expandedKeys: expandedKeys,
        });
    };

    return (
        <React.Fragment>
            {treeObj?.treeNode?.length > 0 ? (
                <>
                    <div className="animation">
                        <style dangerouslySetInnerHTML={{__html: STYLE}} />
                        {height ? (
                            <Tree
                                treeData={treeObj.treeNode}
                                checkable={checkable}
                                onCheck={handleCheck}
                                onSelect={handleSelect}
                                onExpand={handleExpand}
                                motion={motion}
                                height={height}
                                itemHeight={20}
                                expandedKeys={treeObj.expandedKeys}
                                selectedKeys={treeObj.selectedKeys}
                                checkedKeys={treeObj.checkedKeys}
                                showIcon={false}
                                showLine={true}
                                switcherIcon={
                                    disableMember
                                        ? (obj) =>
                                              switcherIcon(obj, disableMember)
                                        : undefined
                                }
                            />
                        ) : (
                            <Tree
                                treeData={treeObj.treeNode}
                                checkable={checkable}
                                onCheck={handleCheck}
                                onSelect={handleSelect}
                                onExpand={handleExpand}
                                motion={motion}
                                expandedKeys={treeObj.expandedKeys}
                                selectedKeys={treeObj.selectedKeys}
                                checkedKeys={treeObj.checkedKeys}
                                showIcon={false}
                                showLine={true}
                                switcherIcon={
                                    disableMember
                                        ? (obj) =>
                                              switcherIcon(obj, disableMember)
                                        : undefined
                                }
                            />
                        )}
                    </div>
                </>
            ) : (
                <div
                    className="basic"
                    style={{
                        textAlign: 'center',
                        padding: '8px',
                        border: border ? '1px solid #ddd' : undefined,
                    }}>
                    {'조회된 항목이 없습니다'}
                </div>
            )}
        </React.Fragment>
    );
}
const switcherIcon = (obj, field) => {
    if (obj.isLeaf) {
        if (obj[field] == 'Y') {
            return <i className={'icon-check'} />;
        } else {
            return <i className={'icon-cancel'} />;
        }
    } else {
        if (obj[field] == 'Y') {
            return <i className={'icon-check'} style={{marginLeft: '18px'}} />;
        } else {
            return <i className={'icon-cancel'} style={{marginLeft: '18px'}} />;
        }
    }
};

const STYLE = `
.rc-tree-child-tree {
  display: block;
}

.node-motion {
  transition: all .2s;
  overflow-y: hidden;
}
`;

const motion = {
    motionName: 'node-motion',
    motionAppear: false,
    onAppearStart: () => ({height: 0}),
    onAppearActive: (node) => ({height: node.scrollHeight}),
    onLeaveStart: (node) => ({height: node.offsetHeight}),
    onLeaveActive: () => ({height: 0}),
};
