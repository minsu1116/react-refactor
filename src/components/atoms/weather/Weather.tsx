import React from 'react';
import API from 'override/api/API';
import useSWR from 'swr';
import './style.scss';

let weatherList = [];

async function getWeather() {
    const params = {
        cityNm: 'anyang-si,seoul,gumi,donghae-si',
    };
    const param = JSON.stringify(params);
    const result = await API.request.post('/api/expend/weather/get', param);

    return result.data.data;
}

export default function Weather() {
    const config = {refreshInterval: 3600000, revalidateOnFocus: false};
    const {data} = useSWR('getWeather', getWeather, config);

    if (data) {
        weatherList = [];
        weatherList.push(
            <div
                key={`weather_contents_${weatherList.length}`}
                className="weather_contents">
                <div
                    className="weather_scroll"
                    key={`weather_contents_${weatherList.length} weather_contents`}>
                    <ul>
                        {data.map((x, index) => {
                            let temper = x.main.temp;
                            if (temper.toString().split('.').length > 1) {
                                const temp = temper.toString().split('.');
                                temper = temp[0];
                            }

                            return (
                                <li key={`li_${index}`}>
                                    <div className="weather_content">
                                        <div
                                            className="weather_img"
                                            key={`weather_img_${index}`}>
                                            {x.weather.map((v, index2) => {
                                                if (index2 > 0) {
                                                    return;
                                                }

                                                switch (v.icon) {
                                                    case '03d':
                                                    case '03n':
                                                    case '04d':
                                                    case '04n':
                                                        return (
                                                            <img
                                                                key={`weather_img_${index2}`}
                                                                src={`./src/style/img/weather/03dn.svg`}
                                                            />
                                                        );
                                                    case '09d':
                                                    case '09n':
                                                    case '10d':
                                                    case '10n':
                                                    case '11d':
                                                    case '11n':
                                                    case '13d':
                                                    case '13n':
                                                    case '50d':
                                                    case '50n':
                                                        return (
                                                            <img
                                                                key={`weather_img_${index2}`}
                                                                src={`./src/style/img/weather/${v.icon.substring(
                                                                    0,
                                                                    2,
                                                                )}dn.svg`}
                                                            />
                                                        );
                                                    default:
                                                        return (
                                                            <img
                                                                key={`weather_img_${index2}`}
                                                                src={`./src/style/img/weather/${v.icon}.svg`}
                                                            />
                                                        );
                                                }
                                            })}
                                        </div>
                                        <div
                                            className="weather_desc"
                                            key={`weather_desc_${index}`}>
                                            <span className="city_name">
                                                {x.name}
                                            </span>
                                            <span className="temper_01">
                                                {temper}
                                            </span>
                                            <span className="temper_02">℃</span>
                                        </div>
                                    </div>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>,
        );
    }
    return <div id="weather">{weatherList}</div>;
}
