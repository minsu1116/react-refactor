/* eslint-disable @typescript-eslint/no-explicit-any */
declare global {
    interface Window {
        count: number;
        token: string;
    }

    interface Result {
        success: string;
        data: Record<string, Record<string, string>>;
    }

    interface localStorage {
        jwt: string;
        codeObject: string;
        userInfo: string;
    }

    interface UserInfo {
        compCd: string;
        compNm: string;
        deptNm?: string;
        deptCd?: string;
        userNm: string;
        empNo: string;
        hrYn?: string;
        officeNo?: string;
        rankNm?: string;
        locale?: Locale;
        email?: string;
        mobileNo?: string;
        addressKr?: string;
        addressEn?: string;
    }

    interface SWRCache<Data> {
        get(key: string): Data | undefined;
        set(key: string, value: Data): void;
        delete(key: string): void;
    }

    interface MenuInfo {
        depthFullName: string;
        menuNm: string;
        formId: string;
        formUrl: string;
        url: string;
        formClass: string;
        systemGrpYn?: string;
        userId: string;
        wordCd: string;
        btnList?: string;
        index?: number;
    }

    interface Navigator {
        msSaveBlob?: (blob: unknown, defaultName?: string) => boolean;
    }

    /**
     * @param headerName 디스플레이명
     * @param field 맵핑 키
     * @param editable 편집가능여부
     * @param align left, right, center
     * @param width 넓이
     * @param type checkbox: 체크 박스, iconButton: 아이콘 버튼, button: 일반버튼, download: 파일다운로드, number: 숫자
     * @param default 초기 값
     * @param codeGroup 코드그룹
     * @param essential 필수입력값
     * @param onButtonClick 버튼 함수
     * @param onClose 모달 닫힐 때 함수 오버로드
     * @param Modal 모달 컴포넌트
     */
    interface ColumnDefs {
        headerName: string;
        field: string;
        editable: boolean;
        align?: string;
        width?: number;
        type?: string;
        default?: string;
        codeGroup?: string;
        essential?: boolean;
        cellClassRules?: unknown;
        hide?: boolean;
        key?: boolean;
        buttonIcon?: string;
        buttonNm?: string;
        // eslint-disable-next-line @typescript-eslint/ban-types
        onButtonClick?: Function;
        // eslint-disable-next-line @typescript-eslint/ban-types
        onClose?: Function;
        // eslint-disable-next-line @typescript-eslint/ban-types
        suppressKeyboardEvent?: Function;
        // eslint-disable-next-line @typescript-eslint/ban-types
        rowSpan?: Function;
        // eslint-disable-next-line @typescript-eslint/ban-types, @typescript-eslint/no-explicit-any
        Modal?: unknown;
        checkbox?: boolean;
        cellRenderer?: unknown;
        validation?: boolean;
        fileNm?: string;
        cellRendererParams?: unknown;
        rowGroup?: boolean;
    }

    /**
     * @param tit 디스플레이명
     * @param mode single: 단일 선택, multi: 다중선택
     * @param validationList 조회시 중복 유저 제거
     */
    interface UserModal {
        tit?: string;
        mode: string;
        validationList: Array<undefined>;
        modal?: any;
    }

    /**
     * @param isResize 팝업 사이즈 변경 모드
     * @param width 초기 넓이 px단위
     * @param height 초기 높이 px단위
     */
    interface SizeObj {
        isResize: boolean;
        width: number;
        height: number;
    }

    /**
     * @param contsId 컨텐츠 id
     * @param contsTit 컨텐츠 제목
     * @param conts 컨텐츠 내용
     */
    interface ContentsObj {
        contsKey: string;
        contsTit?: string;
        conts?: string;
    }

    interface ModalObj {
        path: string;
        tit: string;
        // eslint-disable-next-line @typescript-eslint/ban-types
        onClose: Function;
        sizeObj: SizeObj;
        isOpen: boolean;
        param?: unknown;
    }
}

enum Locale {
    KR = 'KR',
    US = 'EN',
    JP = 'JA',
    VT = 'VT',
}

export {};
