import axios from 'axios';
import {meg} from '@override/alert/Alert';
import {loadingLayer} from '@components/atoms/spinner/Spinner';

const API_ROOT = `${WEBPACK_CONFIG_API_URL}`;
const API_QUARTZ = `${WEBPACK_CONFIG_API_QUARTZ}`;
const request = axios.create({
    baseURL: API_ROOT,
    responseType: 'json',
    headers: {
        'Content-Type': 'application/json',
        Pragma: 'no-cache',
        'Access-Control-Allow-Methods': '*',
    },
});

async function responseFail() {
    await meg({
        icon: 'error',
        title: `세션 만료`,
        html: `과도한 서비스 요청 또는 세션 만료시간으로 인해 세션정보가 삭제 되었습니다.`,
    });
    localStorage.clear();
    window.location.href = '/auth/login';
}

request.defaults.timeout = 120000;

// request 요청할때 값을 추가 할 수 있다.
request.interceptors.request.use(
    function (config) {
        if (
            !config.url.includes('/home') &&
            !config.url.includes('/api/pass') &&
            !config.url.includes('/anal/comm/') &&
            !config.url.includes('/service/common/notice/get') &&
            !config.url.includes('/equip/rev/equip-open/list/get') &&
            !config.url.includes('/equip/rev/equip-rev/list/get')
        ) {
            loadingLayer(true);
        }
        config.headers.common = {
            // Authorization: `Bearer `,
            Authorization: `Bearer ${localStorage.getItem('jwt') || ''}`,
            EmpNo: `${
                localStorage.getItem('empNo')
                    ? localStorage.getItem('empNo')
                    : ''
            }`,
            // , 'loginNm': `${commonStore.loginNm}`
            'Device-Type': 'WEB',
        };
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    },
);

// response 받을 때 오류 처리등 중간 처리 해 줌.
request.interceptors.response.use(
    function (response) {
        loadingLayer(false);
        // 토큰값 다시 설정
        try {
            if (
                response.headers.token != undefined &&
                response.headers.token != null &&
                response.headers.token != ''
            ) {
                if (localStorage.getItem('jwt')) {
                    localStorage.removeItem('jwt');
                }
                localStorage.setItem('jwt', response.headers.token);
            }
        } catch (error) {}

        // 응답은 정상이나 토큰값이 없거나 유효시간이 지났을 경우
        if (response.data.code == 401) {
            responseFail();
            return response;
        }

        try {
            if (response.data.success == false) {
                meg({
                    title: `서버에서 에러가 발생하였습니다.`,
                    html: `${response.data.msg}`,
                });
                console.log('서버에서 에러가 발생하였습니다.', response);
                console.log('ERROR: ', response.data.msg);
            }

            if (response.data.success && response.request.responseURL) {
                if (
                    response.request.responseURL.includes('save') ||
                    response.request.responseURL.includes('update')
                ) {
                    meg({
                        title: `저장 되었습니다.`,
                        icon: 'success',
                        html: '',
                        timer: 1000,
                        showConfirmButton: false,
                    });
                }
                if (response.request.responseURL.includes('delete')) {
                    meg({
                        title: `삭제 되었습니다.`,
                        icon: 'success',
                        html: '',
                        timer: 1000,
                        showConfirmButton: false,
                    });
                }
            }
        } catch {}

        return response;
    },
    function (error) {
        loadingLayer(false);
        // 요청 및 서버오류 공통 처리
        // Do something with response error
        // API 서버에 연결이 안될 경우
        if (error.message === 'Network Error' || error.response === undefined) {
            loadingLayer(false);
            // alert.meg(
            //     `서버에 연결할 수 없습니다. \n 잠시 후 다시 시도해 주시기 바랍니다.`,
            // );
            // ObjectUtility.loadingLayer(false);
            return Promise.reject(error);
        }

        const {response} = error;

        if (response.status == 401) {
            window.location.href = '/auth/sso';
            // window.location.href = '/auth/sso';
        }
        return Promise.reject(error);
    },
);

/* 권한이 없을 경우 사용함 (토큰 적용 안함)*/
const auth = axios.create();
auth.defaults.baseURL = API_ROOT;
auth.defaults.responseType = 'json';
auth.defaults.headers = {
    'Content-Type': 'application/json',
};

/* 파일 업로드 시 사용함 */
const file = axios.create({
    baseURL: API_ROOT,
    responseType: 'json',
    // headers:{
    //   'Content-Type':  'multipart/form-data',
    //   Pragma: 'no-cache'
    // }
});

file.defaults.timeout = 1200000; // 30초

file.interceptors.request.use(
    function (config) {
        loadingLayer(true);

        config.headers.common = {
            Authorization: `Bearer ${localStorage.getItem('jwt') || ''}`,
            EmpNo: `${
                localStorage.getItem('empNo')
                    ? localStorage.getItem('empNo')
                    : ''
            }`,
            'Device-Type': 'WEB',
            Locale: `kr`,
        };
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    },
);

// response 받을 때 오류 처리등 중간 처리 해 줌.
file.interceptors.response.use(
    function (response) {
        loadingLayer(false);

        //console.log(response);
        return response;
    },
    function (error) {
        // 요청 및 서버오류 공통 처리
        ObjectUtility.loadingLayer(false);

        if (error.message === 'Network Error' || error.response === undefined) {
            meg({
                title: `서버에 연결할 수 없습니다. \n 잠시 후 다시 시도해 주시기 바랍니다.`,
                html: '',
            });
            loadingLayer(false);
            return Promise.reject(error);
        }

        meg({
            title: `오류가 발생하였습니다.`,
            html: '',
        });
        return Promise.reject(error);
    },
);

const quartz = axios.create({
    baseURL: API_QUARTZ,
    responseType: 'json',
    headers: {
        'Content-Type': 'application/json',
        Pragma: 'no-cache',
    },
});

quartz.defaults.timeout = 60000;

quartz.interceptors.request.use(
    function (config) {
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    },
);

// response 받을 때 오류 처리등 중간 처리 해 줌.
quartz.interceptors.response.use(
    function (response) {
        loadingLayer(false);

        try {
            if (response.data.success == false) {
                console.log('서버에서 에러가 발생하였습니다.', response);
                console.log('ERROR: ', response.data.msg);
                alert.meg(response.data.msg);
            }
        } catch {}

        //console.log('',response);

        return response;
    },
    function (error) {
        // 요청 및 서버오류 공통 처리
        // Do something with response error
        // API 서버에 연결이 안될 경우
        if (error.message === 'Network Error' || error.response === undefined) {
            alert.meg(
                `서버에 연결할 수 없습니다. \n 잠시 후 다시 시도해 주시기 바랍니다.`,
            );
            loadingLayer(false);
            return Promise.reject(error);
        }

        // const { response } = error;

        // if (response.status == 401) {
        //     window.location.href = '/auth/sso';
        //     // window.location.href = '/auth/sso';
        // }
        return Promise.reject(error);
    },
);

export default {
    request,
    auth,
    quartz,
    file,
};
