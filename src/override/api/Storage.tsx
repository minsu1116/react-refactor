/* eslint-disable react-hooks/rules-of-hooks */
import useSWR from 'swr';
import API from './API';

const getUserInfo = () => {
    const {data, mutate} = useSWR<UserInfo>('userInfo', null);
    let users: UserInfo = {
        empNo: '',
        userNm: '',
        compCd: '',
        compNm: '',
        deptCd: '',
        deptNm: '',
        officeNo: '',
        rankNm: '',
        addressKr: '',
        addressEn: '',
    };
    const getUserIno = localStorage.getItem('userInfo');

    if (getUserIno) {
        const jsonObj: UserInfo = JSON.parse(getUserIno);
        users.userNm = jsonObj.userNm;
        users.empNo = jsonObj.empNo;
        users.compCd = jsonObj.compCd;
        users.compNm = jsonObj.compNm;
        users.deptCd = jsonObj.deptCd;
        users.deptNm = jsonObj.deptNm;
        users.officeNo = jsonObj.officeNo;
        users.mobileNo = jsonObj.mobileNo;
        users.email = jsonObj.email;
        users.rankNm = jsonObj.rankNm;
        users.hrYn = jsonObj.hrYn;
        users.addressKr = jsonObj.addressKr;
        users.addressEn = jsonObj.addressEn;
    }

    return {
        data: users,
        mutate: async (userInfo: UserInfo) => {
            localStorage.setItem('userInfo', JSON.stringify(userInfo));
            users = userInfo;
            if (localStorage.getItem('empNo')) {
                localStorage.removeItem('empNo');
                localStorage.setItem('empNo', userInfo.empNo);
            }
            localStorage.setItem('empNo', userInfo.empNo);
            mutate(userInfo);
            console.log(data, 'data', userInfo);
        },
    };
};

const getGridList = () => {
    const {data, mutate} = useSWR('gridList', null);
    return {
        data: data,
        mutate: (gridNm, grid) => {
            const gridStoreMap = new Map();
            if (!data) {
                if (grid) {
                    gridStoreMap.set(gridNm, grid);
                    mutate(gridStoreMap);
                }
            } else {
                data.forEach((element, key) => {
                    if (element.destroyCalled) {
                        data.delete(key);
                    }
                });

                data.set(gridNm, grid);
                mutate(data);
            }
        },
    };
};

const getLayout = () => {
    const {data, mutate} = useSWR('layout', null);
    return {
        data: data,
        mutate: (data) => {
            mutate(data);
        },
    };
};

const getAlaramCtn = () => {
    const {data, mutate} = useSWR('alarmCtn', null);
    return {
        data: data || 0,
        mutate: (data) => {
            mutate(data);
        },
    };
};

const getLocationInfo = () => {
    const config = {refreshInterval: 5000, revalidateOnFocus: false};
    const {data, mutate} = useSWR('locInfo', null, config);

    return {
        data: data,
        mutate: async (data) => {
            const param = JSON.stringify(data);
            const result = await API.request.post(
                '/api/expend/weather/get',
                param,
            );
            if (result.data.success) {
                mutate(result.data.data);
            }
        },
    };
};

const getGroupCode = () => {
    const groupCodeList = localStorage.getItem('groupCode');
    let stringListToJson;

    if (groupCodeList) {
        stringListToJson = JSON.parse(groupCodeList);
    }

    const {data, mutate} = useSWR('groupCode', async () => stringListToJson);
    return {
        data: data || stringListToJson,
        mutate: async (data) => {
            localStorage.setItem('groupCode', data ? JSON.stringify(data) : '');
            mutate(data);
        },
    };
};

const getCurrentMenu = () => {
    const menuInfo: MenuInfo = {
        depthFullName: 'Home',
        menuNm: 'Home',
        formId: '01',
        formUrl: '',
        url: '',
        formClass: '',
        systemGrpYn: '',
        userId: '',
        wordCd: '',
        index: 0,
    };
    const {data, mutate} = useSWR('currMenu', null);
    return {
        data: data || menuInfo,
        mutate: (v) => {
            mutate(v);
        },
    };
};

export default {
    getUserInfo,
    getGroupCode,
    getCurrentMenu,
    getLocationInfo,
    getGridList,
    getLayout,
    getAlaramCtn,
};
