import React, {useState, useEffect, useCallback, useRef, Suspense} from 'react';
import Modal from 'react-modal';
import {ResizableBox} from 'react-resizable';
import Draggable from 'react-draggable';
import {CircularProgress} from '@mui/material';
import './style.scss';

interface Props {
    path: string;
    tit: string;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
    sizeObj: SizeObj;
    isOpen: boolean;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    param?: any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    modal?: any;
}

interface SizeObj {
    isResize: boolean;
    width: string;
    height: string;
}

interface Prop {
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
    param: unknown;
}

<<<<<<< HEAD
// eslint-disable-next-line react/display-name
export const SModal: React.FC<Props> = React.memo((props: Props) => {
=======
const SModal: React.FC<Props> = (props: Props) => {
>>>>>>> dev_new
    const [isOpen, setIsOpen] = useState(false);
    const modalRef = useRef();
    const [ModalCoponent, setModalCoponent] = useState<React.FC<Prop>>(null);
    const [reSizeObj] = useState({
        width:
            parseInt(props.sizeObj.width) > window.innerWidth
                ? window.innerWidth
                : props.sizeObj.width,
        height: props.sizeObj.height
            ? parseInt(props.sizeObj.height) > window.innerHeight
                ? (window.innerHeight - 50).toString()
                : props.sizeObj.height.toString()
            : '500',
    });

    const toggleModal = useCallback(
        (param) => {
            setIsOpen(!isOpen);
            if (props.onClose) {
                if (param && param.modal) {
                    param.modal = props.param?.modal;
                }
                props.onClose(param);
            }
        },
        [isOpen, props],
    );

<<<<<<< HEAD
    const updateWindowDimensions = () => {
        const innerHeight = window.innerHeight + 'px';
        const resize_contents_Class =
            document.querySelectorAll<HTMLElement>('.resize_contents');
        if (resize_contents_Class[0]) {
            console.log(
                innerHeight,
                resize_contents_Class[0].style?.maxHeight,
                resize_contents_Class[0]?.style.maxHeight > innerHeight,
            );
            resize_contents_Class[0].style.maxHeight = `${
                window.innerHeight - 50
            }px`;
        }
    };
=======
>>>>>>> dev_new
    useEffect(() => {
        return function cleanup() {
            setModalCoponent(null);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const OtherComponent = React.lazy(() => {
            if (props.path) {
                return import(`@page/${props.path}`);
            }
        });
        setModalCoponent(OtherComponent);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props, props.path]);

    useEffect(() => {
        if (ModalCoponent) {
            setIsOpen(props.isOpen);
        }
    }, [ModalCoponent, props.isOpen]);

    return (
        <>
            {props ? (
                <>
                    <Modal
<<<<<<< HEAD
                        isOpen={isOpen}
                        style={modalStyle}
                        onRequestClose={toggleModal}
                        shouldCloseOnOverlayClick={false}
                        ariaHideApp={false}>
                        <Draggable handle=".modal_tit" bounds="body">
                            {props.sizeObj.isResize ? (
                                <div className="modal_main">
                                    <ResizableBox
                                        className="resize_contents"
                                        width={reSizeObj.width}
                                        height={reSizeObj.height}
                                        minConstraints={[350, 500]}
=======
                        ref={modalRef}
                        isOpen={isOpen}
                        style={{
                            overlay: {
                                position: 'fixed',
                                top: 0,
                                left: 0,
                                right: 0,
                                bottom: 0,
                                backgroundColor: 'rgba(0,0,0,0.2)',
                                zIndex: 9999,
                                // pointerEvents: 'none',
                            },
                            content: {
                                backgroundColor: 'transparent',
                                border: '0px',
                                padding: '0px',
                                overflow: 'visible',
                                position: 'relative',
                                top: 10,
                                left: 0,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                            },
                        }}
                        onRequestClose={toggleModal}
                        shouldCloseOnOverlayClick={false}
                        ariaHideApp={false}>
                        {props.sizeObj.isResize ? (
                            <Draggable handle=".modal_tit" bounds="body">
                                <div className="modal_main">
                                    <ResizableBox
                                        className="resize_contents"
                                        width={
                                            reSizeObj.width == 0
                                                ? 900
                                                : reSizeObj.width
                                        }
                                        height={parseInt(reSizeObj.height)}
                                        minConstraints={[350, 500]}
                                        maxConstraints={[1980, 960]}
>>>>>>> dev_new
                                        draggableOpts={{grid: [25, 25]}}>
                                        <div className="modal_header">
                                            <div className="modal_tit">
                                                {props.tit}
                                            </div>
                                            <div
                                                className="close_icon"
                                                onClick={toggleModal}>
                                                X
                                            </div>
                                        </div>
                                        <div className="modal_contents">
                                            {ModalCoponent ? (
<<<<<<< HEAD
                                                <ModalCoponent
                                                    onClose={toggleModal}
                                                    param={props.param}
                                                />
=======
                                                <Suspense
                                                    fallback={
                                                        <CircularProgress className="loading_bar" />
                                                    }>
                                                    <ModalCoponent
                                                        onClose={toggleModal}
                                                        param={props.param}
                                                    />
                                                </Suspense>
>>>>>>> dev_new
                                            ) : null}
                                        </div>
                                    </ResizableBox>
                                </div>
<<<<<<< HEAD
                            ) : (
                                <div
                                    className="modal_main"
=======
                            </Draggable>
                        ) : (
                            <Draggable handle=".modal_tit" bounds="body">
                                <div
                                    className="modal_main resize_contents"
>>>>>>> dev_new
                                    style={{
                                        width: `${reSizeObj.width}px`,
                                        height: `${reSizeObj.height}px`,
                                    }}>
                                    <div className="modal_header">
                                        <div className="modal_tit">
                                            {props.tit}
                                        </div>
                                        <div
                                            className="close_icon"
                                            onClick={toggleModal}>
                                            X
                                        </div>
                                    </div>
                                    <div className="modal_contents">
<<<<<<< HEAD
                                        <ModalCoponent
                                            onClose={toggleModal}
                                            param={props.param}
                                        />
                                    </div>
                                </div>
                            )}
                        </Draggable>
=======
                                        {ModalCoponent && (
                                            <Suspense
                                                fallback={
                                                    <CircularProgress className="loading_bar" />
                                                }>
                                                <ModalCoponent
                                                    onClose={toggleModal}
                                                    param={props.param}
                                                />
                                            </Suspense>
                                        )}
                                    </div>
                                </div>
                            </Draggable>
                        )}
>>>>>>> dev_new
                    </Modal>
                </>
            ) : null}
        </>
    );
<<<<<<< HEAD
});
=======
};

export default SModal;
>>>>>>> dev_new
