import Swal, {SweetAlertIcon, SweetAlertPosition} from 'sweetalert2';
import './style.scss';

interface Options {
    html: string;
    cancelButtonColor?: string;
    showCancelButton?: boolean;
    title?: string;
    icon?: SweetAlertIcon;
    position?: SweetAlertPosition;
    timer?: number;
    showConfirmButton?: boolean;
    showClass?: unknown;
    hideClass?: unknown;
}

/**
 * !필수! 호출할 때 꼭 비동기로 호출해주세요!!
 */
export async function meg(options) {
    options.showClass = {
        popup: 'animate__animated animate__flipInX animate__fast',
    };
    options.hideClass = {
        popup: 'animate__animated animate__flipOutX animate__faster',
    };
    await Swal.fire(options);
}

export async function validation(keys, modal) {
    let novalueList = '';

    const keyList = Object.keys(keys);

    keyList.forEach((key) => {
        if (!modal[key]) {
            if (novalueList) {
                novalueList = `${novalueList}, [${keys[key]}]`;
            } else {
                novalueList = `[${keys[key]}]`;
            }
        }
    });

    if (novalueList) {
        const options = {
            title: novalueList,
            html: '값이 없습니다.',
            showClass: {
                popup: 'animate__animated animate__flipInX animate__fast',
            },
            hideClass: {
                popup: 'animate__animated animate__flipOutX animate__faster',
            },
        };
        await Swal.fire(options);
        return false;
    }

    return true;
}

export async function confirm(options: Options) {
    options.showCancelButton = true;
    options.cancelButtonColor = '#f1547f';
    options.showClass = {
        popup: 'animate__animated animate__fadeInDown animate__faster',
    };
    options.hideClass = {
        popup: 'animate__animated animate__fadeOutDown animate__faster',
    };

    const {value} = await Swal.fire(options).then((result) => {
        return result;
    });

    return !value ? false : true;
}

export function info() {
    console.log('error');
}

export function error() {
    console.log('error');
}

export function success() {
    console.log('success');
}

export function warning() {
    console.log('warning');
}
