import React, {useState, useEffect} from 'react';
import Modal from 'react-modal';
import {ResizableBox} from 'react-resizable';
import Draggable from 'react-draggable';
import './style.scss';

interface Props {
    path: string;
    tit: string;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
    sizeObj: SizeObj;
    isOpen: boolean;
    param?: unknown;
}

interface SizeObj {
    isResize: boolean;
    width: number;
    height: number;
}

interface Prop {
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
    param: unknown;
}

// eslint-disable-next-line react/display-name
export const SModal: React.FC<Props> = React.memo((props: Props) => {
    const [isOpen, setIsOpen] = useState(false);
    const [ModalCoponent, setModalCoponent] = useState<React.FC<Prop>>(null);
    const [reSizeObj, setReSizeObj] = useState({
        width: props.sizeObj.width,
        height: props.sizeObj.height
            ? props.sizeObj.height > window.innerHeight
                ? window.innerHeight - 50
                : props.sizeObj.height
            : 500,
    });

    function toggleModal(param) {
        setIsOpen(!isOpen);
        if (props.onClose) {
            if (isOpen) {
                setReSizeObj({
                    ...reSizeObj,
                    width: props.sizeObj.width,
                    height: 500,
                });
            }
            param.width = reSizeObj.width;
            param.height = reSizeObj.height;
            props.onClose(param);
        }
    }

    const updateWindowDimensions = () => {
        const innerHeight = window.innerHeight + 'px';
        const resize_contents_Class =
            document.querySelectorAll<HTMLElement>('.resize_contents');
        if (resize_contents_Class[0]) {
            console.log(
                innerHeight,
                resize_contents_Class[0].style?.maxHeight,
                resize_contents_Class[0]?.style.maxHeight > innerHeight,
            );
            resize_contents_Class[0].style.maxHeight = `${
                window.innerHeight - 50
            }px`;
        }
    };
    useEffect(() => {
        window.addEventListener('resize', updateWindowDimensions);
        updateWindowDimensions();
        return function cleanup() {
            setReSizeObj({
                ...reSizeObj,
                width: props.sizeObj.width ? props.sizeObj.width : 500,
                height: props.sizeObj.height
                    ? props.sizeObj.height > window.innerHeight
                        ? window.innerHeight - 50
                        : props.sizeObj.height
                    : 500,
            });
            setModalCoponent(null);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const OtherComponent = React.lazy(() => {
            if (props.path) {
                return import(`@page/${props.path}`);
            }
        });
        setModalCoponent(OtherComponent);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.path]);

    useEffect(() => {
        setIsOpen(props.isOpen);
    }, [props.isOpen]);

    const modalStyle = {
        overlay: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: 'rgba(0,0,0,0.2)',
            zIndex: 9999,
            // pointerEvents: 'none',
        },
        content: {
            backgroundColor: 'transparent',
            border: '0px',
            padding: '0px',
            overflow: 'visible',
            position: 'relative',
            top: 10,
            left: 0,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    };
    updateWindowDimensions();
    return (
        <>
            {props ? (
                <>
                    <Modal
                        isOpen={isOpen}
                        style={modalStyle}
                        onRequestClose={toggleModal}
                        shouldCloseOnOverlayClick={false}
                        ariaHideApp={false}>
                        <Draggable handle=".modal_tit" bounds="body">
                            <div>
                                {props.sizeObj.isResize ? (
                                    <div className="modal_main">
                                        <ResizableBox
                                            className="resize_contents"
                                            width={reSizeObj.width}
                                            height={reSizeObj.height}
                                            minConstraints={[350, 500]}
                                            draggableOpts={{grid: [25, 25]}}>
                                            <div className="modal_header">
                                                <div className="modal_tit">
                                                    {props.tit}
                                                </div>
                                                <div
                                                    className="close_icon"
                                                    onClick={toggleModal}>
                                                    X
                                                </div>
                                            </div>
                                            <div className="modal_contents">
                                                {ModalCoponent ? (
                                                    <ModalCoponent
                                                        onClose={toggleModal}
                                                        param={props.param}
                                                    />
                                                ) : null}
                                            </div>
                                        </ResizableBox>
                                    </div>
                                ) : (
                                    <div
                                        className="modal_main"
                                        style={{
                                            width: `${reSizeObj.width}px`,
                                            height: `${reSizeObj.height}px`,
                                        }}>
                                        <div className="modal_header">
                                            <div className="modal_tit">
                                                {props.tit}
                                            </div>
                                            <div
                                                className="close_icon"
                                                onClick={toggleModal}>
                                                X
                                            </div>
                                        </div>
                                        <div className="modal_contents">
                                            <ModalCoponent
                                                onClose={toggleModal}
                                                param={props.param}
                                            />
                                        </div>
                                    </div>
                                )}
                            </div>
                        </Draggable>
                    </Modal>
                </>
            ) : null}
        </>
    );
});
