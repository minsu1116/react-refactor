import React, {
    useEffect,
    useState,
    Suspense as ReactSuspense,
    createContext,
    useContext,
} from 'react';

interface ISuspenseContext {
    isSuspended: boolean;
}
const SuspenseContext = createContext<ISuspenseContext>({isSuspended: true});

/*
 * Suspense apparently renders a suspended tree before it resumes,
 * hiding it with `display: none;`.  This tweaked version lets child
 * components use the `useSuspended` hook to know if they're in a
 * rendered-while-suspended tree.
 */

interface IFallbackProps {
    children?: React.ReactElement;
    setSuspended: (boolean) => void;
}

const Fallback: React.FC<IFallbackProps> = ({setSuspended, children}) => {
    useEffect(() => {
        return () => setSuspended(false);
    }, [setSuspended]);

    return children;
};

interface ISuspense {
    children?: React.ReactElement;
    fallback?: React.ReactElement;
}

const Suspense: React.FC<ISuspense> = ({fallback, children}) => {
    const [suspended, setSuspended] = useState(true);

    return (
        <ReactSuspense
            fallback={
                <Fallback setSuspended={setSuspended}>{fallback}</Fallback>
            }>
            <SuspenseContext.Provider value={{isSuspended: suspended}}>
                {children}
            </SuspenseContext.Provider>
        </ReactSuspense>
    );
};

Suspense.defaultProps = {
    fallback: null,
    children: null,
};

export const useSuspended = (): ISuspenseContext => {
    const context = useContext(SuspenseContext);

    if (!context) {
        throw new Error(
            'useSuspended must be used within an SuspenseContext.Provider',
        );
    }

    return context;
};

export default Suspense;
