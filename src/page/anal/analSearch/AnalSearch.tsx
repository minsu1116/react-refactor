import React, {useEffect, useMemo, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import API from 'override/api/API';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SModal from 'override/modal/SModal';

import './style.scss';
import SSelectbox from 'components/atoms/select/SSelectbox';
import DatePickerRange from 'components/atoms/datePickerRange/DatePickerRange';
import DatePicker from 'components/atoms/datePicker/DatePicker';
import moment from 'moment';

interface SearchObj {
    regId?: string;
    regTit?: string;
    regState?: string;
    keyword?: string;
    equipId?: string;
    regEmpNo: string;
    regEmpNm?: string;
    currEmpNo?: string;
    searchDtmFrom: string;
    searchDtmTo: string;
    regYear: string;
    companyGubun?: string;
}

const AnalSearch = (param) => {
    const {data} = Storage.getGridList();
    const {data: userInfo} = Storage.getUserInfo();
    const [equipList, setEquipList] = useState([]);
    const commonCodeGroup = Storage.getGroupCode().data;

    const [searchObj, seteSearchObj] = useState<SearchObj>({
        regId: '',
        regTit: '',
        regState: param.param.param ? param.param.param.regState : '',
        regEmpNo: commonCodeGroup['ALL_SEARCH_GROUP'].find(
            (x) => x.cd == userInfo.empNo,
        )
            ? ''
            : userInfo.empNo,
        regEmpNm: commonCodeGroup['ALL_SEARCH_GROUP'].find(
            (x) => x.cd == userInfo.empNo,
        )
            ? ''
            : userInfo.userNm,
        keyword: '',
        searchDtmFrom: moment().add(-30, 'days').format('YYYYMMDD'),
        searchDtmTo: moment().format('YYYYMMDD'),
        regYear: moment().format('YYYY'),
        companyGubun: '',
        currEmpNo: param.param.param ? param.param.param.currEmpNo : '',
    });
    const [rowData, setRowData] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);
    const [modalObj, setModalObj] = useState({
        tit: '',
        path: '',
    });
    const sizeObj = useMemo(() => {
        return {
            width: '1395',
            height: 'auto',
            isResize: false,
        };
    }, []);

    useEffect(() => {
        initEquipMtList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const initEquipMtList = async () => {
        const param = {
            surYn: 'Y',
        };
        const result = await API.request.post(`api/equip/equip-mt/get`, param);

        if (result.data.success) {
            const tempEquipList = [];
            result.data.data.forEach((element) => {
                const tempEquipObj = {
                    cd: element.equipId,
                    cdNm: element.equipNm,
                };

                tempEquipList.push(tempEquipObj);
            });
            setEquipList(tempEquipList);
        }

        await handleSearchClick();
    };

    const handleSearchClick = async () => {
        const params = searchObj;
        const result = await API.request.post(`api/anal/comm/reg/get`, params);
        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onSearchObjChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        seteSearchObj(newValue);
    };

    const returnValueChange = (param) => {
        seteSearchObj({
            ...searchObj,
            searchDtmFrom: moment(param[0]).format('YYYYMMDD'),
            searchDtmTo: moment(param[1]).format('YYYYMMDD'),
        });
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const onRegModalOpen = async () => {
        setModalOpen(true);
    };

    const onLbyModalClose = async () => {
        setModalOpen(false);
        handleSearchClick();
    };

    const handleDoubleClick = (e) => {
        if (!modalObj && e.node.group) {
            const modal = e.node.childrenAfterGroup[0].data;
            setModalObj(modal);
        }
        onRegModalOpen();
    };

    const onSelectChange = async () => {
        if (data) {
            const regGrid = data?.get(
                `anal_search_grid${
                    param.param.param.regState
                        ? '_' + param.param.param.regState
                        : ''
                }`,
            );
            if (regGrid && regGrid.getSelectedRows()) {
                const regObj = regGrid.getSelectedRows()[0];
                switch (regObj.srType) {
                    case 'E':
                    case 'S':
                        regObj.tit = '특정설비 분석의뢰';
                        regObj.path = 'anal/reg/utilReq/UtilReq';
                        break;
                    case 'G':
                        regObj.tit = '일반분석 분석의뢰';
                        regObj.path = 'anal/reg/general/General';
                        break;
                    case 'T':
                    case 'C':
                        regObj.tit = '종합및클레임 분석의뢰';
                        regObj.path = 'anal/reg/overallClaim/OverallClaim';
                        break;
                    case 'O':
                        regObj.tit = '공인성적서 발행';
                        regObj.path = 'anal/reg/publicReport/PublicReport';
                        break;
                    case 'V':
                        if (regObj.regHrYn == 'Y') {
                            regObj.tit = '공인성적서 발행';
                            regObj.path = 'anal/reg/publicReport/PublicReport';
                        } else {
                            regObj.tit = '환경유해물질 분석의뢰';
                            regObj.path = 'anal/reg/environ/Environ';
                        }
                        break;
                    default:
                        regObj.tit = '전력실험실전용 분석의뢰';
                        regObj.path = 'anal/reg/electric/Electric';
                        break;
                }
                setModalObj(regObj);
            }
        }
    };

    const columnDefs: Array<ColumnDefs> = useMemo(() => {
        return [
            {
                headerName: '의뢰번호',
                field: 'regId',
                width: 100,
                align: 'center',
                editable: false,
            },
            {
                headerName: '의뢰타입',
                field: 'srType',
                width: 100,
                align: 'center',
                codeGroup: 'SR_TYPE',
                editable: false,
            },
            {
                headerName: '의뢰인',
                field: 'regEmpNm',
                align: 'center',
                width: 80,
                editable: false,
            },
            {
                headerName: '의뢰제목',
                field: 'regTit',
                width: 300,
                editable: false,
            },
            {
                headerName: '의뢰일',
                field: 'regDtm',
                type: 'dateTime',
                width: 80,
                align: 'center',
                editable: false,
            },
            {
                headerName: '접수일',
                field: 'mngDtm',
                type: 'dateTime',
                align: 'center',
                width: 80,
                editable: false,
            },
            {
                headerName: '처리일',
                field: 'procDtm',
                type: 'dateTime',
                align: 'center',
                width: 80,
                editable: false,
            },
            {
                headerName: '현재상태',
                field: 'regState',
                codeGroup: 'REG_STATE',
                align: 'center',
                width: 110,
                editable: false,
            },
            {
                headerName: '대응인',
                field: 'currEmpNm',
                align: 'right',
                width: 200,
                editable: false,
            },
            {
                headerName: '등록구분',
                field: 'regHrYn',
                align: 'center',
                width: 80,
                codeGroup: 'REG_HR_GUBUN',
                editable: false,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="contents_wrap_parents_h">
            <SModal
                tit={modalObj.tit}
                param={modalObj}
                isOpen={modalOpen}
                sizeObj={sizeObj}
                path={modalObj.path}
                onClose={onLbyModalClose}
            />
            <div className="anal_search_main">
                <ContentsWrap>
                    <div className="search_wrap">
                        <div className="search_contents">
                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': {m: 1, width: '25ch'},
                                }}
                                noValidate
                                autoComplete="off">
                                <TextField
                                    id="regId"
                                    onChange={onSearchObjChange}
                                    onKeyDown={handleKeyDown}
                                    label={'의뢰번호'}
                                    value={searchObj.regId}
                                    size="small"
                                />
                                <TextField
                                    id="regEmpNm"
                                    onChange={onSearchObjChange}
                                    onKeyDown={handleKeyDown}
                                    label={'의뢰자'}
                                    value={searchObj.regEmpNm}
                                    disabled={
                                        !commonCodeGroup[
                                            'ALL_SEARCH_GROUP'
                                        ].find((x) => x.cd == userInfo.empNo)
                                    }
                                    size="small"
                                />
                                <TextField
                                    id="regTit"
                                    onChange={onSearchObjChange}
                                    onKeyDown={handleKeyDown}
                                    label={'의뢰제목'}
                                    value={searchObj.regTit}
                                    size="small"
                                />
                                <TextField
                                    id="keyword"
                                    onChange={onSearchObjChange}
                                    onKeyDown={handleKeyDown}
                                    label={'키워드'}
                                    value={searchObj.keyword}
                                    size="small"
                                />
                                <SSelectbox
                                    isCodeGroupMode={false}
                                    selectList={equipList}
                                    label={'분석장비'}
                                    id={'equipId'}
                                    value={searchObj.equipId}
                                    // default={regModal.shareGroup}
                                    handleChange={onSearchObjChange}
                                    disabled={false}
                                />
                            </Box>
                            <div className="search_contents">
                                <Box
                                    component="form"
                                    sx={{
                                        '& > :not(style)': {
                                            m: 1,
                                            width: '25ch',
                                        },
                                    }}
                                    noValidate
                                    autoComplete="off">
                                    <SSelectbox
                                        isCodeGroupMode={true}
                                        codeGroup={'REG_STATE'}
                                        label={'의뢰상태'}
                                        id={'regState'}
                                        value={searchObj.regState}
                                        handleChange={onSearchObjChange}
                                        disabled={false}
                                    />
                                    <SSelectbox
                                        isCodeGroupMode={true}
                                        codeGroup={'COMPANY_GUBUN'}
                                        label={'등록구분'}
                                        id={'companyGubun'}
                                        value={searchObj.companyGubun}
                                        handleChange={onSearchObjChange}
                                        disabled={false}
                                    />
                                    <DatePickerRange
                                        disabled={false}
                                        startTit={'작성일 (From)'}
                                        endTit={'작성일 (to)'}
                                        fromDate={searchObj.searchDtmFrom}
                                        toDate={searchObj.searchDtmTo}
                                        returnValueChange={returnValueChange}
                                    />
                                    <DatePicker
                                        label={'의뢰년도'}
                                        width={150}
                                        handleChange={onSearchObjChange}
                                        id={'regYear'}
                                        inputFormat={'yyyy'}
                                        defaultValue={searchObj.regYear}
                                        mask={'____'}
                                        views={['year']}
                                        disabled={false}
                                    />
                                </Box>
                            </div>
                        </div>
                        <div className="search_btns">
                            <Tooltip title="조회">
                                <IconButton
                                    onClick={handleSearchClick}
                                    className="btn_group">
                                    <ManageSearchIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </div>
                    <div className="contents_wrap">
                        <SGrid
                            grid={`anal_search_grid${
                                param.param.param.regState
                                    ? '_' + param.param.param.regState
                                    : ''
                            }`}
                            rowData={rowData}
                            title={'의뢰목록'}
                            onSelectionChanged={onSelectChange}
                            rowDoubleClick={handleDoubleClick}
                            columnDefs={columnDefs}
                        />
                    </div>
                </ContentsWrap>
            </div>
        </div>
    );
};
export default AnalSearch;
