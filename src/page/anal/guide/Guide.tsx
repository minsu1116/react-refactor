import React, {useEffect, useState} from 'react';
import API from 'override/api/API';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import './style.scss';
import SModal from 'override/modal/SModal';
import {Refresh} from '@material-ui/icons';
import ArchiveOutlinedIcon from '@mui/icons-material/ArchiveOutlined';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Seditor from 'components/atoms/editor/Seditor';
import Box from '@mui/material/Box';

function Guide() {
    const [contenstObj, setContenstObj] = useState<ContentsObj>({
        contsKey: 'guide',
    });
    const [modalObj, setModalObj] = useState({
        isModalOpen: false,
        readOnly: true,
        param: contenstObj,
    });

    useEffect(() => {
        handleSearchConts();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearchConts = async () => {
        const params = {
            contsKey: 'guide',
        };
        const result = await API.request.post(`api/intro/contents/get`, params);

        if (result.data.success) {
            if (result.data.data) {
                setContenstObj({
                    ...contenstObj,
                    conts: result.data.data.conts,
                });
            }
        }
    };

    const onCloseEqInfo = (param) => {
        setModalObj({
            ...modalObj,
            isModalOpen: false,
        });

        if (param.conts) {
            setContenstObj({
                ...contenstObj,
                conts: param.conts,
            });
        }
    };

    const handleRegContents = () => {
        setModalObj({
            ...modalObj,
            param: contenstObj,
            isModalOpen: true,
        });
    };

    return (
        <ContentsWrap>
            {modalObj.isModalOpen ? (
                <SModal
                    tit={'의뢰절차 안내 등록'}
                    param={modalObj.param}
                    isOpen={modalObj.isModalOpen}
                    sizeObj={{
                        width: '1600',
                        height: '950',
                        isResize: false,
                    }}
                    path={'intro/jobInfo/popup/ContsP'}
                    onClose={onCloseEqInfo}
                />
            ) : null}
            <div className="contents_wrap">
                <div className="job_top_contents">
                    <div className="main_tit">{`의뢰절차 안내`}</div>
                    <div className="icon_btn_main">
                        <Tooltip title="등록">
                            <IconButton
                                id="achive_img"
                                aria-label="add to favorites"
                                onClick={handleRegContents}>
                                <ArchiveOutlinedIcon className="icon_btn" />
                            </IconButton>
                        </Tooltip>
                    </div>
                    <div className="icon_btn_main">
                        <Tooltip title="재조회">
                            <IconButton
                                id="research_img"
                                aria-label="add to favorites"
                                onClick={handleSearchConts}>
                                <Refresh className="icon_btn" />
                            </IconButton>
                        </Tooltip>
                    </div>
                </div>
                <div className="job_content">
                    <Box component="div" sx={{p: 2}}>
                        <Seditor readOnly={true} modal={contenstObj.conts} />
                    </Box>
                </div>
            </div>
        </ContentsWrap>
    );
}
export default Guide;
