import React from 'react';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import './style.scss';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Box from '@mui/material/Box';

import './style.scss';
import UtilReq from './utilReq/UtilReq';
import OverallClaim from './overallClaim/OverallClaim';
import Electric from './electric/Electric';
import Storage from 'override/api/Storage';
import General from './general/General';
import Environ from './environ/Environ';
import PublicReport from './publicReport/PublicReport';

function Reg() {
    const {data: userInfo} = Storage.getUserInfo();
    const [value, setValue] = React.useState('1');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    return (
        <div className="contents_wrap_parents_h">
            <div className="anal_reg_main">
                <ContentsWrap>
                    <TabContext value={value}>
                        {userInfo.hrYn == 'Y' ? (
                            <>
                                <Box
                                    id="anal_reg_tab"
                                    sx={{
                                        borderBottom: 1,
                                        borderColor: 'divider',
                                    }}>
                                    <TabList
                                        onChange={handleChange}
                                        aria-label="lab API tabs example">
                                        <Tab
                                            label="종합및클레임분석의뢰"
                                            value="1"
                                        />
                                        <Tab
                                            label="특정설비 분석의뢰"
                                            value="2"
                                        />
                                        <Tab
                                            label="공인성적서 발행"
                                            value="3"
                                        />
                                        <Tab
                                            label="전력실험실전용분석의뢰"
                                            value="4"
                                        />
                                    </TabList>
                                </Box>
                                <div className="contents_wrap">
                                    <TabPanel value="1">
                                        {value === '1' && <OverallClaim />}
                                    </TabPanel>
                                    <TabPanel value="2">
                                        {value === '2' && <UtilReq />}
                                    </TabPanel>
                                    <TabPanel value="3">
                                        {value === '3' && <PublicReport />}
                                    </TabPanel>
                                    <TabPanel value="4">
                                        {value === '4' && <Electric />}
                                    </TabPanel>
                                </div>
                            </>
                        ) : (
                            <>
                                <Box
                                    id="anal_reg_tab"
                                    sx={{
                                        borderBottom: 1,
                                        borderColor: 'divider',
                                    }}>
                                    <TabList
                                        onChange={handleChange}
                                        aria-label="lab API tabs example">
                                        <Tab label="일반분석 의뢰" value="1" />
                                        <Tab
                                            label="환경유해물질 분석의뢰"
                                            value="2"
                                        />
                                    </TabList>
                                </Box>
                                <div className="contents_wrap">
                                    <TabPanel value="1">
                                        {value === '1' && <General />}
                                    </TabPanel>
                                    <TabPanel value="2">
                                        {value === '2' && <Environ />}
                                    </TabPanel>
                                </div>
                            </>
                        )}
                    </TabContext>
                </ContentsWrap>
            </div>
        </div>
    );
}
export default Reg;
