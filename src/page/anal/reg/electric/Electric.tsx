/* eslint-disable react/jsx-no-undef */
import React, {
    Suspense,
    useCallback,
    useEffect,
    useMemo,
    useState,
} from 'react';
import API from 'override/api/API';
import TextField from '@mui/material/TextField';
import Storage from '@override/api/Storage';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import SearchIcon from '@mui/icons-material/Search';
import SGrid from 'components/atoms/grid/SGrid';
import IconButton from '@mui/material/IconButton';

import RegProc from '../regProc/RegProc';
import MngInfo from '../mngInfo/MngInfo';
import SSelectbox from 'components/atoms/select/SSelectbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Spinner from 'components/atoms/spinner/Spinner';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import Radio from '@material-ui/core/Radio';
import SModal from 'override/modal/SModal';
import PendingActionsIcon from '@mui/icons-material/PendingActions';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import FindInPageIcon from '@mui/icons-material/FindInPage';
import {confirm, meg} from 'override/alert/Alert';
import DatePicker from 'components/atoms/datePicker/DatePicker';
import ConstructionIcon from '@mui/icons-material/Construction';
import Badge from '@mui/material/Badge';
import PrintIcon from '@mui/icons-material/Print';
import ForwardToInboxIcon from '@mui/icons-material/ForwardToInbox';
import RecommendIcon from '@mui/icons-material/Recommend';
import moment from 'moment';
import './style.scss';
import {Button} from '@mui/material';

interface RegModal {
    costCenter: string;
    plCenter: string;
    regTit: string;
    productNm: string;
    regType: string;
    gubun: string;
    fileGrpId?: number;
    isRefresh?: boolean;
    purpose: string;
    companyNm: string;
    regState: string;
    regEmpNo?: string;
    comment?: string;
    srType: string;
    keyword: string;
    apprModal?: ApprModal;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srDtlList: any;
    pjtModel?: PjtModal;
    regId?: string;
    pjtCd?: string;
    pjtNm?: string;
    claimDtm?: string;
    claimClient?: string;
    claimDesc?: string;
    claimAddress?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    fixRecordModel?: any;
    regUserInfo?: RegUserInfo;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srMngModel?: any;
}

interface ApprModal {
    apprEmpNm?: string;
    apprEmpNo?: string;
    apprReqDesc?: string;
    apprDeptNm?: string;
    apprReqDtm?: string;
    apprGubun?: string;
    apprDesc?: string;
}

interface PjtModal {
    pjtCd: string;
    pjtNm: string;
    pjtYn: boolean;
}

interface OverallClaimEquipDtlModal {
    regId: string;
    gridId?: number;
    sampleNm: string;
    analItem: string;
    exprtResult: string;
    sort: number;
}

// 변경
interface RegUserInfo {
    regEmpNm: string;
    regDeptNm: string;
    regRankNm: string;
    regOfficeNo: string;
    regMobileNo: string;
    regEmail: string;
    regEmpNo: string;
}

function UserInfo(param) {
    const {data} = Storage.getUserInfo();
    const [userInfo, setUserInfo] = useState<RegUserInfo>({
        regEmpNo: data.empNo,
        regEmpNm: data.userNm,
        regDeptNm: data.deptNm,
        regRankNm: data.rankNm,
        regOfficeNo: data.officeNo,
        regMobileNo: data.mobileNo,
        regEmail: data.email,
    });

    useEffect(() => {
        setUserInfo(param.regUserInfo);
    }, [param?.regUserInfo]);

    return (
        <div className="reg_user_info">
            <div className="reg_main_tit">
                <span className="tit">의뢰자 정보</span>
            </div>
            <div className="reg_main_contents">
                <div className="user_info_conts">
                    <div className="tit">성명</div>
                    <div className="user_info">{userInfo?.regEmpNm}</div>
                    <div className="tit">직위</div>
                    <div className="user_info">{userInfo?.regRankNm}</div>
                    <div className="tit">부서</div>
                    <div className="user_info">{userInfo?.regDeptNm}</div>
                </div>
                <div className="user_info_conts">
                    <div className="tit">E-MAIL</div>
                    <div className="user_info">{userInfo?.regEmail}</div>
                    <div className="tit">전화번호</div>
                    <div className="user_info">{userInfo?.regOfficeNo}</div>
                    <div className="tit">HP</div>
                    <div className="user_info">{userInfo?.regMobileNo}</div>
                </div>
            </div>
        </div>
    );
}

function RegInfo(param) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const regStateList = useMemo(() => {
        return commonCodeGroup['REG_STATE'];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [commonCodeGroup]);
    const {data} = Storage.getGridList();
    const {data: userInfo} = Storage.getUserInfo();
    const [costList, setCostList] = useState([]);
    const [rowData, setRowData] = useState([]);

    const [pjtModalOpen, setPjtModalOpen] = useState(false);
    const [userModalOpen, setUserModalOpen] = useState(false);
    const [reportModalOpen, setReportModalOpen] = useState(false);
    const [etmModalOpen, setEtmModalOpen] = useState(false);
    const [mngEmpList, setMngEmpList] = useState([]);
    const [isMngEmp, setIsMngEmp] = useState(false);
    const [disable, setDisable] = useState(false);
    const sizeObj = useMemo(() => {
        return {
            width: '900',
            height: '820',
            isResize: false,
        };
    }, []);

    const [validationObj, setValidationObl] = useState({
        costCenter: false,
        plCenter: false,
        regTit: false,
        productNm: false,
        regType: false,
        purpose: false,
        gubun: false,
        pjtYn: false,
        apprEmpNo: false,
        companyNm: false,
        keyword: false,
        srDtlList: false,
        apprReqDesc: false,
        apprDesc: false,
        apprGubun: false,
        pjtNm: false,
    });

    const [regModal, setRegModal] = useState<RegModal>({
        costCenter: '',
        plCenter: '',
        regTit: '',
        productNm: '',
        regType: '',
        purpose: '',
        srType: '',
        gubun: '',
        fileGrpId: 0,
        comment: '',
        keyword: '',
        regState: '100',
        srDtlList: null,
        companyNm: '',
        claimDtm: moment().format('YYYYMMDD'),
        regUserInfo: {
            regDeptNm: userInfo.deptNm,
            regEmail: userInfo.email,
            regEmpNm: userInfo.userNm,
            regEmpNo: userInfo.empNo,
            regOfficeNo: userInfo.officeNo,
            regMobileNo: userInfo.mobileNo,
            regRankNm: userInfo.rankNm,
        },
    });

    const [apprModal, setApprModal] = useState<ApprModal>({
        apprEmpNm: '결재자를 지정해 주십시오.',
        apprEmpNo: '',
        apprReqDesc: '',
        apprDeptNm: '',
    });

    const [pjtModal, setPjtModal] = useState<PjtModal>({
        pjtCd: '',
        pjtNm: '',
        pjtYn: false,
    });

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 32,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '번호',
                field: 'sort',
                width: 70,
                type: 'number',
                editable: false,
            },
            {
                headerName: '시료명',
                field: 'sampleNm',
                width: 150,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '알고자하는 사항(분석항목)',
                field: 'analItem',
                width: 300,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '시료설명 및 예상결과',
                field: 'exprtResult',
                width: 300,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                field: 'gridId',
                width: 300,
                editable: false,
                essential: false,
                hide: true,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        setCostList([]);
        const tempList = [];
        if (commonCodeGroup) {
            commonCodeGroup['COST_CENTER']
                .filter((x) => x.desc1 == regModal.plCenter)
                .forEach((element) => {
                    const costObj = {
                        cd: element.cd,
                        cdNm: element.cdNm,
                    };
                    tempList.push(costObj);
                });
        }
        setCostList(tempList);
        if (param?.regInfo?.regId) {
            initSearch(null);
        } else {
            setRegModal({
                ...regModal,
                costCenter: tempList.length > 0 ? tempList[0].cd : '',
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (
            regModal.regState === '110' &&
            regModal.regEmpNo == userInfo.empNo
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        if (
            regModal.regState === '130' &&
            regModal.regEmpNo == userInfo.empNo &&
            apprModal.apprDesc
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        apprModal.apprDesc,
        regModal.regEmpNo,
        regModal.regState,
        userInfo.empNo,
    ]);

    const fixConfirm = async () => {
        if (regModal.regState === '110' || regModal.regState === '130') {
            const tit =
                regModal.regState === '110' ? '의뢰 보완요청' : '부결 내용';

            let confirmResult = undefined;
            if (regModal.regState === '110') {
                confirmResult = await confirm({
                    title: tit,
                    html: `<div class="fix_confirm">
                            <div class="fix_emp_info">
                                <div class="fix_dept_nm">
                                    ${regModal.fixRecordModel.mngDeptNm}
                                </div>
                                <div class="fix_emp_nm">
                                    ${
                                        regModal.fixRecordModel.mngEmpNm +
                                        '  ' +
                                        regModal.fixRecordModel.mngPosNm
                                    }
                                </div>
                            </div>
                            <div class="fix_emp_desc">
                                ${regModal.fixRecordModel.mngDesc}
                            </div>
                        </div>`,
                });
            } else {
                confirmResult = await confirm({
                    title: tit,
                    html: `<div class="fix_confirm">
                            <div class="fix_emp_info">
                                <div class="fix_dept_nm">
                                    ${apprModal.apprDeptNm}
                                </div>
                                <div class="fix_emp_nm">
                                    ${apprModal.apprEmpNm}
                                </div>
                            </div>
                            <div class="fix_emp_desc">
                                ${apprModal.apprDesc}
                            </div>
                        </div>`,
                });
            }

            if (confirmResult) {
                if (regModal.regEmpNo != userInfo.empNo) {
                    meg({
                        title: '의뢰자가 아닙니다.',
                        html: '',
                    });
                    return;
                }

                setDisable(false);
                setRegModal({
                    ...regModal,
                    regState: '100',
                });
            }
        }
    };

    const initSearch = async (modal) => {
        const result = await API.request.post(
            `api/anal/overall-claim/get`,
            param.regInfo ? param.regInfo : modal,
        );

        if (result.data.success) {
            const setModalObj = result.data.data;
            setModalObj.isRefresh = !regModal.isRefresh;

            setRegModal({
                ...setModalObj,
                // 변경
                regUserInfo: {
                    regEmpNo: setModalObj.regEmpNo || '',
                    regEmpNm: setModalObj.regEmpNm || '',
                    regDeptNm: setModalObj.regDeptNm || '',
                    regRankNm: setModalObj.regRankNm || '',
                    regMobileNo: setModalObj.regMobileNo || '',
                    regEmail: setModalObj.regEmail || '',
                    regOfficeNo: setModalObj.regOfficeNo || '',
                },
            });
            setApprModal(
                result.data.data.apprModal
                    ? result.data.data.apprModal
                    : {
                          ...apprModal,
                          apprEmpNm: '결재자를 지정해 주십시오.',
                          apprEmpNo: '',
                          apprReqDesc: '',
                          apprDeptNm: '',
                      },
            );
            setRowData(result.data.data.srDtlList);
            setPjtModal({
                ...pjtModal,
                pjtCd: result.data.data.pjtCd,
                pjtNm: result.data.data.pjtNm,
                pjtYn: result.data.data.pjtYn,
            });

            if (
                (result.data.data.regState > '100' && result.data.data.regId) ||
                result.data.data.regEmpNo != userInfo.empNo
            ) {
                setDisable(true);
            }
        }
    };

    useEffect(() => {
        setCostList([]);
        const tempList = [];
        if (commonCodeGroup) {
            commonCodeGroup['COST_CENTER']
                .filter((x) => x.desc1 == regModal.plCenter)
                .forEach((element) => {
                    const costObj = {
                        cd: element.cd,
                        cdNm: element.cdNm,
                    };
                    tempList.push(costObj);
                });
        }
        setRegModal({
            ...regModal,
            costCenter: tempList.length > 0 ? tempList[0].cd : '',
        });
        setCostList(tempList);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.plCenter]);

    useEffect(() => {
        if (regModal.regState == '400' && regModal.srType) {
            async function mngEmpCheck() {
                const param = {
                    srType: regModal.srType,
                };
                const result = await API.request.post(
                    `api/anal/mng-mt/get`,
                    param,
                );
                if (result.data.success) {
                    const tempMngEmpList = [];
                    result.data.data.forEach((element) => {
                        const mngObj = {
                            cd: element.mngEmpNo,
                            cdNm: `${element.mngEmpNm || ''}${
                                element.mngDeptNm ? '_' + element.mngDeptNm : ''
                            }`,
                        };
                        tempMngEmpList.push(mngObj);
                    });

                    // 변경
                    setMngEmpList(tempMngEmpList);
                    if (regModal.srType > '200') {
                        setIsMngEmp(
                            result.data.data.find(
                                (x) => x.mngEmpNo == userInfo.empNo,
                            ),
                        );
                    }
                }
            }
            mngEmpCheck();
        }
    }, [regModal.regState, regModal.srType, userInfo.empNo]);

    const handleKeyDown = useCallback(async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleRegModalChange = async (e) => {
        const newValue = {
            ...regModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        setRegModal(newValue);
    };

    const handleApprModalChange = async (e) => {
        const newValue = {
            ...apprModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        setApprModal(newValue);
    };

    const handleCheckChange = (e) => {
        let newValue;

        if (e.target.id === 'pjtYn') {
            if (e.target.checked) {
                newValue = {
                    ...pjtModal, // 기존의 값 복사 (spread operator)
                    [e.target.id]: e.target.checked, // 덮어쓰기
                    pjtCd: '',
                    pjtNm: '',
                };
                setRegModal({
                    ...regModal,
                    pjtCd: '',
                    pjtNm: '',
                });
            } else {
                newValue = {
                    ...pjtModal, // 기존의 값 복사 (spread operator)
                    [e.target.id]: e.target.checked, // 덮어쓰기
                };
            }
            setPjtModal(newValue);
        } else {
            newValue = {
                ...regModal, // 기존의 값 복사 (spread operator)
                [e.target.id]: e.target.checked, // 덮어쓰기
            };
            setRegModal(newValue);
        }
    };

    const regGridDelete = async (param) => {
        if (param && param.data) {
            const params = {
                param1: param.data,
                param2: regModal,
            };
            const result = await API.request.post(
                `api/anal/overall-claim/dtl/delete`,
                params,
            );
            if (result) {
                setRowData(result.data.data);
            } else {
                setRowData([]);
            }
        }
    };

    const onClickOpenPjtModal = () => {
        if (pjtModal.pjtYn) {
            meg({
                title: '[해당사항 없음]이 체크되었습니다.',
                html: '',
            });
            return;
        }

        setPjtModalOpen(true);
    };
    const onPjtModalClose = (result) => {
        setPjtModal({
            ...pjtModal,
            pjtCd: result.pjtCd,
            pjtNm: result.pjtNm,
        });
        setPjtModalOpen(false);
    };

    const onClickOpenUserModal = () => {
        setUserModalOpen(true);
    };

    const onUserModalClose = (result) => {
        setUserModalOpen(false);
        if (result.data) {
            setApprModal({
                ...apprModal,
                apprEmpNo: result.data.empNo,
                apprEmpNm: result.data.userNm,
                apprDeptNm: result.data.deptNm,
            });
        }
    };

    const validationCheck = async () => {
        const validationKeyList = {
            costCenter: '코스트센터',
            plCenter: '손익센터',
            regTit: '제목',
            productNm: '제품명',
            regType: '의뢰유형',
            gubun: '시험구분',
            keyword: '키워드',
            srDtlList: '분석요청 리스트',
        };

        const keyObj = Object.keys(validationKeyList);
        const errorList = validationObj;
        const megList = [];
        keyObj.forEach((element) => {
            if (!regModal[element]) {
                megList.push(validationKeyList[element]);
                errorList[element] = true;
            } else {
                errorList[element] = false;
            }
        });

        if (
            !errorList['srDtlList'] &&
            regModal.srDtlList &&
            regModal.srDtlList.length == 0
        ) {
            errorList['srDtlList'] = true;
            megList.push(validationKeyList['srDtlList']);
        }

        if (!pjtModal.pjtYn) {
            if (!pjtModal.pjtCd) {
                errorList['pjtNm'] = true;
                megList.push('Project Code / 여부');
            }
        }

        if (!apprModal.apprEmpNo) {
            errorList['apprEmpNo'] = true;
            megList.push('결재자 정보');
        }

        if (!apprModal.apprReqDesc) {
            errorList['apprReqDesc'] = true;
            megList.push('결재자 요청메모');
        }

        if (regModal.regState === '200') {
            if (!apprModal.apprDesc) {
                errorList['apprDesc'] = true;
                megList.push('결재자 처리메모');
            }

            if (!apprModal.apprGubun) {
                errorList['apprGubun'] = true;
                megList.push('결재처리');
            }
        }

        if (megList.length > 0) {
            setValidationObl({
                ...errorList,
            });
            let validationMeg = `<div class="meg_main">`;
            megList.forEach((element) => {
                validationMeg += `
                        <div class="meg_contents error">
                            ${element}
                        </div>
                    `;
            });

            validationMeg.concat(`</div>`);
            await meg({
                title: `필수 항목을 입력하십시오.`,
                html: validationMeg,
            });
            return false;
        }
        return true;
    };

    const onSaveOverallClaimInfo = async (regState) => {
        if (data) {
            const regOverallClaimGrid = data?.get('reg_overall_claim_grid');
            if (
                regOverallClaimGrid &&
                regOverallClaimGrid.getModel()?.rowsToDisplay?.length > 0
            ) {
                regModal.srDtlList = [];
                regOverallClaimGrid
                    .getModel()
                    .rowsToDisplay.forEach((element) => {
                        if (element.data) {
                            const tempObj: OverallClaimEquipDtlModal = {
                                analItem: element.data.analItem,
                                exprtResult: element.data.exprtResult,
                                gridId: element.data.gridId,
                                sampleNm: element.data.sampleNm,
                                sort: element.data.sort,
                                regId: element.data.regId,
                            };
                            regModal.srDtlList.push(tempObj);
                        }
                    });
            }
        }

        const validationResult = await validationCheck();

        if (validationResult) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (
                regModal.srDtlList.find(
                    (x) => !x.sampleNm || !x.analItem || !x.exprtResult,
                )
            ) {
                await meg({
                    title: `분석요청 필수값을 입력해 주십시오.`,
                    html: '',
                });

                return;
            }
            if (confirmResult) {
                regModal.regState = regState;
                regModal.apprModal = apprModal;
                regModal.pjtModel = pjtModal;
                const fileUploadGrid = data?.get('reg_sr_claim_upload_grid');
                if (fileUploadGrid) {
                    const fileSaveList = [];
                    fileUploadGrid
                        ?.getModel()
                        ?.rowsToDisplay.map((item) =>
                            fileSaveList.push(item.data),
                        );
                    if (fileSaveList.length > 0) {
                        const result = await saveFilesEditAll(fileSaveList);
                        if (result) {
                            regModal.fileGrpId = result;
                        }
                    }

                    switch (regModal.gubun) {
                        case '01':
                            regModal.srType = 'T';
                            break;
                        default:
                            regModal.srType = 'C';
                            break;
                    }

                    if (
                        regModal.srDtlList.find(
                            (x) => !x.sampleNm || !x.analItem || !x.exprtResult,
                        )
                    ) {
                        await meg({
                            title: `분석요청 필수값을 입력해 주십시오.`,
                            html: '',
                        });

                        return;
                    }

                    const result = await API.request.post(
                        `api/anal/overall-claim/save`,
                        regModal,
                    );

                    if (result.data.success) {
                        const saveModal = result.data.data;
                        saveModal.isRefresh = !regModal.isRefresh;
                        setRegModal({
                            ...saveModal,
                            regUserInfo: {
                                regEmpNo: saveModal.regEmpNo || '',
                                regEmpNm: saveModal.regEmpNm || '',
                                regDeptNm: saveModal.regDeptNm || '',
                                regRankNm: saveModal.regRankNm || '',
                                regMobileNo: saveModal.regMobileNo || '',
                                regEmail: saveModal.regEmail || '',
                                regOfficeNo: saveModal.regOfficeNo || '',
                            },
                        });
                        setApprModal(
                            result.data.data.apprModal
                                ? result.data.data.apprModal
                                : {
                                      ...apprModal,
                                      apprEmpNm: '결재자를 지정해 주십시오.',
                                      apprEmpNo: '',
                                      apprReqDesc: '',
                                      apprDeptNm: '',
                                  },
                        );
                        setRowData(result.data.data.srDtlList);
                        setPjtModal({
                            ...pjtModal,
                            pjtCd: result.data.data.pjtCd,
                            pjtNm: result.data.data.pjtNm,
                            pjtYn: result.data.data.pjtYn,
                        });

                        if (
                            result.data.data.regState > '100' &&
                            result.data.data.regId
                        ) {
                            setDisable(true);
                        }

                        if (saveModal.regState == '400') {
                            await meg({
                                title: `시료를 보내주십시오.`,
                                html: '',
                            });
                        }
                    }
                }
            }
        }
    };

    const onCopyOverallClaimInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 복사하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            setDisable(false);
            setRegModal({
                ...regModal,
                regId: '',
                regState: '',
                fileGrpId: 0,
                srDtlList: [],
                // 변경
                regUserInfo: {
                    regDeptNm: userInfo.deptNm,
                    regEmail: userInfo.email,
                    regEmpNm: userInfo.userNm,
                    regEmpNo: userInfo.empNo,
                    regOfficeNo: userInfo.officeNo,
                    regMobileNo: userInfo.mobileNo,
                    regRankNm: userInfo.rankNm,
                },
            });
            setRowData([]);
        }
    };

    const onDeleteOverallClaimInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 삭제하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/anal/overall-claim/delete`,
                regModal,
            );

            if (result.data.success) {
                initModal();
            }
        }
    };

    const initModal = () => {
        setRegModal({
            costCenter: '',
            plCenter: '',
            regTit: '',
            productNm: '',
            regType: '',
            purpose: '',
            gubun: '',
            fileGrpId: 0,
            comment: '',
            keyword: '',
            companyNm: '',
            regState: '100',
            srDtlList: null,
            srType: 'T',
        });

        setApprModal({
            apprEmpNm: '결재자를 지정해 주십시오.',
            apprEmpNo: '',
            apprReqDesc: '',
            apprDeptNm: '',
        });

        setPjtModal({
            pjtCd: '',
            pjtNm: '',
            pjtYn: false,
        });

        setRowData([]);
        setDisable(false);
    };

    const onOpenReport = () => {
        setReportModalOpen(true);
    };

    const onReportModalClose = () => {
        setReportModalOpen(false);
    };

    const onOpenEtm = () => {
        setEtmModalOpen(true);
    };

    const onEtmModalClose = async (param) => {
        if (param.data && param.data.success) {
            await initSearch(null);
        }
        setEtmModalOpen(false);
    };

    return (
        <>
            {isMngEmp && regModal.regState === '400' ? (
                // 변경
                <MngInfo
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        initSearch: initSearch,
                    }}
                />
            ) : null}
            {regModal.regState > '400' || regModal.regState == '140' ? (
                // 변경
                <RegProc
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        isProcEquipReg: false,
                        initSearch: initSearch,
                    }}
                />
            ) : null}

            <div className="reg_info">
                <SModal
                    tit={'프로젝트 검색'}
                    isOpen={pjtModalOpen}
                    sizeObj={sizeObj}
                    path={'anal/reg/project/Project'}
                    onClose={onPjtModalClose}
                />
                <SModal
                    tit={'결재자 등록'}
                    isOpen={userModalOpen}
                    param={{mode: 'single'}}
                    sizeObj={sizeObj}
                    path={'common/popup/user/User'}
                    onClose={onUserModalClose}
                />
                <SModal
                    tit={'전력실험전용분석 Report'}
                    isOpen={reportModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '1200',
                        height: '820',
                        isResize: false,
                    }}
                    path={'common/popup/report/claim/OverallClaimReport'}
                    onClose={onReportModalClose}
                />
                <SModal
                    tit={'분석평가 등록'}
                    isOpen={etmModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '600',
                        height: 'auto',
                        isResize: false,
                    }}
                    path={'anal/reg/etm/Etm'}
                    onClose={onEtmModalClose}
                />
                <>
                    <div className="reg_main_tit">
                        <span className="tit">
                            {regModal.regId ? (
                                <>
                                    <div className="tit_01">
                                        {regModal.regId}
                                    </div>
                                    <div
                                        className="tit_02"
                                        // 변경
                                        style={{
                                            background:
                                                regModal.regState === '110'
                                                    ? '#8382db'
                                                    : regModal.regState ===
                                                      '200'
                                                    ? '#e76e6e'
                                                    : regModal.regState ===
                                                      '400'
                                                    ? '#60b180'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#5ab2e5'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#0b9562'
                                                    : '#db8181',
                                        }}>
                                        {
                                            regStateList.filter(
                                                (x) =>
                                                    x.cd == regModal.regState,
                                            )[0].cdNm
                                        }
                                    </div>
                                </>
                            ) : (
                                '의뢰 등록'
                            )}
                        </span>
                        <div className="search_btns">
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenReport}
                                    startIcon={<PrintIcon />}>
                                    의뢰내용 출력
                                </Button>
                            )}
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onCopyOverallClaimInfo}
                                    startIcon={<ContentCopyIcon />}>
                                    복사
                                </Button>
                            )}
                            {apprModal.apprEmpNo === userInfo.empNo &&
                                regModal.regId &&
                                regModal.regState === '200' && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        disabled={
                                            regModal.regEmpNo != '' &&
                                            userInfo.empNo !=
                                                regModal?.apprModal?.apprEmpNo
                                        }
                                        onClick={() => {
                                            const regState =
                                                apprModal.apprGubun === 'APPR'
                                                    ? '400'
                                                    : '130';
                                            onSaveOverallClaimInfo(regState);
                                        }}
                                        startIcon={<ForwardToInboxIcon />}>
                                        결재하기
                                    </Button>
                                )}
                            {regModal.regId &&
                                (regModal.regState === '110' ||
                                    regModal.regState === '130') && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        onClick={fixConfirm}
                                        startIcon={
                                            <Badge
                                                badgeContent={1}
                                                color="primary">
                                                <ConstructionIcon className="btn_icon" />
                                            </Badge>
                                        }>
                                        {regModal.regState === '130'
                                            ? '부결내용 확인'
                                            : '보완요청 확인'}
                                    </Button>
                                )}
                            {regModal.regId && regModal.regState < '150' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={onDeleteOverallClaimInfo}
                                    startIcon={<DeleteForeverIcon />}>
                                    삭제
                                </Button>
                            )}
                            {regModal.regState <= '100' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={() => {
                                        onSaveOverallClaimInfo('100');
                                    }}
                                    startIcon={<PendingActionsIcon />}>
                                    임시저장
                                </Button>
                            )}
                            {(!regModal.regId || regModal.regState === '100') &&
                                !regModal.srMngModel && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        disabled={
                                            regModal.regEmpNo != '' &&
                                            userInfo.empNo !=
                                                regModal?.regUserInfo?.regEmpNo
                                        }
                                        onClick={() => {
                                            onSaveOverallClaimInfo('200');
                                        }}
                                        startIcon={<FindInPageIcon />}>
                                        결재요청
                                    </Button>
                                )}
                            {apprModal.apprGubun === 'APPR' &&
                                regModal.srMngModel && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        disabled={
                                            regModal.regEmpNo != '' &&
                                            userInfo.empNo !=
                                                regModal?.regUserInfo?.regEmpNo
                                        }
                                        onClick={() => {
                                            let regState = '400';
                                            if (regModal.srMngModel) {
                                                regState = '500';
                                            }

                                            onSaveOverallClaimInfo(regState);
                                        }}
                                        startIcon={
                                            <ForwardToInboxIcon className="btn_icon" />
                                        }>
                                        의뢰하기
                                    </Button>
                                )}
                            {regModal.regId && regModal.regState == '900' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenEtm}
                                    startIcon={<RecommendIcon />}>
                                    평가하기
                                </Button>
                            )}
                        </div>
                    </div>
                    <div className="reg_main_contents">
                        <div className="reg_contents">
                            <TextField
                                id="regTit"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                label={'제목'}
                                sx={{
                                    minWidth: 300,
                                }}
                                error={!regModal.regTit && validationObj.regTit}
                                value={regModal.regTit}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <TextField
                                id="productNm"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                sx={{
                                    minWidth: 200,
                                }}
                                label={'제품명'}
                                error={
                                    !regModal.productNm &&
                                    validationObj.productNm
                                }
                                value={regModal.productNm}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <TextField
                                id="keyword"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                sx={{
                                    minWidth: 200,
                                }}
                                label={'키워드'}
                                error={
                                    validationObj.keyword && !regModal.keyword
                                }
                                value={regModal.keyword}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                        </div>
                        <div className="reg_contents">
                            <TextField
                                id="purpose"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                sx={{
                                    minWidth: 300,
                                }}
                                label={'의뢰목적'}
                                error={
                                    validationObj.purpose && !regModal.purpose
                                }
                                value={regModal.purpose}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'REG_TYPE'}
                                label={'의뢰유형'}
                                id={'regType'}
                                // default={regModal.shareGroup}
                                error={
                                    validationObj.regType && !regModal.regType
                                }
                                value={regModal.regType}
                                handleChange={handleRegModalChange}
                                disabled={disable}
                            />
                            <FormControl
                                id={'radio_main'}
                                error={validationObj.gubun && !regModal.gubun}>
                                <FormLabel id="radio_group">시험구분</FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="radio_group"
                                    name="row-radio-buttons-group">
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="01"
                                                checked={
                                                    regModal.gubun === '01'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="일반분석"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="02"
                                                checked={
                                                    regModal.gubun === '02'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="클레임"
                                    />
                                </RadioGroup>
                            </FormControl>
                        </div>
                        {regModal.gubun === '01' && (
                            <>
                                <div className="reg_contents">
                                    <TextField
                                        id="companyNm"
                                        onChange={handleRegModalChange}
                                        onKeyDown={handleKeyDown}
                                        sx={{
                                            minWidth: 200,
                                        }}
                                        label={'제조사'}
                                        error={
                                            validationObj.companyNm &&
                                            !regModal.companyNm
                                        }
                                        value={regModal.companyNm}
                                        // value={searchObj.equipMngEmpNm}
                                        size="small"
                                        disabled={disable}
                                    />
                                </div>
                                <div className="reg_contents">
                                    <div className="text_area">
                                        <TextField
                                            id="comment"
                                            onChange={handleRegModalChange}
                                            sx={{
                                                width: '100%',
                                                height: 80,
                                                minHeight: 80,
                                            }}
                                            multiline
                                            rows={3}
                                            label={'Comments'}
                                            size="small"
                                            value={regModal.comment}
                                            disabled={disable}
                                        />
                                    </div>
                                </div>
                            </>
                        )}

                        {regModal.gubun === '02' ? (
                            <>
                                <div className="reg_contents">
                                    <TextField
                                        id="claimAddress"
                                        onChange={handleRegModalChange}
                                        onKeyDown={handleKeyDown}
                                        sx={{
                                            minWidth: 200,
                                        }}
                                        label={'클레임 장소'}
                                        error={
                                            validationObj.companyNm &&
                                            !regModal.companyNm
                                        }
                                        value={regModal.claimAddress}
                                        // value={searchObj.equipMngEmpNm}
                                        size="small"
                                        disabled={disable}
                                    />
                                    <TextField
                                        id="claimClient"
                                        onChange={handleRegModalChange}
                                        onKeyDown={handleKeyDown}
                                        sx={{
                                            minWidth: 200,
                                        }}
                                        label={'외부고객'}
                                        error={
                                            validationObj.companyNm &&
                                            !regModal.companyNm
                                        }
                                        value={regModal.claimClient}
                                        // value={searchObj.equipMngEmpNm}
                                        size="small"
                                        disabled={disable}
                                    />
                                    <DatePicker
                                        label={'클레임 날짜'}
                                        disabled={disable}
                                        width={142}
                                        handleChange={handleRegModalChange}
                                        id={'claimDtm'}
                                        inputFormat={'yyyy-MM-dd'}
                                        returnFormat={'YYYYMMDD'}
                                        defaultValue={regModal.claimDtm}
                                        mask={'____-__-__'}
                                    />
                                </div>
                                <div className="reg_contents">
                                    <div className="text_area">
                                        <TextField
                                            id="claimDesc"
                                            onChange={handleRegModalChange}
                                            sx={{
                                                width: '100%',
                                                height: 80,
                                                minHeight: 80,
                                            }}
                                            multiline
                                            rows={3}
                                            label={'클레임 내용 및 규모'}
                                            size="small"
                                            value={regModal.claimDesc}
                                            disabled={disable}
                                        />
                                    </div>
                                </div>
                            </>
                        ) : null}
                        <div className="reg_contents">
                            <Paper
                                sx={{
                                    p: '2px 4px',
                                    display: 'flex',
                                    alignItems: 'center',
                                    width: 450,
                                }}>
                                <InputBase
                                    sx={{ml: 1, flex: 1}}
                                    placeholder="Project Code"
                                    disabled={true}
                                    id={'pjtNm'}
                                    value={pjtModal.pjtNm ? pjtModal.pjtNm : ''}
                                    error={
                                        validationObj.pjtNm &&
                                        !pjtModal.pjtNm &&
                                        pjtModal.pjtYn
                                    }
                                    inputProps={{'aria-label': 'Project Code'}}
                                />
                                <Divider
                                    sx={{height: 28, m: 0.2}}
                                    orientation="vertical"
                                />
                                <IconButton
                                    onClick={onClickOpenPjtModal}
                                    sx={{p: '10px'}}
                                    disabled={disable}
                                    aria-label="search">
                                    <SearchIcon />
                                </IconButton>
                                <Divider
                                    sx={{height: 28, m: 0.2}}
                                    orientation="vertical"
                                />
                                <FormControlLabel
                                    disabled={disable}
                                    control={
                                        <Checkbox
                                            sx={{marginLeft: 0.5}}
                                            id={'pjtYn'}
                                            onChange={handleCheckChange}
                                            checked={pjtModal.pjtYn}
                                            disabled={disable}
                                        />
                                    }
                                    label="해당사항 없음"
                                    labelPlacement="start"
                                />
                            </Paper>
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'PL_CENTER'}
                                label={'손익센터'}
                                id={'plCenter'}
                                value={regModal.plCenter}
                                error={
                                    validationObj.plCenter && !regModal.plCenter
                                }
                                handleChange={handleRegModalChange}
                                disabled={disable}
                            />
                            <SSelectbox
                                id={'costCenter'}
                                isCodeGroupMode={false}
                                codeGroup={'COST_CENTER'}
                                label={'코스트센터'}
                                disabled={disable}
                                selectList={costList}
                                discription={
                                    costList.length == 0
                                        ? '손익센터를 먼저 선택해 주십시오.'
                                        : null
                                }
                                value={regModal.costCenter}
                                error={
                                    validationObj.costCenter &&
                                    !regModal.costCenter
                                }
                                handleChange={handleRegModalChange}
                            />
                        </div>
                        <div className="reg_contents">
                            <SGrid
                                grid={'reg_overall_claim_grid'}
                                rowData={rowData}
                                isForceDelete={true}
                                title={'분석요청'}
                                newRowDefault={{
                                    sort: 'auto',
                                }}
                                isAddBtnOpen={!disable}
                                onClickDelete={disable ? null : regGridDelete}
                                columnDefs={columnDefs}
                            />
                        </div>
                        <div className="reg_contents">
                            <SFile
                                gridId={'reg_sr_claim_upload_grid'}
                                fileGrpId={regModal.fileGrpId}
                                isRefresh={regModal.isRefresh}
                                tit={'첨부파일'}
                                useDownload={true}
                                useFileAttach={!disable}
                                useFileDelete={!disable}
                            />
                        </div>
                    </div>
                </>
                <div className="appr_main">
                    <div className="reg_main_tit">
                        <span className="tit">결재자 지정</span>
                    </div>
                    <div className="reg_main_contents">
                        {apprModal.apprReqDtm && regModal.regState >= '200' ? (
                            <>
                                <div className="reg_contents">
                                    <TextField
                                        id="apprEmpNm"
                                        disabled={true}
                                        value={apprModal.apprEmpNm}
                                        sx={{
                                            minWidth: 150,
                                            width: 'auto',
                                        }}
                                        label={'결재자'}
                                        size="small"
                                    />
                                    <TextField
                                        id="apprDesc"
                                        onChange={handleApprModalChange}
                                        onKeyDown={handleKeyDown}
                                        disabled={
                                            regModal.regState >= '400'
                                                ? true
                                                : false
                                        }
                                        value={
                                            apprModal.apprDesc
                                                ? apprModal.apprDesc
                                                : ''
                                        }
                                        error={
                                            !apprModal.apprDesc &&
                                            validationObj.apprDesc
                                        }
                                        sx={{
                                            minWidth: 200,
                                            width: '100%',
                                        }}
                                        label={'결재자 처리메모'}
                                        size="small"
                                    />
                                    <FormControl
                                        id={'radio_main'}
                                        error={
                                            validationObj.apprGubun &&
                                            !apprModal.apprGubun
                                        }>
                                        <FormLabel id="radio_group">
                                            결재처리
                                        </FormLabel>
                                        <RadioGroup
                                            row
                                            aria-labelledby="radio_group"
                                            name="row-radio-buttons-group">
                                            <FormControlLabel
                                                control={
                                                    <Radio
                                                        value="APPR"
                                                        checked={
                                                            apprModal.apprGubun ===
                                                            'APPR'
                                                        }
                                                        onChange={
                                                            handleApprModalChange
                                                        }
                                                        id={'apprGubun'}
                                                        disabled={
                                                            regModal.regState >=
                                                            '400'
                                                                ? true
                                                                : false
                                                        }
                                                    />
                                                }
                                                label="승인"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Radio
                                                        value="REJECT"
                                                        checked={
                                                            apprModal.apprGubun ===
                                                            'REJECT'
                                                        }
                                                        onChange={
                                                            handleApprModalChange
                                                        }
                                                        id={'apprGubun'}
                                                        disabled={
                                                            regModal.regState >=
                                                            '400'
                                                                ? true
                                                                : false
                                                        }
                                                    />
                                                }
                                                label="부결"
                                            />
                                        </RadioGroup>
                                    </FormControl>
                                </div>
                                <div className="reg_contents">
                                    <TextField
                                        id="regEmpNm"
                                        disabled={true}
                                        value={regModal.regUserInfo.regEmpNm}
                                        sx={{
                                            minWidth: 150,
                                        }}
                                        label={'요청자'}
                                        size="small"
                                    />
                                    <TextField
                                        id="apprReqDesc"
                                        disabled={disable}
                                        value={apprModal.apprReqDesc}
                                        sx={{
                                            minWidth: 200,
                                            width: '100%',
                                        }}
                                        label={'결재 요청메모'}
                                        size="small"
                                    />
                                    <DatePicker
                                        label={'결재요청일'}
                                        disabled={true}
                                        width={142}
                                        handleChange={handleRegModalChange}
                                        id={'claimDtm'}
                                        inputFormat={'yyyy-MM-dd'}
                                        returnFormat={'YYYYMMDD'}
                                        defaultValue={apprModal.apprReqDtm}
                                        mask={'____-__-__'}
                                    />
                                </div>
                            </>
                        ) : (
                            <div className="reg_contents">
                                <Paper
                                    sx={{
                                        p: '2px 4px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        width: 'fit-content',
                                        border:
                                            !apprModal.apprEmpNo &&
                                            validationObj.apprEmpNo
                                                ? '1px solid #d32f2f'
                                                : null,
                                    }}>
                                    <div className="appr_info">
                                        <span id={'apprDeptNm'}>
                                            {apprModal.apprDeptNm}
                                        </span>
                                        <span
                                            id={'apprEmpNm'}
                                            style={{
                                                color:
                                                    !apprModal.apprEmpNo &&
                                                    validationObj.apprEmpNo
                                                        ? '#d32f2f'
                                                        : ' #4f73c3',
                                            }}>
                                            {apprModal.apprEmpNm}
                                        </span>
                                    </div>
                                    <Divider
                                        sx={{height: 28, m: 0.2}}
                                        orientation="vertical"
                                    />
                                    <IconButton
                                        onClick={onClickOpenUserModal}
                                        sx={{p: '10px'}}
                                        disabled={disable}
                                        aria-label="search">
                                        <SearchIcon />
                                    </IconButton>
                                </Paper>
                                <TextField
                                    id="apprReqDesc"
                                    onChange={handleApprModalChange}
                                    onKeyDown={handleKeyDown}
                                    disabled={disable}
                                    value={apprModal.apprReqDesc}
                                    error={
                                        !apprModal.apprReqDesc &&
                                        validationObj.apprReqDesc
                                    }
                                    sx={{
                                        minWidth: 200,
                                        width: '100%',
                                    }}
                                    label={'결재자 요청메모'}
                                    size="small"
                                />
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <UserInfo regUserInfo={regModal.regUserInfo} />
        </>
    );
}

const Electric = (param) => {
    const regInfo = useMemo(() => {
        if (param?.param) {
            return {
                regInfo: {
                    regId: param?.param?.regId || '',
                },
            };
        }
        return;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param?.param]);

    return (
        <div className="contents_wrap_parents_h">
            <div className="overall_claim_req_main">
                <Suspense fallback={<Spinner />}>
                    <ContentsWrap>
                        <RegInfo regInfo={regInfo?.regInfo} />
                    </ContentsWrap>
                </Suspense>
            </div>
        </div>
    );
};
export default Electric;
