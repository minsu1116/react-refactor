/* eslint-disable react/jsx-no-undef */
import React, {
    Suspense,
    useCallback,
    useEffect,
    useMemo,
    useState,
} from 'react';
import API from 'override/api/API';
import TextField from '@mui/material/TextField';
import Storage from '@override/api/Storage';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import SGrid from 'components/atoms/grid/SGrid';

import RegProc from '../regProc/RegProc';
import MngInfo from '../mngInfo/MngInfo';
import SSelectbox from 'components/atoms/select/SSelectbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Spinner from 'components/atoms/spinner/Spinner';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import Radio from '@material-ui/core/Radio';
import SModal from 'override/modal/SModal';
import PendingActionsIcon from '@mui/icons-material/PendingActions';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {confirm, meg} from 'override/alert/Alert';
import ConstructionIcon from '@mui/icons-material/Construction';
import PrintIcon from '@mui/icons-material/Print';
import ForwardToInboxIcon from '@mui/icons-material/ForwardToInbox';
import RecommendIcon from '@mui/icons-material/Recommend';
import Badge from '@mui/material/Badge';
import './style.scss';
import {Button} from '@mui/material';

interface RegModal {
    regTit: string;
    regType: string;
    gubun: string;
    fileGrpId?: number;
    isRefresh?: boolean;
    comment?: string;
    purpose: string;
    regState: string;
    regEmpNo?: string;
    langGubun?: string;
    address?: string;
    srType: string;
    submitDep?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srDtlList: any;
    regId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    fixRecordModel?: any;
    regUserInfo?: RegUserInfo;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srMngModel?: any;
}

interface EnvironDtlModal {
    regId: string;
    gridId?: number;
    sampleNm: string;
    rawMatNm: string;
    productNm: string;
    mfNm: string;
    lotNo: string;
    form: string;
    color: string;
    gubun: string;
    matQty: string;
    sort: number;
}

// 변경
interface RegUserInfo {
    regEmpNm: string;
    regDeptNm: string;
    regRankNm: string;
    regOfficeNo: string;
    regMobileNo: string;
    regEmail: string;
    regEmpNo: string;
}

function UserInfo(param) {
    const {data} = Storage.getUserInfo();
    const [userInfo, setUserInfo] = useState<RegUserInfo>({
        regEmpNo: data.empNo,
        regEmpNm: data.userNm,
        regDeptNm: data.deptNm,
        regRankNm: data.rankNm,
        regOfficeNo: data.officeNo,
        regMobileNo: data.mobileNo,
        regEmail: data.email,
    });

    useEffect(() => {
        setUserInfo(param.regUserInfo);
    }, [param?.regUserInfo]);

    return (
        <div className="reg_user_info">
            <div className="reg_main_tit">
                <span className="tit">요청자 정보</span>
            </div>
            <div className="reg_main_contents">
                <div className="user_info_conts">
                    <div className="tit">성명</div>
                    <div className="user_info">{userInfo?.regEmpNm}</div>
                    <div className="tit">직위</div>
                    <div className="user_info">{userInfo?.regRankNm}</div>
                    <div className="tit">부서</div>
                    <div className="user_info">{userInfo?.regDeptNm}</div>
                </div>
                <div className="user_info_conts">
                    <div className="tit">E-MAIL</div>
                    <div className="user_info">{userInfo?.regEmail}</div>
                    <div className="tit">전화번호</div>
                    <div className="user_info">{userInfo?.regOfficeNo}</div>
                    <div className="tit">HP</div>
                    <div className="user_info">{userInfo?.regMobileNo}</div>
                </div>
            </div>
        </div>
    );
}

function RegInfo(param) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const regStateList = useMemo(() => {
        return commonCodeGroup['REG_STATE'];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [commonCodeGroup]);
    const {data} = Storage.getGridList();
    const {data: userInfo} = Storage.getUserInfo();
    const [rowData, setRowData] = useState([]);

    const [reportModalOpen, setReportModalOpen] = useState(false);
    const [etmModalOpen, setEtmModalOpen] = useState(false);
    const [mngEmpList, setMngEmpList] = useState([]);
    const [isMngEmp, setIsMngEmp] = useState(false);
    const [disable, setDisable] = useState(false);

    const [validationObj, setValidationObl] = useState({
        regTit: false,
        regType: false,
        purpose: false,
        comment: false,
        submitDep: false,
        gubun: false,
        keyword: false,
        address: false,
        srDtlList: false,
    });

    const [regModal, setRegModal] = useState<RegModal>({
        regTit: '',
        regType: '',
        comment: '',
        purpose: '',
        srType: '',
        gubun: '',
        fileGrpId: 0,
        langGubun: '',
        regState: '100',
        srDtlList: null,
        regUserInfo: {
            regDeptNm: userInfo.deptNm,
            regEmail: userInfo.email,
            regEmpNm: userInfo.userNm,
            regEmpNo: userInfo.empNo,
            regOfficeNo: userInfo.officeNo,
            regMobileNo: userInfo.mobileNo,
            regRankNm: userInfo.rankNm,
        },
    });

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 32,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '번호',
                field: 'sort',
                width: 70,
                type: 'number',
                editable: false,
            },
            {
                headerName: '시료명',
                field: 'sampleNm',
                color: '#bd1717',
                width: 90,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: 'Lot No',
                field: 'lotNo',
                width: 90,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '재질',
                field: 'matQty',
                width: 90,
                color: '#bd1717',
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '제품명',
                field: 'productNm',
                width: 160,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '시험항목',
                field: 'gubun',
                width: 90,
                type: 'select',
                codeGroup: 'PR_GUBUN',
                editable: false,
                readOnly: regModal.regState <= '100' ? false : true,
                essential: true,
            },
            {
                headerName: '형태',
                field: 'form',
                width: 120,
                color: '#bd1717',
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '원료명',
                field: 'rawMatNm',
                width: 130,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '제조사',
                field: 'mfNm',
                width: 140,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '색상',
                field: 'color',
                type: 'select',
                codeGroup: 'COLOR_GROUP',
                readOnly: regModal.regState <= '100' ? false : true,
                editable: false,
                width: 120,
                essential: true,
            },
            {
                field: 'gridId',
                width: 0,
                editable: false,
                essential: false,
                hide: true,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        if (param?.regInfo?.regId) {
            initSearch(null);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (
            regModal.regState === '110' &&
            regModal.regEmpNo == userInfo.empNo
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        if (
            regModal.regState === '130' &&
            regModal.regEmpNo == userInfo.empNo
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regEmpNo, regModal.regState, userInfo.empNo]);

    const fixConfirm = async () => {
        if (regModal.regState === '110' || regModal.regState === '130') {
            const tit =
                regModal.regState === '110' ? '의뢰 보완요청' : '부결 내용';

            let confirmResult = undefined;
            if (regModal.regState === '110') {
                confirmResult = await confirm({
                    title: tit,
                    html: `<div class="fix_confirm">
                            <div class="fix_emp_info">
                                <div class="fix_dept_nm">
                                    ${regModal.fixRecordModel.mngDeptNm}
                                </div>
                                <div class="fix_emp_nm">
                                    ${
                                        regModal.fixRecordModel.mngEmpNm +
                                        '  ' +
                                        regModal.fixRecordModel.mngPosNm
                                    }
                                </div>
                            </div>
                            <div class="fix_emp_desc">
                                ${regModal.fixRecordModel.mngDesc}
                            </div>
                        </div>`,
                });
            }

            if (confirmResult) {
                if (regModal.regEmpNo != userInfo.empNo) {
                    meg({
                        title: '의뢰자가 아닙니다.',
                        html: '',
                    });
                    return;
                }

                setDisable(false);
                setRegModal({
                    ...regModal,
                    regState: '100',
                });
            }
        }
    };

    const initSearch = async (modal) => {
        const result = await API.request.post(
            `api/anal/public-report/get`,
            param.regInfo ? param.regInfo : modal,
        );

        if (result.data.success) {
            const setModalObj = result.data.data;
            setModalObj.isRefresh = !regModal.isRefresh;

            setRegModal({
                ...setModalObj,
                // 변경
                regUserInfo: {
                    regEmpNo: setModalObj.regEmpNo || '',
                    regEmpNm: setModalObj.regEmpNm || '',
                    regDeptNm: setModalObj.regDeptNm || '',
                    regRankNm: setModalObj.regRankNm || '',
                    regMobileNo: setModalObj.regMobileNo || '',
                    regEmail: setModalObj.regEmail || '',
                    regOfficeNo: setModalObj.regOfficeNo || '',
                },
            });
            setRowData(result.data.data.srDtlList);
            if (
                (result.data.data.regState > '100' && result.data.data.regId) ||
                result.data.data.regEmpNo != userInfo.empNo
            ) {
                setDisable(true);
            }
        }
    };

    useEffect(() => {
        if (regModal.regState == '400' && regModal.srType) {
            async function mngEmpCheck() {
                const param = {
                    srType: regModal.srType,
                };
                const result = await API.request.post(
                    `api/anal/mng-mt/get`,
                    param,
                );
                if (result.data.success) {
                    const tempMngEmpList = [];
                    result.data.data.forEach((element) => {
                        const mngObj = {
                            cd: element.mngEmpNo,
                            cdNm: `${element.mngEmpNm || ''}${
                                element.mngDeptNm ? '_' + element.mngDeptNm : ''
                            }`,
                        };
                        tempMngEmpList.push(mngObj);
                    });

                    // 변경
                    setMngEmpList(tempMngEmpList);
                    if (regModal.srType > '200') {
                        setIsMngEmp(
                            result.data.data.find(
                                (x) => x.mngEmpNo == userInfo.empNo,
                            ),
                        );
                    }
                }
            }
            mngEmpCheck();
        }
    }, [regModal.regState, regModal.srType, userInfo.empNo]);

    const handleKeyDown = useCallback(async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleRegModalChange = async (e) => {
        const newValue = {
            ...regModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        if (e.target.id === 'langGubun') {
            if (e.target.value == '01') {
                newValue.address = userInfo.addressKr;
            } else {
                newValue.address = userInfo.addressEn;
            }
        }

        setRegModal(newValue);
    };

    const regGridDelete = async (param) => {
        if (param && param.data) {
            const params = {
                param1: param.data,
                param2: regModal,
            };
            const result = await API.request.post(
                `api/anal/public-report/dtl/delete`,
                params,
            );
            if (result) {
                setRowData(result.data.data);
            } else {
                setRowData([]);
            }
        }
    };

    const validationCheck = async () => {
        const validationKeyList = {
            regTit: '제목',
            gubun: '성적서 구분',
            langGubun: '성적서 언어',
            comment: '분석내용 및 요구사항',
            address: '주소',
            srDtlList: '분석요청 리스트',
        };

        const keyObj = Object.keys(validationKeyList);
        const errorList = validationObj;
        const megList = [];
        keyObj.forEach((element) => {
            if (!regModal[element]) {
                megList.push(validationKeyList[element]);
                errorList[element] = true;
            } else {
                errorList[element] = false;
            }
        });

        if (
            !errorList['srDtlList'] &&
            regModal.srDtlList &&
            regModal.srDtlList.length == 0
        ) {
            errorList['srDtlList'] = true;
            megList.push(validationKeyList['srDtlList']);
        }

        if (megList.length > 0) {
            setValidationObl({
                ...errorList,
            });
            let validationMeg = `<div class="meg_main">`;
            megList.forEach((element) => {
                validationMeg += `
                        <div class="meg_contents error">
                            ${element}
                        </div>
                    `;
            });

            validationMeg.concat(`</div>`);
            await meg({
                title: `필수 항목을 입력하십시오.`,
                html: validationMeg,
            });
            return false;
        }
        return true;
    };

    const onSavePublicReportInfo = async (regState) => {
        if (data) {
            const regPublicReportGrid = data?.get('reg_environ_grid');
            if (
                regPublicReportGrid &&
                regPublicReportGrid.getModel()?.rowsToDisplay?.length > 0
            ) {
                regModal.srDtlList = [];
                regPublicReportGrid
                    .getModel()
                    .rowsToDisplay.forEach((element) => {
                        if (element.data) {
                            const tempObj: EnvironDtlModal = {
                                gridId: element.data.gridId,
                                sampleNm: element.data.sampleNm,
                                rawMatNm: element.data.rawMatNm,
                                productNm: element.data.productNm,
                                mfNm: element.data.mfNm,
                                lotNo: element.data.lotNo,
                                form: element.data.form,
                                color: element.data.color,
                                gubun: element.data.gubun,
                                matQty: element.data.matQty,
                                sort: element.data.sort,
                                regId: element.data.regId,
                            };
                            regModal.srDtlList.push(tempObj);
                        }
                    });
            }
        }

        const validationResult = await validationCheck();

        if (validationResult) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (
                regModal.srDtlList.find(
                    (x) =>
                        !x.sampleNm ||
                        !x.rawMatNm ||
                        !x.productNm ||
                        !x.mfNm ||
                        !x.lotNo ||
                        !x.form ||
                        !x.color ||
                        !x.gubun ||
                        !x.matQty,
                )
            ) {
                await meg({
                    title: `분석요청 필수값을 입력해 주십시오.`,
                    html: '',
                });

                return;
            }
            if (confirmResult) {
                regModal.regState = regState;
                const fileUploadGrid = data?.get('reg_sr_environ_upload_grid');
                if (fileUploadGrid) {
                    const fileSaveList = [];
                    fileUploadGrid
                        ?.getModel()
                        ?.rowsToDisplay.map((item) =>
                            fileSaveList.push(item.data),
                        );
                    if (fileSaveList.length > 0) {
                        const result = await saveFilesEditAll(fileSaveList);
                        if (result) {
                            regModal.fileGrpId = result;
                        }
                    }

                    switch (regModal.gubun) {
                        case '01':
                            regModal.srType = 'U';
                            break;
                        default:
                            regModal.srType = 'V';
                            break;
                    }

                    const result = await API.request.post(
                        `api/anal/public-report/save`,
                        regModal,
                    );

                    if (result.data.success) {
                        const saveModal = result.data.data;
                        saveModal.isRefresh = !regModal.isRefresh;
                        setRegModal({
                            ...saveModal,
                            regUserInfo: {
                                regEmpNo: saveModal.regEmpNo || '',
                                regEmpNm: saveModal.regEmpNm || '',
                                regDeptNm: saveModal.regDeptNm || '',
                                regRankNm: saveModal.regRankNm || '',
                                regMobileNo: saveModal.regMobileNo || '',
                                regEmail: saveModal.regEmail || '',
                                regOfficeNo: saveModal.regOfficeNo || '',
                            },
                        });
                        setRowData(result.data.data.srDtlList);

                        if (
                            result.data.data.regState > '100' &&
                            result.data.data.regId
                        ) {
                            setDisable(true);
                        }

                        if (saveModal.regState == '400') {
                            await meg({
                                title: `시료를 보내주십시오.`,
                                html: '',
                            });
                        }
                    } else {
                        regModal.regState = '100';
                        setRegModal(regModal);
                    }
                }
            }
        }
    };

    const onCopyPublicReportInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 복사하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            setDisable(false);
            setRegModal({
                ...regModal,
                regId: '',
                regState: '',
                fileGrpId: 0,
                srDtlList: [],
                // 변경
                regUserInfo: {
                    regDeptNm: userInfo.deptNm,
                    regEmail: userInfo.email,
                    regEmpNm: userInfo.userNm,
                    regEmpNo: userInfo.empNo,
                    regOfficeNo: userInfo.officeNo,
                    regMobileNo: userInfo.mobileNo,
                    regRankNm: userInfo.rankNm,
                },
            });
            setRowData([]);
        }
    };

    const onDeletePublicReportInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 삭제하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/anal/public-report/delete`,
                regModal,
            );

            if (result.data.success) {
                initModal();
            }
        }
    };

    const initModal = () => {
        setRegModal({
            regTit: '',
            regType: '',
            purpose: '',
            gubun: '',
            fileGrpId: 0,
            langGubun: '',
            regState: '100',
            srDtlList: null,
            srType: 'T',
        });

        setRowData([]);
        setDisable(false);
    };

    const onOpenReport = () => {
        setReportModalOpen(true);
    };

    const onReportModalClose = () => {
        setReportModalOpen(false);
    };

    const onOpenEtm = () => {
        setEtmModalOpen(true);
    };

    const onEtmModalClose = () => {
        setEtmModalOpen(false);
    };

    return (
        <>
            {isMngEmp && regModal.regState === '400' ? (
                // 변경
                <MngInfo
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        initSearch: initSearch,
                    }}
                />
            ) : null}
            {regModal.regState > '400' || regModal.regState == '140' ? (
                // 변경
                <RegProc
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        isProcEquipReg: true,
                        initSearch: initSearch,
                    }}
                />
            ) : null}

            <div className="reg_info">
                <SModal
                    tit={'환경유해물질 Report'}
                    isOpen={reportModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '1200',
                        height: '820',
                        isResize: false,
                    }}
                    path={'common/popup/report/environ/EnvironReport'}
                    onClose={onReportModalClose}
                />
                <SModal
                    tit={'분석평가 등록'}
                    isOpen={etmModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '600',
                        height: 'auto',
                        isResize: false,
                    }}
                    path={'anal/reg/etm/Etm'}
                    onClose={onEtmModalClose}
                />
                <>
                    <div className="reg_main_tit">
                        <span className="tit">
                            {regModal.regId ? (
                                <>
                                    <div className="tit_01">
                                        {regModal.regId}
                                    </div>
                                    <div
                                        className="tit_02"
                                        // 변경
                                        style={{
                                            background:
                                                regModal.regState === '110'
                                                    ? '#8382db'
                                                    : regModal.regState ===
                                                      '200'
                                                    ? '#e76e6e'
                                                    : regModal.regState ===
                                                      '400'
                                                    ? '#60b180'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#5ab2e5'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#0b9562'
                                                    : '#db8181',
                                        }}>
                                        {
                                            regStateList.filter(
                                                (x) =>
                                                    x.cd == regModal.regState,
                                            )[0].cdNm
                                        }
                                    </div>
                                </>
                            ) : (
                                '환경유해물질 분석의뢰서'
                            )}
                        </span>
                        <div className="search_btns">
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenReport}
                                    startIcon={<PrintIcon />}>
                                    의뢰내용 출력
                                </Button>
                            )}
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onCopyPublicReportInfo}
                                    startIcon={<ContentCopyIcon />}>
                                    복사
                                </Button>
                            )}
                            {regModal.regId &&
                                (regModal.regState === '110' ||
                                    regModal.regState === '130') && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        onClick={fixConfirm}
                                        startIcon={
                                            <Badge
                                                badgeContent={1}
                                                color="primary">
                                                <ConstructionIcon className="btn_icon" />
                                            </Badge>
                                        }>
                                        {regModal.regState === '130'
                                            ? '부결내용 확인'
                                            : '보완요청 확인'}
                                    </Button>
                                )}
                            {regModal.regId && regModal.regState <= '400' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={onDeletePublicReportInfo}
                                    startIcon={<DeleteForeverIcon />}>
                                    삭제
                                </Button>
                            )}
                            {regModal.regState <= '100' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={() => {
                                        onSavePublicReportInfo('100');
                                    }}
                                    startIcon={<PendingActionsIcon />}>
                                    임시저장
                                </Button>
                            )}
                            <Button
                                className="btn_group"
                                variant="outlined"
                                disabled={
                                    regModal.regEmpNo != '' &&
                                    userInfo.empNo !=
                                        regModal?.regUserInfo?.regEmpNo
                                }
                                onClick={() => {
                                    let regState = '400';
                                    if (regModal.srMngModel) {
                                        regState = '500';
                                    }

                                    onSavePublicReportInfo(regState);
                                }}
                                startIcon={<ForwardToInboxIcon />}>
                                의뢰하기
                            </Button>
                            {regModal.regId && regModal.regState == '900' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenEtm}
                                    startIcon={<RecommendIcon />}>
                                    평가하기
                                </Button>
                            )}
                        </div>
                    </div>
                    <div className="reg_main_contents">
                        <div className="reg_contents">
                            <TextField
                                id="regTit"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                label={'제목'}
                                sx={{
                                    minWidth: 250,
                                    width: '50%',
                                }}
                                error={!regModal.regTit && validationObj.regTit}
                                value={regModal.regTit}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'REG_TYPE'}
                                label={'의뢰유형'}
                                id={'regType'}
                                value={regModal.regType}
                                handleChange={handleRegModalChange}
                                disabled={disable}
                            />
                            <FormControl
                                id={'radio_main'}
                                error={validationObj.gubun && !regModal.gubun}>
                                <FormLabel id="radio_group">
                                    {'성적서 구분'}
                                </FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="radio_group"
                                    name="row-radio-buttons-group">
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="01"
                                                checked={
                                                    regModal.gubun === '01'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="UL성적서"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="02"
                                                checked={
                                                    regModal.gubun === '02'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="TUV성적서(공인)"
                                    />
                                    {/* <FormControlLabel
                                        control={
                                            <Radio
                                                value="03"
                                                checked={
                                                    regModal.gubun === '03'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="내부(관리용)"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="04"
                                                checked={
                                                    regModal.gubun === '04'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="내부(제출용)"
                                    /> */}
                                </RadioGroup>
                            </FormControl>
                            <FormControl
                                id={'radio_main'}
                                error={validationObj.gubun && !regModal.gubun}>
                                <FormLabel id="radio_group">
                                    {'성적서 언어'}
                                </FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="radio_group"
                                    name="row-radio-buttons-group">
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="01"
                                                checked={
                                                    regModal.langGubun === '01'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'langGubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="국문"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="02"
                                                checked={
                                                    regModal.langGubun === '02'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'langGubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="영문"
                                    />
                                </RadioGroup>
                            </FormControl>
                        </div>
                        <div className="reg_contents">
                            <TextField
                                id="purpose"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                sx={{
                                    minWidth: 300,
                                    width: '60%',
                                }}
                                label={'의뢰목적'}
                                error={
                                    validationObj.purpose && !regModal.purpose
                                }
                                value={regModal.purpose}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <TextField
                                id="address"
                                onChange={handleRegModalChange}
                                label={'주소'}
                                sx={{
                                    minWidth: 350,
                                    width: '40%',
                                }}
                                value={regModal.address ? regModal.address : ''}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                        </div>
                        <div
                            className="reg_contents"
                            style={{flexDirection: 'column'}}>
                            <SGrid
                                grid={'reg_environ_grid'}
                                rowData={rowData}
                                isForceDelete={true}
                                title={'분석요청'}
                                newRowDefault={{
                                    sort: 'auto',
                                }}
                                isAddBtnOpen={!disable}
                                onClickDelete={disable ? null : regGridDelete}
                                columnDefs={columnDefs}
                            />
                            <div className="pl_desc">
                                <div className="pl_desc_sub_1">
                                    {'시료명(성적서 표기명)'}
                                </div>
                                <div className="pl_desc_sub_1">
                                    {'재질(PVC, PE, AI합금 등) '}
                                </div>
                                <div className="pl_desc_sub_1">
                                    {'형태(PELLET, SHEET, FILM, PART 등)'}
                                </div>
                                <div
                                    className="pl_desc_sub_2"
                                    style={{color: '#4040e1'}}>
                                    {
                                        '* 필요 시료량 : 6대 물질 25g 이상, PBB/PBDE 15g이상, 4대중금속 및 기타 10g 이상 임.'
                                    }
                                </div>
                                <div className="pl_desc_sub_2">
                                    {
                                        '* 의뢰서 작성시 시험방법을 " 분석 내용 및 요구사항"란에 명시해 주시기 바랍니다.'
                                    }
                                </div>
                                <div className="pl_desc_sub_2">
                                    {`* 시험방법은 "분석수수료" 메뉴를 클릭하여 '시험방법'을 참조하시기 바랍니다.`}
                                </div>
                                <div
                                    className="pl_desc_sub_2"
                                    style={{
                                        marginLeft: '40px',
                                        color: '#bd1717',
                                    }}>
                                    {
                                        '- 시험방법에 대하여, 문의필요시 담당자와 논의하시기 바랍니다.'
                                    }
                                </div>
                                <div className="pl_desc_sub_2">
                                    {
                                        '* 고객이 시험방법을 제시하지 않을시 아래 방법을 사용함.'
                                    }
                                </div>
                                <div
                                    className="pl_desc_sub_2"
                                    style={{
                                        marginLeft: '40px',
                                        color: '#bd1717',
                                    }}>
                                    {'- 4대중금속: EPA 3052 및 EPA 3060A'}
                                </div>
                                <div
                                    className="pl_desc_sub_2"
                                    style={{
                                        marginLeft: '40px',
                                        color: '#bd1717',
                                    }}>
                                    {'- PBBs/PBDEs : EPA 3540C'}
                                </div>
                                <div className="pl_desc_sub_2">
                                    {
                                        '* 영문 성적서 발행 시 시료 정보는 필히 영문으로 작성'
                                    }
                                </div>
                                <div className="pl_desc_sub_2">
                                    {'* 각 공장에서 측정한 XRF DATA 첨부'}
                                </div>
                            </div>
                        </div>
                        <div
                            className="reg_contents"
                            style={{paddingTop: '10px'}}>
                            <TextField
                                id="comment"
                                onChange={handleRegModalChange}
                                sx={{
                                    width: '100%',
                                    paddingRight: '0px !important',
                                }}
                                multiline
                                rows={3}
                                label={'내용/요구사항'}
                                size="small"
                                value={regModal.comment}
                                error={
                                    validationObj.comment && !regModal.comment
                                }
                                disabled={disable}
                            />
                        </div>
                        <div className="reg_contents">
                            <SFile
                                gridId={'reg_sr_environ_upload_grid'}
                                fileGrpId={regModal.fileGrpId}
                                isRefresh={regModal.isRefresh}
                                tit={'첨부파일'}
                                useDownload={true}
                                useFileAttach={!disable}
                                useFileDelete={!disable}
                            />
                        </div>
                    </div>
                </>
            </div>
            <UserInfo regUserInfo={regModal.regUserInfo} />
        </>
    );
}

const Environ = (param) => {
    const regInfo = useMemo(() => {
        if (param?.param) {
            return {
                regInfo: {
                    regId: param?.param?.regId || '',
                },
            };
        }
        return;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param?.param]);

    return (
        <div className="contents_wrap_parents_h">
            <div className="environ_main">
                <Suspense fallback={<Spinner />}>
                    <ContentsWrap>
                        <RegInfo regInfo={regInfo?.regInfo} />
                    </ContentsWrap>
                </Suspense>
            </div>
        </div>
    );
};
export default Environ;
