import React, {useMemo, useState} from 'react';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import Storage from 'override/api/Storage';

import './style.scss';
import API from 'override/api/API';
import {confirm, meg} from 'override/alert/Alert';
import {Button} from '@mui/material';
interface EtmModal {
    regId: string;
    mngEmpNo: string;
    speedEtm: string;
    dataEtm: string;
    resultEtm: string;
    atdEtm: string;
    bestThings: string;
    totalEtm: string;
}

function Etm(params) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const etmGroup = useMemo(() => {
        return commonCodeGroup['ETM_GROUP'];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [commonCodeGroup]);
    const [etmModal, setEtmModal] = useState<EtmModal>({
        regId: params.param.data.regId,
        mngEmpNo: params.param.data.srMngModel.mngEmpNo,
        speedEtm: '',
        dataEtm: '',
        resultEtm: '',
        atdEtm: '',
        bestThings: '',
        totalEtm: '',
    });

    const handleEtmModalChange = (e) => {
        const newValue = {
            ...etmModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        setEtmModal(newValue);
    };

    const saveEtm = async () => {
        const keyObj = Object.keys(etmModal);
        let validation = true;
        keyObj.forEach(async (element) => {
            if (!etmModal[element]) {
                validation = false;
                return;
            }
        });

        if (validation) {
            const confirmResult = await confirm({
                title: '평가 하시겠습니까?',
                html: '',
            });

            if (confirmResult) {
                const result = await API.request.post(
                    `api/anal/comm/etm/save`,
                    etmModal,
                );

                if (result.data.success) {
                    params.onClose(result);
                }
            }
        } else {
            await meg({
                title: `평가를 모두 진행 해 주십시오..`,
                html: '',
            });
            return;
        }
    };

    return (
        <div className="contents_wrap_parents_h">
            <div className="anal_reg_etm_main">
                <ContentsWrap>
                    <div className="search_btns">
                        <Button
                            className="btn_group"
                            variant="outlined"
                            onClick={saveEtm}
                            startIcon={<SaveAltIcon />}>
                            평가하기
                        </Button>
                    </div>
                    {etmGroup.map((x1, index1) => {
                        const codeList = commonCodeGroup[x1.desc1];
                        return (
                            <div
                                className="etm_radio"
                                key={`main_${x1.cd}_${index1}`}>
                                <FormControl className={'radio_main'}>
                                    <FormLabel className="radio_group">
                                        <span
                                            dangerouslySetInnerHTML={{
                                                __html: x1.cdNm,
                                            }}
                                        />
                                    </FormLabel>
                                    <RadioGroup
                                        row
                                        aria-labelledby="radio_group"
                                        name="row-radio-buttons-group">
                                        {codeList.map((x, index1) => {
                                            return (
                                                <FormControlLabel
                                                    key={`${x.cdType}_${index1}_1`}
                                                    control={
                                                        <Radio
                                                            value={x.cd}
                                                            checked={
                                                                etmModal[
                                                                    x1.cd
                                                                ] === x.cd
                                                            }
                                                            onChange={
                                                                handleEtmModalChange
                                                            }
                                                            id={x1.cd}
                                                        />
                                                    }
                                                    label={x.cdNm}
                                                />
                                            );
                                        })}
                                    </RadioGroup>
                                </FormControl>
                            </div>
                        );
                    })}
                    <div className="etm_desc">
                        <div id="tit_01">성실히 답변해 주셔서 감사합니다.</div>
                        <div id="tit_02">
                            고객께서 보내주신 소중한 의견을 바탕으로 변화하는
                            분석기술그룹이 되겠습니다.
                        </div>
                    </div>
                </ContentsWrap>
            </div>
        </div>
    );
}
export default Etm;
