import React, {Suspense, useEffect, useMemo, useState} from 'react';
import API from 'override/api/API';
import TextField from '@mui/material/TextField';
import Storage from '@override/api/Storage';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import SGrid from 'components/atoms/grid/SGrid';
import PrintIcon from '@mui/icons-material/Print';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import SModal from 'override/modal/SModal';
import ForwardToInboxIcon from '@mui/icons-material/ForwardToInbox';
import PendingActionsIcon from '@mui/icons-material/PendingActions';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import ConstructionIcon from '@mui/icons-material/Construction';
import {confirm, meg} from 'override/alert/Alert';
import MngInfo from '../mngInfo/MngInfo';
import Badge from '@mui/material/Badge';
import Spinner from 'components/atoms/spinner/Spinner';
import RegProc from '../regProc/RegProc';
import RecommendIcon from '@mui/icons-material/Recommend';
import './style.scss';
import {Button} from '@mui/material';

interface RegModal {
    regTit: string;
    srType: string;
    fileGrpId?: number;
    isRefresh?: boolean;
    purpose: string;
    regState: string;
    regEmpNo?: string;
    comment?: string;
    keyword?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srDtlList: any;
    regId?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    fixRecordModel?: any;
    regUserInfo?: RegUserInfo;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srMngModel?: any;
}

interface GeneralDtlModal {
    regId: string;
    gridId?: number;
    sampleNm: string;
    analItem: string;
    exprtResult: string;
    sort: number;
}

// 변경
interface RegUserInfo {
    regEmpNm: string;
    regDeptNm: string;
    regRankNm: string;
    regOfficeNo: string;
    regMobileNo: string;
    regEmail: string;
    regEmpNo: string;
}

function UserInfo(param) {
    const {data} = Storage.getUserInfo();
    const [userInfo, setUserInfo] = useState<RegUserInfo>({
        regEmpNo: data.empNo,
        regEmpNm: data.userNm,
        regDeptNm: data.deptNm,
        regRankNm: data.rankNm,
        regOfficeNo: data.officeNo,
        regMobileNo: data.mobileNo,
        regEmail: data.email,
    });

    useEffect(() => {
        setUserInfo(param.regUserInfo);
    }, [param?.regUserInfo]);

    return (
        <div className="reg_user_info">
            <div className="reg_main_tit">
                <span className="tit">의뢰자 정보</span>
            </div>
            <div className="reg_main_contents">
                <div className="user_info_conts">
                    <div className="tit">성명</div>
                    <div className="user_info">{userInfo?.regEmpNm}</div>
                    <div className="tit">직위</div>
                    <div className="user_info">{userInfo?.regRankNm}</div>
                    <div className="tit">부서</div>
                    <div className="user_info">{userInfo?.regDeptNm}</div>
                </div>
                <div className="user_info_conts">
                    <div className="tit">E-MAIL</div>
                    <div className="user_info">{userInfo?.regEmail}</div>
                    <div className="tit">전화번호</div>
                    <div className="user_info">{userInfo?.regOfficeNo}</div>
                    <div className="tit">HP</div>
                    <div className="user_info">{userInfo?.regMobileNo}</div>
                </div>
            </div>
        </div>
    );
}

function RegInfo(param) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const regStateList = useMemo(() => {
        return commonCodeGroup['REG_STATE'];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [commonCodeGroup]);
    const {data: userInfo} = Storage.getUserInfo();
    const {data} = Storage.getGridList();
    const [rowData, setRowData] = useState([]);
    const [reportModalOpen, setReportModalOpen] = useState(false);
    const [etmModalOpen, setEtmModalOpen] = useState(false);
    const [disable, setDisable] = useState(false);
    const [mngEmpList, setMngEmpList] = useState([]);
    const [isMngEmp, setIsMngEmp] = useState(false);

    const [validationObj, setValidationObl] = useState({
        regTit: false,
        purpose: false,
        srDtlList: false,
    });

    const [regModal, setRegModal] = useState<RegModal>({
        regTit: '',
        purpose: '',
        fileGrpId: 0,
        comment: '',
        srType: 'G',
        regEmpNo: '',
        regState: '100',
        keyword: '',
        srDtlList: null,
        // 변경
        regUserInfo: {
            regDeptNm: userInfo.deptNm,
            regEmail: userInfo.email,
            regEmpNm: userInfo.userNm,
            regEmpNo: userInfo.empNo,
            regOfficeNo: userInfo.officeNo,
            regMobileNo: userInfo.mobileNo,
            regRankNm: userInfo.rankNm,
        },
    });

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 32,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '번호',
                field: 'sort',
                width: 70,
                type: 'number',
                editable: false,
            },
            {
                headerName: '시료명',
                field: 'sampleNm',
                width: 150,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '알고자하는 사항(분석항목)',
                field: 'analItem',
                width: 300,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '시료설명 및 예상결과',
                field: 'exprtResult',
                width: 300,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                field: 'gridId',
                width: 300,
                editable: false,
                essential: false,
                hide: true,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        if (regModal.regState == '400' && regModal.srType) {
            async function mngEmpCheck() {
                const param = {
                    srType: regModal.srType,
                };
                const result = await API.request.post(
                    `api/anal/mng-mt/get`,
                    param,
                );
                if (result.data.success) {
                    const tempMngEmpList = [];
                    result.data.data.forEach((element) => {
                        const mngObj = {
                            cd: element.mngEmpNo,
                            cdNm: `${element.mngEmpNm || ''}${
                                element.mngDeptNm ? '_' + element.mngDeptNm : ''
                            }`,
                        };
                        tempMngEmpList.push(mngObj);
                    });

                    // 변경
                    setMngEmpList(tempMngEmpList);
                    if (regModal.srType > '200') {
                        setIsMngEmp(
                            result.data.data.find(
                                (x) => x.mngEmpNo == userInfo.empNo,
                            ),
                        );
                    }
                }
            }
            mngEmpCheck();
        }
    }, [regModal.regState, regModal.srType, userInfo.empNo]);

    useEffect(() => {
        if (param?.regInfo?.regId) {
            initSearch(null);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (
            regModal.regState === '110' &&
            regModal.regEmpNo == userInfo.empNo
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        if (
            regModal.regState === '130' &&
            regModal.regEmpNo == userInfo.empNo
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regEmpNo, regModal.regState, userInfo.empNo]);

    const fixConfirm = async () => {
        if (regModal.regState === '110' || regModal.regState === '130') {
            const tit =
                regModal.regState === '110' ? '의뢰 보완요청' : '부결 내용';

            let confirmResult = undefined;
            if (regModal.regState === '110') {
                confirmResult = await confirm({
                    title: tit,
                    html: `<div class="fix_confirm">
                            <div class="fix_emp_info">
                                <div class="fix_dept_nm">
                                    ${regModal.fixRecordModel.mngDeptNm}
                                </div>
                                <div class="fix_emp_nm">
                                    ${
                                        regModal.fixRecordModel.mngEmpNm +
                                        '  ' +
                                        regModal.fixRecordModel.mngPosNm
                                    }
                                </div>
                            </div>
                            <div class="fix_emp_desc">
                                ${regModal.fixRecordModel.mngDesc}
                            </div>
                        </div>`,
                });
            }

            if (confirmResult) {
                if (regModal.regEmpNo != userInfo.empNo) {
                    meg({
                        title: '의뢰자가 아닙니다.',
                        html: '',
                    });
                    return;
                }

                setDisable(false);
                setRegModal({
                    ...regModal,
                    regState: '100',
                });
            }
        }
    };

    const initSearch = async (modal) => {
        const result = await API.request.post(
            `api/anal/util/get`,
            param.regInfo ? param.regInfo : modal,
        );

        if (result.data.success) {
            const setModalObj = result.data.data;
            setModalObj.isRefresh = !regModal.isRefresh;

            setRegModal({
                ...setModalObj,
                // 변경
                regUserInfo: {
                    regEmpNo: setModalObj.regEmpNo || '',
                    regEmpNm: setModalObj.regEmpNm || '',
                    regDeptNm: setModalObj.regDeptNm || '',
                    regRankNm: setModalObj.regRankNm || '',
                    regMobileNo: setModalObj.regMobileNo || '',
                    regEmail: setModalObj.regEmail || '',
                    regOfficeNo: setModalObj.regOfficeNo || '',
                },
            });
            setRowData(result.data.data.srDtlList);

            if (
                (result.data.data.regState > '100' && result.data.data.regId) ||
                result.data.data.regEmpNo != userInfo.empNo
            ) {
                setDisable(true);
            }
        }
    };

    const handleRegModalChange = async (e) => {
        const newValue = {
            ...regModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        setRegModal(newValue);
    };

    const regGridDelete = async (param) => {
        if (param && param.data) {
            const params = {
                param1: param.data,
                param2: regModal,
            };
            const result = await API.request.post(
                `api/anal/util/dtl/delete`,
                params,
            );
            if (result) {
                setRowData(result.data.data);
            } else {
                setRowData([]);
            }
        }
    };

    const validationCheck = async () => {
        const validationKeyList = {
            regTit: '제목',
            srDtlList: '분석요청 리스트',
        };

        const keyObj = Object.keys(validationKeyList);
        const errorList = validationObj;
        const megList = [];
        keyObj.forEach((element) => {
            if (!regModal[element]) {
                megList.push(validationKeyList[element]);
                errorList[element] = true;
            } else {
                errorList[element] = false;
            }
        });

        if (
            !errorList['srDtlList'] &&
            regModal.srDtlList &&
            regModal.srDtlList.length == 0
        ) {
            errorList['srDtlList'] = true;
            megList.push(validationKeyList['srDtlList']);
        }

        if (megList.length > 0) {
            setValidationObl({
                ...errorList,
            });
            let validationMeg = `<div class="meg_main">`;
            megList.forEach((element) => {
                validationMeg += `
                        <div class="meg_contents error">
                            ${element}
                        </div>
                    `;
            });

            validationMeg.concat(`</div>`);
            await meg({
                title: `필수 항목을 입력하십시오.`,
                html: validationMeg,
            });
            return false;
        }
        return true;
    };

    const onSaveGeneralInfo = async (regState) => {
        if (data) {
            const regGeneralGrid = data?.get('reg_general_grid');
            if (
                regGeneralGrid &&
                regGeneralGrid.getModel()?.rowsToDisplay?.length > 0
            ) {
                regModal.srDtlList = [];
                regGeneralGrid.getModel().rowsToDisplay.forEach((element) => {
                    if (element.data) {
                        const tempObj: GeneralDtlModal = {
                            analItem: element.data.analItem,
                            exprtResult: element.data.exprtResult,
                            sampleNm: element.data.sampleNm,
                            gridId: element.data.gridId,
                            sort: element.data.sort,
                            regId: element.data.regId,
                        };
                        regModal.srDtlList.push(tempObj);
                    }
                });
            }
        }

        const validationResult = await validationCheck();

        if (validationResult) {
            if (
                regModal.srDtlList.find(
                    (x) => !x.sampleNm || !x.analItem || !x.exprtResult,
                )
            ) {
                await meg({
                    title: `분석요청 필수값을 입력해 주십시오.`,
                    html: '',
                });

                return;
            }
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                regModal.regState = regState;
                const fileUploadGrid = data?.get('reg_sr_general_upload_grid');
                if (fileUploadGrid) {
                    const fileSaveList = [];
                    fileUploadGrid
                        ?.getModel()
                        ?.rowsToDisplay.map((item) =>
                            fileSaveList.push(item.data),
                        );
                    if (fileSaveList.length > 0) {
                        const result = await saveFilesEditAll(fileSaveList);
                        if (result) {
                            regModal.fileGrpId = result;
                        }
                    }

                    regModal.srType = 'G';

                    const result = await API.request.post(
                        `api/anal/util/save`,
                        regModal,
                    );

                    if (result.data.success) {
                        const saveModal = result.data.data;
                        saveModal.isRefresh = !regModal.isRefresh;
                        // 변경
                        setRegModal({
                            ...saveModal,
                            regUserInfo: {
                                regEmpNo: saveModal.regEmpNo || '',
                                regEmpNm: saveModal.regEmpNm || '',
                                regDeptNm: saveModal.regDeptNm || '',
                                regRankNm: saveModal.regRankNm || '',
                                regMobileNo: saveModal.regMobileNo || '',
                                regEmail: saveModal.regEmail || '',
                                regOfficeNo: saveModal.regOfficeNo || '',
                            },
                        });
                        setRowData(result.data.data.srDtlList);

                        if (
                            result.data.data.regState > '100' &&
                            result.data.data.regId
                        ) {
                            setDisable(true);
                        }

                        if (saveModal.regState == '400') {
                            await meg({
                                title: `시료를 보내주십시오.`,
                                html: '',
                            });
                        }
                    }
                }
            }
        }
    };

    const onCopyGeneralInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 복사하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            setDisable(false);
            setRegModal({
                ...regModal,
                regId: '',
                regState: '',
                fileGrpId: 0,
                srDtlList: [],
                srMngModel: null,
                // 변경
                regUserInfo: {
                    regDeptNm: userInfo.deptNm,
                    regEmail: userInfo.email,
                    regEmpNm: userInfo.userNm,
                    regEmpNo: userInfo.empNo,
                    regOfficeNo: userInfo.officeNo,
                    regMobileNo: userInfo.mobileNo,
                    regRankNm: userInfo.rankNm,
                },
            });

            // 변경
            setIsMngEmp(false);
            setRowData([]);
        }
    };

    const onDeleteGeneralInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 삭제하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/anal/util/delete`,
                regModal,
            );

            if (result.data.success) {
                initModal();
            }
        }
    };

    const initModal = () => {
        setRegModal({
            regTit: '',
            purpose: '',
            fileGrpId: 0,
            comment: '',
            srType: 'G',
            regEmpNo: '',
            regState: '100',
            srDtlList: null,
            keyword: '',
            // 변경
            regUserInfo: {
                regDeptNm: userInfo.deptNm,
                regEmail: userInfo.email,
                regEmpNm: userInfo.userNm,
                regEmpNo: userInfo.empNo,
                regOfficeNo: userInfo.officeNo,
                regMobileNo: userInfo.mobileNo,
                regRankNm: userInfo.rankNm,
            },
        });
        setRowData([]);
        setDisable(false);
    };

    const onOpenReport = () => {
        setReportModalOpen(true);
    };

    const onReportModalClose = () => {
        setReportModalOpen(false);
    };

    const onOpenEtm = () => {
        setEtmModalOpen(true);
    };

    const onEtmModalClose = async (param) => {
        if (param.data && param.data.success) {
            await initSearch(null);
        }
        setEtmModalOpen(false);
    };

    return (
        <>
            {isMngEmp && regModal.regState === '400' ? (
                // 변경
                <MngInfo
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        initSearch: initSearch,
                    }}
                />
            ) : null}

            {regModal.regState > '400' || regModal.regState == '140' ? (
                // 변경
                <RegProc
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        initSearch: initSearch,
                    }}
                />
            ) : null}
            <div className="reg_info">
                <SModal
                    tit={'일반분석 Report'}
                    isOpen={reportModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '1200',
                        height: '820',
                        isResize: false,
                    }}
                    path={'common/popup/report/general/GeneralReport'}
                    onClose={onReportModalClose}
                />
                <SModal
                    tit={'분석평가 등록'}
                    isOpen={etmModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '600',
                        height: 'auto',
                        isResize: false,
                    }}
                    path={'anal/reg/etm/Etm'}
                    onClose={onEtmModalClose}
                />
                <>
                    <div className="reg_main_tit">
                        <span className="tit">
                            {regModal.regId ? (
                                <>
                                    <div className="tit_01">
                                        {regModal.regId}
                                    </div>
                                    <div
                                        className="tit_02"
                                        // 변경
                                        style={{
                                            background:
                                                regModal.regState === '110'
                                                    ? '#8382db'
                                                    : regModal.regState ===
                                                      '200'
                                                    ? '#e76e6e'
                                                    : regModal.regState ===
                                                      '400'
                                                    ? '#60b180'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#5ab2e5'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#0b9562'
                                                    : '#db8181',
                                        }}>
                                        {
                                            regStateList.filter(
                                                (x) =>
                                                    x.cd == regModal.regState,
                                            )[0].cdNm
                                        }
                                    </div>
                                </>
                            ) : (
                                '일반분석 의뢰서'
                            )}
                        </span>
                        <div className="search_btns">
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenReport}
                                    startIcon={<PrintIcon />}>
                                    의뢰내용 출력
                                </Button>
                            )}
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onCopyGeneralInfo}
                                    startIcon={<ContentCopyIcon />}>
                                    복사
                                </Button>
                            )}
                            {regModal.regId &&
                                (regModal.regState === '110' ||
                                    regModal.regState === '130') && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        onClick={fixConfirm}
                                        startIcon={
                                            <Badge
                                                badgeContent={1}
                                                color="primary">
                                                <ConstructionIcon className="btn_icon" />
                                            </Badge>
                                        }>
                                        {regModal.regState === '130'
                                            ? '부결내용 확인'
                                            : '보완요청 확인'}
                                    </Button>
                                )}
                            {regModal.regId && regModal.regState <= '400' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={onDeleteGeneralInfo}
                                    startIcon={<DeleteForeverIcon />}>
                                    삭제
                                </Button>
                            )}
                            {regModal.regState <= '100' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={() => {
                                        onSaveGeneralInfo('100');
                                    }}
                                    startIcon={<PendingActionsIcon />}>
                                    임시저장
                                </Button>
                            )}
                            {(!regModal.regId ||
                                regModal.regState === '100') && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={() => {
                                        let regState = '400';
                                        if (regModal.srMngModel) {
                                            regState = '500';
                                        }

                                        onSaveGeneralInfo(regState);
                                    }}
                                    startIcon={<ForwardToInboxIcon />}>
                                    의뢰하기
                                </Button>
                            )}
                            {regModal.regId && regModal.regState == '900' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenEtm}
                                    startIcon={<RecommendIcon />}>
                                    평가하기
                                </Button>
                            )}
                        </div>
                    </div>
                    <div className="reg_main_contents">
                        <div className="reg_contents">
                            <TextField
                                id="regTit"
                                onChange={handleRegModalChange}
                                label={'제목'}
                                sx={{
                                    minWidth: 300,
                                    width: '60%',
                                }}
                                error={!regModal.regTit && validationObj.regTit}
                                value={regModal.regTit}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <TextField
                                id="keyword"
                                onChange={handleRegModalChange}
                                label={'키워드'}
                                sx={{
                                    minWidth: 300,
                                    width: '40%',
                                }}
                                value={regModal.keyword}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                        </div>
                        <div className="reg_contents">
                            <TextField
                                id="purpose"
                                onChange={handleRegModalChange}
                                sx={{
                                    minWidth: 300,
                                    width: '100%',
                                }}
                                label={'의뢰목적'}
                                error={
                                    validationObj.purpose && !regModal.purpose
                                }
                                value={regModal.purpose}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                        </div>
                        <div className="reg_contents">
                            <div className="text_area">
                                <TextField
                                    id="comment"
                                    onChange={handleRegModalChange}
                                    sx={{
                                        width: '100%',
                                        height: 80,
                                        minHeight: 80,
                                    }}
                                    multiline
                                    rows={3}
                                    label={'의뢰자 의견'}
                                    size="small"
                                    value={regModal.comment}
                                    disabled={disable}
                                />
                            </div>
                        </div>
                        <div className="reg_contents">
                            <SGrid
                                grid={'reg_general_grid'}
                                rowData={rowData}
                                isForceDelete={true}
                                title={'분석요청'}
                                newRowDefault={{
                                    sort: 'auto',
                                }}
                                isAddBtnOpen={!disable}
                                onClickDelete={disable ? null : regGridDelete}
                                columnDefs={columnDefs}
                            />
                        </div>
                        <div className="reg_contents">
                            <SFile
                                gridId={'reg_sr_general_upload_grid'}
                                fileGrpId={regModal.fileGrpId}
                                isRefresh={regModal.isRefresh}
                                tit={'첨부파일'}
                                useDownload={true}
                                useFileAttach={!disable}
                                useFileDelete={!disable}
                            />
                        </div>
                    </div>
                </>
            </div>
            <UserInfo regUserInfo={regModal.regUserInfo} />
        </>
    );
}

const General = (param) => {
    // 변경
    const regInfo = useMemo(() => {
        if (param?.param) {
            return {
                regInfo: {
                    regId: param?.param?.regId || '',
                },
            };
        }
        return;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param?.param]);

    return (
        <div className="contents_wrap_parents_h">
            <div className="general_req_main">
                <Suspense fallback={<Spinner />}>
                    <ContentsWrap>
                        <RegInfo regInfo={regInfo?.regInfo} />
                    </ContentsWrap>
                </Suspense>
            </div>
        </div>
    );
};
export default General;
