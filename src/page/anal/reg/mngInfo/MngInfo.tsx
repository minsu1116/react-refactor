/* eslint-disable react/jsx-no-undef */
import React, {useMemo, useState} from 'react';
import API from 'override/api/API';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';

import './style.scss';
import SSelectbox from 'components/atoms/select/SSelectbox';
import {confirm, meg} from 'override/alert/Alert';
import SModal from 'override/modal/SModal';
import moment from 'moment';
import DatePicker from 'components/atoms/datePicker/DatePicker';
import {Button} from '@mui/material';

const MngInfo = (param) => {
    const [mngEmpInfo, setMngEmpInfo] = useState({
        mngEmpNo: '',
        mngEmpNm: '',
        appoinDtm: moment().format('YYYYMMDD'),
        regState: param.param.regModal.regState,
    });

    const [modalOpen, setModalOpen] = useState(false);

    const modalObj = useMemo(() => {
        return {
            tit: '보완요청 등록',
            path: 'anal/reg/mngInfo/fixPopup/FixPopup',
            param: param.param.regModal,
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const sizeObj = useMemo(() => {
        return {
            width: '700',
            height: '210',
            isResize: false,
        };
    }, []);

    const handleMngModalChange = async (e) => {
        const newValue = {
            ...mngEmpInfo, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        setMngEmpInfo(newValue);
    };

    const onSaveMngInfo = async (regState) => {
        if (!mngEmpInfo.mngEmpNo || !mngEmpInfo.appoinDtm) {
            await meg({
                title: `담당자/ 고객과의 약속 시간을 입력 해 주십시오.`,
                html: '',
            });

            return;
        }

        const regModal = param.param.regModal;
        regModal.appoinDtm = mngEmpInfo.appoinDtm;
        regModal.regState = regState;
        regModal.srMngModel = {
            regId: regModal.regId,
            mngEmpNo: mngEmpInfo.mngEmpNo,
        };

        const confirmResult = await confirm({
            title: '저장하시겠습니까?',
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/anal/comm/sr/state/update`,
                regModal,
            );
            if (result.data.success) {
                param.param.initSearch(param.param.regModal);
            }
        }
    };

    const onFixPopupOpen = async () => {
        setModalOpen(true);
    };

    const onFixPopupClose = () => {
        setModalOpen(false);
        param.param.initSearch(param.param.regModal);
    };

    return (
        <>
            <SModal
                tit={'보완요청 등록'}
                param={modalObj}
                isOpen={modalOpen}
                sizeObj={sizeObj}
                path={modalObj.path}
                onClose={onFixPopupClose}
            />
            <div className="reg_mng_main">
                <div className="reg_info">
                    <div className="reg_main_tit">
                        <span className="tit">담당자 현황</span>
                    </div>
                    <div className="search_btns mng_btn_group">
                        <Button
                            className="btn_group"
                            variant="outlined"
                            onClick={onFixPopupOpen}
                            startIcon={
                                <AppRegistrationIcon className="btn_icon" />
                            }>
                            보완요청
                        </Button>
                        <Button
                            className="btn_group"
                            variant="outlined"
                            onClick={() => {
                                onSaveMngInfo('500');
                            }}
                            startIcon={<GroupAddIcon className="btn_icon" />}>
                            담당자 변경
                        </Button>
                    </div>
                    <div className="reg_main_contents">
                        <div className="reg_contents">
                            <SSelectbox
                                id={'mngEmpNo'}
                                isCodeGroupMode={false}
                                label={'담당자 지정'}
                                selectList={param.param.mngEmpList}
                                handleChange={handleMngModalChange}
                            />
                            <DatePicker
                                label={'고객과의 약속'}
                                disabled={
                                    mngEmpInfo.regState == '400' ? false : true
                                }
                                width={160}
                                handleChange={handleMngModalChange}
                                id={'appoinDtm'}
                                inputFormat={'yyyy-MM-dd'}
                                returnFormat={'YYYYMMDD'}
                                validation={true}
                                defaultValue={mngEmpInfo.appoinDtm}
                                mask={'____-__-__'}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default MngInfo;
