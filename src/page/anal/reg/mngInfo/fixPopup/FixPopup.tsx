/* eslint-disable react/jsx-no-undef */
import React, {useState} from 'react';
import API from 'override/api/API';
import TextField from '@mui/material/TextField';

import './style.scss';
import {confirm} from 'override/alert/Alert';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import Storage from 'override/api/Storage';
import {Button} from '@mui/material';

const FixPopup = (param) => {
    const {data: userInfo} = Storage.getUserInfo();

    const [fixRecordModal, setFixRecordModal] = useState({
        regId: param.param.param.regId,
        mngEmpNo: userInfo.empNo,
        mngDesc: '',
    });

    const onMngFixSave = async () => {
        if (param.onClose) {
            const confirmResult = await confirm({
                title: '보완요청을 하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                const regModal = param.param.param;
                regModal.regState = '110';
                regModal.fixRecordModel = fixRecordModal;
                const result = await API.request.post(
                    `api/anal/comm/sr/state/update`,
                    regModal,
                );

                if (result.data.success) {
                    param.onClose();
                }
            }
        }
    };

    const handleMngModalChange = async (e) => {
        const newValue = {
            ...fixRecordModal,
            [e.target.id]: e.target.value,
        };

        setFixRecordModal(newValue);
    };

    return (
        <div className="contents_main fix_popup_main">
            <ContentsWrap>
                <div className="search_wrap">
                    <div className="search_btns">
                        <Button
                            className="btn_group"
                            variant="outlined"
                            onClick={onMngFixSave}
                            startIcon={<SaveAltIcon className="btn_icon" />}>
                            저장
                        </Button>
                    </div>
                </div>
                <div className="contents_wrap">
                    <TextField
                        inputRef={(input) => input && input.focus()}
                        id="mngDesc"
                        onChange={handleMngModalChange}
                        sx={{
                            width: '100%',
                            height: 80,
                            minHeight: 80,
                        }}
                        multiline
                        rows={3}
                        label={'보완요청사유'}
                        size="small"
                    />
                </div>
            </ContentsWrap>
        </div>
    );
};

export default FixPopup;
