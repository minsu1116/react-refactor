import React, {useEffect, useMemo, useState} from 'react';
import API from 'override/api/API';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import './style.scss';
import TextField from '@mui/material/TextField';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DatePicker from 'components/atoms/datePicker/DatePicker';
import moment from 'moment';
import SGrid from 'components/atoms/grid/SGrid';
import {confirm} from 'override/alert/Alert';
function Project(params) {
    const [rowData, setRowData] = useState([]);
    const [searchObj, setSearchObj] = useState({
        pjtYear: moment().format('YYYY'),
        pjtNm: '',
        pjtCd: '',
        pjtPl: '',
    });

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '프로젝트 코드',
                field: 'pjtCd',
                width: 140,
                editable: false,
                essential: true,
            },
            {
                headerName: '프로젝트 명',
                field: 'pjtNm',
                width: 280,
                editable: false,
            },
            {
                headerName: '수행연도',
                field: 'pjtYear',
                align: 'center',
                width: 100,
                editable: false,
            },
            {
                headerName: 'PL',
                field: 'pjtPl',
                width: 80,
                align: 'center',
                editable: false,
            },
            {
                headerName: 'PL이름',
                field: 'pjtPlNm',
                align: 'center',
                width: 80,
                editable: false,
            },
            {
                headerName: 'PL부서',
                field: 'deptNm',
                width: 160,
                editable: false,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(`/api/anal/comm/pjt/get`, param);

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const handleDoubleClick = async (result) => {
        if (result.data) {
            const confirmResult = await confirm({
                title: `[${result.data.pjtNm}] 프로젝트를 선택 하시겠습니까?`,
                html: '',
            });
            if (confirmResult) {
                params.onClose(result.data);
            }
        }
    };

    const handleChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            e.preventDefault();
            await handleSearchClick();
        }
    };

    return (
        <div className="contents_wrap_parents_h">
            <div className="anal_reg_main">
                <ContentsWrap>
                    <div className="search_wrap">
                        <div className="search_contents">
                            <TextField
                                id="pjtNm"
                                label={'프로젝트명'}
                                value={searchObj.pjtNm}
                                onChange={handleChange}
                                onKeyDown={handleKeyDown}
                                size="small"
                            />
                            <DatePicker
                                label={'수행연도(To)'}
                                disabled={false}
                                width={120}
                                handleChange={handleChange}
                                id={'pjtYear'}
                                inputFormat={'yyyy'}
                                defaultValue={searchObj.pjtYear}
                                mask={'____'}
                                views={['year']}
                            />
                        </div>
                        <div className="search_contents">
                            <TextField
                                id="pjtCd"
                                label={'코드'}
                                value={searchObj.pjtCd}
                                onChange={handleChange}
                                onKeyDown={handleKeyDown}
                                size="small"
                            />
                            <TextField
                                id="pjtPl"
                                label={'PL 코드/명/부서'}
                                value={searchObj.pjtPl}
                                onChange={handleChange}
                                onKeyDown={handleKeyDown}
                                size="small"
                            />
                        </div>
                        <div className="search_btns">
                            <Tooltip title="조회">
                                <IconButton
                                    onClick={handleSearchClick}
                                    className="btn_group">
                                    <ManageSearchIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </div>
                    <div className="contents_wrap">
                        <SGrid
                            grid={'anal_pjt_grid'}
                            rowData={rowData}
                            title={'프로젝트 목록'}
                            rowDoubleClick={handleDoubleClick}
                            columnDefs={columnDefs}
                        />
                    </div>
                </ContentsWrap>
            </div>
        </div>
    );
}
export default Project;
