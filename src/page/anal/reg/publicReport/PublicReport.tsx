/* eslint-disable react/jsx-no-undef */
import React, {
    Suspense,
    useCallback,
    useEffect,
    useMemo,
    useState,
} from 'react';
import API from 'override/api/API';
import TextField from '@mui/material/TextField';
import Storage from '@override/api/Storage';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import SearchIcon from '@mui/icons-material/Search';
import SGrid from 'components/atoms/grid/SGrid';
import IconButton from '@mui/material/IconButton';

import RegProc from '../regProc/RegProc';
import MngInfo from '../mngInfo/MngInfo';
import SSelectbox from 'components/atoms/select/SSelectbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Spinner from 'components/atoms/spinner/Spinner';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import Radio from '@material-ui/core/Radio';
import SModal from 'override/modal/SModal';
import PendingActionsIcon from '@mui/icons-material/PendingActions';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import FindInPageIcon from '@mui/icons-material/FindInPage';
import {confirm, meg} from 'override/alert/Alert';
import DatePicker from 'components/atoms/datePicker/DatePicker';
import ConstructionIcon from '@mui/icons-material/Construction';
import PrintIcon from '@mui/icons-material/Print';
import ForwardToInboxIcon from '@mui/icons-material/ForwardToInbox';
import RecommendIcon from '@mui/icons-material/Recommend';
import Badge from '@mui/material/Badge';

import './style.scss';
import {Button} from '@mui/material';

interface RegModal {
    costCenter: string;
    plCenter: string;
    regTit: string;
    regType: string;
    gubun: string;
    fileGrpId?: number;
    isRefresh?: boolean;
    comment?: string;
    purpose: string;
    regState: string;
    regEmpNo?: string;
    langGubun?: string;
    address?: string;
    srType: string;
    submitDep?: string;
    apprModal?: ApprModal;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srDtlList: any;
    pjtModel?: PjtModal;
    regId?: string;
    pjtCd?: string;
    pjtNm?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    fixRecordModel?: any;
    regUserInfo?: RegUserInfo;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    srMngModel?: any;
}

interface ApprModal {
    apprEmpNm?: string;
    apprEmpNo?: string;
    apprReqDesc?: string;
    apprDeptNm?: string;
    apprReqDtm?: string;
    apprGubun?: string;
    apprDesc?: string;
}

interface PjtModal {
    pjtCd: string;
    pjtNm: string;
    pjtYn: boolean;
}

interface PublicReportDtlModal {
    regId: string;
    gridId?: number;
    sampleNm: string;
    rawMatNm: string;
    productNm: string;
    mfNm: string;
    lotNo: string;
    form: string;
    color: string;
    gubun: string;
    matQty: string;
    sort: number;
}

// 변경
interface RegUserInfo {
    regEmpNm: string;
    regDeptNm: string;
    regRankNm: string;
    regOfficeNo: string;
    regMobileNo: string;
    regEmail: string;
    regEmpNo: string;
}

function UserInfo(param) {
    const {data} = Storage.getUserInfo();
    const [userInfo, setUserInfo] = useState<RegUserInfo>({
        regEmpNo: data.empNo,
        regEmpNm: data.userNm,
        regDeptNm: data.deptNm,
        regRankNm: data.rankNm,
        regOfficeNo: data.officeNo,
        regMobileNo: data.mobileNo,
        regEmail: data.email,
    });

    useEffect(() => {
        setUserInfo(param.regUserInfo);
    }, [param?.regUserInfo]);

    return (
        <div className="reg_user_info">
            <div className="reg_main_tit">
                <span className="tit">요청자 정보</span>
            </div>
            <div className="reg_main_contents">
                <div className="user_info_conts">
                    <div className="tit">성명</div>
                    <div className="user_info">{userInfo?.regEmpNm}</div>
                    <div className="tit">직위</div>
                    <div className="user_info">{userInfo?.regRankNm}</div>
                    <div className="tit">부서</div>
                    <div className="user_info">{userInfo?.regDeptNm}</div>
                </div>
                <div className="user_info_conts">
                    <div className="tit">E-MAIL</div>
                    <div className="user_info">{userInfo?.regEmail}</div>
                    <div className="tit">전화번호</div>
                    <div className="user_info">{userInfo?.regOfficeNo}</div>
                    <div className="tit">HP</div>
                    <div className="user_info">{userInfo?.regMobileNo}</div>
                </div>
            </div>
        </div>
    );
}

function RegInfo(param) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const regStateList = useMemo(() => {
        return commonCodeGroup['REG_STATE'];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [commonCodeGroup]);
    const {data} = Storage.getGridList();
    const {data: userInfo} = Storage.getUserInfo();
    const [costList, setCostList] = useState([]);
    const [rowData, setRowData] = useState([]);
    const [etmModalOpen, setEtmModalOpen] = useState(false);
    const [pjtModalOpen, setPjtModalOpen] = useState(false);
    const [userModalOpen, setUserModalOpen] = useState(false);
    const [reportModalOpen, setReportModalOpen] = useState(false);
    const [mngEmpList, setMngEmpList] = useState([]);
    const [isMngEmp, setIsMngEmp] = useState(false);
    const [disable, setDisable] = useState(false);
    const sizeObj = useMemo(() => {
        return {
            width: '900',
            height: '820',
            isResize: false,
        };
    }, []);

    const [validationObj, setValidationObl] = useState({
        costCenter: false,
        plCenter: false,
        regTit: false,
        regType: false,
        purpose: false,
        comment: false,
        submitDep: false,
        gubun: false,
        pjtYn: false,
        apprEmpNo: false,
        keyword: false,
        address: false,
        srDtlList: false,
        apprReqDesc: false,
        apprDesc: false,
        apprGubun: false,
        pjtNm: false,
    });

    const [regModal, setRegModal] = useState<RegModal>({
        costCenter: '',
        plCenter: '',
        regTit: '',
        regType: '',
        comment: '',
        purpose: '',
        srType: '',
        gubun: '',
        fileGrpId: 0,
        langGubun: '',
        regState: '100',
        submitDep: '',
        srDtlList: null,
        regUserInfo: {
            regDeptNm: userInfo.deptNm,
            regEmail: userInfo.email,
            regEmpNm: userInfo.userNm,
            regEmpNo: userInfo.empNo,
            regOfficeNo: userInfo.officeNo,
            regMobileNo: userInfo.mobileNo,
            regRankNm: userInfo.rankNm,
        },
    });

    const [apprModal, setApprModal] = useState<ApprModal>({
        apprEmpNm: '결재자를 지정해 주십시오.',
        apprEmpNo: '',
        apprReqDesc: '',
        apprDeptNm: '',
    });

    const [pjtModal, setPjtModal] = useState<PjtModal>({
        pjtCd: '',
        pjtNm: '',
        pjtYn: false,
    });

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 32,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '번호',
                field: 'sort',
                width: 70,
                type: 'number',
                editable: false,
            },
            {
                headerName: '시료명',
                field: 'sampleNm',
                color: '#bd1717',
                width: 90,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: 'Lot No',
                field: 'lotNo',
                width: 90,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '재질',
                field: 'matQty',
                width: 90,
                color: '#bd1717',
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '제품명',
                field: 'productNm',
                width: 160,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '시험항목',
                field: 'gubun',
                width: 90,
                type: 'select',
                codeGroup: 'PR_GUBUN',
                editable: false,
                readOnly: regModal.regState <= '100' ? false : true,
                essential: true,
            },
            {
                headerName: '형태',
                field: 'form',
                width: 120,
                color: '#bd1717',
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '원료명',
                field: 'rawMatNm',
                width: 130,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '제조사',
                field: 'mfNm',
                width: 140,
                editable: regModal.regState <= '100' ? true : false,
                essential: true,
            },
            {
                headerName: '색상',
                field: 'color',
                type: 'select',
                codeGroup: 'COLOR_GROUP',
                readOnly: regModal.regState <= '100' ? false : true,
                editable: false,
                width: 120,
                essential: true,
            },
            {
                field: 'gridId',
                width: 0,
                editable: false,
                essential: false,
                hide: true,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        setCostList([]);
        const tempList = [];
        if (commonCodeGroup) {
            commonCodeGroup['COST_CENTER']
                .filter((x) => x.desc1 == regModal.plCenter)
                .forEach((element) => {
                    const costObj = {
                        cd: element.cd,
                        cdNm: element.cdNm,
                    };
                    tempList.push(costObj);
                });
        }
        setCostList(tempList);
        if (param?.regInfo?.regId) {
            initSearch(null);
        } else {
            setRegModal({
                ...regModal,
                costCenter: tempList.length > 0 ? tempList[0].cd : '',
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (
            regModal.regState === '110' &&
            regModal.regEmpNo == userInfo.empNo
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.regState]);

    useEffect(() => {
        if (
            regModal.regState === '130' &&
            regModal.regEmpNo == userInfo.empNo &&
            apprModal.apprDesc
        ) {
            fixConfirm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        apprModal.apprDesc,
        regModal.regEmpNo,
        regModal.regState,
        userInfo.empNo,
    ]);

    const fixConfirm = async () => {
        if (regModal.regState === '110' || regModal.regState === '130') {
            const tit =
                regModal.regState === '110' ? '의뢰 보완요청' : '부결 내용';

            let confirmResult = undefined;
            if (regModal.regState === '110') {
                confirmResult = await confirm({
                    title: tit,
                    html: `<div class="fix_confirm">
                            <div class="fix_emp_info">
                                <div class="fix_dept_nm">
                                    ${regModal.fixRecordModel.mngDeptNm}
                                </div>
                                <div class="fix_emp_nm">
                                    ${
                                        regModal.fixRecordModel.mngEmpNm +
                                        '  ' +
                                        regModal.fixRecordModel.mngPosNm
                                    }
                                </div>
                            </div>
                            <div class="fix_emp_desc">
                                ${regModal.fixRecordModel.mngDesc}
                            </div>
                        </div>`,
                });
            } else {
                confirmResult = await confirm({
                    title: tit,
                    html: `<div class="fix_confirm">
                            <div class="fix_emp_info">
                                <div class="fix_dept_nm">
                                    ${apprModal.apprDeptNm}
                                </div>
                                <div class="fix_emp_nm">
                                    ${apprModal.apprEmpNm}
                                </div>
                            </div>
                            <div class="fix_emp_desc">
                                ${apprModal.apprDesc}
                            </div>
                        </div>`,
                });
            }

            if (confirmResult) {
                if (regModal.regEmpNo != userInfo.empNo) {
                    meg({
                        title: '의뢰자가 아닙니다.',
                        html: '',
                    });
                    return;
                }

                setDisable(false);
                setRegModal({
                    ...regModal,
                    regState: '100',
                });
            }
        }
    };

    const initSearch = async (modal) => {
        const result = await API.request.post(
            `api/anal/public-report/get`,
            param.regInfo ? param.regInfo : modal,
        );

        if (result.data.success) {
            const setModalObj = result.data.data;
            setModalObj.isRefresh = !regModal.isRefresh;

            setRegModal({
                ...setModalObj,
                // 변경
                regUserInfo: {
                    regEmpNo: setModalObj.regEmpNo || '',
                    regEmpNm: setModalObj.regEmpNm || '',
                    regDeptNm: setModalObj.regDeptNm || '',
                    regRankNm: setModalObj.regRankNm || '',
                    regMobileNo: setModalObj.regMobileNo || '',
                    regEmail: setModalObj.regEmail || '',
                    regOfficeNo: setModalObj.regOfficeNo || '',
                },
            });
            setApprModal(
                result.data.data.apprModal
                    ? result.data.data.apprModal
                    : {
                          ...apprModal,
                          apprEmpNm: '결재자를 지정해 주십시오.',
                          apprEmpNo: '',
                          apprReqDesc: '',
                          apprDeptNm: '',
                      },
            );
            setRowData(result.data.data.srDtlList);
            setPjtModal({
                ...pjtModal,
                pjtCd: result.data.data.pjtCd,
                pjtNm: result.data.data.pjtNm,
                pjtYn: result.data.data.pjtYn,
            });

            if (
                (result.data.data.regState > '100' && result.data.data.regId) ||
                result.data.data.regEmpNo != userInfo.empNo
            ) {
                setDisable(true);
            }
        }
    };

    useEffect(() => {
        setCostList([]);
        const tempList = [];
        if (commonCodeGroup) {
            commonCodeGroup['COST_CENTER']
                .filter((x) => x.desc1 == regModal.plCenter)
                .forEach((element) => {
                    const costObj = {
                        cd: element.cd,
                        cdNm: element.cdNm,
                    };
                    tempList.push(costObj);
                });
        }
        setRegModal({
            ...regModal,
            costCenter: tempList.length > 0 ? tempList[0].cd : '',
        });
        setCostList(tempList);

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [regModal.plCenter]);

    useEffect(() => {
        if (regModal.regState == '400' && regModal.srType) {
            async function mngEmpCheck() {
                const param = {
                    srType: regModal.srType,
                };
                const result = await API.request.post(
                    `api/anal/mng-mt/get`,
                    param,
                );
                if (result.data.success) {
                    const tempMngEmpList = [];
                    result.data.data.forEach((element) => {
                        const mngObj = {
                            cd: element.mngEmpNo,
                            cdNm: `${element.mngEmpNm || ''}${
                                element.mngDeptNm ? '_' + element.mngDeptNm : ''
                            }`,
                        };
                        tempMngEmpList.push(mngObj);
                    });

                    // 변경
                    setMngEmpList(tempMngEmpList);
                    if (regModal.srType > '200') {
                        setIsMngEmp(
                            result.data.data.find(
                                (x) => x.mngEmpNo == userInfo.empNo,
                            ),
                        );
                    }
                }
            }
            mngEmpCheck();
        }
    }, [regModal.regState, regModal.srType, userInfo.empNo]);

    const handleKeyDown = useCallback(async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleRegModalChange = async (e) => {
        const newValue = {
            ...regModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        if (e.target.id === 'langGubun') {
            newValue.address = '';
        }

        setRegModal(newValue);
    };

    const handleApprModalChange = async (e) => {
        const newValue = {
            ...apprModal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };

        setApprModal(newValue);
    };

    const handleCheckChange = (e) => {
        let newValue;

        if (e.target.id === 'pjtYn') {
            if (e.target.checked) {
                newValue = {
                    ...pjtModal, // 기존의 값 복사 (spread operator)
                    [e.target.id]: e.target.checked, // 덮어쓰기
                    pjtCd: '',
                    pjtNm: '',
                };
                setRegModal({
                    ...regModal,
                    pjtCd: '',
                    pjtNm: '',
                });
            } else {
                newValue = {
                    ...pjtModal, // 기존의 값 복사 (spread operator)
                    [e.target.id]: e.target.checked, // 덮어쓰기
                };
            }
            setPjtModal(newValue);
        } else {
            newValue = {
                ...regModal, // 기존의 값 복사 (spread operator)
                [e.target.id]: e.target.checked, // 덮어쓰기
            };
            setRegModal(newValue);
        }
    };

    const regGridDelete = async (param) => {
        if (param && param.data) {
            const params = {
                param1: param.data,
                param2: regModal,
            };
            const result = await API.request.post(
                `api/anal/public-report/dtl/delete`,
                params,
            );
            if (result) {
                setRowData(result.data.data);
            } else {
                setRowData([]);
            }
        }
    };

    const onClickOpenPjtModal = () => {
        if (pjtModal.pjtYn) {
            meg({
                title: '[해당사항 없음]이 체크되었습니다.',
                html: '',
            });
            return;
        }

        setPjtModalOpen(true);
    };
    const onPjtModalClose = (result) => {
        setPjtModal({
            ...pjtModal,
            pjtCd: result.pjtCd,
            pjtNm: result.pjtNm,
        });
        setPjtModalOpen(false);
    };

    const onClickOpenUserModal = () => {
        setUserModalOpen(true);
    };

    const onUserModalClose = (result) => {
        setUserModalOpen(false);
        if (result.data) {
            setApprModal({
                ...apprModal,
                apprEmpNo: result.data.empNo,
                apprEmpNm: result.data.userNm,
                apprDeptNm: result.data.deptNm,
            });
        }
    };

    const validationCheck = async () => {
        const validationKeyList = {
            costCenter: '코스트센터',
            plCenter: '손익센터',
            regTit: '제목',
            gubun: '성적서 구분',
            langGubun: '성적서 언어',
            comment: '분석내용 및 요구사항',
            submitDep: '제출처',
            address: '주소',
            srDtlList: '분석요청 리스트',
        };

        const keyObj = Object.keys(validationKeyList);
        const errorList = validationObj;
        const megList = [];
        keyObj.forEach((element) => {
            if (!regModal[element]) {
                megList.push(validationKeyList[element]);
                errorList[element] = true;
            } else {
                errorList[element] = false;
            }
        });

        if (
            !errorList['srDtlList'] &&
            regModal.srDtlList &&
            regModal.srDtlList.length == 0
        ) {
            errorList['srDtlList'] = true;
            megList.push(validationKeyList['srDtlList']);
        }

        if (!pjtModal.pjtYn) {
            if (!pjtModal.pjtCd) {
                errorList['pjtNm'] = true;
                megList.push('Project Code / 여부');
            }
        }

        if (!apprModal.apprEmpNo) {
            errorList['apprEmpNo'] = true;
            megList.push('결재자 정보');
        }

        if (!apprModal.apprReqDesc) {
            errorList['apprReqDesc'] = true;
            megList.push('결재자 요청메모');
        }

        if (regModal.regState === '200') {
            if (!apprModal.apprDesc) {
                errorList['apprDesc'] = true;
                megList.push('결재자 처리메모');
            }

            if (!apprModal.apprGubun) {
                errorList['apprGubun'] = true;
                megList.push('결재처리');
            }
        }

        if (megList.length > 0) {
            setValidationObl({
                ...errorList,
            });
            let validationMeg = `<div class="meg_main">`;
            megList.forEach((element) => {
                validationMeg += `
                        <div class="meg_contents error">
                            ${element}
                        </div>
                    `;
            });

            validationMeg.concat(`</div>`);
            await meg({
                title: `필수 항목을 입력하십시오.`,
                html: validationMeg,
            });
            return false;
        }
        return true;
    };

    const onSavePublicReportInfo = async (regState) => {
        if (data) {
            const regPublicReportGrid = data?.get('reg_pl_grid');
            if (
                regPublicReportGrid &&
                regPublicReportGrid.getModel()?.rowsToDisplay?.length > 0
            ) {
                regModal.srDtlList = [];
                regPublicReportGrid
                    .getModel()
                    .rowsToDisplay.forEach((element) => {
                        if (element.data) {
                            const tempObj: PublicReportDtlModal = {
                                gridId: element.data.gridId,
                                sampleNm: element.data.sampleNm,
                                rawMatNm: element.data.rawMatNm,
                                productNm: element.data.productNm,
                                mfNm: element.data.mfNm,
                                lotNo: element.data.lotNo,
                                form: element.data.form,
                                color: element.data.color,
                                gubun: element.data.gubun,
                                matQty: element.data.matQty,
                                sort: element.data.sort,
                                regId: element.data.regId,
                            };
                            regModal.srDtlList.push(tempObj);
                        }
                    });
            }
        }

        const validationResult = await validationCheck();

        if (validationResult) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (
                regModal.srDtlList.find(
                    (x) =>
                        !x.sampleNm ||
                        !x.rawMatNm ||
                        !x.productNm ||
                        !x.mfNm ||
                        !x.lotNo ||
                        !x.form ||
                        !x.color ||
                        !x.gubun ||
                        !x.matQty,
                )
            ) {
                await meg({
                    title: `분석요청 필수값을 입력해 주십시오.`,
                    html: '',
                });

                return;
            }
            if (confirmResult) {
                regModal.regState = regState;
                regModal.apprModal = apprModal;
                regModal.pjtModel = pjtModal;
                const fileUploadGrid = data?.get('reg_sr_pl_upload_grid');
                if (fileUploadGrid) {
                    const fileSaveList = [];
                    fileUploadGrid
                        ?.getModel()
                        ?.rowsToDisplay.map((item) =>
                            fileSaveList.push(item.data),
                        );
                    if (fileSaveList.length > 0) {
                        const result = await saveFilesEditAll(fileSaveList);
                        if (result) {
                            regModal.fileGrpId = result;
                        }
                    }

                    switch (regModal.gubun) {
                        case '01':
                            regModal.srType = 'O';
                            break;
                        default:
                            regModal.srType = 'V';
                            break;
                    }

                    const result = await API.request.post(
                        `api/anal/public-report/save`,
                        regModal,
                    );

                    if (result.data.success) {
                        const saveModal = result.data.data;
                        saveModal.isRefresh = !regModal.isRefresh;
                        setRegModal({
                            ...saveModal,
                            regUserInfo: {
                                regEmpNo: saveModal.regEmpNo || '',
                                regEmpNm: saveModal.regEmpNm || '',
                                regDeptNm: saveModal.regDeptNm || '',
                                regRankNm: saveModal.regRankNm || '',
                                regMobileNo: saveModal.regMobileNo || '',
                                regEmail: saveModal.regEmail || '',
                                regOfficeNo: saveModal.regOfficeNo || '',
                            },
                        });
                        setApprModal(
                            result.data.data.apprModal
                                ? result.data.data.apprModal
                                : {
                                      ...apprModal,
                                      apprEmpNm: '결재자를 지정해 주십시오.',
                                      apprEmpNo: '',
                                      apprReqDesc: '',
                                      apprDeptNm: '',
                                  },
                        );
                        setRowData(result.data.data.srDtlList);
                        setPjtModal({
                            ...pjtModal,
                            pjtCd: result.data.data.pjtCd,
                            pjtNm: result.data.data.pjtNm,
                            pjtYn: result.data.data.pjtYn,
                        });

                        if (
                            result.data.data.regState > '100' &&
                            result.data.data.regId
                        ) {
                            setDisable(true);
                        }

                        if (saveModal.regState == '400') {
                            await meg({
                                title: `시료를 보내주십시오.`,
                                html: '',
                            });
                        }
                    } else {
                        regModal.regState = '100';
                        setRegModal(regModal);
                    }
                }
            }
        }
    };

    const onCopyPublicReportInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 복사하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            setDisable(false);
            setRegModal({
                ...regModal,
                regId: '',
                regState: '',
                fileGrpId: 0,
                srDtlList: [],
                // 변경
                regUserInfo: {
                    regDeptNm: userInfo.deptNm,
                    regEmail: userInfo.email,
                    regEmpNm: userInfo.userNm,
                    regEmpNo: userInfo.empNo,
                    regOfficeNo: userInfo.officeNo,
                    regMobileNo: userInfo.mobileNo,
                    regRankNm: userInfo.rankNm,
                },
            });
            setRowData([]);
        }
    };

    const onDeletePublicReportInfo = async () => {
        const confirmResult = await confirm({
            title: `[${regModal.regTit}] 의뢰를 삭제하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/anal/public-report/delete`,
                regModal,
            );

            if (result.data.success) {
                initModal();
            }
        }
    };

    const initModal = () => {
        setRegModal({
            costCenter: '',
            plCenter: '',
            regTit: '',
            regType: '',
            purpose: '',
            gubun: '',
            fileGrpId: 0,
            submitDep: '',
            langGubun: '',
            regState: '100',
            srDtlList: null,
            srType: 'T',
        });

        setApprModal({
            apprEmpNm: '결재자를 지정해 주십시오.',
            apprEmpNo: '',
            apprReqDesc: '',
            apprDeptNm: '',
        });

        setPjtModal({
            pjtCd: '',
            pjtNm: '',
            pjtYn: false,
        });

        setRowData([]);
        setDisable(false);
    };

    const onOpenReport = () => {
        setReportModalOpen(true);
    };

    const onReportModalClose = () => {
        setReportModalOpen(false);
    };

    const onOpenEtm = () => {
        setEtmModalOpen(true);
    };

    const onEtmModalClose = async (param) => {
        if (param.data && param.data.success) {
            await initSearch(null);
        }
        setEtmModalOpen(false);
    };

    return (
        <>
            {isMngEmp && regModal.regState === '400' ? (
                // 변경
                <MngInfo
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        initSearch: initSearch,
                    }}
                />
            ) : null}
            {regModal.regState > '400' || regModal.regState == '140' ? (
                // 변경
                <RegProc
                    param={{
                        mngEmpList: mngEmpList,
                        regModal: regModal,
                        isProcEquipReg: true,
                        initSearch: initSearch,
                    }}
                />
            ) : null}

            <div className="reg_info">
                <SModal
                    tit={'프로젝트 검색'}
                    isOpen={pjtModalOpen}
                    sizeObj={sizeObj}
                    path={'anal/reg/project/Project'}
                    onClose={onPjtModalClose}
                />
                <SModal
                    tit={'결재자 등록'}
                    isOpen={userModalOpen}
                    param={{mode: 'single'}}
                    sizeObj={sizeObj}
                    path={'common/popup/user/User'}
                    onClose={onUserModalClose}
                />
                <SModal
                    tit={'공인성적서 Report'}
                    isOpen={reportModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '1200',
                        height: '820',
                        isResize: false,
                    }}
                    path={'common/popup/report/publicReport/PublicReportReport'}
                    onClose={onReportModalClose}
                />
                <SModal
                    tit={'분석평가 등록'}
                    isOpen={etmModalOpen}
                    param={{data: regModal}}
                    sizeObj={{
                        width: '600',
                        height: 'auto',
                        isResize: false,
                    }}
                    path={'anal/reg/etm/Etm'}
                    onClose={onEtmModalClose}
                />
                <>
                    <div className="reg_main_tit">
                        <span className="tit">
                            {regModal.regId ? (
                                <>
                                    <div className="tit_01">
                                        {regModal.regId}
                                    </div>
                                    <div
                                        className="tit_02"
                                        // 변경
                                        style={{
                                            background:
                                                regModal.regState === '110'
                                                    ? '#8382db'
                                                    : regModal.regState ===
                                                      '200'
                                                    ? '#e76e6e'
                                                    : regModal.regState ===
                                                      '400'
                                                    ? '#60b180'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#5ab2e5'
                                                    : regModal.regState ===
                                                      '500'
                                                    ? '#0b9562'
                                                    : '#db8181',
                                        }}>
                                        {
                                            regStateList.filter(
                                                (x) =>
                                                    x.cd == regModal.regState,
                                            )[0].cdNm
                                        }
                                    </div>
                                </>
                            ) : (
                                '공인성적서 발행'
                            )}
                        </span>
                        <div className="search_btns">
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenReport}
                                    startIcon={<PrintIcon />}>
                                    의뢰내용 출력
                                </Button>
                            )}
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onCopyPublicReportInfo}
                                    startIcon={<ContentCopyIcon />}>
                                    복사
                                </Button>
                            )}
                            {apprModal.apprEmpNo === userInfo.empNo &&
                                regModal.regId &&
                                regModal.regState === '200' && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        disabled={
                                            regModal.regEmpNo != '' &&
                                            userInfo.empNo !=
                                                regModal?.apprModal?.apprEmpNo
                                        }
                                        onClick={() => {
                                            const regState =
                                                apprModal.apprGubun === 'APPR'
                                                    ? '400'
                                                    : '130';
                                            onSavePublicReportInfo(regState);
                                        }}
                                        startIcon={<ForwardToInboxIcon />}>
                                        결재하기
                                    </Button>
                                )}
                            {regModal.regId &&
                                (regModal.regState === '110' ||
                                    regModal.regState === '130') && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        onClick={fixConfirm}
                                        startIcon={
                                            <Badge
                                                badgeContent={1}
                                                color="primary">
                                                <ConstructionIcon className="btn_icon" />
                                            </Badge>
                                        }>
                                        {regModal.regState === '130'
                                            ? '부결내용 확인'
                                            : '보완요청 확인'}
                                    </Button>
                                )}
                            {regModal.regId && regModal.regState < '150' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    disabled={
                                        regModal.regEmpNo != '' &&
                                        userInfo.empNo !=
                                            regModal?.regUserInfo?.regEmpNo
                                    }
                                    onClick={onDeletePublicReportInfo}
                                    startIcon={<DeleteForeverIcon />}>
                                    삭제
                                </Button>
                            )}
                            {regModal.regState <= '100' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={() => {
                                        onSavePublicReportInfo('100');
                                    }}
                                    startIcon={<PendingActionsIcon />}>
                                    임시저장
                                </Button>
                            )}
                            {(!regModal.regId || regModal.regState === '100') &&
                                !regModal.srMngModel && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        disabled={
                                            regModal.regEmpNo != '' &&
                                            userInfo.empNo !=
                                                regModal?.regUserInfo?.regEmpNo
                                        }
                                        onClick={() => {
                                            onSavePublicReportInfo('200');
                                        }}
                                        startIcon={<FindInPageIcon />}>
                                        결재요청
                                    </Button>
                                )}
                            {apprModal.apprGubun === 'APPR' &&
                                regModal.srMngModel && (
                                    <Button
                                        className="btn_group"
                                        variant="outlined"
                                        disabled={
                                            regModal.regEmpNo != '' &&
                                            userInfo.empNo !=
                                                regModal?.regUserInfo?.regEmpNo
                                        }
                                        onClick={() => {
                                            let regState = '400';
                                            if (regModal.srMngModel) {
                                                regState = '500';
                                            }

                                            onSavePublicReportInfo(regState);
                                        }}
                                        startIcon={
                                            <ForwardToInboxIcon className="btn_icon" />
                                        }>
                                        의뢰하기
                                    </Button>
                                )}
                            {regModal.regId && regModal.regState == '900' && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenEtm}
                                    startIcon={<RecommendIcon />}>
                                    평가하기
                                </Button>
                            )}
                        </div>
                    </div>
                    <div className="reg_main_contents">
                        <div className="reg_contents">
                            <TextField
                                id="regTit"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                label={'제목'}
                                sx={{
                                    minWidth: 300,
                                    width: '30%',
                                }}
                                error={!regModal.regTit && validationObj.regTit}
                                value={regModal.regTit}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <TextField
                                id="purpose"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                sx={{
                                    minWidth: 300,
                                    width: '30%',
                                }}
                                label={'의뢰목적'}
                                error={
                                    validationObj.purpose && !regModal.purpose
                                }
                                value={regModal.purpose}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'REG_TYPE'}
                                label={'의뢰유형'}
                                id={'regType'}
                                value={regModal.regType}
                                handleChange={handleRegModalChange}
                                disabled={disable}
                            />
                        </div>
                        <div
                            className="reg_contents"
                            style={{
                                marginBottom: !regModal.langGubun
                                    ? '15px'
                                    : '0px',
                            }}>
                            <FormControl
                                id={'radio_main'}
                                error={validationObj.gubun && !regModal.gubun}>
                                <FormLabel id="radio_group">
                                    {'성적서 구분'}
                                </FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="radio_group"
                                    name="row-radio-buttons-group">
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="01"
                                                checked={
                                                    regModal.gubun === '01'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="UL성적서"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="02"
                                                checked={
                                                    regModal.gubun === '02'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'gubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="TUV성적서(공인)"
                                    />
                                </RadioGroup>
                            </FormControl>
                            <FormControl
                                id={'radio_main'}
                                error={validationObj.gubun && !regModal.gubun}>
                                <FormLabel id="radio_group">
                                    {'성적서 언어'}
                                </FormLabel>
                                <RadioGroup
                                    row
                                    aria-labelledby="radio_group"
                                    name="row-radio-buttons-group">
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="01"
                                                checked={
                                                    regModal.langGubun === '01'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'langGubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="국문"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                value="02"
                                                checked={
                                                    regModal.langGubun === '02'
                                                }
                                                onChange={handleRegModalChange}
                                                id={'langGubun'}
                                                disabled={disable}
                                            />
                                        }
                                        label="영문"
                                    />
                                </RadioGroup>
                            </FormControl>
                            <TextField
                                id="submitDep"
                                onChange={handleRegModalChange}
                                onKeyDown={handleKeyDown}
                                label={'제출처'}
                                sx={{
                                    minWidth: 250,
                                    width: '25%',
                                }}
                                error={
                                    !regModal.submitDep &&
                                    validationObj.submitDep
                                }
                                value={regModal.submitDep}
                                // value={searchObj.equipMngEmpNm}
                                size="small"
                                disabled={disable}
                            />
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={
                                    regModal.langGubun == '01'
                                        ? 'ADDRESS_KR'
                                        : 'ADDRESS_EN'
                                }
                                label={'주소'}
                                id={'address'}
                                // default={regModal.shareGroup}
                                error={
                                    validationObj.address && !regModal.address
                                }
                                discription={
                                    !regModal.langGubun
                                        ? '성적서 언어를 먼저 선택해 주십시오.'
                                        : null
                                }
                                value={regModal.address}
                                handleChange={handleRegModalChange}
                                disabled={disable || !regModal.langGubun}
                            />
                        </div>
                        <div
                            className="reg_contents"
                            style={{flexDirection: 'column'}}>
                            <SGrid
                                grid={'reg_pl_grid'}
                                rowData={rowData}
                                isForceDelete={true}
                                title={'분석요청'}
                                newRowDefault={{
                                    sort: 'auto',
                                }}
                                isAddBtnOpen={!disable}
                                onClickDelete={disable ? null : regGridDelete}
                                columnDefs={columnDefs}
                            />
                            <div className="pl_desc">
                                <div className="pl_desc_sub_1">
                                    {'시료명(성적서 표기명)'}
                                </div>
                                <div className="pl_desc_sub_1">
                                    {'재질(PVC, PE, AI합금 등) '}
                                </div>
                                <div className="pl_desc_sub_1">
                                    {'형태(PELLET, SHEET, FILM, PART 등)'}
                                </div>
                                <div className="pl_desc_sub_2">
                                    {
                                        '* 필요 시료량 : 6대 물질 25g 이상, PBB/PBDE 15g이상, 4대중금속 및 기타 10g 이상 임.'
                                    }
                                </div>
                                <div className="pl_desc_sub_2">
                                    {
                                        '* 영문 성적서 발행 시 시료 정보는 필히 영문으로 작성'
                                    }
                                </div>
                                <div className="pl_desc_sub_2">
                                    {'* 각 공장에서 측정한 XRF DATA 첨부'}
                                </div>
                            </div>
                        </div>
                        <div className="reg_contents">
                            {regModal.regState <= '110' && (
                                <Paper
                                    sx={{
                                        p: '2px 4px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        width: 450,
                                    }}>
                                    <InputBase
                                        sx={{ml: 1, flex: 1}}
                                        placeholder="Project Code"
                                        disabled={true}
                                        id={'pjtNm'}
                                        value={
                                            pjtModal.pjtNm ? pjtModal.pjtNm : ''
                                        }
                                        error={
                                            validationObj.pjtNm &&
                                            !pjtModal.pjtNm &&
                                            pjtModal.pjtYn
                                        }
                                        inputProps={{
                                            'aria-label': 'Project Code',
                                        }}
                                    />
                                    <Divider
                                        sx={{height: 28, m: 0.2}}
                                        orientation="vertical"
                                    />
                                    <IconButton
                                        onClick={onClickOpenPjtModal}
                                        sx={{p: '10px'}}
                                        disabled={disable}
                                        aria-label="search">
                                        <SearchIcon />
                                    </IconButton>
                                    <Divider
                                        sx={{height: 28, m: 0.2}}
                                        orientation="vertical"
                                    />
                                    <FormControlLabel
                                        disabled={disable}
                                        control={
                                            <Checkbox
                                                sx={{marginLeft: 0.5}}
                                                id={'pjtYn'}
                                                onChange={handleCheckChange}
                                                checked={pjtModal.pjtYn}
                                                disabled={disable}
                                            />
                                        }
                                        label="해당사항 없음"
                                        labelPlacement="start"
                                    />
                                </Paper>
                            )}
                            {regModal.regState > '110' && (
                                <TextField
                                    id="pjtNm"
                                    sx={{
                                        minWidth: 300,
                                    }}
                                    label={'프로젝트 명'}
                                    value={
                                        regModal.pjtNm
                                            ? regModal.pjtNm
                                            : '해당사항 없음'
                                    }
                                    // value={searchObj.equipMngEmpNm}
                                    size="small"
                                    disabled={disable}
                                />
                            )}
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'PL_CENTER'}
                                label={'손익센터'}
                                id={'plCenter'}
                                value={regModal.plCenter}
                                error={
                                    validationObj.plCenter && !regModal.plCenter
                                }
                                handleChange={handleRegModalChange}
                                disabled={disable}
                            />
                            <SSelectbox
                                id={'costCenter'}
                                isCodeGroupMode={false}
                                codeGroup={'COST_CENTER'}
                                label={'코스트센터'}
                                disabled={disable}
                                selectList={costList}
                                discription={
                                    costList.length == 0
                                        ? '손익센터를 먼저 선택해 주십시오.'
                                        : null
                                }
                                value={regModal.costCenter}
                                error={
                                    validationObj.costCenter &&
                                    !regModal.costCenter
                                }
                                handleChange={handleRegModalChange}
                            />
                        </div>
                        <div
                            className="reg_contents"
                            style={{paddingTop: '10px'}}>
                            <TextField
                                id="comment"
                                onChange={handleRegModalChange}
                                sx={{
                                    width: '100%',
                                    paddingRight: '0px !important',
                                }}
                                multiline
                                rows={3}
                                label={'내용/요구사항'}
                                size="small"
                                value={regModal.comment}
                                error={
                                    validationObj.comment && !regModal.comment
                                }
                                disabled={disable}
                            />
                        </div>
                        <div className="reg_contents">
                            <SFile
                                gridId={'reg_sr_pl_upload_grid'}
                                fileGrpId={regModal.fileGrpId}
                                isRefresh={regModal.isRefresh}
                                tit={'첨부파일'}
                                useDownload={true}
                                useFileAttach={!disable}
                                useFileDelete={!disable}
                            />
                        </div>
                    </div>
                </>
                <div className="appr_main">
                    <div className="reg_main_tit">
                        <span className="tit">결재자 지정</span>
                    </div>
                    <div className="reg_main_contents">
                        {apprModal.apprReqDtm && regModal.regState >= '200' ? (
                            <>
                                <div className="reg_contents">
                                    <TextField
                                        id="apprEmpNm"
                                        disabled={true}
                                        value={apprModal.apprEmpNm}
                                        sx={{
                                            minWidth: 150,
                                            width: 'auto',
                                        }}
                                        label={'결재자'}
                                        size="small"
                                    />
                                    <TextField
                                        id="apprDesc"
                                        onChange={handleApprModalChange}
                                        onKeyDown={handleKeyDown}
                                        disabled={
                                            regModal.regState >= '400'
                                                ? true
                                                : false
                                        }
                                        value={
                                            apprModal.apprDesc
                                                ? apprModal.apprDesc
                                                : ''
                                        }
                                        error={
                                            !apprModal.apprDesc &&
                                            validationObj.apprDesc
                                        }
                                        sx={{
                                            minWidth: 200,
                                            width: '100%',
                                        }}
                                        label={'결재자 처리메모'}
                                        size="small"
                                    />
                                    <FormControl
                                        id={'radio_main'}
                                        error={
                                            validationObj.apprGubun &&
                                            !apprModal.apprGubun
                                        }>
                                        <FormLabel id="radio_group">
                                            결재처리
                                        </FormLabel>
                                        <RadioGroup
                                            row
                                            aria-labelledby="radio_group"
                                            name="row-radio-buttons-group">
                                            <FormControlLabel
                                                control={
                                                    <Radio
                                                        value="APPR"
                                                        checked={
                                                            apprModal.apprGubun ===
                                                            'APPR'
                                                        }
                                                        onChange={
                                                            handleApprModalChange
                                                        }
                                                        id={'apprGubun'}
                                                        disabled={
                                                            regModal.regState >=
                                                            '400'
                                                                ? true
                                                                : false
                                                        }
                                                    />
                                                }
                                                label="승인"
                                            />
                                            <FormControlLabel
                                                control={
                                                    <Radio
                                                        value="REJECT"
                                                        checked={
                                                            apprModal.apprGubun ===
                                                            'REJECT'
                                                        }
                                                        onChange={
                                                            handleApprModalChange
                                                        }
                                                        id={'apprGubun'}
                                                        disabled={
                                                            regModal.regState >=
                                                            '400'
                                                                ? true
                                                                : false
                                                        }
                                                    />
                                                }
                                                label="부결"
                                            />
                                        </RadioGroup>
                                    </FormControl>
                                </div>
                                <div className="reg_contents">
                                    <TextField
                                        id="regEmpNm"
                                        disabled={true}
                                        value={regModal.regUserInfo.regEmpNm}
                                        sx={{
                                            minWidth: 150,
                                        }}
                                        label={'요청자'}
                                        size="small"
                                    />
                                    <TextField
                                        id="apprReqDesc"
                                        disabled={disable}
                                        value={apprModal.apprReqDesc}
                                        sx={{
                                            minWidth: 200,
                                            width: '100%',
                                        }}
                                        label={'결재 요청메모'}
                                        size="small"
                                    />
                                    <DatePicker
                                        label={'결재요청일'}
                                        disabled={true}
                                        width={142}
                                        handleChange={handleRegModalChange}
                                        id={'claimDtm'}
                                        inputFormat={'yyyy-MM-dd'}
                                        returnFormat={'YYYYMMDD'}
                                        defaultValue={apprModal.apprReqDtm}
                                        mask={'____-__-__'}
                                    />
                                </div>
                            </>
                        ) : (
                            <div className="reg_contents">
                                <Paper
                                    sx={{
                                        p: '2px 4px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        width: 'fit-content',
                                        border:
                                            !apprModal.apprEmpNo &&
                                            validationObj.apprEmpNo
                                                ? '1px solid #d32f2f'
                                                : null,
                                    }}>
                                    <div className="appr_info">
                                        <span id={'apprDeptNm'}>
                                            {apprModal.apprDeptNm}
                                        </span>
                                        <span
                                            id={'apprEmpNm'}
                                            style={{
                                                color:
                                                    !apprModal.apprEmpNo &&
                                                    validationObj.apprEmpNo
                                                        ? '#d32f2f'
                                                        : ' #4f73c3',
                                            }}>
                                            {apprModal.apprEmpNm}
                                        </span>
                                    </div>
                                    <Divider
                                        sx={{height: 28, m: 0.2}}
                                        orientation="vertical"
                                    />
                                    <IconButton
                                        onClick={onClickOpenUserModal}
                                        sx={{p: '10px'}}
                                        disabled={disable}
                                        aria-label="search">
                                        <SearchIcon />
                                    </IconButton>
                                </Paper>
                                <TextField
                                    id="apprReqDesc"
                                    onChange={handleApprModalChange}
                                    onKeyDown={handleKeyDown}
                                    disabled={disable}
                                    value={apprModal.apprReqDesc}
                                    error={
                                        !apprModal.apprReqDesc &&
                                        validationObj.apprReqDesc
                                    }
                                    sx={{
                                        minWidth: 200,
                                        width: '100%',
                                    }}
                                    label={'결재자 요청메모'}
                                    size="small"
                                />
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <UserInfo regUserInfo={regModal.regUserInfo} />
        </>
    );
}

const PublicReport = (param) => {
    const regInfo = useMemo(() => {
        if (param?.param) {
            return {
                regInfo: {
                    regId: param?.param?.regId || '',
                },
            };
        }
        return;

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param?.param]);

    return (
        <div className="contents_wrap_parents_h">
            <div className="public_report_main">
                <Suspense fallback={<Spinner />}>
                    <ContentsWrap>
                        <RegInfo regInfo={regInfo?.regInfo} />
                    </ContentsWrap>
                </Suspense>
            </div>
        </div>
    );
};
export default PublicReport;
