/* eslint-disable react/jsx-no-undef */
import React, {useEffect, useMemo, useState} from 'react';
import API from 'override/api/API';
import {confirm, meg} from 'override/alert/Alert';
import PrintIcon from '@mui/icons-material/Print';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import TextField from '@mui/material/TextField';
import {loadingLayer} from '@components/atoms/spinner/Spinner';

import SSelectbox from 'components/atoms/select/SSelectbox';
import Paper from '@mui/material/Paper';
import SModal from 'override/modal/SModal';
import moment from 'moment';
import DatePicker from 'components/atoms/datePicker/DatePicker';
import Storage from 'override/api/Storage';
import Seditor from 'components/atoms/editor/Seditor';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import './style.scss';
import TransferList from 'components/atoms/transferList/TransferList';
import {Button} from '@mui/material';

const RegProc = (param) => {
    const {data} = Storage.getGridList();
    const [modalOpen, setModalOpen] = useState(false);
    const [disable, setDisable] = useState(false);
    const [mailCheck, setMailCheck] = useState(false);
    const [reportModalOpen, setReportModalOpen] = useState(false);
    const {data: userInfo} = Storage.getUserInfo();
    const [procValidation, setProcValidation] = useState(false);
    const [isProcEquipReg, setIsProcEquipReg] = useState(false);
    const [totalEquipList, setTotalEquipList] = useState([]);
    const [reportInfo, setReportInfo] = useState({
        path: '',
        tit: '',
    });
    const commonCodeGroup = Storage.getGroupCode().data;
    const regStateList = useMemo(() => {
        const returnList = [];
        if (param.param.regModal.regState == '990') {
            returnList.push({
                cd: '990',
                cdNm: '최종완료',
            });

            return returnList;
        }

        const tempRegStateList = commonCodeGroup['REG_STATE'];

        tempRegStateList.forEach((element) => {
            if (element.desc1 === 'Y') {
                returnList.push(element);
            }
        });

        return returnList;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [commonCodeGroup]);

    const regModal = useMemo(() => {
        switch (param.param.regModal.srType) {
            case 'S':
            case 'E':
                setReportInfo({
                    path: 'common/popup/report/util/UtilProcReport',
                    tit: '특정설비분석 처리내역서',
                });
                break;
            case 'T':
                setReportInfo({
                    path: 'common/popup/report/claim/OverallClaimProcReport',
                    tit: '종합분석 처리내역서',
                });
                break;
            case 'C':
                setReportInfo({
                    path: 'common/popup/report/claim/OverallClaimProcReport',
                    tit: '클레임 처리내역서',
                });
                break;
            case 'O':
                setReportInfo({
                    path: 'common/popup/report/publicReport/PublicReportProcReport',
                    tit: '공인성적서 처리내역',
                });
                break;
            case 'V':
                if (param.param.regModal.regHrYn == 'Y') {
                    setReportInfo({
                        path: 'common/popup/report/publicReport/PublicReportProcReport',
                        tit: '공인성적서 처리내역',
                    });
                } else {
                    setReportInfo({
                        path: 'common/popup/report/environ/EnvironProcReport',
                        tit: '환경유해물질 처리내역',
                    });
                }
                break;
            case 'G':
                setReportInfo({
                    path: 'common/popup/report/general/GeneralProcReport',
                    tit: '일반분석 처리내역',
                });
                break;
        }

        return param.param.regModal;
    }, [param.param.regModal]);

    const [mngEmpInfo, setMngEmpInfo] = useState({
        condtConts: '',
        oppniConts: '',
        regState: '',
        fileGrpdId: '',
        isRefresh: false,
        procDtm: moment().format('YYYYMMDD'),
        keyword: '',
        resultMailYn: 'N',
        mailDesc: '',
    });

    const modalObj = useMemo(() => {
        return {
            tit: '보완요청 등록',
            path: 'anal/reg/mngInfo/fixPopup/FixPopup',
            param: param.param.regModal,
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const sizeObj = useMemo(() => {
        return {
            width: '700',
            height: '210',
            isResize: false,
        };
    }, []);

    useEffect(() => {
        if (param.param.isProcEquipReg) {
            setIsProcEquipReg(param.param.isProcEquipReg);
            searchEquipList();
        }

        if (param.param?.regModal?.srMngModel?.mngEmpNo) {
            const srMngModel = param.param.regModal.srMngModel;
            if (userInfo.empNo != srMngModel.mngEmpNo) {
                setDisable(true);
            }
            setMngEmpInfo({
                condtConts: srMngModel?.condtConts || '',
                oppniConts: srMngModel?.oppniConts || '',
                regState: param.param.regModal.regState,
                fileGrpdId: srMngModel.fileGrpId || '',
                isRefresh: false,
                resultMailYn: srMngModel.resultMailYn || 'N',
                procDtm: srMngModel.procDtm || moment().format('YYYYMMDD'),
                keyword: param.param.regModal.keyword || '',
                mailDesc: srMngModel.mailDesc || '',
            });

            if (param.param.regModal.regState == '990') {
                setDisable(true);
            }

            if (srMngModel.resultMailYn == 'Y') {
                setMailCheck(true);
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        param.param.isProcEquipReg,
        param.param.regModal.keyword,
        param.param.regModal.regState,
        param.param.regModal.srMngModel,
        userInfo.empNo,
    ]);

    const searchEquipList = async () => {
        const param = {
            regId: regModal.regId,
        };

        const result = await API.request.post(
            `api/anal/comm/proc/equip/get`,
            param,
        );

        if (result.data.success && result.data.data) {
            setTotalEquipList(result.data.data);
        }
    };

    const handleMngModalChange = async (e) => {
        if (e.target.id === 'mailCheck') {
            const newValue = {
                ...mngEmpInfo, // 기존의 값 복사 (spread operator)
                resultMailYn: e.target.checked ? 'Y' : 'N', // 덮어쓰기
                mailDesc: !e.target.checked ? '' : mngEmpInfo.mailDesc,
            };
            setMailCheck(e.target.checked);
            setMngEmpInfo(newValue);
        } else {
            const newValue = {
                ...mngEmpInfo, // 기존의 값 복사 (spread operator)
                [e.target.id]: e.target.value, // 덮어쓰기
            };
            setMngEmpInfo(newValue);
        }

        if (e.target.id === 'regState' && e.target.value === '110') {
            onFixPopupOpen();
        }
    };

    const onSaveMngInfo = async () => {
        if (!mngEmpInfo.regState) {
            meg({
                title: '처리상태를 선택 해주십시오..',
                html: '',
            });
            setProcValidation(true);
            return;
        }

        regModal.srMngModel = {
            ...regModal.srMngModel,
            condtConts: mngEmpInfo.condtConts,
            oppniConts: mngEmpInfo.oppniConts,
            resultMailYn: mngEmpInfo.resultMailYn,
            mailDesc: mngEmpInfo.mailDesc,
            fileGrpdId: '',
            procDtm: mngEmpInfo.procDtm,
        };

        regModal.keyword = mngEmpInfo.keyword;
        regModal.regState = mngEmpInfo.regState;
        regModal.procEquipRegYn = isProcEquipReg;

        const fileUploadGrid = data?.get('reg_sr_proc_upload_grid');
        if (fileUploadGrid) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });

            if (confirmResult) {
                loadingLayer(false);
                const fileSaveList = [];
                fileUploadGrid
                    ?.getModel()
                    ?.rowsToDisplay.map((item) => fileSaveList.push(item.data));
                if (fileSaveList.length > 0) {
                    const result = await saveFilesEditAll(fileSaveList);
                    if (result) {
                        regModal.srMngModel.fileGrpId = result;
                    }
                }
                regModal.srProcEquipModelList = regModal.tempEquipList;

                const result = await API.request.post(
                    `api/anal/comm/sr/state/update`,
                    JSON.stringify(regModal),
                );

                if (result.data.success) {
                    mngEmpInfo.isRefresh = !mngEmpInfo.isRefresh;
                    param.param.initSearch(param.param.regModal);
                    searchEquipList();
                }
                loadingLayer(true);
            }
        }
    };

    const onFixPopupClose = () => {
        setModalOpen(false);
        setMngEmpInfo({
            ...mngEmpInfo,
            regState: '',
        });
        param.param.initSearch(param.param.regModal);
    };

    const onFixPopupOpen = async () => {
        setModalOpen(true);
    };

    const onOpenReport = () => {
        setReportModalOpen(true);
    };

    const onReportModalClose = () => {
        setReportModalOpen(false);
    };

    const onChangeEquipList = (param) => {
        if (param) {
            const equipList = [];
            param.forEach((element) => {
                const equipObj = regModal.srProcEquipModelList.filter(
                    (x) => x.equipId == element,
                )[0];
                if (equipObj) {
                    equipObj.regId = regModal.regId;
                    equipList.push(equipObj);
                }
            });

            regModal.tempEquipList = equipList;
        }
    };

    const setLeftList = () => {
        const leftList = [];
        if (regModal.tempEquipList && regModal.tempEquipList.length > 0) {
            totalEquipList.forEach((element) => {
                if (
                    !regModal.tempEquipList.find(
                        (x) => x.equipId == element.equipId,
                    )
                ) {
                    leftList.push(element);
                }
            });

            return leftList;
        }
        return totalEquipList.filter((x) => !x.regId);
    };

    const setSaveList = () => {
        if (regModal.tempEquipList && regModal.tempEquipList.length > 0) {
            return regModal.tempEquipList;
        }

        return totalEquipList.filter((x) => x.regId);
    };

    return (
        <div className="reg_proc_main">
            <SModal
                tit={'보완요청 등록'}
                param={modalObj}
                isOpen={modalOpen}
                sizeObj={sizeObj}
                path={modalObj.path}
                onClose={onFixPopupClose}
            />
            <SModal
                tit={reportInfo.tit}
                isOpen={reportModalOpen}
                param={{data: regModal}}
                sizeObj={{
                    width: '1200',
                    height: '820',
                    isResize: false,
                }}
                path={reportInfo.path}
                onClose={onReportModalClose}
            />
            <div className="reg_mng_main">
                <div
                    className="reg_info"
                    style={{
                        background:
                            regModal.regState >= '500' ? '#e3e7f5' : '#f6f7fa',
                    }}>
                    <div className="reg_main_tit">
                        <span className="tit">
                            <div className="tit_01">처리 현황</div>
                            <div
                                className="tit_02"
                                // 변경
                                style={{
                                    background: '#7066f7',
                                }}>
                                <span className="mng_info_dept">
                                    {`담당자: ${regModal.srMngModel?.mngDeptNm}(${regModal.srMngModel?.mngOfficeNo})`}
                                </span>
                                <span className="mng_info_emp">
                                    {`${regModal.srMngModel?.mngEmpNm} ${regModal.srMngModel?.mngPosNm}`}
                                </span>
                            </div>
                        </span>
                        <div className="search_btns">
                            {regModal.regId && (
                                <Button
                                    className="btn_group"
                                    variant="outlined"
                                    onClick={onOpenReport}
                                    startIcon={
                                        <PrintIcon className="btn_icon" />
                                    }>
                                    처리내역 출력
                                </Button>
                            )}
                            <Button
                                className="btn_group"
                                variant="outlined"
                                disabled={disable}
                                onClick={onFixPopupOpen}
                                startIcon={
                                    <AppRegistrationIcon className="btn_icon" />
                                }>
                                보완요청
                            </Button>
                            <Button
                                className="btn_group"
                                variant="outlined"
                                onClick={onSaveMngInfo}
                                disabled={disable}
                                startIcon={
                                    <SaveAltIcon className="btn_icon" />
                                }>
                                저장
                            </Button>
                        </div>
                    </div>
                    <div className="reg_main_contents">
                        <div className="reg_contents">
                            <SSelectbox
                                id={'regState'}
                                isCodeGroupMode={false}
                                label={'처리상태'}
                                value={mngEmpInfo.regState}
                                selectList={regStateList}
                                error={procValidation && !mngEmpInfo.regState}
                                handleChange={handleMngModalChange}
                                disabled={disable}
                            />
                            <DatePicker
                                label={'처리일'}
                                width={160}
                                handleChange={handleMngModalChange}
                                id={'procDtm'}
                                inputFormat={'yyyy-MM-dd'}
                                returnFormat={'YYYYMMDD'}
                                validation={true}
                                defaultValue={mngEmpInfo.procDtm}
                                mask={'____-__-__'}
                                disabled={disable}
                            />
                            <TextField
                                id="keyword"
                                onChange={handleMngModalChange}
                                sx={{
                                    minWidth: 200,
                                }}
                                label={'키워드'}
                                value={mngEmpInfo.keyword}
                                size="small"
                                disabled={disable}
                            />
                            <Paper
                                sx={{
                                    p: '2px 4px',
                                    display: 'flex',
                                    alignItems: 'center',
                                    width: 108,
                                }}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            sx={{marginLeft: 0.3}}
                                            id={'mailCheck'}
                                            onChange={handleMngModalChange}
                                            checked={mailCheck}
                                            disabled={disable}
                                        />
                                    }
                                    label="알람메일"
                                    labelPlacement="start"
                                />
                            </Paper>
                        </div>
                        {isProcEquipReg && (
                            <div className="reg_contents">
                                <TransferList
                                    prop={{
                                        leftInfo: {
                                            list: setLeftList(),
                                            tit: '분석장비 리스트',
                                        },
                                        rightInfo: {
                                            list: setSaveList(),
                                            tit: '추가된 분석장비 리스트',
                                        },
                                        disable: disable,
                                    }}
                                    onChange={onChangeEquipList}
                                />
                            </div>
                        )}

                        <div className="reg_contents">
                            <TextField
                                id="condtConts"
                                onChange={handleMngModalChange}
                                sx={{
                                    width: '100%',
                                    paddingRight: '0px',
                                }}
                                multiline
                                rows={5}
                                label={'분석조건'}
                                size="small"
                                value={mngEmpInfo.condtConts}
                                disabled={disable}
                            />
                        </div>
                        <div className="reg_contents">
                            <TextField
                                id="oppniConts"
                                onChange={handleMngModalChange}
                                sx={{
                                    width: '100%',
                                    paddingRight: '0px',
                                }}
                                multiline
                                rows={5}
                                label={'분석의견'}
                                size="small"
                                value={mngEmpInfo.oppniConts}
                                disabled={disable}
                            />
                        </div>
                        {mailCheck && (
                            <div
                                className={
                                    mailCheck
                                        ? 'reg_contents animate__animated animate__fadeInLeft animate__fast'
                                        : 'reg_contents'
                                }>
                                <Seditor
                                    tit={'메일내용'}
                                    readOnly={disable}
                                    handleChange={(value) => {
                                        setMngEmpInfo({
                                            ...mngEmpInfo,
                                            mailDesc: value,
                                        });
                                    }}
                                    modal={mngEmpInfo.mailDesc}
                                />
                            </div>
                        )}
                        <div className="reg_contents">
                            <SFile
                                gridId={'reg_sr_proc_upload_grid'}
                                fileGrpId={mngEmpInfo.fileGrpdId}
                                isRefresh={mngEmpInfo.isRefresh}
                                tit={'첨부파일'}
                                useDownload={true}
                                useFileAttach={!disable}
                                useFileDelete={!disable}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default RegProc;
