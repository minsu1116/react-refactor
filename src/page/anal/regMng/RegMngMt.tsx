import React, {useEffect, useState, useMemo, useCallback} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import API from 'override/api/API';
import './style.scss';

import SModal from 'override/modal/SModal';
import SGridUtil from 'components/atoms/grid/SGridUtil';

// eslint-disable-next-line react/display-name
const RegMngMt = React.memo(() => {
    const sizeObj = useMemo(() => {
        return {
            width: '900',
            height: '820',
            isResize: false,
        };
    }, []);
    const [modalObj, seteModalObj] = useState<UserModal>();
    const {data} = Storage.getGridList();
    const srTypeGrid = data?.get('sr_type_mt_grid');

    const [modalOpen, setModalOpen] = useState(false);
    // const [searchObj, setSearchObj] = useState({
    //     equipMngEmpNm: userInfo.userNm,
    //     equipMngDeptNm: userInfo.deptNm,
    // });
    const [srTypeRowData, setSrTypeRowData] = useState([]);
    const [srType, setSrType] = useState('');
    const [mngMtRowData, setMngMtRowData] = useState([]);

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (srTypeGrid) {
            SGridUtil.setRowSpanning(
                srTypeColumnDefs,
                'categoryNm',
                null,
                srTypeGrid,
            );
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [srTypeGrid]);

    const srTypeColumnDefs = useMemo(() => {
        return [
            {
                headerName: '칸테고리',
                field: 'categoryNm',
                width: 200,
                editable: false,
                essential: false,
            },
            {
                headerName: '의뢰타입',
                field: 'srType',
                codeGroup: 'SR_TYPE',
                width: 200,
                editable: false,
                essential: false,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const mngColumnDefs = useMemo(() => {
        return [
            {
                headerName: '',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 32,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '의뢰타입',
                field: 'srType',
                codeGroup: 'SR_TYPE',
                width: 120,
                editable: false,
                essential: true,
            },
            {
                headerName: '담당자',
                field: 'mngEmpNm',
                width: 120,
                editable: false,
                essential: true,
            },
            {
                headerName: '담당자부서',
                field: 'mngDeptNm',
                width: 120,
                editable: false,
                essential: true,
            },
            {
                headerName: '담당자사번',
                field: 'mngEmpNo',
                width: 120,
                editable: false,
                essential: true,
                hide: true,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearchClick = async () => {
        const params = {
            cdType: 'SR_TYPE',
            useYn: 'Y',
        };

        const result = await API.request.post(
            `api/system/master/code/detail/get`,
            params,
        );

        if (result.data.success) {
            const tempList = [];
            result.data.data.forEach((element) => {
                const tempObj = {
                    srType: element.cd,
                    categoryNm: element.desc1,
                };
                tempList.push(tempObj);
            });
            setSrTypeRowData(tempList);
        }
    };

    const onSelectChange = async (param) => {
        if (param.data) {
            setSrType(param.data.srType);
            onSelectChangeDtlSearch(param.data.srType);
        }
    };

    const handleDelMng = async (param) => {
        const params = {
            param1: param.data,
            param2: {srType: srType},
        };

        const result = await API.request.post(`api/anal/mng-mt/delete`, params);

        if (result) {
            setMngMtRowData(result.data.data);
        } else {
            setMngMtRowData([]);
        }
    };

    const onCloseUser = useCallback(
        async (param) => {
            setModalOpen(false);
            if (param && param.data) {
                const saveList = [];
                param.data.forEach((element) => {
                    const tempObj = {
                        srType: srType,
                        mngEmpNo: element.empNo,
                        mngEmpNm: element.userNm,
                        mngDeptNm: element.deptNm,
                        mngPosNm: element.posNm,
                    };
                    saveList.push(tempObj);
                });

                const params = {
                    param1: saveList,
                    param2: {srType: srType},
                };

                const result = await API.request.post(
                    `api/anal/mng-mt/save`,
                    params,
                );

                if (result.data.success) {
                    setMngMtRowData(result.data.data);
                }
            }
        },
        [srType],
    );

    const onSelectChangeDtlSearch = async (srType) => {
        const param = {
            srType: srType,
        };

        const result = await API.request.post(`api/anal/mng-mt/get`, param);

        if (result.data.success) {
            setMngMtRowData(result.data.data);
        }
    };

    const onUserModalOpen = () => {
        const validationList = () => {
            const tempList = [];
            if (mngMtRowData) {
                mngMtRowData.forEach((element) => {
                    const tempObj = {
                        userId: element.mngEmpNo,
                    };
                    tempList.push(tempObj);
                });
            }
            return tempList;
        };
        seteModalObj({
            ...modalObj,
            mode: 'multi',
            modal: srType,
            validationList: validationList(),
        });

        setModalOpen(true);
    };

    return (
        <div className="contents_wrap_parents_h">
            <SModal
                tit={'담당자 등록'}
                param={modalObj}
                isOpen={modalOpen}
                sizeObj={sizeObj}
                path={'common/popup/user/User'}
                onClose={onCloseUser}
            />
            {/* <div className="search_wrap">
                 <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <TextField
                            id="equipMngEmpNm"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'관리자명'}
                            value={searchObj.equipMngEmpNm}
                            size="small"
                        />
                        <TextField
                            id="equipMngDeptNm"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'관리부서명'}
                            value={searchObj.equipMngDeptNm}
                            size="small"
                        />
                    </Box>
                </div> 
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div> */}
            <div className="contents_wrap_parents_w">
                <div className="contents_wrap">
                    <SGrid
                        grid={'sr_type_mt_grid'}
                        rowData={srTypeRowData}
                        title={'의뢰 목록'}
                        onSelectionChanged={onSelectChange}
                        columnDefs={srTypeColumnDefs}
                    />
                </div>
                <div className="contents_wrap_parents_h">
                    <div className="contents_wrap">
                        <SGrid
                            grid={'sr_mng_mt_grid'}
                            rowData={mngMtRowData}
                            title={'담당자 마스터'}
                            isModalOpen={true}
                            onModalOpen={onUserModalOpen}
                            onClickDelete={handleDelMng}
                            columnDefs={mngColumnDefs}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
});
export default RegMngMt;
