import React, {Suspense} from 'react';
import {Routes, Route, Navigate} from 'react-router-dom';
import {CircularProgress} from '@mui/material';

const Login = React.lazy(() => import('@page/login/Login'));
const SSO = React.lazy(() => import('@page/login/SSO'));

function UnauthorizedLayout() {
    return (
        <div className="unauthorized-layout">
            <Suspense fallback={<CircularProgress />}>
                <Routes>
                    <Route path="login" element={<Login />} />
                    <Route path="login/:id" element={<SSO />} />
                    <Route
                        path=""
                        element={<Navigate replace to="/auth/login" />}
                    />
                    <Route path="*" element={<Navigate replace to="login" />} />
                </Routes>
            </Suspense>
        </div>
    );
}

export default UnauthorizedLayout;
