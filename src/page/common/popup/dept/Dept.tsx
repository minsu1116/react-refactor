import React, {useEffect, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import API from 'override/api/API';

interface SearchObj {
    user: string;
    dept: string;
}

const SearchCompponent = (param) => {
    const handleSearchClick = param.handleSearchClick;
    const [searchObj, setSearchObj] = useState<SearchObj>({
        user: '',
        dept: '',
    });

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            e.preventDefault();
            await handleSearchClick(searchObj);
        }
    };
    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    return (
        <>
            <div className="search_wrap">
                <div className="search_contents">
                    <Box component="form" noValidate autoComplete="on">
                        <TextField
                            id="user"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'사번/이름'}
                            value={searchObj.user}
                            size="small"
                        />
                        <TextField
                            id="dept"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'부서명'}
                            value={searchObj.dept}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
        </>
    );
};

const Dept = (params) => {
    const tit = params.tit || '사용자 등록';
    const validationList = params?.initParam?.validationList || [];

    const [rowData, setRowData] = useState([]);

    const handleSearchClick = async (params) => {
        const searchParam = {
            user: params?.user || '',
            dept: params?.dept || '',
        };
        const result = await API.request.post(
            'api/helper/user/get',
            searchParam,
        );

        if (result.data.success) {
            const setList = [];
            if (validationList.length > 0) {
                result.data.data.map((item) => {
                    if (validationList.find((x) => x.userId == item.userId)) {
                        setRowData(result.data.data);
                    } else {
                        setList.push(item);
                    }
                });
                setRowData(setList);
            } else {
                setRowData(result.data.data);
            }
        }
    };

    const onClickSave = (param) => {
        if (params.onClose) {
            const result = {
                data: param.data,
            };
            params.onClose(result);
        }
    };

    const columnDefs = [
        {
            headerName: '',
            field: 'check',
            width: 32,
            type: 'checkbox',
        },
        {headerName: '부서명', field: 'deptNm', width: 170},
        {headerName: '직급', field: 'posNm', width: 120, align: 'center'},
        {headerName: '이메일', field: 'email', width: 150},
        {headerName: '전화번호', field: 'officeNo', width: 120},
        {
            headerName: '회사코드',
            field: 'compCd',
            hide: true,
        },
        {
            headerName: '부서코드',
            field: 'deptCd',
            hide: true,
        },
    ];

    return (
        <>
            <div className="popup_tit">{tit}</div>
            <SearchCompponent handleSearchClick={handleSearchClick} />
            <div className="contents_wrap h100">
                <SGrid
                    grid={'SysMngAuthBtnP_btnGrid'}
                    title={'사용자 정보'}
                    rowData={rowData}
                    height={500}
                    columnDefs={columnDefs}
                    onClickSave={onClickSave}
                />
            </div>
        </>
    );
};
export default Dept;
