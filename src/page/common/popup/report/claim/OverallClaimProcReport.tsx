/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports-localization';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';
import Storage from 'override/api/Storage';
import * as overallClaim_report_proc from '!json-loader!@/report/overallClaim/overallClaim_report_proc.rdlx-json';

function OverallClaimProcReport(data) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [previewDisabled, setPreviewDisabled] = useState(false);
    const viewerRef = useRef(null);

    const setData = useMemo(() => {
        return data.param.data;
    }, [data.param.data]);

    useEffect(() => {
        onPreview();
        // eslint-disable-next-line prettier/prettier, react-hooks/exhaustive-deps
      }, [])

    function onPreview() {
        const regStateNm = commonCodeGroup['REG_STATE'].filter(
            (x) => x.cd == setData.regState,
        )[0].cdNm;

        const gubunNm =
            setData.gubun == '01'
                ? '종합'
                : setData.gubun == '02'
                ? '사내 클레임'
                : '사외 클레임';

        setData.srMngModel.regStateNm = regStateNm;
        setData.srMngModel.keyword = setData.keyword;
        setData.srMngModel.gubunNm = gubunNm;
        setData.srMngModel.reportNm =
            setData.gubun == '01'
                ? `${gubunNm}분석 처리내역서`
                : `${gubunNm} 처리내역서`;

        let srProcEquipModelList = [];
        if (
            setData.srProcEquipModelList.length > 0 &&
            setData.srProcEquipModelList.filter((x) => x.regId).length > 0
        ) {
            srProcEquipModelList = setData.srProcEquipModelList.filter(
                (x) => x.regId,
            );
        } else {
            srProcEquipModelList.push({
                equipNm: '없음',
                equipEnNm: 'Empty',
            });
        }

        viewerRef.current.Viewer.open(overallClaim_report_proc, {
            ReportParams: [
                {
                    Name: 'MngInfo',
                    Value: [`[${JSON.stringify(setData.srMngModel)}]`],
                },
                {
                    Name: 'EquipInfo',
                    Value: [JSON.stringify(srProcEquipModelList)],
                },
            ],
        });
    }

    return (
        <div className="demo-app" style={{height: '780px', width: '100%'}}>
            <Viewer
                ref={viewerRef}
                documentLoaded={() => setPreviewDisabled(false)}
                reportLoaded={() => setPreviewDisabled(true)}
                language="ko"
            />
        </div>
    );
}

export default OverallClaimProcReport;
