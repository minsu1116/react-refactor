/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports-localization';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';
import Storage from 'override/api/Storage';
import * as environ_report from '!json-loader!@/report/environ/environ_report.rdlx-json';

function EnvironReport(data) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [categories, setCategories] = useState();
    const [previewDisabled, setPreviewDisabled] = useState(false);
    const viewerRef = useRef(null);

    const setData = useMemo(() => {
        return data.param.data;
    }, [data.param.data]);

    useEffect(() => {
        onPreview();
        // eslint-disable-next-line prettier/prettier, react-hooks/exhaustive-deps
      }, [])

    function onPreview() {
        const regStateNm = commonCodeGroup['REG_STATE'].filter(
            (x) => x.cd == setData.regState,
        )[0].cdNm;

        const gubunNm = setData.gubun == '01' ? 'UL성적서' : 'TUV성적서';
        const langGubunNm = setData.langGubun == '01' ? '국문' : '영문';

        const regModal = {
            regId: setData.regId,
            tit: setData.regTit,
            purpose: setData.purpose,
            equipNm: setData.equipNm,
            regTypeNm: setData.regTypeNm,
            gubunNm: gubunNm,
            regStateNm: regStateNm,
            pjtNm: setData.pjtNm,
            plCenterNm: setData.plCenterNm,
            costCenterNm: setData.costCenterNm,
            inputDtm: setData.inputDtm,
            regDtm: setData.regDtm,
            appoinDtm: setData.appoinDtm,
            mngDtm: setData.mngDtm,
            procDtm: setData.procDtm,
            comment: setData.comment,
            reportNm: ` 환경유해물질분석 의뢰서(${gubunNm})`,
            langGubunNm: langGubunNm,
            submitDep: setData.submitDep,
            address: setData.address,
        };

        viewerRef.current.Viewer.open(environ_report, {
            ReportParams: [
                {
                    Name: 'RegModal',
                    Value: [`[${JSON.stringify(regModal)}]`],
                },
                {
                    Name: 'Data',
                    Value: [JSON.stringify(setData.srDtlList)],
                },
                {
                    Name: 'RegUserInfo',
                    Value: [`[${JSON.stringify(setData.regUserInfo)}]`],
                },
            ],
        });
    }

    return (
        <div className="demo-app" style={{height: '780px', width: '100%'}}>
            <Viewer
                ref={viewerRef}
                documentLoaded={() => setPreviewDisabled(false)}
                reportLoaded={() => setPreviewDisabled(true)}
                language="ko"
            />
        </div>
    );
}

export default EnvironReport;
