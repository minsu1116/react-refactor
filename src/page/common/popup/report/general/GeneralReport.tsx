/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports-localization';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';
import Storage from 'override/api/Storage';
import * as general_report from '!json-loader!@/report/general/general_report.rdlx-json';

function GeneralReport(data) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [previewDisabled, setPreviewDisabled] = useState(false);
    const viewerRef = useRef(null);

    const setData = useMemo(() => {
        return data.param.data;
    }, [data.param.data]);

    useEffect(() => {
        onPreview();
        // eslint-disable-next-line prettier/prettier, react-hooks/exhaustive-deps
      }, [])

    function onPreview() {
        const regStateNm = commonCodeGroup['REG_STATE'].filter(
            (x) => x.cd == setData.regState,
        )[0].cdNm;

        const regModal = {
            regId: setData.regId,
            tit: setData.regTit,
            keyword: setData.keyword,
            purpose: setData.purpose,
            equipNm: setData.equipNm,
            regTypeNm: setData.regTypeNm,
            comment: setData.comment,
            regStateNm: regStateNm,
            inputDtm: setData.inputDtm,
            regDtm: setData.regDtm,
            appoinDtm: setData.appoinDtm,
            mngDtm: setData.mngDtm,
            procDtm: setData.procDtm,
            reportNm: '일반분석 의뢰서',
        };

        viewerRef.current.Viewer.open(general_report, {
            ReportParams: [
                {
                    Name: 'RegModal',
                    Value: [`[${JSON.stringify(regModal)}]`],
                },
                {
                    Name: 'Data',
                    Value: [JSON.stringify(setData.srDtlList)],
                },
                {
                    Name: 'RegUserInfo',
                    Value: [`[${JSON.stringify(setData.regUserInfo)}]`],
                },
            ],
        });
    }

    return (
        <div className="demo-app" style={{height: '780px', width: '100%'}}>
            <Viewer
                ref={viewerRef}
                documentLoaded={() => setPreviewDisabled(false)}
                reportLoaded={() => setPreviewDisabled(true)}
                language="ko"
            />
        </div>
    );
}

export default GeneralReport;
