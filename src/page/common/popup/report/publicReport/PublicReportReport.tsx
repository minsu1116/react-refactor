/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports-localization';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';
import Storage from 'override/api/Storage';
import * as public_report from '!json-loader!@/report/public/public_report.rdlx-json';

function PublicReportReport(data) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [categories, setCategories] = useState();
    const [previewDisabled, setPreviewDisabled] = useState(false);
    const viewerRef = useRef(null);

    const setData = useMemo(() => {
        return data.param.data;
    }, [data.param.data]);

    useEffect(() => {
        onPreview();
        // eslint-disable-next-line prettier/prettier, react-hooks/exhaustive-deps
      }, [])

    function onPreview() {
        let addressNm = '';
        const regStateNm = commonCodeGroup['REG_STATE'].filter(
            (x) => x.cd == setData.regState,
        )[0].cdNm;

        if (setData.langGubun == '01') {
            addressNm = commonCodeGroup['ADDRESS_KR'].filter(
                (x) => x.cd == setData.address,
            )[0].cdNm;
        } else {
            addressNm = commonCodeGroup['ADDRESS_EN'].filter(
                (x) => x.cd == setData.address,
            )[0].cdNm;
        }

        const gubunNm =
            setData.gubun == '01'
                ? 'UL성적서'
                : setData.gubun == '02'
                ? 'TUV성적서'
                : setData.gubun == '03'
                ? '내부(관리용)'
                : '내부(제출용)';
        const langGubunNm = setData.langGubun == '01' ? '국문' : '영문';

        const regModal = {
            regId: setData.regId,
            tit: setData.regTit,
            purpose: setData.purpose,
            equipNm: setData.equipNm,
            regTypeNm: setData.regTypeNm,
            gubunNm: gubunNm,
            regStateNm: regStateNm,
            pjtNm: setData.pjtNm,
            plCenterNm: setData.plCenterNm,
            costCenterNm: setData.costCenterNm,
            inputDtm: setData.inputDtm,
            regDtm: setData.regDtm,
            appoinDtm: setData.appoinDtm,
            mngDtm: setData.mngDtm,
            procDtm: setData.procDtm,
            comment: setData.comment,
            reportNm: `공인성적서 요청서(${gubunNm})`,
            langGubunNm: langGubunNm,
            submitDep: setData.submitDep,
            address: addressNm,
        };

        if (setData.apprModal.apprGubun) {
            setData.apprModal.apprGubunNm =
                setData.apprModal.apprGubun == 'APPR' ? '승인' : '부결';
        } else {
            setData.apprModal.apprGubunNm = '미결';
        }

        viewerRef.current.Viewer.open(public_report, {
            ReportParams: [
                {
                    Name: 'RegModal',
                    Value: [`[${JSON.stringify(regModal)}]`],
                },
                {
                    Name: 'Data',
                    Value: [JSON.stringify(setData.srDtlList)],
                },
                {
                    Name: 'RegUserInfo',
                    Value: [`[${JSON.stringify(setData.regUserInfo)}]`],
                },
                {
                    Name: 'RegApprInfo',
                    Value: [`[${JSON.stringify(setData.apprModal)}]`],
                },
            ],
        });
    }

    return (
        <div className="demo-app" style={{height: '780px', width: '100%'}}>
            <Viewer
                ref={viewerRef}
                documentLoaded={() => setPreviewDisabled(false)}
                reportLoaded={() => setPreviewDisabled(true)}
                language="ko"
            />
        </div>
    );
}

export default PublicReportReport;
