/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports-localization';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';
import Storage from 'override/api/Storage';
import * as util_report_proc from '!json-loader!@/report/util/util_report_proc.rdlx-json';

function UtilProcReport(data) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [previewDisabled, setPreviewDisabled] = useState(false);
    const viewerRef = useRef(null);

    const setData = useMemo(() => {
        return data.param.data;
    }, [data.param.data]);

    useEffect(() => {
        onPreview();
        // eslint-disable-next-line prettier/prettier, react-hooks/exhaustive-deps
      }, [])

    function onPreview() {
        const regStateNm = commonCodeGroup['REG_STATE'].filter(
            (x) => x.cd == setData.regState,
        )[0].cdNm;
        setData.srMngModel.regStateNm = regStateNm;
        setData.srMngModel.keyword = setData.keyword;
        setData.srMngModel.gubunNm = setData.gubun == '01' ? '보통' : '지급';
        setData.srMngModel.reportNm = '특정설비분석 처리내역서';

        viewerRef.current.Viewer.open(util_report_proc, {
            ReportParams: [
                {
                    Name: 'MngInfo',
                    Value: [`[${JSON.stringify(setData.srMngModel)}]`],
                },
            ],
        });
    }

    return (
        <div className="demo-app" style={{height: '780px', width: '100%'}}>
            <Viewer
                ref={viewerRef}
                documentLoaded={() => setPreviewDisabled(false)}
                reportLoaded={() => setPreviewDisabled(true)}
                language="ko"
            />
        </div>
    );
}

export default UtilProcReport;
