/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports-localization';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';
import * as util_02_report from '!json-loader!@/report/util/util_02_report.rdlx-json';
import * as util_01_report from '!json-loader!@/report/util/util_01_report.rdlx-json';
import Storage from 'override/api/Storage';

function UtilReport(data) {
    const commonCodeGroup = Storage.getGroupCode().data;
    const [categories, setCategories] = useState();
    const [previewDisabled, setPreviewDisabled] = useState(false);
    const viewerRef = useRef(null);

    const setData = useMemo(() => {
        return data.param.data;
    }, [data.param.data]);

    useEffect(() => {
        onPreview();
        // eslint-disable-next-line prettier/prettier, react-hooks/exhaustive-deps
      }, [])

    function onPreview() {
        const regStateNm = commonCodeGroup['REG_STATE'].filter(
            (x) => x.cd == setData.regState,
        )[0].cdNm;

        const regModal = {
            regId: setData.regId,
            tit: setData.regTit,
            productNm: setData.productNm,
            keyword: setData.keyword,
            purpose: setData.purpose,
            equipNm: setData.equipNm,
            regTypeNm: setData.regTypeNm,
            gubunNm: setData.gubun == '01' ? '보통' : '지급',
            comment: setData.comment,
            regStateNm: regStateNm,
            pjtNm: setData.pjtNm,
            plCenterNm: setData.plCenterNm,
            costCenterNm: setData.costCenterNm,
            inputDtm: setData.inputDtm,
            regDtm: setData.regDtm,
            appoinDtm: setData.appoinDtm,
            mngDtm: setData.mngDtm,
            procDtm: setData.procDtm,
            reportNm: '특정설비분석 의뢰서',
        };

        if (setData.gubun == '02' && setData.apprModal) {
            if (setData.apprModal?.apprGubun) {
                setData.apprModal.apprGubunNm =
                    setData.apprModal?.apprGubun == 'APPR' ? '승인' : '부결';
            } else {
                setData.apprModal.apprGubunNm = '미결';
            }

            viewerRef.current.Viewer.open(util_02_report, {
                ReportParams: [
                    {
                        Name: 'RegModal',
                        Value: [`[${JSON.stringify(regModal)}]`],
                    },
                    {
                        Name: 'Data',
                        Value: [JSON.stringify(setData.srDtlList)],
                    },
                    {
                        Name: 'RegUserInfo',
                        Value: [`[${JSON.stringify(setData.regUserInfo)}]`],
                    },
                    {
                        Name: 'RegApprInfo',
                        Value: [`[${JSON.stringify(setData.apprModal)}]`],
                    },
                ],
            });
        } else {
            viewerRef.current.Viewer.open(util_01_report, {
                ReportParams: [
                    {
                        Name: 'RegModal',
                        Value: [`[${JSON.stringify(regModal)}]`],
                    },
                    {
                        Name: 'Data',
                        Value: [JSON.stringify(setData.srDtlList)],
                    },
                    {
                        Name: 'RegUserInfo',
                        Value: [`[${JSON.stringify(setData.regUserInfo)}]`],
                    },
                ],
            });
        }
    }

    return (
        <div className="demo-app" style={{height: '780px', width: '100%'}}>
            <Viewer
                ref={viewerRef}
                documentLoaded={() => setPreviewDisabled(false)}
                reportLoaded={() => setPreviewDisabled(true)}
                language="ko"
            />
        </div>
    );
}

export default UtilReport;
