import React, {useEffect, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import API from 'override/api/API';
import Storage from '@override/api/Storage';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import {confirm} from 'override/alert/Alert';

interface SearchObj {
    user: string;
    dept: string;
}

const SearchCompponent = (param) => {
    const handleSearchClick = param.handleSearchClick;
    const [searchObj, setSearchObj] = useState<SearchObj>({
        user: '',
        dept: '',
    });

    useEffect(() => {
        if (param) {
            handleSearchClick();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            e.preventDefault();
            await handleSearchClick(searchObj);
        }
    };
    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    return (
        <>
            <div className="search_wrap">
                <div className="search_contents">
                    <Box component="form" noValidate autoComplete="on">
                        <TextField
                            id="user"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'사번/이름'}
                            value={searchObj.user}
                            size="small"
                        />
                        <TextField
                            id="dept"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'부서명'}
                            value={searchObj.dept}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
        </>
    );
};

const User = (params) => {
    const {data} = Storage.getGridList();
    const [renderObj, setRenderObj] = useState({
        tit: params.tit || '사용자 등록',
        mode: params?.param?.mode ? params.param.mode : 'multi',
        validationList: params?.param?.validationList
            ? params?.param?.validationList
            : [],
        userGrid: null,
    });
    const [selectedObj, setSelectedObj] = useState({});
    const [rowData, setRowData] = useState([]);
    const [columnDefs, setColumnDefs] = useState(null);
    useEffect(() => {
        setRenderObj({
            ...renderObj,
            tit: params.tit || '사용자 등록',
            mode: params?.param?.mode ? params.param.mode : 'multi',
            validationList: params?.param?.validationList
                ? params?.param?.validationList
                : [],
            userGrid: data?.get('popup_user_grid'),
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);
    useEffect(() => {
        setColumnDefs(() => {
            if (renderObj.mode == 'single') {
                return [
                    {
                        headerName: '이름',
                        field: 'userNm',
                        width: 150,
                        align: 'center',
                    },
                    {headerName: '부서명', field: 'deptNm', width: 110},
                    {
                        headerName: '직급',
                        field: 'posNm',
                        width: 100,
                        align: 'center',
                    },
                    {headerName: '이메일', field: 'email', width: 150},
                    {headerName: '전화번호', field: 'officeNo', width: 110},
                    {
                        headerName: '사번',
                        field: 'empNo',
                        key: true,
                        hide: true,
                    },
                ];
            } else {
                return [
                    {
                        headerName: '',
                        field: 'check',
                        width: 32,
                        type: 'checkbox',
                    },
                    {
                        headerName: '이름',
                        field: 'userNm',
                        width: 150,
                        align: 'center',
                    },
                    {headerName: '부서명', field: 'deptNm', width: 110},
                    {
                        headerName: '직급',
                        field: 'posNm',
                        width: 100,
                        align: 'center',
                    },
                    {headerName: '이메일', field: 'email', width: 150},
                    {headerName: '전화번호', field: 'officeNo', width: 110},
                    {
                        headerName: '사번',
                        field: 'empNo',
                        key: true,
                        hide: true,
                    },
                ];
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearchClick = async (params) => {
        const searchParam = {
            user: params?.user || '',
            dept: params?.dept || '',
        };
        const result = await API.request.post(
            'api/helper/user/get',
            searchParam,
        );

        if (result.data.success) {
            const setList = [];
            if (renderObj.validationList.length > 0) {
                result.data.data.map((item) => {
                    if (
                        renderObj.validationList.find(
                            (x) => x.userId == item.userId,
                        )
                    ) {
                        setRowData(result.data.data);
                    } else {
                        setList.push(item);
                    }
                });
                setRowData(setList);
            } else {
                setRowData(result.data.data);
            }
        }
    };

    const onClickSave = (param) => {
        if (renderObj.mode != 'single') {
            const result = {
                data: param.data,
            };
            params.onClose(result);
        } else {
            params.onClose(selectedObj);
        }
    };

    const handleChoiceClick = async (result) => {
        if (params.onClose) {
            if (result.data) {
                const confirmResult = await confirm({
                    title: `[${result.data.userNm}]님을 결재자로 선택 하시겠습니까?`,
                    html: '',
                });
                if (confirmResult) {
                    const returnValue = {
                        data: result.data,
                    };
                    params.onClose(returnValue);
                }
            }
        }
    };

    const onSelectChange = async () => {
        if (data) {
            if (renderObj.userGrid && renderObj.userGrid.getSelectedRows()) {
                setSelectedObj({
                    data: renderObj.userGrid.getSelectedRows()[0],
                });
            }
        }
    };

    return (
        <>
            <div className="popup_tit">{renderObj.tit}</div>
            <SearchCompponent handleSearchClick={handleSearchClick} />
            <div className="contents_wrap h100">
                {columnDefs ? (
                    <SGrid
                        grid={'popup_user_grid'}
                        title={'사용자 정보'}
                        rowData={rowData}
                        height={500}
                        columnDefs={columnDefs}
                        onClickSave={
                            renderObj.mode == 'multi' ? onClickSave : null
                        }
                        onSelectionChanged={onSelectChange}
                        rowDoubleClick={handleChoiceClick}
                    />
                ) : null}
            </div>
        </>
    );
};
export default User;
