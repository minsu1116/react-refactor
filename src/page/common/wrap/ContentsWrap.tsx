import {Box} from '@mui/material';
import React from 'react';

const ContentsWrap = (children) => {
    return <Box className="contents_wrap_parents_h">{children.children}</Box>;
};
export default ContentsWrap;
