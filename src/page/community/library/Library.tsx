import React, {useEffect, useMemo, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import API from 'override/api/API';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SModal from 'override/modal/SModal';
import {meg} from 'override/alert/Alert';

import './style.scss';

const Library = () => {
    const {data} = Storage.getGridList();
    const commonCodeGroup = Storage.getGroupCode().data;
    const {data: userInfo} = Storage.getUserInfo();
    const [readOnly, setReadOnly] = useState(false);

    const [searchObj, seteSearchObj] = useState({
        lbyTit: '',
        orgFileNm: '',
    });
    const [rowData, setRowData] = useState([]);
    const [modalOpen, setModalOpen] = useState(false);
    const [modalObj, setModalObj] = useState();
    const sizeObj = useMemo(() => {
        return {
            width: '750',
            height: '820',
            isResize: false,
        };
    }, []);

    useEffect(() => {
        if (commonCodeGroup) {
            if (
                !commonCodeGroup['COMM_REG_GROUP'].find(
                    (x) => x.cd == userInfo.empNo,
                )
            ) {
                setReadOnly(true);
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(`api/comty/lby/get`, param);

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onSearchObjChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        seteSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const handleFileDelete = async () => {
        const fileGrid = data?.get('lby_grid');
        const rows = [];

        if (fileGrid) {
            fileGrid.getModel().rowsToDisplay.map((item) => {
                if (item.data) {
                    rows.push(item.data);
                }
            });

            const delList = rows.filter((x) => x.check == 'Y');
            if (delList.length == 0) {
                meg({
                    title: '삭제 할 항목이 없습니다.',
                    html: '',
                });
                return;
            }

            const apiDeleteList = rows.filter(
                (x) => !x.newRow && x.check == 'Y',
            );
            apiDeleteList.forEach(async (element) => {
                await API.request.get(
                    `/api/file/delete?fileId=${element.fileId}`,
                );
            });
            const rowData = rows.filter((x) => x.check != 'Y');
            setRowData(rowData);
        }
    };

    const onLbyModalOpen = async () => {
        setModalOpen(true);
    };

    const onLbyModalClose = async () => {
        setModalOpen(false);
        // handleSearchClick();
    };

    const handleDoubleClick = (e) => {
        if (!modalObj && e.node.group) {
            setModalObj(e.node.childrenAfterGroup[0].data);
        }
        onLbyModalOpen();
    };

    const onSelectChange = async () => {
        if (data) {
            const fileGrid = data?.get('lby_grid');
            if (fileGrid && fileGrid.getSelectedRows()) {
                setModalObj(fileGrid.getSelectedRows()[0]);
            }
        }
    };

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '제목',
                field: 'lbyTit',
                width: 200,
                editable: true,
                essential: true,
                hide: true,
                rowGroup: true,
            },
            {
                headerName: '선택',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 60,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '파일명',
                field: 'fileId',
                width: 200,
                type: 'download',
                fileNm: 'orgFileNm',
                cellRendererParams: {byFileId: true},
                editable: false,
            },
            {
                headerName: '게시자',
                field: 'inputEmpNm',
                align: 'center',
                width: 90,
                editable: false,
            },
            {
                headerName: '게시일',
                field: 'inputDtm',
                width: 130,
                type: 'dateTimeFull',
                editable: false,
            },
            {
                headerName: '자료실ID',
                field: 'lbyId',
                essential: true,
                hide: true,
                editable: false,
            },
            {
                headerName: '파일그룹ID',
                field: 'fileGroupId',
                hide: true,
                editable: false,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="contents_wrap_parents_h">
            <SModal
                tit={'자료 등록'}
                param={modalObj}
                isOpen={modalOpen}
                sizeObj={sizeObj}
                path={'community/library/popup/LibraryP'}
                onClose={onLbyModalClose}
            />
            <div className="library_main">
                <ContentsWrap>
                    <div className="search_wrap">
                        <div className="search_contents">
                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': {m: 1, width: '25ch'},
                                }}
                                noValidate
                                autoComplete="off">
                                <TextField
                                    id="lbyTit"
                                    onChange={onSearchObjChange}
                                    onKeyDown={handleKeyDown}
                                    label={'제목'}
                                    // value={searchObj.equipMngEmpNm}
                                    size="small"
                                />
                                <TextField
                                    id="orgFileNm"
                                    onChange={onSearchObjChange}
                                    onKeyDown={handleKeyDown}
                                    label={'파일명'}
                                    // value={searchObj.equipMngDeptNm}
                                    size="small"
                                />
                            </Box>
                        </div>
                        <div className="search_btns">
                            <Tooltip title="조회">
                                <IconButton
                                    onClick={handleSearchClick}
                                    className="btn_group">
                                    <ManageSearchIcon className="btn_icon" />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </div>
                    <div className="contents_wrap">
                        <SGrid
                            grid={'lby_grid'}
                            rowData={rowData}
                            isModalOpen={!readOnly}
                            onModalOpen={() => {
                                setModalObj(null);
                                onLbyModalOpen();
                            }}
                            title={'자료실 목록'}
                            onSelectionChanged={onSelectChange}
                            onClickDelete={!readOnly ? handleFileDelete : null}
                            rowDoubleClick={handleDoubleClick}
                            columnDefs={columnDefs}
                        />
                    </div>
                </ContentsWrap>
            </div>
        </div>
    );
};
export default Library;
