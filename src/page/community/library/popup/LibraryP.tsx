import React, {useEffect, useState} from 'react';
import {Seditor} from 'components/atoms/editor/Seditor';
import SSelectbox from 'components/atoms/select/SSelectbox';
import Storage from '@override/api/Storage';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import {validation} from '@override/alert/Alert';
import TextField from '@mui/material/TextField';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import API from 'override/api/API';
import './style.scss';
import ContentsWrap from 'page/common/wrap/ContentsWrap';

interface Param {
    readOnly: boolean;
    tit: string;
    param?: LbyModal;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
}

interface LbyModal {
    lbyTit: string;
    lbyConts: string;
    shareGroup: string;
    lbyId?: number;
    fileGrpId?: string;
}

const LibraryP = (param: Param) => {
    const {data} = Storage.getGridList();
    const [modal, setModal] = useState<LbyModal>({
        lbyTit: param.param ? param.param.lbyTit : '',
        lbyConts: param.param ? param.param.lbyConts : '',
        shareGroup: param.param ? param.param.shareGroup : '',
        lbyId: param.param ? param.param.lbyId : 0,
        fileGrpId: param.param ? param.param.fileGrpId : '',
    });

    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onChange = (e) => {
        const newModal = {
            ...modal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setModal(newModal);
    };

    const handleChange = (e) => {
        setModal({
            ...modal,
            [e.target.id]: e.target.value,
        });
    };

    const handleContetnsChange = (value) => {
        modal.lbyConts = value;
    };

    const onSaveNotice = async () => {
        const fileUploadGrid = data?.get('lby_upload_grid');
        const fileSaveList = [];

        const validationKeyList = {
            lbyTit: '제목',
            lbyConts: '내용',
            shareGroup: '공유대상',
            fileGrpId: '첨부파일',
        };
        if (fileUploadGrid) {
            fileUploadGrid
                ?.getModel()
                ?.rowsToDisplay.map((item) => fileSaveList.push(item.data));
            if (fileSaveList.length > 0) {
                const result = await saveFilesEditAll(fileSaveList);
                if (result) {
                    modal.fileGrpId = result;
                }
            }
        }
        const isTrue = await validation(validationKeyList, modal);

        if (isTrue) {
            const params = {
                param1: modal,
                param2: modal,
            };
            const result = await API.request.post(`api/comty/lby/save`, params);

            if (result.data.success) {
                param.onClose(result.data.data);
            }
        }
    };
    return (
        <div id="library_p_main">
            <ContentsWrap>
                <div className="library">
                    <div className="popup_btns">
                        <Tooltip title="저장">
                            <IconButton
                                onClick={onSaveNotice}
                                className="btn_group">
                                <SaveAltIcon className="btn_icon" />
                            </IconButton>
                        </Tooltip>
                    </div>
                    <div className="top_contents">
                        <div className="contents">
                            {/* <TextField
                                id="title"
                                onChange={onChange}
                                value={modal.title}
                                placeholder="타이틀을 입력해 주십시오."
                                size="small"
                            /> */}
                            <TextField
                                id="lbyTit"
                                onChange={onChange}
                                value={modal.lbyTit}
                                size="small"
                                label="제목"
                                placeholder="제목을 입력해 주십시오."
                            />
                        </div>
                    </div>
                    <div className="top_contents">
                        <div className="contents">
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'NOTICE_GROUP'}
                                label={'공유대상'}
                                value={modal.shareGroup}
                                id={'shareGroup'}
                                handleChange={handleChange}
                            />
                        </div>
                    </div>
                </div>
                <div className="library">
                    <div className="contents">
                        <div className="contents">
                            <Seditor
                                readOnly={param.readOnly}
                                modal={modal.lbyConts}
                                tit={'내용'}
                                handleChange={handleContetnsChange}
                            />
                        </div>
                    </div>
                    <div className="top_contents" style={{top: 0}}>
                        <div className="contents">
                            <SFile
                                gridId={'lby_upload_grid'}
                                fileGrpId={modal.fileGrpId}
                                tit={'첨부파일'}
                                useDownload={true}
                                useFileAttach={true}
                                useFileDelete={true}
                            />
                        </div>
                    </div>
                </div>
            </ContentsWrap>
        </div>
    );
};
export default LibraryP;
