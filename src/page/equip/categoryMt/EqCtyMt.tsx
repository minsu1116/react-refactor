import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import SGrid from 'components/atoms/grid/SGrid';
import API from 'override/api/API';
import React, {useEffect, useState} from 'react';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Storage from '@override/api/Storage';
import SGridUtil from 'components/atoms/grid/SGridUtil';

const EquipMt = (param) => {
    const {data} = Storage.getGridList();
    const eqMtGrid = data?.get('quip_cty_mt_grid');

    useEffect(() => {
        if (eqMtGrid) {
            SGridUtil.setRowSpanning(columnDefs, 'equipNm', null, eqMtGrid);
            // eqMtGrid.setRowData(rowData);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [eqMtGrid]);

    const [searchObj, setSearchObj] = useState({
        equipNm: '',
    });
    const [rowData, setRowData] = useState([]);

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columnDefs: Array<ColumnDefs> = [
        {
            headerName: '장비명(KR)',
            field: 'equipNm',
            width: 120,
            editable: false,
            essential: true,
        },
        {
            headerName: '장비명(EN)',
            field: 'equipEnNm',
            width: 120,
            editable: false,
        },
        {headerName: '장비ID', field: 'equipId', hide: true, editable: false},
    ];

    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(`api/equip/equip-mt/get`, param);

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const handleChoiceClick = (params) => {
        if (params.data) {
            if (param.onClose) {
                param.onClose(params.data);
            }
            return params.data;
        }
    };

    return (
        <>
            {param.tit ? <div className="popup_tit">{param.tit}</div> : null}
            <div className="contents_wrap_parents_h" style={{width: '350px'}}>
                <div className="search_wrap">
                    <div className="search_contents">
                        <Box
                            component="form"
                            sx={{
                                '& > :not(style)': {m: 1, width: '25ch'},
                            }}
                            noValidate
                            autoComplete="off">
                            <TextField
                                id="equipNm"
                                onChange={onChange}
                                onKeyDown={handleKeyDown}
                                label={'장비명'}
                                value={searchObj.equipNm}
                                size="small"
                            />
                        </Box>
                    </div>
                    <div className="search_btns">
                        <Tooltip title="조회">
                            <IconButton
                                onClick={handleSearchClick}
                                className="btn_group">
                                <ManageSearchIcon className="btn_icon" />
                            </IconButton>
                        </Tooltip>
                    </div>
                </div>
                <div className="contents_wrap">
                    <SGrid
                        grid={'equip_cty_mt_grid'}
                        rowData={rowData}
                        isAddBtnOpen={false}
                        title={'장비 마스터 목록'}
                        newRowDefault={{
                            sort: 'auto',
                            useYn: 'Y',
                        }}
                        height={500}
                        width={'500'}
                        rowDoubleClick={handleChoiceClick}
                        columnDefs={columnDefs}
                    />
                </div>
            </div>
        </>
    );
};

const keybordEvent = (params) => {
    if (params.event.which == 46 || params.event.which == 8) {
        params.data.formId = '';
        params.api.redrawRows();
    }
};
const ctyMtColumnDefs: Array<ColumnDefs> = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
        editable: false,
    },
    {
        headerName: '카테고리명',
        field: 'ctyNm',
        width: 120,
        editable: true,
        essential: true,
    },
    {
        headerName: '카테고리 설명',
        field: 'ctyDesc',
        width: 250,
        editable: true,
    },
    {
        headerName: '순서',
        field: 'sort',
        width: 70,
        type: 'number',
        editable: true,
    },
    {
        headerName: '카테고리ID',
        field: 'ctyId',
        hide: true,
        editable: false,
    },
];

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const ctyDtlColumnDefs: Array<ColumnDefs> = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
        editable: false,
    },
    {
        headerName: '카테고리명',
        field: 'ctyNm',
        width: 120,
        editable: false,
        essential: true,
    },
    {
        headerName: '장비',
        field: 'equipNm',
        width: 200,
        editable: true,
        key: true,
        essential: true,
        type: 'iconButton',
        buttonIcon: 'icon-search',
        // onButtonClick: buttonClick,
        suppressKeyboardEvent: keybordEvent,
        Modal: <EquipMt tit={'장비 선택'} />,
    },
    {
        headerName: '순서',
        field: 'sort',
        width: 70,
        type: 'number',
        editable: true,
    },
    {
        headerName: '카테고리ID',
        field: 'ctyId',
        essential: true,
        hide: true,
        editable: false,
    },
];

const EqCtyMt = () => {
    const [searchObj, setSearchObj] = useState({
        ctyNm: '',
    });

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [selectObj, setSelectObj] = useState({
        ctyId: '',
        ctyNm: '',
    });

    const {data} = Storage.getGridList();
    const ctyDtlGrid = data?.get('equip_mt_cty_dtl_grid');

    useEffect(() => {
        if (ctyDtlGrid) {
            SGridUtil.setRowSpanning(
                ctyDtlColumnDefs,
                'ctyNm',
                null,
                ctyDtlGrid,
            );
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ctyDtlGrid]);

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const [rowMtData, setRowMtData] = useState([]);
    const [rowDtlData, setRowDtlData] = useState([]);

    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(
            `api/equip/equip-cty/mt/get`,
            param,
        );

        if (result.data.success) {
            setRowMtData(result.data.data);
        }
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const onSelectChange = async (param) => {
        if (param.data) {
            selectObj.ctyId = param.data.ctyId;
            selectObj.ctyNm = param.data.ctyNm;
            await onChangeSearchDtl(selectObj);
        }
    };

    const onChangeSearchDtl = async (param) => {
        const result = await API.request.post(
            `api/equip/equip-cty/dtl/get`,
            param,
        );

        if (result) {
            setRowDtlData(result.data.data);
        }
    };

    const equipCtyMtSave = async (param) => {
        const params = JSON.stringify({param1: param.data, param2: searchObj});
        const result = await API.request.post(
            `api/equip/equip-cty/mt/save`,
            params,
        );

        if (result.data.success) {
            setRowMtData(result.data.data);
        }
    };

    const equipCtyDtlSave = async (param) => {
        const params = JSON.stringify({param1: param.data, param2: selectObj});
        const result = await API.request.post(
            `api/equip/equip-cty/dtl/save`,
            params,
        );

        if (result.data.success) {
            setRowDtlData(result.data.data);
        }
    };

    const equipCtyMtDelete = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };

        const result = await API.request.post(
            `api/equip/equip-cty/mt/delete`,
            params,
        );

        if (result) {
            setRowMtData(result.data.data);
        } else {
            setRowMtData([]);
        }
    };

    const equipCtyDtlDelete = async (param) => {
        const params = {
            param1: param.data,
            param2: selectObj,
        };

        const result = await API.request.post(
            `api/equip/equip-cty/dtl/delete`,
            params,
        );

        if (result) {
            setRowDtlData(result.data.data);
        }
    };

    return (
        <div className="contents_wrap_parents_h">
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <TextField
                            id="ctyNm"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'기능명'}
                            value={searchObj.ctyNm}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap_parents_w">
                <div className="contents_wrap" style={{width: '955px'}}>
                    <SGrid
                        grid={'equip_mt_cty_mt_grid'}
                        rowData={rowMtData}
                        isAddBtnOpen={true}
                        onClickSave={equipCtyMtSave}
                        onClickDelete={equipCtyMtDelete}
                        title={'기능 마스터 목록'}
                        newRowDefault={{
                            sort: 'auto',
                        }}
                        onSelectionChanged={onSelectChange}
                        columnDefs={ctyMtColumnDefs}
                    />
                </div>
                <div className="contents_wrap">
                    <SGrid
                        grid={'equip_mt_cty_dtl_grid'}
                        rowData={rowDtlData}
                        isAddBtnOpen={true}
                        title={
                            selectObj.ctyId
                                ? selectObj.ctyNm + '의 장비 목록'
                                : '카테고리를 먼저 선택 및 저장 해 주십시오.'
                        }
                        newRowDefault={{
                            sort: 'auto',
                            ctyId: selectObj.ctyId,
                            ctyNm: selectObj.ctyNm,
                        }}
                        onClickSave={equipCtyDtlSave}
                        onClickDelete={equipCtyDtlDelete}
                        columnDefs={ctyDtlColumnDefs}
                    />
                </div>
            </div>
        </div>
    );
};
export default EqCtyMt;
