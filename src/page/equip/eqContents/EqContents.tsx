<<<<<<< HEAD
import React, {useMemo, useRef, useState} from 'react';

import {Viewer} from '@grapecity/activereports-react';
import '@grapecity/activereports/styles/ar-js-ui.css';
import '@grapecity/activereports/styles/ar-js-viewer.css';

function EqContents({words}) {
    const report = {
        Name: 'Report',
        Type: 'report',
        Width: '9.7215in',
        Body: {
            Name: 'Body',
            Type: 'section',
            ReportItems: [
                {
                    Type: 'textbox',
                    Name: 'textbox1',
                    Value: 'Hello from ActiveReports23213123123',
                    Height: '10in',
                },
                {
                    Type: 'textbox',
                    Name: 'textbox2',
                    Value: 'Hello from ActiveReports444444444444',
                    Height: '20in',
                },
                {
                    Type: 'textbox',
                    Name: 'textbox3',
                    Value: 'Hello from ActiveReports255555',
                    Height: '30in',
                },
                {
                    Type: 'textbox',
                    Name: 'textbox4',
                    Value: 'Hello from ActiveReports11111111',
                    Height: '40in',
                },
                {
                    Type: 'textbox',
                    Name: 'textbox5',
                    Value: 'Hello from ActiveReports1222222222222222222222',
                    Height: '50in',
                },
            ],
        },
    };
    return (
        <>
            <Viewer report={{Uri: report}} />
        </>
    );
}

=======
import React, {useEffect, useMemo, useState} from 'react';
import API from 'override/api/API';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import './style.scss';
import SModal from 'override/modal/SModal';

import research from '@img/refresh.png';

interface EqInfoModalObj {
    readOnly: boolean;
    isModalOpen: boolean;
    width?: number;
    height?: number;
    param?: EqModal;
}

interface EqModal {
    equipNm: string;
    equipId: number;
    contsWidth: string;
    contsHeight: string;
}

function EqContents() {
    const [modalObj, setModalObj] = useState<EqInfoModalObj>({
        isModalOpen: false,
        readOnly: true,
    });
    const [contenstObj, setContenstObj] = useState([]);

    useEffect(() => {
        handleSearchCtns();
    }, []);

    const handleSearchCtns = async () => {
        const params = {};
        const result = await API.request.post(
            `api/equip/equip-ctns/get`,
            params,
        );

        if (result.data.success) {
            setContenstObj(result.data.data);
        }
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const openEqInfoP = (param) => {
        setModalObj({
            ...modalObj,
            param: param,
            isModalOpen: true,
        });
    };

    const onCloseEqInfo = () => {
        setModalObj({
            ...modalObj,
            isModalOpen: false,
        });
    };

    const Contents = useMemo(() => {
        return (
            <div className="equip_contents_main">
                {contenstObj.map((element, index) => {
                    return (
                        <div
                            key={`contents_${index}`}
                            className={'equip_contents_mid'}>
                            <div className="contents_mid_1">
                                <div className="contents_1">
                                    <div className="tit">{element.ctyNm}</div>
                                    <div className="equip_ctn">
                                        {`장비 수 : ${element.equipDtlModelList.length}대`}
                                    </div>
                                </div>
                            </div>
                            <div className="contents_mid_2">
                                <div className="contents_mid_2_1 label_before">
                                    {element.ctyDesc}
                                </div>
                                <div className="contents_mid_2_2">
                                    <div className="equipInfo">{`보유 장비: `}</div>
                                    {element.equipDtlModelList.length > 0 ? (
                                        element.equipDtlModelList.map(
                                            (element2, index2) => {
                                                return (
                                                    <div
                                                        className="equip_link"
                                                        key={`equip_info_${index2}`}
                                                        onClick={() => {
                                                            openEqInfoP(
                                                                element2,
                                                            );
                                                        }}>
                                                        {element2.equipNm}
                                                    </div>
                                                );
                                            },
                                        )
                                    ) : (
                                        <div className="equipInfo">-</div>
                                    )}
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }, [contenstObj, openEqInfoP]);

    return (
        <ContentsWrap>
            {modalObj.isModalOpen ? (
                <SModal
                    tit={modalObj.param.equipNm}
                    param={modalObj.param}
                    isOpen={modalObj.isModalOpen}
                    sizeObj={{
                        width: modalObj.param.contsWidth,
                        height: modalObj.param.contsHeight,
                        isResize: true,
                    }}
                    path={'equip/eqContents/popup/EquipInfoP'}
                    onClose={onCloseEqInfo}
                />
            ) : null}
            <div className="contents_wrap">
                <div className="top_contents">
                    <div className="main_tit">{`연구 안내`}</div>
                    <div className="research">
                        <img
                            className="research_img"
                            src={research}
                            onClick={handleSearchCtns}
                        />
                    </div>
                </div>
                <div>{Contents && Contents}</div>
            </div>
        </ContentsWrap>
    );
}
>>>>>>> dev_new
export default EqContents;
