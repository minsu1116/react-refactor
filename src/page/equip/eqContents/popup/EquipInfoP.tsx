import React, {useEffect, useMemo, useState} from 'react';
import {Seditor} from 'components/atoms/editor/Seditor';
import CustomPicker from 'components/atoms/colorPicker/ColorPicker';
import API from 'override/api/API';

interface Props {
    param?: EuipMt;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
}

interface EuipMt {
    equipId: number;
    equipNm: string;
    equipConts?: string;
    readOnly: boolean;
}

// eslint-disable-next-line react/display-name
const EquipInfoP = (props: Props) => {
    const [equipConts, setEquipConts] = useState('');
    useEffect(() => {
        handleSearchCtns();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSearchCtns = async () => {
        const params = props.param;
        const result = await API.request.post(
            `api/equip/equip-ctns/equip-info/get`,
            params,
        );

        if (result.data.success) {
            setEquipConts(result.data.data.equipConts);
        }
    };

    const [bkColor, setBkColor] = useState('#fff');
    const [ftColor, setFtColor] = useState('#000');

    // useEffect(() => {
    //     return function cleanup() {};
    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, []);

    // const handleContetnsChange = (value) => {
    // };

    const style = useMemo(() => {
        return {
            backgroundColor: bkColor ? bkColor : '#fff',
            color: ftColor ? ftColor : '#000',
            borderRadius: '9px',
        };
    }, [bkColor, ftColor]);

    return (
        <div className="contents_wrap_parents_h" style={{overflow: 'auto'}}>
            <div className="search_wrap">
                <div className="search_contents">
                    <CustomPicker
                        color={bkColor}
                        tit="바탕화면"
                        onChange={setBkColor}
                    />
                    <CustomPicker
                        color={ftColor}
                        tit="폰트"
                        onChange={setFtColor}
                    />
                </div>
            </div>
            <div className="contents_wrap" style={style}>
                <Seditor modal={equipConts} readOnly={true} />
            </div>
        </div>
    );
};
export default EquipInfoP;
