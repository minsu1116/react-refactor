import React, {useEffect, useState, useMemo} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import API from 'override/api/API';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import './style.scss';
import EquipMngEmpMt from 'page/equip/mt/mngEmpMt/EquipMngEmpMt';

import SModal from 'override/modal/SModal';

interface EuipMt {
    equipId?: number;
    equipNm: string;
    equipEnNm?: string;
    sort: number;
    apprYn: string;
    useYn: string;
    equipConts?: string;
    fileContsId?: number;
    contsHeight?: number;
    contsWidth?: number;
}

interface EqModalObj {
    equipId?: number;
    equipConts?: string;
    isModalOpen: boolean;
    isResize?: boolean;
    width?: string;
    height?: string;
}

interface EqModalObj {
    equipId?: number;
    equipConts?: string;
    readOnly: boolean;
    isModalOpen: boolean;
}

const EquipMt = React.memo(() => {
    const {data} = Storage.getGridList();
    const {data: userInfo} = Storage.getUserInfo();
    const [modalObj, setModalObj] = useState<EqModalObj>({
        isModalOpen: false,
        readOnly: false,
    });

    const equipMtGrid = data?.get('equip_mt_grid');
    const [searchObj, setSearchObj] = useState({
        equipMngEmpNm: userInfo.userNm,
        equipMngDeptNm: userInfo.deptNm,
    });
    const [rowData, setRowData] = useState([]);
    const [selectObj, setSelectObj] = useState<EuipMt>(null);

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleAddEquipConts = (param) => {
        if (param?.data) {
            setModalObj({
                ...modalObj,
                equipId: param.data.equipId,
                equipConts: param?.data.equipConts,
                isModalOpen: !modalObj.isModalOpen,
                isResize: true,
                width: param.data.contsWidth,
                height: param.data.contsHeight,
            });
        }
    };

    const onCloseEqConts = async (param) => {
        setModalObj({
            ...modalObj,
            isModalOpen: !modalObj.isModalOpen,
        });

        if (param.equipConts) {
            selectObj.equipConts = param.equipConts;
            selectObj.contsHeight = param?.height?.replace('px', '');
            selectObj.contsWidth = param?.width?.replace('px', '');
            const saveList = [selectObj];
            const params = {
                data: saveList,
            };

            await equipMtSave(params);
        }
    };

    const columnDefs = useMemo(() => {
        return [
            {
                headerName: '',
                field: 'check',
                default: 'N',
                type: 'checkbox',
                width: 32,
                checkbox: true,
                editable: false,
            },
            {
                headerName: '장비명(KR)',
                field: 'equipNm',
                width: 120,
                editable: true,
                essential: true,
            },
            {
                headerName: '장비명(EN)',
                field: 'equipEnNm',
                width: 120,
                editable: true,
            },
            {
                headerName: '순서',
                field: 'sort',
                width: 70,
                type: 'number',
                editable: true,
            },
            {
                headerName: '승인여부',
                field: 'apprYn',
                align: 'center',
                width: 80,
                default: 'Y',
                codeGroup: 'USE_YN',
                editable: true,
            },
            {
                headerName: '오픈여부',
                field: 'openYn',
                align: 'center',
                width: 80,
                default: 'Y',
                codeGroup: 'USE_YN',
                editable: true,
            },
            {
                headerName: '의뢰표시여부',
                field: 'surYn',
                align: 'center',
                width: 100,
                default: 'N',
                codeGroup: 'USE_YN',
                editable: true,
            },
            {
                headerName: '사용여부',
                field: 'useYn',
                align: 'center',
                width: 80,
                default: 'Y',
                codeGroup: 'USE_YN',
                editable: true,
            },
            {
                headerName: '컨텐츠',
                field: 'equipConts',
                width: 100,
                editable: false,
                align: 'center',
                buttonNm: '등록',
                type: 'button',
                validation: false,
                // buttonName: this.l10n('M01032', '권한등록'),
                onButtonClick: handleAddEquipConts,
            },
            {
                headerName: '장비ID',
                field: 'equipId',
                hide: true,
                editable: false,
            },
            {
                headerName: '컨텐츠파일ID',
                field: 'fileContsid',
                hide: true,
                editable: false,
            },
        ];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(`api/equip/equip-mt/get`, param);

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const equipMtSave = async (param) => {
        const params = JSON.stringify({param1: param.data, param2: searchObj});
        const result = await API.request.post(
            `api/equip/equip-mt/save`,
            params,
        );

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onSelectChange = async () => {
        if (data) {
            if (equipMtGrid && equipMtGrid.getSelectedRows()) {
                const param = equipMtGrid.getSelectedRows()[0];

                if (param) {
                    const tempObj: EuipMt = {
                        equipId: param.equipId,
                        equipNm: param.equipNm,
                        equipEnNm: param.equipEnNm,
                        sort: param.sort,
                        apprYn: param.apprYn,
                        useYn: param.useYn,
                        equipConts: param.equipConts,
                        fileContsId: param.fileContsId,
                        contsHeight: param.contsHeight,
                        contsWidth: param.contsWidth,
                    };
                    setSelectObj(tempObj);
                }
            }
        }
    };

    const handleDelEq = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };

        const result = await API.request.post(
            `api/equip/equip-mt/delete`,
            params,
        );

        if (result) {
            setRowData(result.data.data);
        } else {
            setRowData([]);
        }
    };

    return (
        <div className="contents_wrap_parents_h">
            {modalObj.isModalOpen ? (
                <SModal
                    tit={'컨텐츠 등록'}
                    param={modalObj}
                    isOpen={modalObj.isModalOpen}
                    sizeObj={{
                        width: '1700',
                        height: '900',
                        isResize: true,
                    }}
                    path={'equip/mt/popup/EquipContsP'}
                    onClose={onCloseEqConts}
                />
            ) : null}
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <TextField
                            id="equipMngEmpNm"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'관리자명'}
                            value={searchObj.equipMngEmpNm}
                            size="small"
                        />
                        <TextField
                            id="equipMngDeptNm"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'관리부서명'}
                            value={searchObj.equipMngDeptNm}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap_parents_w">
                <div className="contents_wrap">
                    <SGrid
                        grid={'equip_mt_grid'}
                        rowData={rowData}
                        isAddBtnOpen={true}
                        newRowDefault={{
                            sort: 'auto',
                            useYn: 'Y',
                            surYn: 'N',
                            openYn: 'Y',
                        }}
                        title={'장비 마스터 목록'}
                        onSelectionChanged={onSelectChange}
                        onClickSave={equipMtSave}
                        onClickDelete={handleDelEq}
                        columnDefs={columnDefs}
                    />
                </div>
                <div className="contents_wrap_parents_h">
                    {/* <div className="contents_wrap">
                        <EquipDeptMt selectObj={selectObj} />
                    </div> */}
                    <div className="contents_wrap">
                        <EquipMngEmpMt selectObj={selectObj} />
                    </div>
                </div>
            </div>
        </div>
    );
});
export default EquipMt;
