import React, {useState, useEffect} from 'react';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import API from 'override/api/API';

import './style.scss';
import {confirm, meg} from 'override/alert/Alert';
import STree from 'components/atoms/tree/STree';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@material-ui/core/IconButton';

interface EuipMt {
    equipId?: number;
    equipNm: string;
    sort: number;
    apprYn: string;
    useYn: string;
    equipConts?: string;
}

interface Props {
    selectObj?: EuipMt;
}

// interface EuipMngDeptMt {
//     equipId: number;
//     equipMngDeptCd: string;
//     equipMngDeptNm?: string;
//     useYn: string;
// }

const EquipDeptMt: React.FC<Props> = (props: Props) => {
    const [treeObj, setTreeObj] = useState({
        treeData: [],
        expandedTreeKeys: [],
        checkedTreeKeys: [],
    });
    const [eqiupMtObj, seteEiupMtObj] = useState<EuipMt>();

    useEffect(() => {
        if (props.selectObj) {
            seteEiupMtObj(props.selectObj);
        }
    }, [props.selectObj]);

    useEffect(() => {
        if (eqiupMtObj && eqiupMtObj.equipId) {
            handleSearchDeptTree();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [eqiupMtObj]);

    const handleSearchDeptTree = async () => {
        const param = eqiupMtObj;
        const result = await API.request.post(
            `api/equip/equip-mt/mng-dept/mt/get`,
            param,
        );

        let authList = '';
        if (result.data.success) {
            result.data.data.forEach((e) => {
                if (e.equipMngAuthYn == 'Y') {
                    authList += e.equipMngDeptCd + ',';
                }
            });

            authList = authList.slice(0, authList.length - 1);
            const authCheck = authList.split(',');
            setTreeObj({
                ...treeObj,
                treeData: result.data.data,
                expandedTreeKeys: result.data.data[0].equipMngDeptCd,
                checkedTreeKeys: authCheck,
            });
        } else {
            setTreeObj({
                ...treeObj,
                treeData: [],
                expandedTreeKeys: null,
                checkedTreeKeys: null,
            });
        }
    };

    const handleTreeCheck = async (keys, info) => {
        const params = treeObj.treeData;
        childrenCheck(params, info.node, info.checked);

        function childrenCheck(data, info, checked) {
            const children = data.filter((x) => x['parentDeptCd'] == info.key);
            const childrenYn = data.some((x) => x['parentDeptCd'] == info.key);
            const yn = checked ? 'Y' : 'N';

            //자식이 있으면
            if (children.length > 0) {
                children.map((x) => {
                    const temp = {
                        key: x['equipMngDeptCd'],
                        equipMngAuthYn: yn,
                        edit: true,
                        useYn: yn,
                    };
                    const child = Object.assign(temp);

                    if (childrenYn) {
                        childrenCheck(data, child, checked);
                    }
                    //자식과 부모 edit -> true
                    treeObj?.treeData.map((e) => {
                        if (
                            e.equipMngDeptCd == info.key ||
                            e.equipMngDeptCd == x.equipMngDeptCd
                        ) {
                            e.equipMngAuthYn = yn;
                            e.useYn = yn;
                            e.edit = true;
                        }
                    });
                });
            } else {
                treeObj?.treeData.map((e) => {
                    if (e.equipMngDeptCd == info.equipMngDeptCd) {
                        e.equipMngAuthYn = yn;
                        e.useYn = yn;
                        e.edit = true;
                    }
                });
            }
        }
    };

    const hendleSaveDept = async () => {
        const param1 = [];

        treeObj.treeData.forEach((e) => {
            if (e.edit) {
                e.useYn = e.equipMngAuthYn;
                e.equipId = eqiupMtObj.equipId;
                param1.push(e);
            }
        });

        if (param1.length > 0) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                const params = {param1: param1, param2: eqiupMtObj};
                const result = await API.request.post(
                    `api/equip/equip-mt/mng-dept/mt/save`,
                    params,
                );

                let authList = '';
                if (result.data.success) {
                    result.data.data.forEach((e) => {
                        if (e.equipMngAuthYn == 'Y') {
                            authList += e.equipMngDeptCd + ',';
                        }
                    });

                    authList = authList.slice(0, authList.length - 1);
                    const authCheck = authList.split(',');
                    setTreeObj({
                        ...treeObj,
                        treeData: result.data.data,
                        expandedTreeKeys: result.data.data[0].equipMngDeptCd,
                        checkedTreeKeys: authCheck,
                    });
                } else {
                    setTreeObj({
                        ...treeObj,
                        treeData: [],
                        expandedTreeKeys: null,
                        checkedTreeKeys: null,
                    });
                }
            }
        } else {
            await meg({
                title: `저장할 데이터가 없습니다.`,
                html: '',
            });
            return;
        }
    };

    return (
        <div className="equip_dept_main">
            <div className="head_contents" id="dept_head_contents">
                <div className="tit">부서 정보</div>
                <div className="head_btns">
                    <Tooltip title="저장">
                        <IconButton
                            id="dept_save"
                            onClick={hendleSaveDept}
                            className="btn_group">
                            <SaveAltIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <STree
                treeData={treeObj.treeData}
                keyMember={'equipMngDeptCd'}
                pIdMember={'parentDeptCd'}
                height={500}
                titleMember={'equipMngDeptNm'}
                onCheck={handleTreeCheck}
                checkable={true}
                expandedKeysId={treeObj.expandedTreeKeys}
                checkedKeysId={treeObj.checkedTreeKeys}
                // defaultExpandAll={true}
                border={true}
            />
        </div>
    );
};
export default EquipDeptMt;
