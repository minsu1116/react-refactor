import React, {useState, useEffect} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import API from 'override/api/API';
import {meg} from 'override/alert/Alert';
import './style.scss';
import SModal from 'override/modal/SModal';

interface EuipMt {
    equipId?: number;
    equipNm: string;
    sort: number;
    apprYn: string;
    useYn: string;
    equipConts?: string;
}

interface Props {
    selectObj?: EuipMt;
}

interface EuipMngEmpMt {
    equipId: number;
    equipNm?: string;
    equipMngEmpNo: string;
    equipMngEmpNm?: string;
    equipMngPosNm?: string;
    equipMngDeptNm?: string;
    equipMngOfficeNo?: string;
    useYn: string;
    userId?: string;
}

const columnDefs: Array<ColumnDefs> = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
        editable: false,
    },
    {
        headerName: '장비ID',
        field: 'equipId',
        hide: true,
        key: true,
        editable: false,
    },
    {headerName: '장비명', field: 'equipNm', editable: false, width: 120},
    {
        headerName: '관리자',
        field: 'equipMngEmpNm',
        width: 90,
        editable: false,
    },
    {
        headerName: '관리자직급',
        field: 'equipMngPosNm',
        width: 90,
        editable: false,
    },
    {
        headerName: '관리자부서',
        field: 'equipMngDeptNm',
        width: 120,
        editable: false,
    },
    {
        headerName: '관리자연락처',
        field: 'equipMngOfficeNo',
        width: 100,
        editable: false,
    },
    {
        headerName: '장비관리자사번',
        field: 'equipMngEmpNo',
        essential: true,
        hide: true,
        editable: false,
    },
];

const EquipMngEmpMt: React.FC<Props> = (props: Props) => {
    // const {data} = Storage.getGridList();
    const [rowData, setRowData] = useState([]);
    const [eqiupMtObj, seteEiupMtObj] = useState<EuipMt>();
    const [modalObj, seteModalObj] = useState<UserModal>();
    const sizeObj = {
        width: '675',
        height: '650',
        isResize: true,
    };

    const [modalOpen, setModalOpen] = useState(false);

    useEffect(() => {
        if (props.selectObj) {
            seteEiupMtObj(props.selectObj);
        }
    }, [props.selectObj]);

    useEffect(() => {
        if (eqiupMtObj && eqiupMtObj.equipId) {
            handleChangeSearch();
        } else {
            setRowData([]);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [eqiupMtObj]);

    async function handleChangeSearch() {
        const param = eqiupMtObj;
        const result = await API.request.post(
            `api/equip/equip-mt/mng-user/mt/get`,
            param,
        );

        if (result.data.success) {
            if (result.data.data) {
                setRowData(result.data.data);
            } else {
                setRowData([]);
            }
        } else {
            setRowData([]);
        }
    }

    const onUserModalOpen = () => {
        const validationList = () => {
            const tempList = [];
            if (rowData) {
                rowData.forEach((element) => {
                    const tempObj = {
                        userId: element.equipMngEmpNo,
                    };
                    tempList.push(tempObj);
                });
            }
            return tempList;
        };
        seteModalObj({
            ...modalObj,
            mode: 'multi',
            validationList: validationList(),
        });
        if (!eqiupMtObj.equipId) {
            meg({
                title: `${eqiupMtObj.equipNm}장비를 먼저 저장 해 주십시오. 장비ID가 없습니다.`,
                html: '',
            });
            return;
        }

        if (eqiupMtObj.apprYn == 'N') {
            meg({
                title: `${eqiupMtObj.equipNm}장비의 승인 여부가 'N'입니다. </br> 담당자를 지정 할 필요가 없습니다.`,
                html: '',
            });
            return;
        }

        setModalOpen(true);
    };

    const onCloseUser = async (param) => {
        setModalOpen(false);

        if (param && param.data) {
            const tmepList = [];
            param.data.forEach((element) => {
                const tempObj: EuipMngEmpMt = {
                    equipId: eqiupMtObj?.equipId,
                    equipNm: eqiupMtObj?.equipNm,
                    equipMngEmpNo: element.empNo,
                    equipMngEmpNm: element.userNm,
                    equipMngDeptNm: element.deptNm,
                    equipMngOfficeNo: element.officeNo,
                    equipMngPosNm: element.posNm,
                    userId: element.empNo,
                    useYn: 'Y',
                };
                tmepList.push(tempObj);
            });

            const params = {
                param1: tmepList,
                param2: eqiupMtObj,
            };

            const result = await API.request.post(
                `api/equip/equip-mt/mng-user/mt/save`,
                params,
            );

            if (result.data.success) {
                if (result.data.data) {
                    setRowData(result.data.data);
                } else {
                    if (rowData.length > 0) {
                        setRowData(rowData.concat(tmepList));
                    } else {
                        setRowData(tmepList);
                    }
                }
            }
        }
    };

    const handleDelEqMngUser = async (param) => {
        const params = {
            param1: param.data,
            param2: eqiupMtObj,
        };
        const result = await API.request.post(
            `api/equip/equip-mt/mng-user/mt/delete`,
            params,
        );
        if (result) {
            setRowData(result.data.data);
        } else {
            setRowData([]);
        }
    };

    return (
        <div className="equip_mng_user_main">
            <SModal
                tit={'장비관리자 등록'}
                param={modalObj}
                isOpen={modalOpen}
                sizeObj={sizeObj}
                path={'common/popup/user/User'}
                onClose={onCloseUser}
            />
            <SGrid
                grid={'equip_mng_user_mt_grid'}
                rowData={rowData}
                isModalOpen={true}
                onModalOpen={onUserModalOpen}
                onClickDelete={handleDelEqMngUser}
                title={'장비 관리자 목록'}
                columnDefs={columnDefs}
            />
        </div>
    );
};

export default EquipMngEmpMt;
