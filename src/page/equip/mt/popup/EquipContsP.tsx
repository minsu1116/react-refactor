import React, {useEffect, useMemo, useState} from 'react';
import {Seditor} from 'components/atoms/editor/Seditor';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import {confirm} from 'override/alert/Alert';
import './style.scss';
import CustomPicker from 'components/atoms/colorPicker/ColorPicker';

interface Props {
    param?: EuipMt;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
}

interface EuipMt {
    equipId: number;
    equipNm: string;
    equipConts?: string;
    readOnly: boolean;
}

// eslint-disable-next-line react/display-name
const EquipContsP = React.memo((props: Props) => {
    const [modal, setModal] = useState<EuipMt>({
        equipId: props.param.equipId,
        equipNm: props.param.equipNm,
        equipConts: props.param.equipConts,
        readOnly: props.param.readOnly,
    });

<<<<<<< HEAD
    const [bkColor, setBkColor] = useState('#ff5f00');
    const [ftColor, setFtColor] = useState('#000000');
=======
    const [bkColor, setBkColor] = useState('#fff');
    const [ftColor, setFtColor] = useState('#000');
>>>>>>> dev_new

    useEffect(() => {
        setModal({
            equipId: props.param.equipId,
            equipNm: props.param.equipNm,
            equipConts: props.param.equipConts,
            readOnly: props.param.readOnly,
        });
        return function cleanup() {
            setModal(null);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleContetnsChange = (value) => {
<<<<<<< HEAD
        // modal.equipConts = value;
        setModal({
            ...modal,
            equipConts: value,
        });
=======
        modal.equipConts = value;
>>>>>>> dev_new
    };

    const onSaveEqConts = async () => {
        if (props.onClose) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                props.onClose(modal);
            }
        }
    };

    const style = useMemo(() => {
<<<<<<< HEAD
        return {
            backgroundColor: bkColor ? bkColor : '#fff',
            color: ftColor ? ftColor : '#000',
            borderRadius: '9px',
        };
    }, [bkColor, ftColor]);
=======
        if (!modal.readOnly) {
            return {
                backgroundColor: 'inherit',
                color: 'inherit',
            };
        } else {
            return {
                backgroundColor: bkColor ? bkColor : '#fff',
                color: ftColor ? ftColor : '#000',
                borderRadius: '9px',
            };
        }
    }, [bkColor, ftColor, modal.readOnly]);
>>>>>>> dev_new

    return (
        <div className="contents_wrap_parents_h" style={{overflow: 'auto'}}>
            <div className="search_wrap">
                <div className="search_contents">
<<<<<<< HEAD
                    {!modal.readOnly && (
=======
                    {modal.readOnly && (
>>>>>>> dev_new
                        <>
                            <CustomPicker
                                color={bkColor}
                                tit="바탕화면"
                                onChange={setBkColor}
                            />
                            <CustomPicker
                                color={ftColor}
                                tit="폰트"
                                onChange={setFtColor}
                            />
                        </>
                    )}
                </div>
                <div className="search_btns">
                    <Tooltip title="저장">
                        <IconButton
                            onClick={onSaveEqConts}
                            className="btn_group">
                            <SaveAltIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap" style={style}>
                <Seditor
                    readOnly={modal.readOnly}
                    modal={modal.equipConts}
                    handleChange={handleContetnsChange}
                />
            </div>
        </div>
    );
});
export default EquipContsP;
