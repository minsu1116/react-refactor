import React, {Suspense} from 'react';
import './style.scss';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import {CircularProgress} from '@mui/material';
import HomeCommunity from './HomeCommunity';

const HomeTodo = React.lazy(() => import('./HomeTodo'));
const SCalendar = React.lazy(
    () => import('@components/atoms/scheduler/calendar/SCalendar'),
);
const HomeChart = React.lazy(() => import('./HomeChart'));
const Home = () => {
    return (
        <div className="contents_wrap_parents_h">
            <div className="home_main">
                <ContentsWrap>
                    <div className="contents_wrap">
                        <div className="main_contents">
                            <div className="top animate__animated animate__fadeIn animate__slower">
                                <Suspense
                                    fallback={
                                        <CircularProgress className="loading_bar" />
                                    }>
                                    <HomeTodo />
                                </Suspense>
                                <Suspense
                                    fallback={
                                        <div style={{width: '65%'}}>
                                            <CircularProgress className="loading_bar" />
                                        </div>
                                    }>
                                    <HomeChart />
                                </Suspense>
                            </div>
                        </div>
                        <div className="main_cotents_p">
                            <HomeCommunity />
                            <div className="main_contents" id="contents_02">
                                <div className="top animate__animated animate__fadeIn animate__slower">
                                    <div className="contents_02_header">
                                        <div className="header_tit">
                                            장비/설비 관리
                                        </div>
                                    </div>
                                </div>
                                <div id="contents_02_contents">
                                    <Suspense
                                        fallback={
                                            <CircularProgress className="loading_bar" />
                                        }>
                                        <SCalendar />
                                    </Suspense>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentsWrap>
            </div>
        </div>
    );
};

export default Home;
