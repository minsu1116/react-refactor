import React, {useCallback, useEffect, useMemo, useState} from 'react';
import './style.scss';
import {Carousel} from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import {
    CartesianGrid,
    Legend,
    Bar,
    BarChart,
    Tooltip,
    XAxis,
    YAxis,
    ResponsiveContainer,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    PolarRadiusAxis,
    Radar,
    LabelList,
} from 'recharts';
import API from 'override/api/API';
import {uniqBy} from 'lodash';
import Storage from 'override/api/Storage';
import moment from 'moment';
import {CircularProgress} from '@mui/material';

const SrEtmChart = (params) => {
    const [srChartEtmList, setSrChartEtmList] = useState([]);
    const [chatData] = useState([
        {
            displayNm: '처리속도',
            key: 'speedEtmAvg',
            fullMark: 5,
        },
        {
            displayNm: '신뢰',
            key: 'dataEtmAvg',
            fullMark: 5,
        },
        {
            displayNm: '도움',
            key: 'resultEtmAvg',
            fullMark: 5,
        },
        {
            displayNm: '태도',
            key: 'atdEtmAvg',
            fullMark: 5,
        },
        {
            displayNm: '만족',
            key: 'totalEtmAvg',
            fullMark: 5,
        },
    ]);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const setData = useMemo(() => {
        if (srChartEtmList.length > 0) {
            srChartEtmList.forEach((e) => {
                chatData.forEach((e2) => {
                    e2[e.chartKey] = parseFloat(e[e2.key]).toFixed(2);
                });
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [srChartEtmList]);

    useEffect(() => {
        setSrChartEtmList(params.param);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params.param]);

    return (
        <div className="chart_contents" id="home_chart_3">
            <div className="chart_tit_info">
                <span className="tit">의뢰평가</span>
            </div>
            <ResponsiveContainer
                id={'radar_chart'}
                className={'chart_main'}
                width="99%"
                height="100%">
                <RadarChart data={chatData}>
                    <PolarGrid />
                    <PolarAngleAxis dataKey="displayNm" />
                    <PolarRadiusAxis angle={50} domain={[0, 5]} />
                    <Radar
                        name="전체평점"
                        dataKey="전체평점"
                        dot={false}
                        stroke="#5dc5a6"
                        fill="#ffffff00"
                        fillOpacity={0.6}>
                        <LabelList
                            dataKey="전체평점"
                            position="left"
                            fill="#40b32f"
                            angle={20}
                        />
                    </Radar>
                    <Radar
                        name="개인평점(본인)"
                        dataKey="개인평점(본인)"
                        dot={false}
                        stroke="#517cd1"
                        fill="#517cd175"
                        fillOpacity={0.6}>
                        <LabelList
                            dataKey="개인평점(본인)"
                            position="right"
                            fill="#517cd1"
                            angle={20}
                        />
                    </Radar>
                    <Legend />
                </RadarChart>
            </ResponsiveContainer>
        </div>
    );
};

const SrChartDtl = (params) => {
    const [srChartDtlList, setSrChartDtlList] = useState([]);
    const commonCodeGroup = Storage.getGroupCode().data;
    const [chartData, setChartData] = useState([]);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const setData = useMemo(() => {
        if (commonCodeGroup['SR_TYPE'] && srChartDtlList.length > 0) {
            const dataKey = ['의뢰중', '분석중', '분석완료'];
            const DataMap = [];
            const key = [
                moment().add(-2, 'month').format('MM월'),
                moment().add(-1, 'month').format('MM월'),
                moment().format('MM월'),
            ];

            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const result: any = uniqBy(commonCodeGroup['SR_TYPE'], 'desc1');

            result.forEach((e) => {
                const chartData = [];
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                const dataObj: any = {
                    tit: `${e.desc1}(최근 3개월)`,
                };
                key.forEach((key) => {
                    let totalCtn = 0;
                    const tempData = {
                        displayNm: key,
                    };
                    const dataList = srChartDtlList.filter(
                        (x) => x.chartKey == e.desc1 && x.month == key,
                    );

                    dataKey.forEach((dataKey) => {
                        const result = parseInt(
                            dataList.filter((x) => x.regState === dataKey)[0]
                                ?.srCtn || 0,
                        );
                        tempData[dataKey] = result;
                        totalCtn += result;
                    });
                    tempData['총누계'] = totalCtn;
                    chartData.push(tempData);
                });
                dataObj['data'] = chartData;
                DataMap.push(dataObj);
            });

            setChartData(DataMap);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [srChartDtlList]);

    useEffect(() => {
        setSrChartDtlList(params.param);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params.param]);

    return (
        <>
            {chartData.length > 0 && (
                <Carousel
                    interval={3000}
                    autoPlay={true}
                    showThumbs={false}
                    infiniteLoop={true}>
                    {chartData.map((x, index) => {
                        return (
                            <div
                                className="chart_contents"
                                key={`${x.tit}_${index}`}>
                                <div className="chart_tit_info">
                                    <span className="tit">{x.tit}</span>
                                </div>
                                <ResponsiveContainer
                                    className={'chart_main'}
                                    width="99%"
                                    height="100%">
                                    <BarChart data={x.data} margin={{top: 20}}>
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis
                                            dataKey="displayNm"
                                            tickSize={0}
                                            padding={{
                                                left: 10,
                                                right: 10,
                                            }}
                                            tickMargin={10}
                                        />
                                        <YAxis />
                                        <Tooltip />
                                        <Legend />
                                        <Bar dataKey="총누계" fill="#82ca9d">
                                            <LabelList
                                                dataKey="총누계"
                                                position="top"
                                                fill="#82ca9d"
                                            />
                                        </Bar>
                                        <Bar dataKey="의뢰중" fill="#9c82af">
                                            <LabelList
                                                dataKey="의뢰중"
                                                position="top"
                                                fill="#9c82af"
                                            />
                                        </Bar>
                                        <Bar dataKey="분석중" fill="#df8c8c">
                                            <LabelList
                                                dataKey="분석중"
                                                position="top"
                                                fill="#df8c8c"
                                            />
                                        </Bar>
                                        <Bar dataKey="분석완료" fill="#8884d8">
                                            <LabelList
                                                dataKey="분석완료"
                                                position="top"
                                                fill="#8884d8"
                                            />
                                        </Bar>
                                    </BarChart>
                                </ResponsiveContainer>
                            </div>
                        );
                    })}
                </Carousel>
            )}
        </>
    );
};

const SrChart = (params) => {
    const [srChartList, setSrChartList] = useState([]);
    const [chatData, setChartData] = useState([]);
    const commonCodeGroup = Storage.getGroupCode().data;

    useEffect(() => {
        setSrChartList(params.param);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params.param]);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const setData = useMemo(() => {
        if (commonCodeGroup['SR_TYPE'] && srChartList.length > 0) {
            const key = [
                moment().add(-2, 'month').format('MM월'),
                moment().add(-1, 'month').format('MM월'),
                moment().format('MM월'),
            ];

            const data = [];
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const result: any = uniqBy(commonCodeGroup['SR_TYPE'], 'desc1');

            key.forEach((key) => {
                const tempObj = {
                    displayNm: key,
                };

                result.forEach((element) => {
                    const desc1: string = element.desc1;
                    tempObj[desc1] = parseInt(
                        srChartList.filter(
                            (x) => x.chartKey == desc1 && x.month === key,
                        )[0]?.srCtn || 0,
                    );
                });
                data.push(tempObj);
            });
            setChartData(data);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [srChartList]);

    return (
        <>
            {srChartList.length > 0 && (
                <div className="chart_contents" id="home_chart_1">
                    <div className="chart_tit_info">
                        <span className="tit">의뢰건수(최근 3개월)</span>
                    </div>
                    <ResponsiveContainer
                        className={'chart_main'}
                        width="99%"
                        height="100%">
                        <BarChart data={chatData} margin={{top: 20}}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis
                                dataKey="displayNm"
                                tickSize={0}
                                padding={{
                                    left: 10,
                                    right: 10,
                                }}
                                tickMargin={10}
                            />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="종합/클레임" fill="#517cd1">
                                <LabelList
                                    dataKey="종합/클레임"
                                    position="top"
                                    fill="#517cd1"
                                />
                            </Bar>
                            <Bar dataKey="일반 분석" fill="#9b8c8c">
                                <LabelList
                                    dataKey="일반 분석"
                                    position="top"
                                    fill="#9b8c8c"
                                />
                            </Bar>
                            <Bar dataKey="특정설비 분석" fill="#26a0af">
                                <LabelList
                                    dataKey="특정설비 분석"
                                    position="top"
                                    fill="#26a0af"
                                />
                            </Bar>
                            <Bar dataKey="전력실험실전용" fill="#a56262">
                                <LabelList
                                    dataKey="전력실험실전용"
                                    position="top"
                                    fill="#a56262"
                                />
                            </Bar>
                            <Bar dataKey="공인성적서" fill="#ff2c2c">
                                <LabelList
                                    dataKey="공인성적서"
                                    position="top"
                                    fill="#ff2c2c"
                                />
                            </Bar>
                            <Bar dataKey="TUV" fill="#8884d8">
                                <LabelList
                                    dataKey="TUV"
                                    position="top"
                                    fill="#8884d8"
                                />
                            </Bar>
                            <Bar dataKey="환경유해물질 분석" fill="#a0aa">
                                <LabelList
                                    dataKey="환경유해물질 분석"
                                    position="top"
                                    fill="#a0aa"
                                />
                            </Bar>
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            )}
        </>
    );
};

const HomeChart = () => {
    const {data: manuInfo} = Storage.getCurrentMenu();
    const [chartList, setChartList] = useState({
        srChartList: [],
        srChartDtlList: [],
        srChartEtmList: [],
    });

    useEffect(() => {
        initChartSearch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (manuInfo.menuNm === 'Home') {
            initChartSearch();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [manuInfo]);

    const initChartSearch = useCallback(async () => {
        const params = {};
        const result = await API.request.post(
            `api/anal/comm/chart/sr/get`,
            params,
        );

        if (result.data.success) {
            setChartList({
                ...chartList,
                srChartList: result.data.data.srChartList,
                srChartDtlList: result.data.data.srChartDtlList,
                srChartEtmList: result.data.data.srChartEtmList,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="chart">
            {chartList.srChartList.length == 0 ? (
                <CircularProgress className="loading_bar" />
            ) : (
                <>
                    <div className="chart_split" id={'chart_split_01'}>
                        <Carousel
                            interval={6000}
                            showThumbs={false}
                            autoPlay={true}
                            infiniteLoop={true}>
                            <SrChart param={chartList.srChartList} />
                            <SrEtmChart param={chartList.srChartEtmList} />
                        </Carousel>
                    </div>
                    <div className="chart_split">
                        <SrChartDtl param={chartList.srChartDtlList} />
                    </div>
                </>
            )}
        </div>
    );
};
export default HomeChart;
