import React, {Suspense} from 'react';
import './style.scss';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import {CircularProgress} from '@mui/material';

const HomeCommunity = () => {
    const [value, setValue] = React.useState('1');

    const Library = React.lazy(() => import('@page/community/library/Library'));
    const NoticeReg = React.lazy(
        () => import('@page/system/etc/noticeReg/NoticeReg'),
    );

    const handleChange = (
        event: React.SyntheticEvent,
        newValue: string,
    ): void => {
        setValue(newValue);
    };

    return (
        <div className="main_contents" id="contents_01">
            <div className="top animate__animated animate__fadeIn animate__slower">
                <div className="contents_01_header">
                    <div className="header_tit">커뮤니티</div>
                </div>
            </div>
            <div id="contents_01_contents">
                <TabContext value={value}>
                    <TabList
                        onChange={handleChange}
                        aria-label="lab API tabs example">
                        <Tab label="공지사항" value="1" />
                        <Tab label="자료실" value="2" />
                    </TabList>
                    <TabPanel value="1">
                        {value === '1' && (
                            <Suspense fallback={<CircularProgress />}>
                                <NoticeReg />
                            </Suspense>
                        )}
                    </TabPanel>
                    <TabPanel value="2">
                        {value === '2' && (
                            <Suspense fallback={<CircularProgress />}>
                                <Library />
                            </Suspense>
                        )}
                    </TabPanel>
                </TabContext>
            </div>
        </div>
    );
};

export default HomeCommunity;
