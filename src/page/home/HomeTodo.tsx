/* eslint-disable react/display-name */
import React, {useCallback, useEffect, useState} from 'react';
import './style.scss';
import Storage from '@override/api/Storage';
import API from 'override/api/API';

const analySearchObj = {
    btnList: null,
    buList: 'A002,Top',
    childCnt: 0,
    childOpen: 'N',
    childYn: 'N',
    depth: 3,
    depthFullName: '분석의뢰 > 의뢰 목록조회 > 의뢰목록 조회',
    displayYn: 'Y',
    formClass: 'AnalSearch',
    formNm: null,
    formUrl: '/anal/analSearch',
    helpId: null,
    iconNm: '',
    mainYn: null,
    menuDesc: null,
    menuId: '10201',
    formId: 'anal_search',
    menuNm: '의뢰목록 조회',
    newRow: null,
    parentMenuId: '102',
    parentName: '분석의뢰 > 의뢰 목록조회',
    plntList: 'A002,Top',
    sort: 1,
    sort1: 2,
    subMenu: null,
    systemGrpYn: 'Y',
    url: '/anal/analSearch/AnalSearch',
    userId: null,
    wordCd: null,
    param: {},
};

async function handleSearchClick(userInfo: UserInfo) {
    const params = {
        currEmpNo: userInfo.empNo,
    };
    const result = await API.request.post(`api/anal/comm/reg/get`, params);
    if (result.data.success) {
        return result.data.data;
    }
}

const HomeTodo = React.memo(() => {
    const {mutate, data: manuInfo} = Storage.getCurrentMenu();
    const {data: userInfo} = Storage.getUserInfo();
    const [onClickTrue, setOnClickTrue] = useState(false);
    const [todoList, setTodoList] = useState([]);
    const [todoObj, setTodoObj] = useState({
        todo_200: [],
        todo_400: [],
        todo_500: [],
        todo_900: [],
    });

    useEffect(() => {
        getTodoList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (manuInfo.menuNm === 'Home') {
            getTodoList();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [manuInfo]);

    useEffect(() => {
        if (todoList && todoList.length > 0) {
            setTodoObj({
                ...todoObj,
                todo_200: todoList.filter((x) => x.regState == '200'),
                todo_400: todoList.filter((x) => x.regState == '400'),
                todo_500: todoList.filter((x) => x.regState == '500'),
                todo_900: todoList.filter((x) => x.regState == '900'),
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [todoList]);

    const getTodoList = useCallback(async () => {
        const result = await handleSearchClick(userInfo);
        setTodoList(result);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function onClickMenuOpen(param) {
        mutate(null);
        switch (param) {
            case '200':
                analySearchObj.formId = 'anal_search_200';
                analySearchObj.menuNm = '의뢰목록 조회(결재)';
                break;
            case '400':
                analySearchObj.formId = 'anal_search_400';
                analySearchObj.menuNm = '의뢰목록 조회(접수)';
                break;
            case '500':
                analySearchObj.formId = 'anal_search_500';
                analySearchObj.menuNm = '의뢰목록 조회(처리)';
                break;
            case '900':
                analySearchObj.formId = 'anal_search_900';
                analySearchObj.menuNm = '의뢰목록 조회(평가)';
                break;
        }

        analySearchObj.param = {
            regState: param,
            currEmpNo: userInfo.empNo,
        };
        const isMenuActive =
            document.getElementsByClassName('main_menu active');

        if (isMenuActive) {
            const subMenu = isMenuActive[0]?.classList;
            subMenu?.remove('active');
        }
        mutate(analySearchObj);
    }

    return (
        <div className="todo">
            <div className="todo_01">
                <div
                    className={
                        todoObj.todo_500.length > 0
                            ? 'todo_02'
                            : 'todo_02 none_active'
                    }
                    id="todo_proc"
                    onMouseOver={() => setOnClickTrue(true)}
                    onMouseLeave={() => setOnClickTrue(false)}
                    onClick={() => {
                        if (todoObj.todo_500.length > 0) {
                            onClickMenuOpen('500');
                        }
                    }}>
                    <div className="home_tit">의뢰 처리하기</div>
                    <div
                        className={
                            !onClickTrue
                                ? todoObj.todo_500.length > 0
                                    ? 'info_ctn animate__animated animate__heartBeat animate__slower animate__delay-2s'
                                    : 'info_ctn'
                                : 'info_ctn animate__animated animate__hinge'
                        }>
                        {todoObj.todo_500.length}
                        <span className="info_tit">건</span>
                    </div>
                </div>
                <div
                    className={
                        todoObj.todo_400.length > 0
                            ? 'todo_02'
                            : 'todo_02 none_active'
                    }
                    id="todo_reg"
                    onClick={() => {
                        if (todoObj.todo_400.length > 0) {
                            onClickMenuOpen('400');
                        }
                    }}>
                    <div className="home_tit">의뢰 접수하기</div>
                    <div
                        className={
                            todoObj.todo_400.length > 0
                                ? 'info_ctn animate__animated animate__heartBeat animate__slower animate__delay-2s'
                                : 'info_ctn'
                        }>
                        {todoObj.todo_400.length}
                        <span className="info_tit">건</span>
                    </div>
                </div>
            </div>
            <div className="todo_01">
                <div
                    className={
                        todoObj.todo_200.length > 0
                            ? 'todo_02'
                            : 'todo_02 none_active'
                    }
                    id="todo_appr"
                    onClick={() => {
                        if (todoObj.todo_200.length > 0) {
                            onClickMenuOpen('200');
                        }
                    }}>
                    <div className="home_tit">의뢰 결재하기</div>
                    <div
                        className={
                            todoObj.todo_200.length > 0
                                ? 'info_ctn animate__animated animate__heartBeat animate__slower animate__delay-2s'
                                : 'info_ctn'
                        }>
                        {todoObj.todo_200.length}
                        <span className="info_tit">건</span>
                    </div>
                </div>
                <div
                    className={
                        todoObj.todo_900.length > 0
                            ? 'todo_02'
                            : 'todo_02 none_active'
                    }
                    id="todo_eval"
                    onClick={() => {
                        if (todoObj.todo_900.length > 0) {
                            onClickMenuOpen('900');
                        }
                    }}>
                    <div className="home_tit">의뢰 평가하기</div>
                    <div
                        className={
                            todoObj.todo_900.length > 0
                                ? 'info_ctn animate__animated animate__heartBeat animate__slower animate__delay-2s'
                                : 'info_ctn'
                        }>
                        {todoObj.todo_900.length}
                        <span className="info_tit">건</span>
                    </div>
                </div>
            </div>
        </div>
    );
});
export default HomeTodo;
