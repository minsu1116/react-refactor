import React, {useCallback, useEffect, useState} from 'react';
import API from 'override/api/API';
import ContentsWrap from 'page/common/wrap/ContentsWrap';
import Seditor from 'components/atoms/editor/Seditor';
import CachedRoundedIcon from '@mui/icons-material/CachedRounded';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import {confirm} from '@override/alert/Alert';
import './style.scss';

const ContsP = (params) => {
    const [contenstObj, setContenstObj] = useState<ContentsObj>({
        contsKey: params.param.contsKey,
        contsTit: params.param.contsTit || '',
        conts: params.param.conts || '',
    });

    useEffect(() => {
        setContenstObj({
            contsKey: params.param.contsKey,
            contsTit: params.param.contsTit || '',
            conts: params.param.conts || '',
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    const handleChange = useCallback((param) => {
        contenstObj.conts = param;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleContsSave = useCallback(async () => {
        const param = {
            param1: contenstObj,
            param2: contenstObj,
        };

        const confirmResult = await confirm({
            title: '저장하시겠습니까?',
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/intro/contents/save`,
                param,
            );

            if (result.data.success) {
                params.onClose(result.data.data || contenstObj);
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="contents_main">
            <ContentsWrap>
                <div className="search_wrap">
                    <div className="search_btns">
                        <Tooltip title="저장">
                            <IconButton
                                id="dept_save"
                                className="btn_group"
                                onClick={handleContsSave}>
                                <SaveAltIcon className="btn_icon" />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="새로고침">
                            <IconButton
                                // onClick={handleSearchClick}
                                className="btn_group">
                                <CachedRoundedIcon className="btn_icon" />
                            </IconButton>
                        </Tooltip>
                    </div>
                </div>
                <div className="contents_wrap">
                    <Seditor
                        readOnly={false}
                        modal={contenstObj.conts}
                        handleChange={handleChange}
                    />
                </div>
            </ContentsWrap>
        </div>
    );
};
export default ContsP;
