import React, {Suspense, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import {CircularProgress} from '@mui/material';
import Storage from '@override/api/Storage';

const Contents = React.lazy(
    () => import('@page/layout/contentsLayout/Contents'),
);
const MainMenu = React.lazy(() => import('@page/layout/menuLayout/MainMenu'));
const SetContents = () => {
    return (
        <>
            <Suspense fallback={<CircularProgress className="loading_bar" />}>
                <MainMenu />
                <Contents />
            </Suspense>
        </>
    );
};

const MainLayout = () => {
    const navigate = useNavigate();
    const userInfo = Storage.getUserInfo().data;
    const token = localStorage.getItem('jwt');
    useEffect(() => {
        const ele = document.getElementById('ipl-progress-indicator');
        if (ele) {
            ele.classList.add('available');
            ele.outerHTML = '';
        }
        if (!token || !userInfo.empNo) {
            navigate('/auth');
        }
    });

    return (
        <div id="wrapper">
            <div
                id="layerOverlap"
                style={{
                    zIndex: 9999,
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    backgroundColor: '#ffffff',
                    overflow: 'hidden',
                    opacity: 0,
                    display: 'none',
                }}
            />
            <div className="container">
                {token ? (
                    <>
                        <SetContents />
                    </>
                ) : null}
            </div>
        </div>
    );
};

export default MainLayout;
