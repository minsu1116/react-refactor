/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/display-name */
import React, {Suspense, useEffect, useState} from 'react';
import Tabs from '@mui/material/Tabs';
import Storage from '@override/api/Storage';
import SwipeableViews from 'react-swipeable-views';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import './style.scss';
import Spinner from 'components/atoms/spinner/Spinner';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import NotificationsIcon from '@mui/icons-material/Notifications';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import Badge from '@mui/material/Badge';
import {Button, Dialog, IconButton, Tooltip} from '@mui/material';
import {confirm} from 'override/alert/Alert';
import API from 'override/api/API';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import SModal from 'override/modal/SModal';

// import ContentsTop from './top/ContentsTop';

function menuClose() {
    const isMenuActive = document.getElementsByClassName('main_menu active');

    if (isMenuActive) {
        const subMenu = isMenuActive[0]?.classList;
        subMenu?.remove('active');
    }
}

let maxTabIndex = 0;
let tabKeys = ['Home'];
let homeInfo = undefined;

interface ReqParam {
    reqList: Array<any>;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
}

const ReqList = (params: ReqParam) => {
    const {mutate} = Storage.getAlaramCtn();
    const [alarmReqList, setAlarmReqList] = useState<Array<any>>([]);

    useEffect(() => {
        getAlarmReqList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getAlarmReqList = async () => {
        const param = {};
        const result = await API.request.post(`api/home/alarm/get`, param);

        if (result.data.success) {
            let alarmList = [];

            if (result.data.data) {
                if (result.data.data.userList) {
                    if (result.data.data.equipList) {
                        alarmList = JSON.parse(
                            result.data.data.userList,
                        )?.concat(JSON.parse(result.data.data.equipList));
                    } else {
                        alarmList = JSON.parse(result.data.data.userList);
                    }
                    setAlarmReqList(alarmList);
                } else {
                    if (result.data.data.equipList) {
                        alarmList = JSON.parse(result.data.data.equipList);
                        setAlarmReqList(alarmList);
                    } else {
                        setAlarmReqList([]);
                    }
                }
                mutate(alarmList.length);
                setAlarmReqList(alarmList);
            } else {
                mutate(0);
                params.onClose();
            }
        }
    };

    const onClickUserSave = async (userInfo) => {
        const confirmResult = await confirm({
            title:
                userInfo.useApprYn == 'Y'
                    ? `${userInfo.userNm}님의 가입을 승인 하시겠습니까?`
                    : `${userInfo.userNm}님의 가입을 반려 하시겠습니까?`,
            html: '',
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/home/user-reg-req/update`,
                userInfo,
            );

            if (result.data.success) {
                let alarmList = [];

                if (result.data.data) {
                    if (result.data.data.userList) {
                        if (result.data.data.equipList) {
                            alarmList = JSON.parse(
                                result.data.data.userList,
                            )?.concat(JSON.parse(result.data.data.equipList));
                        } else {
                            alarmList = JSON.parse(result.data.data.userList);
                        }
                        setAlarmReqList(alarmList);
                    } else {
                        if (result.data.data.equipList) {
                            alarmList = JSON.parse(result.data.data.equipList);
                            setAlarmReqList(alarmList);
                        } else {
                            setAlarmReqList([]);
                        }
                    }
                    mutate(alarmList.length);
                    setAlarmReqList(alarmList);
                } else {
                    mutate(0);
                    params.onClose();
                }
            }
        }
    };

    const onClickEquipRevSave = async (revInfo) => {
        const confirmResult = await confirm({
            title: `${revInfo.equipNm} 사용요청을 승인 하시겠습니까?`,
            html: `<div>
            <div class="rev_confirm">
                <div class="equip_tit">
                   장비명
                </div>
                <div class="equip_nm">
                    ${revInfo.equipNm}
                </div>
            </div>
            <div class="rev_tit">
                <div class="rev_tit_conts">
                    ${revInfo.revTit}
                </div>
            </div>
            <div class="rev_day">
                <div class="rev_day_cont">
                    <div class="day_cont1">
                        사용일(<span class="force_tit">From</span>)
                    </div>
                    <div class="day_cont2">
                        ${revInfo.revStartDtm}
                    </div>
                </div>
                <div class="rev_day_cont">
                    <div class="day_cont1">
                        사용일(<span class="force_tit">To</span>)
                    </div>
                    <div class="day_cont2">
                        ${revInfo.revEndDtm}
                    </div>
                </div>
            </div>
        </div>`,
        });

        if (confirmResult) {
            const result = await API.request.post(
                `api/home/equip-rev-req/update`,
                revInfo,
            );
            if (result.data.success) {
                let alarmList = [];

                if (result.data.data) {
                    if (result.data.data.userList) {
                        if (result.data.data.equipList) {
                            alarmList = JSON.parse(
                                result.data.data.userList,
                            )?.concat(JSON.parse(result.data.data.equipList));
                        } else {
                            alarmList = JSON.parse(result.data.data.userList);
                        }
                        setAlarmReqList(alarmList);
                    } else {
                        if (result.data.data.equipList) {
                            alarmList = JSON.parse(result.data.data.equipList);
                            setAlarmReqList(alarmList);
                        } else {
                            setAlarmReqList([]);
                        }
                    }
                    mutate(alarmList.length);
                    setAlarmReqList(alarmList);
                } else {
                    mutate(0);
                    params.onClose();
                }
            }
        }
    };

    return (
        <>
            {alarmReqList.length > 0 && (
                <>
                    {alarmReqList.map((x, index) => {
                        console.log(alarmReqList, '????', x);
                        if (x.alarmType == 'U') {
                            return (
                                <div key={`user_req_${index}`}>
                                    <ListItem
                                        key={`user_req_${index}`}
                                        secondaryAction={
                                            <>
                                                <Tooltip
                                                    title={'가입 승인'}
                                                    placement="bottom">
                                                    <IconButton
                                                        className="alaram_btn"
                                                        onClick={() => {
                                                            x.useApprYn = 'Y';
                                                            onClickUserSave(x);
                                                        }}
                                                        edge="end"
                                                        aria-label="delete">
                                                        <AppRegistrationIcon />
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip
                                                    title={'가입 거절'}
                                                    placement="bottom">
                                                    <IconButton
                                                        className="alaram_btn"
                                                        onClick={() => {
                                                            x.useApprYn = 'N';
                                                            onClickUserSave(x);
                                                        }}
                                                        edge="end"
                                                        aria-label="delete">
                                                        <DeleteForeverIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            </>
                                        }>
                                        <ListItemText
                                            className="alarm_contents"
                                            primary="회원가입 신청"
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        className="alarm_contents_01"
                                                        sx={{display: 'inline'}}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary">
                                                        <span className="alarm_contents_main_tit">
                                                            {`${x.userNm} ${x.rankNm}(`}
                                                            <span className="tit_forece">
                                                                {x.empNo}
                                                            </span>
                                                            {')'}
                                                        </span>
                                                        <br></br>
                                                        <span className="alarm_contents_sub_tit">{`${x.compNm}/${x.deptNm} ${x.officeNo}`}</span>
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                    {alarmReqList.length - 1 > index && (
                                        <Divider
                                            className="thin_border"
                                            variant="inset"
                                            component="li"
                                        />
                                    )}
                                </div>
                            );
                        } else {
                            return (
                                <div key={`user_req_${index}`}>
                                    <ListItem
                                        key={`user_req_${index}`}
                                        secondaryAction={
                                            <>
                                                <Tooltip
                                                    title={'사용 승인'}
                                                    placement="bottom">
                                                    <IconButton
                                                        className="alaram_btn"
                                                        onClick={() => {
                                                            x.apprYn = 'Y';
                                                            x.revState = '200';
                                                            onClickEquipRevSave(
                                                                x,
                                                            );
                                                        }}
                                                        edge="end"
                                                        aria-label="delete">
                                                        <AppRegistrationIcon />
                                                    </IconButton>
                                                </Tooltip>
                                                <Tooltip
                                                    title={'사용 거절'}
                                                    placement="bottom">
                                                    <IconButton
                                                        className="alaram_btn"
                                                        onClick={() => {
                                                            x.apprYn = 'N';
                                                            x.revState = '400';
                                                            onClickEquipRevSave(
                                                                x,
                                                            );
                                                        }}
                                                        edge="end"
                                                        aria-label="delete">
                                                        <DeleteForeverIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            </>
                                        }>
                                        <ListItemText
                                            className="alarm_contents"
                                            primary="장비사용 신청"
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        className="alarm_contents_01"
                                                        sx={{display: 'inline'}}
                                                        component="span"
                                                        variant="body2"
                                                        color="text.primary">
                                                        <span className="alarm_contents_main_tit">
                                                            {`${x.equipNm} ${x.equipEnNm}`}
                                                            <br></br>
                                                            <span
                                                                className="tit_forece"
                                                                id="equip_tit_forece">
                                                                {x.revDate}
                                                            </span>
                                                        </span>
                                                        <br></br>
                                                        <span className="alarm_contents_sub_tit">{`${x.revEmpNm}/${x.revDeptNm} ${x.revOfficeNo}`}</span>
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                    {alarmReqList.length - 1 > index && (
                                        <Divider
                                            className="thin_border"
                                            variant="inset"
                                            component="li"
                                        />
                                    )}
                                </div>
                            );
                        }
                    })}
                </>
            )}
        </>
    );
};

const AlarmItemsList = (param: any) => {
    return (
        <List
            id="alarm_list"
            sx={{width: '100%', maxWidth: 360, bgcolor: 'background.paper'}}>
            <ReqList onClose={param.onClose} reqList={param.param} />
        </List>
    );
};

const CommunityHeaderContents = () => {
    const {data: alarmCtn, mutate} = Storage.getAlaramCtn();
    const [userModalOpen, setUserModalOpen] = useState(false);
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        if (alarmCtn > 0) {
            setOpen(true);
        }
    };

    const handleClose = () => {
        setOpen(false);
        getAlarmReqList();
    };

    useEffect(() => {
        getAlarmReqList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getAlarmReqList = async () => {
        const params = {};
        const result = await API.request.post(`api/home/alarm/get`, params);

        if (result.data.success) {
            let alarmList = [];
            if (result.data.data) {
                if (result.data.data.userList) {
                    alarmList = JSON.parse(result.data.data.userList)?.concat(
                        JSON.parse(result.data.data.equipList),
                    );
                } else {
                    if (result.data.data.equipList) {
                        alarmList = JSON.parse(result.data.data.equipList);
                    } else {
                    }
                }
                mutate(alarmList.length);
            } else {
                mutate(0);
            }
        }
    };

    // const getEquipReqList = async () => {
    //     const params = {};
    //     const result = await API.request.post(
    //         `api/home/equip-rev-req/get`,
    //         params,
    //     );

    //     if (result.data.success) {
    //         setAlarmList(alarmList.concat(result.data.data));
    //         mutate(alarmCtn + result.data.data.length);
    //     }
    // };

    const onUserModalClose = () => {
        setUserModalOpen(false);
    };

    return (
        <>
            <Dialog
                id="alarm_modal"
                open={open}
                onClose={handleClose}
                PaperComponent={() => (
                    <AlarmItemsList onClose={handleClose} />
                )}></Dialog>
            <SModal
                tit={'유저 정보'}
                isOpen={userModalOpen}
                sizeObj={{
                    width: '900',
                    height: 'auto',
                    isResize: false,
                }}
                path={'layout/contentsLayout/top/UserInfo'}
                onClose={onUserModalClose}
            />
            <Tooltip title={'알람'} placement="bottom">
                <Button
                    className="btn_group"
                    variant="outlined"
                    onClick={() => handleClickOpen()}
                    startIcon={
                        <Badge badgeContent={alarmCtn} color="primary">
                            <NotificationsIcon
                                className={
                                    alarmCtn > 0
                                        ? `btn_icon animate__animated animate__heartBeat animate__slower animate__infinite`
                                        : 'btn_icon'
                                }
                            />
                        </Badge>
                    }></Button>
            </Tooltip>
            <Tooltip title={'유저정보'} placement="bottom">
                <Button
                    className="btn_group"
                    variant="outlined"
                    onClick={() => setUserModalOpen(true)}
                    startIcon={
                        <AccountCircleIcon className="btn_icon" />
                    }></Button>
            </Tooltip>
        </>
    );
};

const Contents = React.memo(() => {
    const layout = Storage.getLayout().data;
    const {data, mutate} = Storage.getCurrentMenu();

    // Handle Tab Button Click
    const [tabId, setTabId] = useState(0);
    const [width, setWidth] = useState('calc(100% - 220px)');
    const [tabs, setAddTab] = useState([]);
    const handleTabChange = (event, newTabId) => {
        setTabId(newTabId);
    };

    const handleChangeIndex = (index) => {
        setTabId(index);
    };

    const currMenu: MenuInfo = data;
    let url;
    if (currMenu.url) {
        url = `${currMenu.url.substring(1, currMenu.url.length)}`;
    } else {
        homeInfo = currMenu;
        url = 'home/Home';
    }

    const OtherComponent = React.lazy(() => import(`@page/${url}`));

    // Handle Add Tab Button
    const handleAddTab = async () => {
        maxTabIndex = maxTabIndex + 1;
        setAddTab([...tabs, <Tab label={currMenu.menuNm} key={maxTabIndex} />]);
        await handleTabsContent();
        setTabId(maxTabIndex);
    };

    // Handle Add Tab Content
    const [tabsContent, setTabsContent] = useState([
        <Suspense key={currMenu.formId} fallback={<Spinner />}>
            <div id="contents_main" key={`tab_panel_${currMenu.formId}`}>
                <TabPanel tabId={tabId} key={currMenu.formId}>
                    <OtherComponent param={data} />
                </TabPanel>
            </div>
        </Suspense>,
    ]);
    const handleTabsContent = () => {
        setTabsContent([
            ...tabsContent,
            <Suspense key={currMenu.formId} fallback={<Spinner />}>
                <div id="contents_main" key={`tab_panel_${currMenu.formId}`}>
                    <TabPanel tabId={tabId} key={currMenu.formId}>
                        <OtherComponent param={data} />
                    </TabPanel>
                </div>
            </Suspense>,
        ]);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => {
        if (!tabKeys.includes(currMenu.menuNm)) {
            tabKeys.push(currMenu.menuNm);
            handleAddTab();
        } else {
            setTabId(tabKeys.indexOf(currMenu.menuNm));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currMenu.menuNm]);

    useEffect(() => {
        if (tabId != 0) {
            setTabId(tabId);
        } else {
            mutate(homeInfo);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tabId]);

    useEffect(() => {
        return function cleanup() {
            tabKeys = ['Home'];
            setAddTab([]);
            setTabsContent(tabsContent.filter((x) => x.key == 0));
            setTabId(0);
            maxTabIndex = 0;
        };

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (layout) {
            if (layout.tabId == 0) {
                // eslint-disable-next-line react-hooks/exhaustive-deps
                if (currMenu.menuNm == homeInfo.menuNm && tabId != 0) {
                    setTabId(0);
                } else {
                    mutate(homeInfo);
                }
            }
            if (layout.initWidth) {
                if (layout.initWidth == '60px') {
                    setWidth('calc(100% - 60px)');
                } else {
                    setWidth('calc(100% - 220px)');
                }
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [layout]);

    const onInitContents = async () => {
        const confirmResult = await confirm({
            title: '모든 탭을 삭제 하시겠습니까?',
            html: '',
        });
        if (confirmResult) {
            setTabId(0);
            setAddTab([]);
            maxTabIndex = 0;
            setTabsContent(tabsContent.filter((x) => x.key == '01'));
            tabKeys = ['Home'];
        }
    };

    return (
        <div
            id="contents_main"
            style={{
                width: width,
            }}
            onClick={menuClose}>
            <div className="contents_top">
                <Tabs
                    id="contents_top_1"
                    value={tabId}
                    textColor="inherit"
                    onChange={handleTabChange}
                    variant="scrollable">
                    <Tab label="Home" id="home" />
                    {tabs.map((child) => child)}
                </Tabs>
                <div className="contents_01_header">
                    <div className="header_btn">
                        <Tooltip title={'탭 제거'} placement="bottom">
                            <Button
                                className="btn_group"
                                variant="outlined"
                                onClick={onInitContents}
                                startIcon={
                                    <DeleteForeverIcon className="btn_icon" />
                                }></Button>
                        </Tooltip>
                        <CommunityHeaderContents />
                    </div>
                </div>
            </div>
            <div id="contents_mid">
                <SwipeableViews
                    id={'swipeableViews'}
                    index={tabId}
                    disabled={true}
                    onChangeIndex={handleChangeIndex}>
                    {tabsContent.map((child) => child)}
                </SwipeableViews>
            </div>
        </div>
    );
});
export default Contents;

function TabPanel(props) {
    const {children} = props;
    return (
        <Box className="contents_layout" key={maxTabIndex}>
            {children}
        </Box>
    );
}
