/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import AccountCircle from '@mui/icons-material/AccountCircle';
// import VpnKeyIcon from '@mui/icons-material/VpnKey';

import HomeIcon from '@mui/icons-material/Home';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import BadgeIcon from '@mui/icons-material/Badge';
import GroupIcon from '@mui/icons-material/Group';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Autocomplete from '@mui/material/Autocomplete';
import {IMaskInput} from 'react-imask';

import Input from '@mui/material/Input';
import InputLabel from '@mui/material/InputLabel';

import './style.scss';
import API from 'override/api/API';
import {confirm} from 'override/alert/Alert';
import Storage from 'override/api/Storage';

interface CustomProps {
    onChange: (event: {target: {id: string; value: string}}) => void;
    id: string;
}

interface UserInfo {
    empNo: string;
    deptNm: string;
    password: string;
    passwordVali: string;
    compNm: string;
    mobileNo: string;
    officeNo: string;
    email: string;
    posNm: string;
    addressKr: string;
    addressEn: string;
    userNm: string;
    userEnNm: string;
    compCd: string;
    rankNm: string;
}

const TextMaskCustom = React.forwardRef<HTMLElement, CustomProps>(
    function TextMaskCustom(props, ref: any) {
        const [values, setValues] = useState('');
        const [mask, setMask] = useState('#00-0000-0000');
        const {onChange, ...other} = props;

        useEffect(() => {
            if (values.slice(0, 2) == '02') {
                if (values.length == 11) {
                    setMask('#0-000-0000');
                } else {
                    setMask('#0-0000-0000');
                }
            } else {
                if (values.length == 12) {
                    setMask('#00-000-0000');
                } else {
                    setMask('#00-0000-0000');
                }
            }
        }, [values]);

        const otherList = useMemo(() => {
            return {
                ...other,
                onBlur: () => {
                    if (values.slice(0, 2) == '02') {
                        if (values.length == 11) {
                            setMask('#0-000-0000');
                        } else {
                            setMask('#0-0000-0000');
                        }
                    } else {
                        if (values.length == 12) {
                            setMask('#00-000-0000');
                        } else {
                            setMask('#00-0000-0000');
                        }
                    }
                },
                onFocus: () => {
                    if (values.slice(0, 2) == '02') {
                        setMask('#0-0000-0000');
                    } else {
                        setMask('#00-0000-0000');
                    }
                },
            };
        }, [other, values]);

        return (
            <IMaskInput
                {...otherList}
                mask={mask}
                definitions={{
                    '#': /[0-9]/,
                }}
                inputRef={ref}
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                onAccept={(value: any) => {
                    onChange({target: {id: props.id, value}});
                    setValues(value);
                }}
                overwrite
            />
        );
    },
);

const UserInfo = (param) => {
    const {data, mutate} = Storage.getUserInfo();
    const [companyList, setCompanyList] = useState([]);
    const [userInfo, setUserInfo] = useState<UserInfo>({
        empNo: '',
        deptNm: '',
        password: '',
        passwordVali: '',
        compNm: '',
        mobileNo: '',
        officeNo: '',
        email: '',
        posNm: '',
        addressKr: '',
        addressEn: '',
        userNm: '',
        userEnNm: '',
        compCd: '',
        rankNm: '',
    });

    const [validationList, setValidationList] = useState({
        officeNo: false,
        email: false,
        compNm: false,
        addressKr: false,
        userNm: false,
    });

    const [validationDesc, setValidationDescList] = useState({
        empNoDesc: '필수값 입니다.',
        passwordDesc: '필수값 입니다.',
        passwordValiDesc: '필수값 입니다.',
        officeNoDesc: '필수값 입니다.',
        compNmDesc: '필수값 입니다.',
        emailDesc: '필수값 입니다.',
        addressKrDesc: '필수값 입니다.',
        userNmDesc: '필수값 입니다.',
    });

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (data.empNo) {
            setUserInfo({
                ...userInfo,
                empNo: data.empNo,
                deptNm: data.deptNm,
                compNm: data.compNm,
                addressKr: data.addressKr,
                addressEn: data.addressEn,
                posNm: data.rankNm,
                officeNo: data.officeNo,
                mobileNo: data.mobileNo,
                email: data.email,
                userNm: data.userNm,
                rankNm: data.rankNm,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        getUserList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getUserList = useCallback(async () => {
        const result = await API.request.post(`api/pass/get/user-list`);
        if (result.data.success) {
            const tempCompList = [];
            result.data.data
                .filter((x) => x.compNm != 'LS전선')
                .forEach((element) => {
                    if (!tempCompList.find((x) => x.label == element.compNm)) {
                        tempCompList.push({
                            label: element.compNm,
                        });
                    }
                });

            setCompanyList(tempCompList);
        }
    }, []);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = {
            ...userInfo, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value.trim(), // 덮어쓰기
        };

        setUserInfo(newValue);
    };

    const onCheckValidation = (id, value) => {
        switch (id) {
            case 'email':
                function isEmail(value) {
                    const regExp =
                        /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

                    return regExp.test(value);
                }
                if (!isEmail(value)) {
                    setValidationDescList({
                        ...validationDesc,
                        emailDesc: 'E-Mail 형식이 아닙니다.',
                    });
                    setValidationList({
                        ...validationList,
                        email: true,
                    });
                } else {
                    setValidationDescList({
                        ...validationDesc,
                        emailDesc: '',
                    });
                    setValidationList({
                        ...validationList,
                        email: false,
                    });
                }
                break;
            case 'compNm':
                if (
                    value === 'LS전선' ||
                    value === 'ls전선' ||
                    value === 'lscns' ||
                    value === 'LSCNS' ||
                    value === '엘에스전선'
                ) {
                    setValidationDescList({
                        ...validationDesc,
                        compNmDesc: `${value}는(은) 입력 할 수 없습니다.`,
                    });
                    setValidationList({
                        ...validationList,
                        compNm: true,
                    });
                } else {
                    setValidationDescList({
                        ...validationDesc,
                        compNmDesc: '',
                    });
                    setValidationList({
                        ...validationList,
                        compNm: false,
                    });
                }
        }
    };

    return (
        <React.Fragment>
            <Box className="user_info_main" sx={{'& > :not(style)': {m: 1}}}>
                {data.hrYn == 'Y' && (
                    <div className="user_info_tit">
                        {'임직원은 변경이 불가능 합니다. HR에서 변경하십시오.'}
                    </div>
                )}
                <div className="btn_contetns">
                    <Tooltip title="변경하기">
                        <IconButton
                            id="send_btn"
                            disabled={data.hrYn == 'Y'}
                            onClick={async () => {
                                const keyList = Object.keys(validationList);

                                const newValue = {
                                    ...validationList,
                                };

                                let comfirm = true;
                                keyList.forEach((element) => {
                                    if (!userInfo[element]) {
                                        newValue[element] = true;
                                        comfirm = false;
                                    }
                                });

                                setValidationList(newValue);

                                if (comfirm) {
                                    const confirmResult = await confirm({
                                        title: '저장하시겠습니까?',
                                        html: '',
                                    });
                                    if (confirmResult) {
                                        const result = await API.request.post(
                                            `api/home/user/update`,
                                            userInfo,
                                        );

                                        if (result.data.success) {
                                            userInfo.rankNm = userInfo.posNm;
                                            await mutate(userInfo);
                                            param.onClose();
                                        }
                                    }
                                }
                            }}
                            className="btn_group">
                            <SaveAltIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
                <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="empNo"
                            label="아이디"
                            inputRef={inputRef}
                            disabled={true}
                            value={userInfo.empNo}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircle />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="email"
                            label="E-MAIL"
                            error={validationList.email}
                            value={userInfo.email}
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            onChange={handleChange}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AlternateEmailIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.email && (
                            <FormHelperText>
                                {validationDesc.emailDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="userNm"
                            onChange={handleChange}
                            label="이름"
                            disabled={data.hrYn == 'Y'}
                            value={userInfo.userNm}
                            error={validationList.userNm && !userInfo.userNm}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircle />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.userNm && !userInfo.userNm && (
                            <FormHelperText>
                                {'필수 입력값입니다.'}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <FormControl size="small">
                        <Autocomplete
                            freeSolo
                            id="compNm"
                            value={userInfo.compNm}
                            disabled={data.hrYn == 'Y'}
                            options={companyList}
                            onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                                setUserInfo({
                                    ...userInfo,
                                    compNm: e.target.value,
                                });
                            }}
                            sx={{width: 175}}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="회사명"
                                    value={userInfo.compNm}
                                    onBlur={(e) =>
                                        onCheckValidation(
                                            e.target.id,
                                            e.target.value.trim(),
                                        )
                                    }
                                    id="compNm"
                                    onChange={handleChange}
                                    error={
                                        validationList.compNm &&
                                        !userInfo.compNm
                                    }
                                    variant="standard"
                                />
                            )}
                        />
                        {validationList.compNm && (
                            <FormHelperText>
                                {validationDesc.compNmDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
                {/* <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="password"
                            onChange={handleChange}
                            error={validationList.password}
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            label="비밀번호"
                            type="password"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <VpnKeyIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.password && (
                            <FormHelperText>
                                {validationDesc.passwordDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="passwordVali"
                            onChange={handleChange}
                            label="비밀번호 확인"
                            error={validationList.passwordVali}
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            type="password"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <VpnKeyIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.passwordVali && (
                            <FormHelperText>
                                {validationDesc.passwordValiDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div> */}
                <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="deptNm"
                            onChange={handleChange}
                            value={userInfo.deptNm}
                            disabled={data.hrYn == 'Y'}
                            label="부서"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <GroupIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="posNm"
                            onChange={handleChange}
                            label="직급"
                            value={userInfo.posNm}
                            disabled={data.hrYn == 'Y'}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <BadgeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                    <FormControl size="small" className="tel_main">
                        <FormControl
                            className="tel_format"
                            disabled={data.hrYn == 'Y'}
                            size="small"
                            variant="standard">
                            <InputLabel
                                htmlFor="officeNo"
                                sx={{
                                    color:
                                        validationList.officeNo &&
                                        !userInfo.officeNo
                                            ? '#d52f2f'
                                            : '#00000099',
                                }}>
                                회사 연락처
                            </InputLabel>
                            <Input
                                value={
                                    userInfo.officeNo ? userInfo.officeNo : ''
                                }
                                error={
                                    validationList.officeNo &&
                                    !userInfo.officeNo
                                }
                                onChange={handleChange}
                                id="officeNo"
                                inputComponent={TextMaskCustom as any}
                            />
                            {validationList.officeNo && !userInfo.officeNo && (
                                <FormHelperText>
                                    {'필수 입력값입니다.'}
                                </FormHelperText>
                            )}
                        </FormControl>
                        <FormControl
                            className="tel_format"
                            size="small"
                            variant="standard">
                            <InputLabel htmlFor="mobileNo">휴대폰</InputLabel>
                            <Input
                                value={
                                    userInfo.mobileNo ? userInfo.mobileNo : ''
                                }
                                onChange={handleChange}
                                disabled={data.hrYn == 'Y'}
                                name="mobileNo"
                                id="mobileNo"
                                inputComponent={TextMaskCustom as any}
                            />
                        </FormControl>
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl
                        size="small"
                        variant="standard"
                        className="full_main">
                        <TextField
                            id="addressKr"
                            onChange={handleChange}
                            value={userInfo.addressKr}
                            disabled={data.hrYn == 'Y'}
                            label="회사 주소(국문)"
                            error={
                                validationList.addressKr && !userInfo.addressKr
                            }
                            className="full_contents"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <HomeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.addressKr && !userInfo.addressKr && (
                            <FormHelperText sx={{marginLeft: '17px'}}>
                                {'필수 입력값입니다.'}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl
                        size="small"
                        variant="standard"
                        className="full_main">
                        <TextField
                            id="addressEn"
                            onChange={handleChange}
                            label="회사 주소(영문)"
                            value={userInfo.addressEn}
                            disabled={data.hrYn == 'Y'}
                            className="full_contents"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <HomeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                </div>
            </Box>
        </React.Fragment>
    );
};

export default UserInfo;
