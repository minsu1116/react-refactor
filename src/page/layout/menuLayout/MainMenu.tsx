import React, {useEffect, useState} from 'react';
import Mid1Menu from './mid1/MainMid1Contents';
import Mid2Menu from './mid2/MainMid2Menu';
import Mid3Menu from './mid3/MainMid3Contents';
import MainBottom from './bottom/BottomContents';
import Storage from '@override/api/Storage';
import wing from '@img/ls_logo_wing.png';
import './style.scss';

interface LayoutObj {
    initWidth: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    tit: any;
    opacity: string;
    tabId?: number;
}

const titExpand = (
    <div id="top_tit">
        <div id="tit_1">TAS System</div>
        <div id="tit_2">Total Analytical Solution System</div>
    </div>
);

const titDownscale = (
    <div id="top_tit">
        <div id="tit_1" style={{paddingTop: '0.6rem'}}>
            TAS
        </div>
    </div>
);

// let layoutObj: LayoutObj;

const MainMenu = () => {
    const {mutate} = Storage.getLayout();

    const [layoutObj, widthChange] = useState<LayoutObj>({
        initWidth: window.innerWidth > 1023 ? '220px' : '60px',
        tit: window.innerWidth > 1023 ? titExpand : titDownscale,
        opacity: window.innerWidth > 1023 ? '1' : '0',
        // tabId: 0,
    });

    function layoutChange() {
        const tempObj: LayoutObj = {
            initWidth: layoutObj.initWidth == '60px' ? '220px' : '60px',
            tit: layoutObj.initWidth == '60px' ? titExpand : titDownscale,
            opacity: layoutObj.initWidth == '60px' ? '1' : '0',
            // tabId: 0,
        };

        widthChange((layoutObj) => ({
            ...layoutObj,
            initWidth: tempObj.initWidth,
            tit: tempObj.tit,
            opacity: tempObj.opacity,
            // tabId: 0,
        }));

        mutate(tempObj);
    }

    useEffect(() => {
        const updateWindowDimensions = () => {
            const innerWidth = window.innerWidth;
            if (innerWidth < 1023) {
                const tempObj: LayoutObj = {
                    initWidth: '60px',
                    tit: titDownscale,
                    opacity: '0',
                };
                widthChange((layoutObj) => ({
                    ...layoutObj,
                    initWidth: tempObj.initWidth,
                    tit: tempObj.tit,
                    opacity: tempObj.opacity,
                }));
                mutate(tempObj);
            } else {
                const tempObj: LayoutObj = {
                    initWidth: '220px',
                    tit: titExpand,
                    opacity: '1',
                };
                widthChange((layoutObj) => ({
                    ...layoutObj,
                    initWidth: tempObj.initWidth,
                    tit: tempObj.tit,
                    opacity: tempObj.opacity,
                }));
                mutate(tempObj);
            }
        };
        window.addEventListener('resize', updateWindowDimensions);
        updateWindowDimensions();

        return () => {
            window.removeEventListener('resize', updateWindowDimensions); //clean up
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div
            id="main_menu"
            style={{width: layoutObj.initWidth, minWidth: layoutObj.initWidth}}>
            <div id="main_top_menu" onClick={layoutChange}>
                <img
                    id="wing"
                    src={wing}
                    style={{
                        opacity: layoutObj.opacity,
                        left: layoutObj.opacity == '0' ? '0rem' : '7.5rem',
                        width: layoutObj.opacity == '0' ? '0rem' : '5rem',
                        height: layoutObj.opacity == '0' ? '0rem' : '3rem',
                    }}
                />
                <div style={{height: '100%'}}>{layoutObj.tit}</div>
            </div>
            <div id="main_mid_1_menu">
                <Mid1Menu width={layoutObj.initWidth} />
            </div>
            <div id="main_mid_2_menu">
                <Mid2Menu width={layoutObj.initWidth} />
            </div>
            {layoutObj.initWidth == '220px' ? (
                <div id="main_mid_3_menu">
                    <Mid3Menu />
                </div>
            ) : null}
            <div id="main_btm_menu">
                <MainBottom width={layoutObj.initWidth} />
            </div>
        </div>
    );
};

export default MainMenu;
