import React from 'react';
import ReactTooltip from 'react-tooltip';
// import Storage from '@override/api/Storage';
import './style.scss';
import help from '@img/help.svg';

const MainBottom = (v: {width: string}) => {
    const width = v.width;
    return (
        <div
            className="main_btm"
            style={{background: width == '220px' ? 'inherit' : '#444444'}}>
            {v.width == '220px' ? (
                <div className="main_btm_expand">
                    <div className="main_btm_top">분석상담 문의</div>
                    <div className="main_btm_contents">
                        <div>
                            <div className="main_btm_mid_1">
                                담당자: 남기준 연구위원
                            </div>
                            <div className="main_btm_mid_2">
                                연락처: 031-450-8335
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <div data-tip="" data-for="main_btm_tooltip">
                    <img className="main_btm_img" src={help} />
                    <ReactTooltip
                        id="main_btm_tooltip"
                        place={'right'}
                        html={true}>
                        {`<div id="tooltip">
                                    <div className="main_btm_top">
                                        분석상담 문의
                                    </div>
                                    <div className="main_btm_contents">
                                        <div>
                                            <div className="main_btm_mid_1">
                                                담당자: 남기준 수석
                                            </div>
                                            <div className="main_btm_mid_2">
                                                연락처: 031-450-8335
                                            </div>
                                        </div>
                                    </div>
                                </div>`}
                    </ReactTooltip>
                </div>
            )}
        </div>
    );
};

export default MainBottom;
