import React, {useEffect, useState} from 'react';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import moment from 'moment-timezone';
import Storage from '@override/api/Storage';
import Avatar from '@mui/material/Avatar';
import {styled} from '@mui/material/styles';
import Badge from '@mui/material/Badge';
import './style.scss';
import {useSWRConfig} from 'swr';
import API from 'override/api/API';
import {Tooltip} from '@mui/material';

let width = '';

const StyledBadge = styled(Badge)(() => ({
    '& .MuiBadge-badge': {
        backgroundColor: '#44b700',
        color: '#44b70000',
        boxShadow: `0 0 0 2px #fff0`,
        bottom: '9%',
        width: '0.4rem',
        height: '0.4rem',
        minWidth: '0.4rem',
        right: '10%',
    },
}));

const SetTime = () => {
    const setTimezone = () => {
        try {
            setTime(moment().tz('Asia/Seoul').format('YYYY-MM-DD HH:mm'));
        } catch (ex) {
            console.warn(ex);
        }
    };
    const [time, setTime] = useState(
        moment().tz('Asia/Seoul').format('YYYY-MM-DD, HH:mm'),
    );

    useEffect(() => {
        const timer = setInterval(() => (setTimezone(), 60000));
        return () => {
            clearTimeout(timer);
        };
    }, []);

    return <>{time}</>;
};

// const SetLocWeather = () => {
//     const {data, mutate} = Storage.getLocationInfo();

//     useEffect(() => {
//         navigator.geolocation.getCurrentPosition(async function (position) {
//             const newValue = {
//                 lat: position.coords.latitude,
//                 lon: position.coords.longitude,
//             };
//             mutate(newValue);
//         });
//         // eslint-disable-next-line react-hooks/exhaustive-deps
//     }, []);

//     return (
//         <>
//             {data ? (
//                 <>
//                     {data.map((x, index) => {
//                         let temper = x.main.temp;
//                         if (temper.toString().split('.').length > 1) {
//                             const temp = temper.toString().split('.');
//                             temper = temp[0];
//                         }

//                         return (
//                             <div
//                                 className="main_weather_content"
//                                 key={`weather_content_${index}`}>
//                                 <div
//                                     className="main_weather_desc"
//                                     key={`weather_desc_${index}`}>
//                                     <div className="main_city_name">
//                                         {x.name}
//                                     </div>
//                                     <div className="main_temper_01">
//                                         {temper}
//                                         <span className="main_temper_02">
//                                             ℃
//                                         </span>
//                                     </div>
//                                 </div>
//                                 <div
//                                     className="main_weather_img"
//                                     key={`weather_img_${index}`}>
//                                     {x.weather.map((v, index2) => {
//                                         if (index2 > 0) {
//                                             return;
//                                         }

//                                         switch (v.icon) {
//                                             case '03d':
//                                             case '03n':
//                                             case '04d':
//                                             case '04n':
//                                                 return (
//                                                     <img
//                                                         key={`weather_img_${index2}`}
//                                                         src={`./src/style/img/weather/03dn.svg`}
//                                                     />
//                                                 );
//                                             case '09d':
//                                             case '09n':
//                                             case '10d':
//                                             case '10n':
//                                             case '11d':
//                                             case '11n':
//                                             case '13d':
//                                             case '13n':
//                                             case '50d':
//                                             case '50n':
//                                                 return (
//                                                     <img
//                                                         key={`weather_img_${index2}`}
//                                                         src={`./src/style/img/weather/${v.icon.substring(
//                                                             0,
//                                                             2,
//                                                         )}dn.svg`}
//                                                     />
//                                                 );
//                                             default:
//                                                 return (
//                                                     <img
//                                                         key={`weather_img_${index2}`}
//                                                         src={`./src/style/img/weather/${v.icon}.svg`}
//                                                     />
//                                                 );
//                                         }
//                                     })}
//                                 </div>
//                             </div>
//                         );
//                     })}
//                 </>
//             ) : null}
//         </>
//     );
// };

const SetUserInfo = () => {
    const {data} = Storage.getUserInfo();
    return (
        <>
            {width == '220px' ? (
                <>
                    <div
                        id="mid_user_nm"
                        style={{
                            marginLeft: width == '220px' ? '0rem' : '0.15rem',
                        }}>
                        {data.userNm}
                    </div>
                    <div id="mid_dept_nm">{data.deptNm}</div>
                </>
            ) : (
                <StyledBadge
                    overlap="circular"
                    anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                    variant="dot">
                    <Avatar
                        style={{
                            width: '1.5rem',
                            fontSize: '1rem',
                            marginLeft: '0.6rem',
                            backgroundColor: '#8083de',
                        }}
                        aria-label="recipe">
                        {data.userNm[0]}
                    </Avatar>
                </StyledBadge>
            )}
        </>
    );
};

const Mid1Menu = (v: {width: string}) => {
    width = v.width;
    const {cache} = useSWRConfig();
    const {data} = Storage.getUserInfo();
    async function logout() {
        const params = {
            bempNo: data.empNo,
        };

        await API.request.post(`api/pass/logout`, params);
        localStorage.clear();
        cache.delete('currMenu');
        window.location.reload();
    }

    return (
        <>
            <div id="main_mid_1">
                <div
                    id="mid1_top"
                    style={{display: width != '220px' ? 'none' : ''}}>
                    <div className="top_time_contents">
                        <SetTime />
                    </div>
                    {/* <SetLocWeather /> */}
                </div>
                <div id="mid1_mid">
                    <SetUserInfo />
                </div>
                <Tooltip title="로그아웃">
                    <div
                        id="mid1_btm"
                        onClick={logout}
                        data-tip=""
                        data-for={'logout'}>
                        <LogoutRoundedIcon
                            id="logout"
                            style={{
                                margin:
                                    width == '220px'
                                        ? '0rem'
                                        : '0.6rem 0rem 0.3rem 1rem',
                            }}
                        />
                        {width == '220px' ? '로그아웃' : ''}
                    </div>
                </Tooltip>
            </div>
        </>
    );
};

export default Mid1Menu;
