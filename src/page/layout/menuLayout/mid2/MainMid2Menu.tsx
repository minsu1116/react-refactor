import React from 'react';
import Storage from '@override/api/Storage';
import API from '@override/api/API';
import useSWR from 'swr';
import Button from '@mui/material/Button';
import getIcons from '@override/icons/Icons';
import {meg} from '@override/alert/Alert';
import './style.scss';
import {Tooltip} from '@mui/material';

const Icon2 = getIcons('List');

function familySiteClose() {
    const familySiteCss =
        document.querySelectorAll<HTMLElement>('.family_child');

    if (familySiteCss[0] && familySiteCss[0].style.opacity == '1') {
        const familySiteCss =
            document.querySelectorAll<HTMLElement>('.family_child');
        familySiteCss[0].style.opacity = '0';
        familySiteCss[0].style.visibility = 'collapse';
        const isFSActive = document.getElementsByClassName(
            'family_site_img close',
        );

        if (isFSActive) {
            const subMenu = isFSActive[0]?.classList;
            subMenu?.remove('close');
            subMenu?.add('plus');
        }
    }
}

async function onCickMainMenu(e, v) {
    const subMenu = v?.subMenu;

    if (subMenu) {
        const currentNode = e.currentTarget.parentNode;
        const activeNodes =
            currentNode.parentNode.getElementsByClassName('active');
        if (activeNodes && activeNodes.length > 0) {
            const beforeSelectedNode =
                currentNode.parentNode.getElementsByClassName('active')[0]
                    .classList;
            beforeSelectedNode.remove('active');
        }
        currentNode.classList.add('active');
    } else {
        await meg({
            title: '하위 메뉴가 없습니다. <br> 관리자에게 문의 하십시오.',
            icon: 'error',
            html: '',
        });
    }
}

const Mid2Menu = (v: {width: string}) => {
    const width = v.width;
    const {data: userInfo} = Storage.getUserInfo();
    const {mutate} = Storage.getCurrentMenu();
    const param = {
        userId: userInfo.empNo,
    };

    const options = {
        revalidateOnFocus: false,
    };

    function onClickMenuOpen(v) {
        mutate(null);
        const isMenuActive =
            document.getElementsByClassName('main_menu active');

        if (isMenuActive) {
            const subMenu = isMenuActive[0]?.classList;
            subMenu?.remove('active');
        }
        mutate(v);
    }

    const {data} = useSWR(
        '/api/pass/login/menu',
        async () => {
            const result = await API.request.post(
                '/api/pass/login/menu',
                param,
            );

            if (result.data.success) {
                return result.data.data;
            } else {
                return result.data.msg;
            }
        },
        options,
    );
    const mainMenuList = [];
    if (data && data.length > 0) {
        data.sort((a, b) => (a.sort > b.sort ? 1 : -1));
        data.map(async (x, index) => {
            const midMenu = x.subMenu;
            const Icon = getIcons(x.iconNm);
            if (midMenu?.length > 0) {
                const result = midMenu.find((x) => x.subMenu?.length > 0);
                if (!result) {
                    return;
                }
                mainMenuList.push(
                    <div className="main_menu" key={'main_menu' + index}>
                        {width == '220px' ? (
                            <Button
                                key={'menu_btn' + x.menuId + index}
                                className="menu_btn"
                                id={x.menuId}
                                variant="outlined"
                                onClick={(event) => onCickMainMenu(event, x)}
                                style={{
                                    paddingLeft:
                                        width == '220px' ? '0.8rem' : '1.45rem',
                                }}
                                startIcon={
                                    <Icon key={'Icon' + x.menuId + index} />
                                }>
                                {x.menuNm}
                            </Button>
                        ) : (
                            <Tooltip title={x.menuNm}>
                                <Button
                                    data-tip=""
                                    data-for={x.menuId}
                                    key={'menu_btn' + x.menuId + index}
                                    className="menu_btn"
                                    id={x.menuId}
                                    variant="outlined"
                                    onClick={(event) =>
                                        onCickMainMenu(event, x)
                                    }
                                    style={{
                                        paddingLeft:
                                            width == '220px'
                                                ? '0.8rem'
                                                : '1.45rem',
                                    }}
                                    startIcon={
                                        <Icon key={'Icon' + x.menuId + index} />
                                    }></Button>
                            </Tooltip>
                        )}
                        {midMenu ? (
                            <div
                                className="mid_menu"
                                style={{
                                    left: width == '220px' ? '220px' : '60px',
                                }}>
                                {midMenu.map((x1, index1) => {
                                    const subMenu = x1.subMenu;
                                    if (!subMenu) {
                                        return;
                                    }
                                    return (
                                        <React.Fragment
                                            key={
                                                'mid_btn' + x1.menuId + index1
                                            }>
                                            <Button
                                                key={
                                                    'mid_btn' +
                                                    x1.menuId +
                                                    index1
                                                }
                                                className="mid_btn"
                                                id={x1.menuId}
                                                variant="outlined"
                                                startIcon={
                                                    <Icon2
                                                        key={
                                                            'Icon2' +
                                                            x1.menuId +
                                                            index1
                                                        }
                                                    />
                                                }>
                                                {x1.menuNm}
                                            </Button>
                                            {subMenu ? (
                                                <div
                                                    className="sub_menu"
                                                    key={
                                                        'sub_menu' +
                                                        x1.menuId +
                                                        index1
                                                    }>
                                                    {subMenu.map(
                                                        (x2, index2) => {
                                                            return (
                                                                <Button
                                                                    key={
                                                                        'sub_btn' +
                                                                        x2.menuId +
                                                                        index2
                                                                    }
                                                                    onClick={() =>
                                                                        onClickMenuOpen(
                                                                            x2,
                                                                        )
                                                                    }
                                                                    className="sub_btn"
                                                                    id={
                                                                        x2.menuId
                                                                    }
                                                                    variant="outlined">
                                                                    {x2.menuNm}
                                                                </Button>
                                                            );
                                                        },
                                                    )}
                                                </div>
                                            ) : null}
                                        </React.Fragment>
                                    );
                                })}
                            </div>
                        ) : null}
                    </div>,
                );
            }
        });
    }

    return (
        <>
            <div id="main_mid_2" onClick={familySiteClose}>
                <div>{mainMenuList}</div>
            </div>
        </>
    );
};

export default Mid2Menu;
