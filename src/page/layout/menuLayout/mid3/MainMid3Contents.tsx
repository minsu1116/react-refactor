/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import Storage from '@override/api/Storage';
import ico_pluse from '@img/ico_plus.png';
import './style.scss';

function openFailySite(url) {
    if (url) {
        window.open(url, '_blank');
    }
}

const Mid3Menu = () => {
    const [isFamilySiteOpen, setValue] = useState<boolean>(false);

    function familySiteSatusChange() {
        const familySiteCss =
            document.querySelectorAll<HTMLElement>('.family_child');
        if (familySiteCss[0]) {
            if (familySiteCss[0].style.opacity == '0' && isFamilySiteOpen) {
                familySiteCss[0].style.opacity = '1';
                familySiteCss[0].style.visibility = 'inherit';
                const isFSActive = document.getElementsByClassName(
                    'family_site_img plus',
                );

                if (isFSActive) {
                    const subMenu = isFSActive[0]?.classList;
                    subMenu?.add('close');
                    subMenu?.remove('plus');
                }
                setValue(isFamilySiteOpen);
                return;
            }
        }
        setValue(!isFamilySiteOpen);
    }
    const commonCodeGroup = Storage.getGroupCode().data;
    const dynamicRenderList = [];
    if (commonCodeGroup) {
        const familySiteList = commonCodeGroup['FAMILY_GROUP'];
        dynamicRenderList.push(
            <ul className="family_group" key="family_group">
                {familySiteList.map((x, i) => {
                    if (x.desc1 == 'label') {
                        return (
                            <li
                                key={`family-site-${i}`}
                                className={'family_label'}>
                                {x.cdNm}
                            </li>
                        );
                    } else {
                        return (
                            <li
                                key={`family-site-${i}`}
                                className={'family_list'}
                                onClick={() => openFailySite(x.desc1)}>
                                <span>{x.cdNm}</span>
                            </li>
                        );
                    }
                })}
            </ul>,
        );
    }
    return (
        <>
            <div className="family_site">
                <div className="family_main" onClick={familySiteSatusChange}>
                    Family Site
                    <img
                        className={
                            !isFamilySiteOpen
                                ? 'family_site_img plus'
                                : 'family_site_img close'
                        }
                        src={ico_pluse}
                    />
                </div>
                <div
                    className="family_child"
                    style={{
                        bottom: !isFamilySiteOpen ? '3.5rem' : '5.6rem',
                        opacity: !isFamilySiteOpen ? 0 : 1,
                        visibility: !isFamilySiteOpen ? 'collapse' : 'inherit',
                    }}>
                    {dynamicRenderList}
                </div>
            </div>
        </>
    );
};

export default Mid3Menu;
