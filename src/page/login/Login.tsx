import React, {useEffect, useRef, useState} from 'react';
import IconButton from '@mui/material/IconButton';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import LockIcon from '@mui/icons-material/Lock';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import LoginIcon from '@mui/icons-material/Login';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import HomeIcon from '@mui/icons-material/Home';
import LockResetIcon from '@mui/icons-material/LockReset';
import PersonAddAlt1Icon from '@mui/icons-material/PersonAddAlt1';
import Storage from '@override/api/Storage';
import API from '@override/api/API';
import logo from '@img/ls_logo_white.png';
import wing from '@img/ls_logo_wing.png';
import Chip from '@mui/material/Chip';
import {meg} from '@override/alert/Alert';
import {useNavigate} from 'react-router-dom';
import {emphasize, styled} from '@mui/material/styles';
import './login.scss';
import SModal from 'override/modal/SModal';
import LocalPoliceIcon from '@mui/icons-material/LocalPolice';

interface LoginObj {
    loginId: string;
    loginPass: string;
    showPassword: boolean;
}

const StyledBreadcrumb = styled(Chip)(({theme}) => {
    const backgroundColor =
        theme.palette.mode === 'light'
            ? theme.palette.grey[100]
            : theme.palette.grey[800];
    return {
        backgroundColor,
        height: theme.spacing(3),
        color: theme.palette.text.primary,
        fontWeight: theme.typography.fontWeightRegular,
        '&:hover, &:focus': {
            backgroundColor: emphasize(backgroundColor, 0.06),
        },
        '&:active': {
            boxShadow: theme.shadows[1],
            backgroundColor: emphasize(backgroundColor, 0.12),
        },
    };
}) as typeof Chip;

const Login = () => {
    const userInfoMutate = Storage.getUserInfo().mutate;
    const groupCodeMutate = Storage.getGroupCode().mutate;
    const [addModalOpen, setAddModalOpen] = useState(false);
    const navigate = useNavigate();
    const [loginObj, setValue] = useState<LoginObj>({
        loginId: '',
        loginPass: '',
        showPassword: false,
    });

    const inputRef = useRef<HTMLInputElement>(null);
    useEffect(() => {
        if (inputRef.current !== null && inputRef) inputRef.current.focus();

        const ele = document.getElementById('ipl-progress-indicator');
        if (ele) {
            ele.classList.add('available');
            ele.outerHTML = '';
        }
    }, []);

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await getLoginUser(loginObj);
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...loginObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setValue(newValue);
    };
    const {loginId, loginPass, showPassword} = loginObj;

    const handleClickShowPassword = () => {
        setValue({
            ...loginObj,
            showPassword: !loginObj.showPassword,
        });
    };

    const handleMouseDownPassword = (
        e: React.MouseEvent<HTMLButtonElement>,
    ) => {
        e.preventDefault();
    };

    const getLoginUser = async (loginObj) => {
        if (loginObj.loginId == '' || loginObj.loginPass == '') {
            const describe = loginObj.loginId == '' ? 'ID' : 'Password';
            await meg({
                title: `${describe}의 값이 없습니다.`,
                icon: 'warning',
                html: '',
            });
            return;
        }

        const params = {
            bempNo: loginObj.loginId,
            bpassword: loginObj.loginPass,
        };
        const param = JSON.stringify(params);
        const result: Result = await API.request.post('/api/pass/login', param);

        if (result.data.success) {
            const token = result.data.data.accessToken;
            localStorage.setItem('jwt', token);

            const userInfo: UserInfo = {
                compCd: result.data.data.bcompCd,
                compNm: result.data.data.bcompNm,
                deptNm: result.data.data.bdeptNm,
                deptCd: result.data.data.bdeptCd,
                empNo: result.data.data.bempNo,
                email: result.data.data.bemail,
                userNm: result.data.data.buserNm,
                hrYn: result.data.data.bhrYn,
                officeNo: result.data.data.bofficeNo,
                mobileNo: result.data.data.bmobileNo,
                rankNm: result.data.data.brankNm,
                addressEn: result.data.data.baddressEn,
                addressKr: result.data.data.baddressKr,
            };

            await userInfoMutate(userInfo);

            const params = {
                cdGubn: '',
                useFlag: 'Y',
                compCd: result.data.data.bcompCd,
            };

            const codResult = await API.request.post(
                'api/pass/login/codes',
                params,
            );

            if (codResult.data.success) {
                await groupCodeMutate(codResult.data.data);
            }

            navigate('/app', {replace: true});
        } else {
            await meg({
                title: '사용자 정보가 다릅니다. <br> 재입력 하십시오.',
                icon: 'error',
                html: '',
            });
        }
    };

    const onAddModalClose = () => {
        setAddModalOpen(false);
    };

    const onAddModalOpen = () => {
        setAddModalOpen(true);
    };

    return (
        <React.Fragment>
            <SModal
                tit={'회원가입'}
                isOpen={addModalOpen}
                sizeObj={{
                    width: '430',
                    height: '860',
                    isResize: false,
                }}
                path={'login/addUserPopup/AddUserP'}
                onClose={onAddModalClose}
            />
            <div className="login_Main">
                <div className="back_main animate__animated animate__backInLeft">
                    <div className="back_tit">
                        <img className="back_logo img" src={logo} />
                        <div className="tit_1">계정이 없으신가요?</div>
                        <div className="sub">
                            <div className="desc">
                                <span>계정 생성을 ? </span>
                                <PersonAddAlt1Icon
                                    className="animate__animated animate__heartBeat animate__slower animate__infinite"
                                    onClick={onAddModalOpen}
                                    id="add_img"
                                />
                                <span>
                                    {' 회원가입 버튼을 클릭 하여 주십시오.'}
                                </span>
                            </div>
                            <div>
                                로그인은{' '}
                                <span
                                    style={{
                                        background: '#8d8d8d8f',
                                        padding: '0px 2px 0px 2px',
                                        borderRadius: '5px',
                                    }}>
                                    관리자 승인
                                </span>{' '}
                                이후에 가능합니다.
                            </div>
                        </div>
                        <div className="bottom_info">
                            <Button
                                id="siginBtn"
                                variant="outlined"
                                onClick={onAddModalOpen}
                                startIcon={<PersonAddAlt1Icon />}>
                                회원가입
                            </Button>
                            <div className="com_info">
                                <div className="tel">
                                    <LocalPhoneIcon className="info" />
                                    <span className="info_txt">
                                        031-450-8114
                                    </span>
                                </div>
                                <div className="address">
                                    <HomeIcon className="info" />
                                    <span className="info_txt">
                                        {`군포시 공단로 140번길 27(당정동) LS전선 연구소`}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="back_mid">
                        <LocalPoliceIcon />
                        COPYRIGHT
                        <span
                            style={{
                                fontSize: '13px',
                                position: 'relative',
                                top: '-1px',
                            }}>
                            ⓒ
                        </span>
                        2022 BY LS전선. ALL RIGHTS RESERVED.
                    </div>
                </div>
                <div className="login_page">
                    <img
                        className="wing animate__animated animate__fadeInLeft animate__slower"
                        src={wing}
                    />
                    <div className="login_tit">HELLO TAS</div>
                    <div className="main_form">
                        <label className="IdText">
                            <TextField
                                id="loginId"
                                onChange={onChange}
                                onKeyDown={handleKeyDown}
                                label="아이디"
                                value={loginId}
                                placeholder={'Wels ID'}
                                autoFocus={true}
                                ref={inputRef}
                                sx={{
                                    width: '100%',
                                    mt: 2,
                                    opacity: 0.9,
                                }}
                            />
                        </label>
                        <label className="PassText">
                            <FormControl
                                variant="outlined"
                                sx={{
                                    width: '100%',
                                    mb: 2,
                                    mt: 2,
                                    opacity: 0.9,
                                }}>
                                <InputLabel htmlFor="outlined-adornment-password">
                                    비밀번호
                                </InputLabel>
                                <OutlinedInput
                                    id="loginPass"
                                    type={showPassword ? 'text' : 'password'}
                                    value={loginPass}
                                    placeholder={'Wels PWD'}
                                    autoComplete="on"
                                    onChange={onChange}
                                    onKeyDown={handleKeyDown}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={
                                                    handleClickShowPassword
                                                }
                                                onMouseDown={
                                                    handleMouseDownPassword
                                                }
                                                edge="end">
                                                {showPassword ? (
                                                    <LockOpenIcon
                                                        sx={{color: '#707faa'}}
                                                    />
                                                ) : (
                                                    <LockIcon
                                                        sx={{color: '#cd9395'}}
                                                    />
                                                )}
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                    label="Password"
                                />
                            </FormControl>
                        </label>
                    </div>
                    <div className="navi_contents">
                        <StyledBreadcrumb
                            component="div"
                            disabled={true}
                            id="navi"
                            label="비밀번호 찾기"
                            icon={<LockResetIcon fontSize="small" />}
                        />
                    </div>
                    <div className="login_btn">
                        <Button
                            id="loginBtn"
                            variant="outlined"
                            onClick={() => getLoginUser(loginObj)}
                            startIcon={<LoginIcon />}>
                            LOG-IN
                        </Button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Login;
