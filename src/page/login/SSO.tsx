import React, {useEffect} from 'react';
import Storage from '@override/api/Storage';
import API from '@override/api/API';
import {useParams, useNavigate} from 'react-router-dom';
import './login.scss';

const SSO = () => {
    const params = useParams();
    const navigate = useNavigate();
    const userInfoMutate = Storage.getUserInfo().mutate;
    const groupCodeMutate = Storage.getGroupCode().mutate;
    // const navigate = useNavigate();

    useEffect(() => {
        if (params.id) {
            getSSO();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [params]);

    const getSSO = async () => {
        const param = {
            loginKey: params.id,
        };

        const result = await API.request.post('api/pass/sso/get', param);

        if (result.data.success) {
            const token = result.data.data.accessToken;
            localStorage.setItem('jwt', token);

            const userInfo: UserInfo = {
                compCd: result.data.data.bcompCd,
                compNm: result.data.data.bcompNm,
                deptNm: result.data.data.bdeptNm,
                deptCd: result.data.data.bdeptCd,
                empNo: result.data.data.bempNo,
                email: result.data.data.bemail,
                userNm: result.data.data.buserNm,
                hrYn: result.data.data.bhrYn,
                officeNo: result.data.data.bofficeNo,
                mobileNo: result.data.data.bmobileNo,
                rankNm: result.data.data.brankNm,
                addressEn: result.data.data.baddressEn,
                addressKr: result.data.data.baddressKr,
            };
            await userInfoMutate(userInfo);

            const params = {
                cdGubn: '',
                useFlag: 'Y',
                compCd: result.data.data.bcompCd,
            };

            const codResult = await API.request.post(
                'api/pass/login/codes',
                params,
            );

            if (codResult.data.success) {
                await groupCodeMutate(codResult.data.data);
            }

            navigate('/app', {replace: true});
        }
    };
    return (
        <React.Fragment>
            <div className="animate__animated animate__heartBeat animate__slower animate__infinite">
                SSO 진행중입니다.
            </div>
        </React.Fragment>
    );
};

export default SSO;
