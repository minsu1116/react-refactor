/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import AccountCircle from '@mui/icons-material/AccountCircle';
import VpnKeyIcon from '@mui/icons-material/VpnKey';

import HomeIcon from '@mui/icons-material/Home';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import BadgeIcon from '@mui/icons-material/Badge';
import GroupIcon from '@mui/icons-material/Group';
import LocalPoliceIcon from '@mui/icons-material/LocalPolice';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import ForwardToInboxIcon from '@mui/icons-material/ForwardToInbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Autocomplete from '@mui/material/Autocomplete';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import {IMaskInput} from 'react-imask';
import Checkbox from '@mui/material/Checkbox';

import Input from '@mui/material/Input';
import InputLabel from '@mui/material/InputLabel';

import './style.scss';
import API from 'override/api/API';
import {confirm, meg} from 'override/alert/Alert';

interface CustomProps {
    onChange: (event: {target: {id: string; value: string}}) => void;
    id: string;
}

interface UserInfo {
    empNo: string;
    deptNm: string;
    password: string;
    passwordVali: string;
    compNm: string;
    mobileNo: string;
    officeNo: string;
    email: string;
    posNm: string;
    addressKr: string;
    addressEn: string;
    userNm: string;
    userEnNm: string;
    useApprYn: string;
    hrYn: string;
}

const TextMaskCustom = React.forwardRef<HTMLElement, CustomProps>(
    function TextMaskCustom(props, ref: any) {
        const [values, setValues] = useState('');
        const [mask, setMask] = useState('#00-0000-0000');
        const {onChange, ...other} = props;

        const otherList = useMemo(() => {
            return {
                ...other,
                onBlur: () => {
                    if (values.slice(0, 2) == '02') {
                        if (values.length == 11) {
                            setMask('#0-000-0000');
                        } else {
                            setMask('#0-0000-0000');
                        }
                    } else {
                        if (values.length == 12) {
                            setMask('#00-000-0000');
                        } else {
                            setMask('#00-0000-0000');
                        }
                    }
                },
                onFocus: () => {
                    if (values.slice(0, 2) == '02') {
                        setMask('#0-0000-0000');
                    } else {
                        setMask('#00-0000-0000');
                    }
                },
            };
        }, [other, values]);

        return (
            <IMaskInput
                {...otherList}
                mask={mask}
                definitions={{
                    '#': /[0-9]/,
                }}
                inputRef={ref}
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                onAccept={(value: any) => {
                    onChange({target: {id: props.id, value}});
                    setValues(value);
                }}
                overwrite
            />
        );
    },
);

const AddUserP = (param) => {
    const [personalInfoYn, setPersonalInfoYn] = useState(false);
    const [userIdList, setUserIdList] = useState([]);
    const [companyList, setCompanyList] = useState([]);
    const [userInfo, setUserInfo] = useState<UserInfo>({
        empNo: '',
        deptNm: '',
        password: '',
        passwordVali: '',
        compNm: '',
        mobileNo: '',
        officeNo: '',
        email: '',
        posNm: '',
        addressKr: '',
        addressEn: '',
        userNm: '',
        userEnNm: '',
        useApprYn: 'N',
        hrYn: 'N',
    });

    const [validationList, setValidationList] = useState({
        empNo: false,
        passwordVali: false,
        password: false,
        officeNo: false,
        email: false,
        compNm: false,
        addressKr: false,
        userNm: false,
    });

    const [validationDesc, setValidationDescList] = useState({
        empNoDesc: '필수값 입니다.',
        passwordDesc: '필수값 입니다.',
        passwordValiDesc: '필수값 입니다.',
        officeNoDesc: '필수값 입니다.',
        compNmDesc: '필수값 입니다.',
        emailDesc: '필수값 입니다.',
        addressKrDesc: '필수값 입니다.',
        userNmDesc: '필수값 입니다.',
    });

    const inputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (inputRef.current !== null && inputRef) inputRef.current.focus();
        getUserList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getUserList = useCallback(async () => {
        const result = await API.request.post(`api/pass/get/user-list`);
        if (result.data.success) {
            setUserIdList(result.data.data);

            const tempCompList = [];
            result.data.data
                .filter((x) => x.compNm != 'LS전선')
                .forEach((element) => {
                    if (!tempCompList.find((x) => x.label == element.compNm)) {
                        tempCompList.push({
                            label: element.compNm,
                        });
                    }
                });

            setCompanyList(tempCompList);
        }
    }, []);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = {
            ...userInfo, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value.trim(), // 덮어쓰기
        };

        setUserInfo(newValue);
    };

    const onCheckValidation = (id, value) => {
        switch (id) {
            case 'empNo':
                if (userIdList.find((x) => x.userId === value)) {
                    setValidationDescList({
                        ...validationDesc,
                        empNoDesc: '중복/ 탈퇴 한 ID입니다.',
                    });
                    setValidationList({
                        ...validationList,
                        empNo: true,
                    });
                } else {
                    function isId(value) {
                        const regExp = /^[a-z]+[a-z0-9]{5,19}$/g;

                        return regExp.test(value);
                    }

                    if (!isId(value)) {
                        setValidationDescList({
                            ...validationDesc,
                            empNoDesc: '영문/숫자 조합 6~20글자',
                        });
                        setValidationList({
                            ...validationList,
                            empNo: true,
                        });
                    } else {
                        setValidationDescList({
                            ...validationDesc,
                            empNoDesc: '',
                        });
                        setValidationList({
                            ...validationList,
                            empNo: false,
                        });
                    }
                }
                break;
            case 'email':
                function isEmail(value) {
                    const regExp =
                        /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

                    return regExp.test(value);
                }
                if (!isEmail(value)) {
                    setValidationDescList({
                        ...validationDesc,
                        emailDesc: 'E-Mail 형식이 아닙니다.',
                    });
                    setValidationList({
                        ...validationList,
                        email: true,
                    });
                } else {
                    setValidationDescList({
                        ...validationDesc,
                        emailDesc: '',
                    });
                    setValidationList({
                        ...validationList,
                        email: false,
                    });
                }
                break;
            case 'password':
                function isPassword(value) {
                    const regExp =
                        /^(?=.*[a-zA-z])(?=.*[0-9])(?=.*[$`~!@$!%*#^?&\\(\\)\-_=+]).{8,16}$/;

                    return regExp.test(value); // 형식에 맞는 경우 true 리턴
                }
                if (!isPassword(value)) {
                    const newValueDesc = {
                        ...validationDesc,
                        passwordDesc:
                            '8 ~ 16자 영문, 숫자, 특수문자를 최소 한가지씩 조합',
                    };

                    const newValueVali = {
                        ...validationList,
                        password: true,
                    };

                    if (
                        userInfo.passwordVali &&
                        value != userInfo.passwordVali
                    ) {
                        newValueDesc.passwordValiDesc =
                            '비밀번호가 일치하지 않습니다.';
                        newValueVali.passwordVali = true;
                    }

                    setValidationDescList(newValueDesc);
                    setValidationList(newValueVali);
                } else {
                    const newValueDesc = {
                        ...validationDesc,
                        passwordDesc: '',
                    };

                    const newValueVali = {
                        ...validationList,
                        password: false,
                    };

                    if (
                        userInfo.passwordVali &&
                        value != userInfo.passwordVali
                    ) {
                        newValueDesc.passwordValiDesc =
                            '비밀번호가 일치하지 않습니다.';
                        newValueVali.passwordVali = true;
                    }

                    setValidationDescList(newValueDesc);
                    setValidationList(newValueVali);
                }
                break;
            case 'passwordVali':
                if (value != userInfo.password) {
                    setValidationDescList({
                        ...validationDesc,
                        passwordValiDesc: '비밀번호가 일치하지 않습니다.',
                    });
                    setValidationList({
                        ...validationList,
                        passwordVali: true,
                    });
                } else {
                    setValidationDescList({
                        ...validationDesc,
                        passwordValiDesc: '',
                    });
                    setValidationList({
                        ...validationList,
                        passwordVali: false,
                    });
                }
                break;
            case 'compNm':
                if (
                    value === 'LS전선' ||
                    value === 'ls전선' ||
                    value === 'lscns' ||
                    value === 'LSCNS' ||
                    value === '엘에스전선'
                ) {
                    setValidationDescList({
                        ...validationDesc,
                        compNmDesc: `${value}는(은) 입력 할 수 없습니다.`,
                    });
                    setValidationList({
                        ...validationList,
                        compNm: true,
                    });
                } else {
                    setValidationDescList({
                        ...validationDesc,
                        compNmDesc: '',
                    });
                    setValidationList({
                        ...validationList,
                        compNm: false,
                    });
                }
        }
    };

    return (
        <React.Fragment>
            <Box className="add_user_main" sx={{'& > :not(style)': {m: 1}}}>
                <div className="btn_contetns">
                    {!personalInfoYn ? (
                        <IconButton
                            id="send_btn"
                            disabled={!personalInfoYn}
                            className="btn_group">
                            <ForwardToInboxIcon className="btn_icon" />
                        </IconButton>
                    ) : (
                        <Tooltip title="가입하기">
                            <IconButton
                                id="send_btn"
                                onClick={async () => {
                                    const keyList = Object.keys(validationList);

                                    const newValue = {
                                        ...validationList,
                                    };

                                    let comfirm = true;
                                    keyList.forEach((element) => {
                                        if (!userInfo[element]) {
                                            newValue[element] = true;
                                            comfirm = false;
                                        }
                                    });

                                    setValidationList(newValue);

                                    if (comfirm) {
                                        const confirmResult = await confirm({
                                            title: '저장하시겠습니까?',
                                            html: '',
                                        });
                                        if (confirmResult) {
                                            const result =
                                                await API.request.post(
                                                    `api/pass/user-info/save`,
                                                    userInfo,
                                                );

                                            if (result.data.success) {
                                                await meg({
                                                    title: `가입승인 후 메일로 회신됩니다.`,
                                                    html: '',
                                                });

                                                param.onClose();
                                            }
                                        }
                                    }
                                }}
                                className="btn_group">
                                <ForwardToInboxIcon className="btn_icon" />
                            </IconButton>
                        </Tooltip>
                    )}
                </div>
                <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="empNo"
                            label="아이디"
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            inputRef={inputRef}
                            onChange={handleChange}
                            error={validationList.empNo}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircle />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.empNo && (
                            <FormHelperText>
                                {validationDesc.empNoDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="email"
                            label="E-MAIL"
                            error={validationList.email}
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            onChange={handleChange}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AlternateEmailIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.email && (
                            <FormHelperText>
                                {validationDesc.emailDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="password"
                            onChange={handleChange}
                            error={validationList.password}
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            label="비밀번호"
                            type="password"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <VpnKeyIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.password && (
                            <FormHelperText>
                                {validationDesc.passwordDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="passwordVali"
                            onChange={handleChange}
                            label="비밀번호 확인"
                            error={validationList.passwordVali}
                            onBlur={(e) =>
                                onCheckValidation(e.target.id, e.target.value)
                            }
                            type="password"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <VpnKeyIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.passwordVali && (
                            <FormHelperText>
                                {validationDesc.passwordValiDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="userNm"
                            onChange={handleChange}
                            label="이름"
                            error={validationList.userNm && !userInfo.userNm}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircle />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.userNm && !userInfo.userNm && (
                            <FormHelperText>
                                {'필수 입력값입니다.'}
                            </FormHelperText>
                        )}
                    </FormControl>
                    <FormControl size="small">
                        <Autocomplete
                            freeSolo
                            id="compNm"
                            value={userInfo.compNm}
                            options={companyList}
                            onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                                setUserInfo({
                                    ...userInfo,
                                    compNm: e.target.value,
                                });
                            }}
                            sx={{width: 175}}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="회사명"
                                    value={userInfo.compNm}
                                    onBlur={(e) =>
                                        onCheckValidation(
                                            e.target.id,
                                            e.target.value.trim(),
                                        )
                                    }
                                    id="compNm"
                                    onChange={handleChange}
                                    error={
                                        validationList.compNm &&
                                        !userInfo.compNm
                                    }
                                    variant="standard"
                                />
                            )}
                        />
                        {validationList.compNm && (
                            <FormHelperText>
                                {validationDesc.compNmDesc}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl size="small">
                        <TextField
                            id="deptNm"
                            onChange={handleChange}
                            label="부서"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <GroupIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                    <FormControl size="small">
                        <TextField
                            id="posNm"
                            onChange={handleChange}
                            label="직급"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <BadgeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl size="small">
                        <FormControl
                            className="tel_format"
                            size="small"
                            variant="standard">
                            <InputLabel
                                htmlFor="officeNo"
                                sx={{
                                    color:
                                        validationList.officeNo &&
                                        !userInfo.officeNo
                                            ? '#d52f2f'
                                            : '#00000099',
                                }}>
                                회사 연락처
                            </InputLabel>
                            <Input
                                value={
                                    userInfo.officeNo ? userInfo.officeNo : ''
                                }
                                error={
                                    validationList.officeNo &&
                                    !userInfo.officeNo
                                }
                                onChange={handleChange}
                                id="officeNo"
                                inputComponent={TextMaskCustom as any}
                            />
                            {validationList.officeNo && !userInfo.officeNo && (
                                <FormHelperText>
                                    {'필수 입력값입니다.'}
                                </FormHelperText>
                            )}
                        </FormControl>
                        <FormControl
                            className="tel_format"
                            size="small"
                            variant="standard">
                            <InputLabel htmlFor="mobileNo">휴대폰</InputLabel>
                            <Input
                                value={
                                    userInfo.mobileNo ? userInfo.mobileNo : ''
                                }
                                onChange={handleChange}
                                name="mobileNo"
                                id="mobileNo"
                                inputComponent={TextMaskCustom as any}
                            />
                        </FormControl>
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl size="small" variant="standard">
                        <TextField
                            id="addressKr"
                            onChange={handleChange}
                            label="회사 주소(국문)"
                            error={
                                validationList.addressKr && !userInfo.addressKr
                            }
                            className="full_contents"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <HomeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                        {validationList.addressKr && !userInfo.addressKr && (
                            <FormHelperText sx={{marginLeft: '17px'}}>
                                {'필수 입력값입니다.'}
                            </FormHelperText>
                        )}
                    </FormControl>
                </div>
                <div className="add_contents">
                    <FormControl size="small" variant="standard">
                        <TextField
                            id="addressEn"
                            onChange={handleChange}
                            label="회사 주소(영문)"
                            className="full_contents"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <HomeIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="standard"
                        />
                    </FormControl>
                </div>
                <Card sx={{maxWidth: 405, height: 275}}>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            <LocalPoliceIcon
                                sx={{
                                    fontSize: '1.5rem',
                                    position: 'relative',
                                    top: '2px',
                                    color: '#458723',
                                }}
                            />
                            개인정보 수집 이용 동의
                        </Typography>
                        <Typography
                            component="div"
                            sx={{height: 150, overflow: 'auto'}}>
                            <div>
                                (주)LS전선(이하 ‘회사’라 합니다)는 이용자의
                                개인정보를 중요시하며, 정보통신망 이용촉진 및
                                정보보호에 관한 법률을 준수하고 있습니다. 회사는
                                개인정보수집정책을 통하여 이용자가 제공하는
                                개인정보가 어떠한 용도와 방식으로 이용되고
                                있으며, 개인정보보호를 위해 어떠한 조치가
                                취해지고 있는지 알려드립니다.
                            </div>
                            <div>
                                <div>1. 수집하는 개인정보 항목 및 수집방법</div>
                                <div>
                                    회사는 문의 접수 및 응대 등을 위해 다음과
                                    같은 개인정보를 수집하고 있습니다.
                                </div>
                                <div>
                                    수집항목: 이름, 이메일, 휴대폰 번호, 주소
                                </div>
                            </div>
                            <div>
                                <div>2. 개인정보 수집 및 이용목적</div>
                                <div>
                                    회사는 수집한 개인정보를 다음의 목적을 위해
                                    활용합니다.
                                </div>
                                <div>
                                    - 이용목적: 본인 식별 확인, 문의 접수 및
                                    응대 등
                                </div>
                            </div>
                            <div>
                                <div>3. 개인정보의 보유 및 이용기간</div>
                                <div>
                                    원칙적으로, 개인정보 수집 및 이용목적이
                                    달성된 후에는 해당 정보를 지체 없이
                                    파기합니다. 단, 다음의 정보에 대해서는
                                    아래의 이유로 명시한 기간 동안 보존합니다.
                                </div>
                                <div>- 보존항목: 이름, 이메일, 휴대폰 번호</div>
                                <div>- 보존근거: 접수 및 응대</div>
                                <div>
                                    - 보존기간: 회원 탈퇴전 또는 휴먼계정 1년
                                </div>
                            </div>
                        </Typography>
                    </CardContent>
                    <CardActions id="personal_check_info">
                        <FormGroup>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        color="success"
                                        checked={personalInfoYn}
                                        onChange={(e) =>
                                            setPersonalInfoYn(e.target.checked)
                                        }
                                    />
                                }
                                label="동의"
                            />
                        </FormGroup>
                    </CardActions>
                </Card>
            </Box>
        </React.Fragment>
    );
};

export default AddUserP;
