import React, {useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import DatePickerRange from 'components/atoms/datePickerRange/DatePickerRange';
import TextField from '@mui/material/TextField';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';
import moment from 'moment';
import API from 'override/api/API';
import './style.scss';

const LoginLog = () => {
    const [searchObj, setSearchObj] = useState({
        searchDateFrom: moment().format('YYYYMMDD'),
        searchDateTo: moment().add(7, 'days').format('YYYYMMDD'),
        empNo: '',
    });
    const [rowData, setRowData] = useState([]);
    const columnDefs = [
        {headerName: '회사명', field: 'compNm', width: 110},
        {headerName: '부서명', field: 'deptNm', width: 110},
        {
            headerName: '사번',
            field: 'empNo',
            width: 100,
            align: 'center',
            hide: true,
        },
        {headerName: '이름', field: 'empNm', width: 80, align: 'center'},
        {headerName: '직급', field: 'rankNm', width: 80, align: 'center'},
        {
            headerName: '전화번호',
            field: 'officeNo',
            width: 100,
            align: 'center',
        },
        {headerName: 'IP', field: 'ip', width: 120, align: 'center'},
        {
            headerName: '성공여부',
            field: 'successYn',
            width: 80,
            align: 'center',
        },
        {
            headerName: '요청일',
            field: 'requestDtm',
            width: 150,
            type: 'dateTimeFull',
        },
    ];

    const returnValueChange = (param) => {
        setSearchObj({
            ...searchObj,
            searchDateFrom: moment(param[0]).format('YYYYMMDD'),
            searchDateTo: moment(param[1]).format('YYYYMMDD'),
        });
    };

    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(
            `api/system/mng/log/login/getloginlog`,
            param,
        );

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    return (
        <div className="sys_mng_login_log">
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <DatePickerRange
                            startTit={'로그인 이력 (From)'}
                            endTit={'로그인 이력 (to)'}
                            disabled={false}
                            fromDate={searchObj.searchDateFrom}
                            toDate={searchObj.searchDateTo}
                            returnValueChange={returnValueChange}
                        />
                        <TextField
                            id="empNo"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'사번/이름'}
                            value={searchObj.empNo}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'sys_etc_loginLog_loginGrid'}
                    rowData={rowData}
                    title={'로그인 정보'}
                    columnDefs={columnDefs}
                />
            </div>
        </div>
    );
};
export default LoginLog;
