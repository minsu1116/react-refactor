import React, {useEffect, useMemo, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Storage from '@override/api/Storage';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';

import API from 'override/api/API';
import './style.scss';
import SModal from 'override/modal/SModal';

const NoticeReg = () => {
    const commonCodeGroup = Storage.getGroupCode().data;
    const {data: userInfo} = Storage.getUserInfo();
    const [readOnly, setReadOnly] = useState(false);
    const [searchObj, setSearchObj] = useState({
        title: '',
    });

    const sizeObj = useMemo(() => {
        return {
            width: '750',
            height: '820',
            isResize: false,
        };
    }, []);

    const [selectedObj, setSelectedObj] = useState();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const {data} = Storage.getGridList();
    const noticeGrid = data?.get('noitce_reg_grid');

    const [rowData, setRowData] = useState([]);

    useEffect(() => {
        if (commonCodeGroup) {
            if (
                !commonCodeGroup['COMM_REG_GROUP'].find(
                    (x) => x.cd == userInfo.empNo,
                )
            ) {
                setReadOnly(true);
            }
        }
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columnDefs = [
        {headerName: '', field: 'check', width: 32, type: 'checkbox'},
        {headerName: '제목', field: 'title', width: 300, align: 'center'},
        {headerName: '조회수', field: 'rowCnt', width: 80, type: 'number'},
        {
            headerName: '등록일시',
            field: 'inputDtm',
            width: 120,
            align: 'center',
            type: 'dateTimeFull',
        },
    ];
    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.request.post(
            `api/service/common/notice/get`,
            param,
        );

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const onNoticeModalOpen = () => {
        setSelectedObj(null);
        setIsModalOpen(true);
    };

    const onNoticeModalClose = async () => {
        setIsModalOpen(false);
        await handleSearchClick();
    };

    const handleChoiceClick = () => {
        setIsModalOpen(true);
    };

    const handelNoticeDelete = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };
        const result = await API.request.post(
            `api/service/common/notice/delete`,
            params,
        );

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const onSelectChange = async () => {
        if (data) {
            if (noticeGrid && noticeGrid.getSelectedRows()) {
                setSelectedObj(noticeGrid.getSelectedRows()[0]);
            }
        }
    };

    return (
        <div className="sys_etc_noitce_reg">
            <SModal
                tit={'자료 등록'}
                param={selectedObj}
                isOpen={isModalOpen}
                sizeObj={sizeObj}
                path={'system/etc/noticeReg/popup/NoticeRegP'}
                onClose={onNoticeModalClose}
            />
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <TextField
                            id="title"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'제목'}
                            value={searchObj.title}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'noitce_reg_grid'}
                    rowData={rowData}
                    isModalOpen={!readOnly}
                    title={'공지사항 목록'}
                    rowDoubleClick={handleChoiceClick}
                    onSelectionChanged={onSelectChange}
                    onClickDelete={!readOnly ? handelNoticeDelete : null}
                    onModalOpen={onNoticeModalOpen}
                    columnDefs={columnDefs}
                />
            </div>
        </div>
    );
};
export default NoticeReg;
