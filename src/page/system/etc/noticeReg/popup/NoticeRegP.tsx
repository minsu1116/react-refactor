import React, {useEffect, useState} from 'react';
import {Seditor} from 'components/atoms/editor/Seditor';
import DatePickerRange from 'components/atoms/datePickerRange/DatePickerRange';
import SSelectbox from 'components/atoms/select/SSelectbox';
import Storage from '@override/api/Storage';
import SFile, {saveFilesEditAll} from 'components/atoms/file/SFile';
import {validation} from '@override/alert/Alert';
import TextField from '@mui/material/TextField';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import moment from 'moment';
import API from 'override/api/API';
import './style.scss';

interface Param {
    readOnly: boolean;
    selectedObj?: NoticeModal;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    param: any;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onClose: Function;
}

interface NoticeModal {
    title: string;
    contents: string;
    startDtm: string;
    endDtm: string;
    notiGroup: string;
    notiId?: number;
    fileContsId?: string;
    fileGrpId?: string;
}

const NoticeRegP = (params: Param) => {
    const commonCodeGroup = Storage.getGroupCode().data;
    const {data: userInfo} = Storage.getUserInfo();
    const {data} = Storage.getGridList();
    const [modal, setModal] = useState<NoticeModal>({
        title: params.param ? params.param.title : '',
        contents: params.param ? params.param.contents : '',
        startDtm: params.param
            ? params.param.startDtm
            : moment().format('YYYYMMDD'),
        endDtm: params.param
            ? params.param.endDtm
            : moment().add(7, 'days').format('YYYYMMDD'),
        notiGroup: params.param ? params.param.notiGroup : '',
        notiId: params.param ? params.param.notiId : 0,
        fileContsId: params.param ? params.param.fileContsId : '',
        fileGrpId: params.param ? params.param.fileGrpId : '',
    });
    const [readOnly, setReadOnly] = useState(false);

    useEffect(() => {
        if (commonCodeGroup) {
            if (
                !commonCodeGroup['COMM_REG_GROUP'].find(
                    (x) => x.cd == userInfo.empNo,
                )
            ) {
                setReadOnly(true);
            }
        }

        return function cleanup() {
            setModal(null);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [modal]);

    const onChange = (e) => {
        const newModal = {
            ...modal, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setModal(newModal);
    };

    const handleContetnsChange = (value) => {
        modal.contents = value;
    };

    const returnValueChange = (param) => {
        setModal({
            ...modal,
            startDtm: moment(param[0]).format('YYYYMMDD'),
            endDtm: moment(param[1]).format('YYYYMMDD'),
        });
    };

    const onSaveNotice = async () => {
        const fileUploadGrid = data?.get('noticeP_upload_grid');
        const fileSaveList = [];
        const validationKeyList = {
            title: '제목',
            startDtm: '공지기간(from)',
            endDtm: '공지기간(to)',
            contents: '내용',
            notiGroup: '공지대상',
        };
        if (fileUploadGrid) {
            fileUploadGrid
                ?.getModel()
                ?.rowsToDisplay.map((item) => fileSaveList.push(item.data));
            if (fileSaveList.length > 0) {
                const result = await saveFilesEditAll(fileSaveList);
                modal.fileGrpId = result;
            }
        }

        const isTrue = await validation(validationKeyList, modal);

        if (isTrue) {
            const result = await API.request.post(
                `api/service/common/notice/save`,
                modal,
            );

            if (result.data.success) {
                params.onClose();
            }
        }
    };
    return (
        <div className="notice_p">
            <div className="popup_btns">
                <Tooltip title="저장">
                    <IconButton
                        onClick={onSaveNotice}
                        className="btn_group"
                        disabled={readOnly}>
                        <SaveAltIcon className="btn_icon" />
                    </IconButton>
                </Tooltip>
            </div>
            <div className="table_main">
                <div className="table_tr">
                    <div className="table_td">
                        <div className="table_tit essential">{'제목'}</div>
                        <div className="table_contents">
                            <TextField
                                id="title"
                                onChange={onChange}
                                disabled={readOnly}
                                value={modal.title}
                                placeholder="타이틀을 입력해 주십시오."
                                size="small"
                            />
                        </div>
                    </div>
                </div>
                <div className="table_tr">
                    <div className="table_td">
                        <div className="table_tit essential">{'공지기간'}</div>
                        <div className="table_contents">
                            <DatePickerRange
                                startTit={'공지기간 (From)'}
                                endTit={'공지기간 (to)'}
                                fromDate={modal.startDtm}
                                disabled={readOnly}
                                toDate={modal.endDtm}
                                returnValueChange={returnValueChange}
                            />
                        </div>
                    </div>
                </div>
                <div className="table_tr">
                    <div className="table_td">
                        <div className="table_tit essential">{'공지대상'}</div>
                        <div className="table_contents">
                            <SSelectbox
                                isCodeGroupMode={true}
                                codeGroup={'NOTICE_GROUP'}
                                label={'공지대상'}
                                value={modal.notiGroup}
                                disabled={readOnly}
                                id={'notiGroup'}
                                handleChange={onChange}
                            />
                        </div>
                    </div>
                </div>
                <div className="table_tr">
                    <div className="table_td">
                        <div className="table_tit essential">{'내용'}</div>
                        <div className="table_contents">
                            <Seditor
                                readOnly={readOnly}
                                modal={modal.contents}
                                handleChange={handleContetnsChange}
                            />
                        </div>
                    </div>
                </div>
                <div
                    className="table_tr"
                    style={{
                        padding: '0rem',
                        marginBottom: '5px',
                        position: 'relative',
                        top: '0px',
                    }}>
                    <div className="table_td" style={{width: '100%'}}>
                        <div
                            className="table_tit"
                            style={{position: 'absolute', top: '20px'}}>
                            {'첨부파일'}
                        </div>
                        <div className="table_contents">
                            <SFile
                                gridId={'noticeP_upload_grid'}
                                fileGrpId={modal.fileGrpId}
                                useDownload={!readOnly}
                                useFileAttach={!readOnly}
                                useFileDelete={!readOnly}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default NoticeRegP;
