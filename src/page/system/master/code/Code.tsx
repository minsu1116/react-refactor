import React, {useEffect, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import API from 'override/api/API';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Storage from '@override/api/Storage';
import SSelectbox from 'components/atoms/select/SSelectbox';
import {meg} from 'override/alert/Alert';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';

import './style.scss';

interface SearchObj {
    cdType?: string;
    cdGrp?: string;
}

const Code = () => {
    const {data} = Storage.getGridList();
    const [mtRowData, setMtRowData] = useState([]);
    const [dtlRowData, setDtlRowData] = useState([]);
    const [dtlAddObj, setDtlAddObj] = useState({
        sort: 'auto',
        useYn: 'Y',
        cdType: '',
        compCd: 'A002',
    });

    const [searchObj, setSearchObj] = useState<SearchObj>({
        cdType: '',
        cdGrp: 'ALL',
    });

    useEffect(() => {
        // onClickSearch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    async function onClickSearch() {
        const result = await API.request.post(
            `api/system/master/code/get`,
            searchObj,
        );
        if (result.data.success) {
            setMtRowData(result.data.data);
        } else {
            setDtlRowData([]);
        }
    }

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await onClickSearch();
        }
    };

    const handleMasterSaveClick = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };
        await API.request.post(`api/system/master/code/save`, params);
    };

    const handleMasterDeleteClick = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };
        const result = await API.request.post(
            `api/system/master/code/list/delete`,
            params,
        );

        if (result.data.success) {
            setMtRowData(result.data.data);
        }
    };

    const handleMasterRowSelect = async () => {
        const mtGrid = data?.get('SysMng_code_mtGrid');
        const params = mtGrid?.getSelectedRows()[0];
        if (params) {
            setDtlAddObj({
                ...dtlAddObj,
                cdType: params.cdType,
            });
            const result = await API.request.post(
                `api/system/master/code/detail/get`,
                params,
            );
            if (result.data.success) {
                setDtlRowData(result.data.data);
            }
        }
    };

    const handleDetailSaveClick = async (param) => {
        const mtGrid = data?.get('SysMng_code_mtGrid');
        const selectedRow = mtGrid?.getSelectedRows()[0];
        if (!selectedRow) {
            meg({
                title: '선택된 항목이 없습니다.',
                html: '',
            });
            return;
        } else {
            const dtlSearchObj = {
                compCd: 'A002',
                cdType: selectedRow.cdType,
            };

            const params = {
                param1: param.data,
                param2: dtlSearchObj,
            };
            await API.request.post(
                `api/system/master/code/detail/save`,
                params,
            );
        }
    };

    const handleDetailDeleteClick = async (param) => {
        const mtGrid = data?.get('SysMng_code_mtGrid');
        const selectedRow = mtGrid?.getSelectedRows()[0];
        if (!selectedRow) {
            meg({
                title: '선택된 항목이 없습니다.',
                html: '',
            });
            return;
        }
        const dtlSearchObj = {
            compCd: 'A002',
            cdType: selectedRow.cdType,
        };

        const params = {
            param1: param.data,
            param2: dtlSearchObj,
        };
        const result = await API.request.post(
            `api/system/master/code/detail/list/delete`,
            params,
        );

        if (result.data.success) {
            setDtlRowData(result.data.data);
        }
    };

    return (
        <div className="sys_mt_code">
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="on">
                        <SSelectbox
                            isCodeGroupMode={true}
                            codeGroup={'CD_GROUP'}
                            label={'코드그룹'}
                            id={'cdGrp'}
                            value={'ALL'}
                            handleChange={onChange}
                        />
                        <TextField
                            id="cdType"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'코드 그룹명'}
                            value={searchObj.cdType}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={onClickSearch}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'SysMng_code_mtGrid'}
                    rowData={mtRowData}
                    title={'코드 마스터 정보'}
                    isAddBtnOpen={true}
                    onSelectionChanged={handleMasterRowSelect}
                    newRowDefault={{cdGrp: 'CM', useYn: 'Y'}}
                    onClickSave={handleMasterSaveClick}
                    onClickDelete={handleMasterDeleteClick}
                    columnDefs={codeMtColumnDefs}
                />
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'SysMng_code_dtlGrid'}
                    isAddBtnOpen={true}
                    newRowDefault={dtlAddObj}
                    onClickSave={handleDetailSaveClick}
                    onClickDelete={handleDetailDeleteClick}
                    title={'코드 상세 정보'}
                    rowData={dtlRowData}
                    columnDefs={codeDtlcolumnDefs}
                />
            </div>
        </div>
    );
};

const codeMtColumnDefs = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
    },
    {
        headerName: '코드그룹',
        field: 'cdGrp',
        width: 80,
        align: 'center',
        codeGroup: 'CD_GROUP',
        essential: true,
        editable: true,
    },
    {
        headerName: '마스터 코드',
        field: 'cdType',
        width: 120,
        align: 'center',
        key: true,
    },
    {
        headerName: '코드명',
        field: 'cdTypeNm',
        width: 200,
        essential: true,
        editable: true,
    },
    {
        headerName: '사용여부',
        field: 'useYn',
        width: 80,
        align: 'center',
        default: 'Y',
        codeGroup: 'USE_YN',
    },
];

const codeDtlcolumnDefs = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
    },
    {
        headerName: '코드',
        field: 'cdType',
        width: 120,
        align: 'center',
        key: true,
        hide: true,
    },
    {
        headerName: '상세코드',
        field: 'cd',
        width: 120,
        align: 'center',
        key: true,
    },
    {
        headerName: '코드명',
        field: 'cdNm',
        width: 200,
        essential: true,
        editable: true,
    },
    {headerName: 'Desc1', field: 'desc1', width: 100, editable: true},
    {headerName: 'Desc2', field: 'desc2', width: 100, editable: true},
    {headerName: 'Desc3', field: 'desc3', width: 100, editable: true},
    {headerName: 'Desc4', field: 'desc4', width: 100, editable: true},
    {headerName: 'Desc5', field: 'desc5', width: 100, editable: true},
    {
        headerName: '순서',
        field: 'sort',
        width: 70,
        type: 'number',
        editable: true,
    },
    {
        headerName: '사용여부',
        field: 'useYn',
        align: 'center',
        width: 80,
        default: 'Y',
        codeGroup: 'USE_YN',
    },
];
export default Code;
