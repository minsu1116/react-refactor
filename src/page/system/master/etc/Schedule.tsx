import React, {Suspense} from 'react';
import {CircularProgress} from '@mui/material';
import './style.scss';

const SCalendar = React.lazy(
    () => import('@components/atoms/scheduler/calendar/SCalendar'),
);
const Schedule = () => {
    return (
        <div className="equiip_rev_main">
            <div className="contents_wrap" id="contents_wrap">
                <Suspense
                    fallback={<CircularProgress className="loading_bar" />}>
                    <SCalendar />
                </Suspense>
            </div>
        </div>
    );
};

export default Schedule;
