import React, {useState, useEffect, useMemo} from 'react';
import API from 'override/api/API';
import STree from 'components/atoms/tree/STree';
import Button from '@mui/material/Button';
import SGrid from 'components/atoms/grid/SGrid';
import SGridUtil from 'components/atoms/grid/SGridUtil';
import Storage from '@override/api/Storage';
import SysMngAuthBtnP from '@page/system/mng/auth/popup/SysMngAuthBtnP';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import {meg, confirm} from '@override/alert/Alert';
import './style.scss';
import SModal from 'override/modal/SModal';

const DeptTree = (param) => {
    const authGrid = param.authGrid;
    const [treeObj, setTreeObj] = useState({
        treeData: [],
        expandedTreeKeys: [],
        checkedTreeKeys: [],
    });

    useEffect(() => {
        if (param.treeObj) {
            setTreeObj({
                ...treeObj,
                treeData: param.treeObj.treeData,
                expandedTreeKeys: param.treeObj.expandedTreeKeys,
                checkedTreeKeys: param.treeObj.checkedTreeKeys,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param]);

    const handleTreeCheck = async (keys, info) => {
        const params = treeObj.treeData;
        childrenCheck(params, info.node, info.checked);

        function childrenCheck(data, info, checked) {
            const children = data.filter((x) => x['parentDeptCd'] == info.key);
            const childrenYn = data.some((x) => x['parentDeptCd'] == info.key);
            const yn = checked ? 'Y' : 'N';

            //자식이 있으면
            if (children.length > 0) {
                children.map((x) => {
                    const temp = {key: x['deptCd'], authYn: yn, edit: true};
                    const child = Object.assign(temp);

                    if (childrenYn) {
                        childrenCheck(data, child, checked);
                    }
                    //자식과 부모 edit -> true
                    treeObj?.treeData.map((e) => {
                        if (e.deptCd == info.key || e.deptCd == x.deptCd) {
                            e.authYn = yn;
                            e.edit = true;
                        }
                    });
                });
            } else {
                treeObj?.treeData.map((e) => {
                    if (e.deptCd == info.deptCd) {
                        e.authYn = yn;
                        e.edit = true;
                    }
                });
            }
        }
    };

    const hendleSaveDept = async () => {
        const param1 = [];
        const param2 = authGrid.getSelectedRows()[0];

        treeObj.treeData.forEach((e) => {
            if (e.edit) {
                param1.push(e);
            }
        });

        if (param1.length > 0) {
            const confirmResult = await confirm({
                title: '저장하시겠습니까?',
                html: '',
            });
            if (confirmResult) {
                const params = {param1: param1, param2: param2};
                const result = await API.request.post(
                    `api/system/mng/auth/saveauthdept`,
                    params,
                );

                if (result.data.success) {
                    await meg({
                        title: `저장 되었습니다.`,
                        html: '',
                    });
                }
            }
        } else {
            await meg({
                title: `저장할 데이터가 없습니다.`,
                html: '',
            });
            return;
        }
    };

    return (
        <>
            <div className="head_contents">
                <div className="tit">부서 정보</div>
                <div className="head_btns">
                    <Button
                        id="dept_save"
                        variant="outlined"
                        onClick={hendleSaveDept}>
                        저장
                    </Button>
                </div>
            </div>
            <STree
                treeData={treeObj.treeData}
                keyMember={'deptCd'}
                pIdMember={'parentDeptCd'}
                height={288}
                titleMember={'deptNm'}
                onCheck={handleTreeCheck}
                checkable={true}
                expandedKeysId={treeObj.expandedTreeKeys}
                checkedKeysId={treeObj.checkedTreeKeys}
                // defaultExpandAll={true}
                border={true}
            />
        </>
    );
};

const MenuGrid = (param) => {
    const {data} = Storage.getGridList();
    const authGrid = param.authGrid;
    const menuGrid = data?.get('SysMngAuth_menuGrid');
    const [modalOpen, setModalOpen] = useState({
        btnModal: false,
    });
    const handleAddAuthBtn = async () => {
        setModalOpen({
            ...modalOpen,
            btnModal: true,
        });
    };

    const [rowData, setRowData] = useState({
        menuRowData: param?.menuRowData || [],
    });

    useEffect(() => {
        if (param.menuRowData) {
            setRowData({
                ...rowData,
                menuRowData: param.menuRowData,
            });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param]);

    useEffect(() => {
        if (menuGrid) {
            //메뉴ID 없는건 상위 메뉴ID와 같기 떄문에 check박스는 맨위 메뉴만 나옴
            function authYnVisible(params) {
                if (params.data.menuId == '') {
                    return {
                        contentVisibility: 'hidden',
                    };
                }
            }

            function grpMenuIdVisible(params) {
                if (!params.data.btnList && params.data.authYn == 'Y') {
                    return {
                        contentVisibility: 'hidden',
                    };
                }
            }
            SGridUtil.setOption(
                'authYn',
                'rendererVisible',
                authYnVisible,
                menuGrid,
                menuColumnDefs,
            );
            SGridUtil.setOption(
                'grpMenuId',
                'rendererVisible',
                grpMenuIdVisible,
                menuGrid,
                menuColumnDefs,
            );
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [menuGrid]);

    const handleSaveMenu = async (param) => {
        const resultObj = authGrid.getSelectedRows()[0];
        if (resultObj) {
            const searchObj = {
                grpNm: '',
                menuId: '',
                formId: '',
                grpMenuId: 0,
                grpId: resultObj.grpId,
                compCd: resultObj.compCd,
            };

            const params = {
                param1: param.data,
                param2: searchObj,
            };
            const result = await API.request.post(
                `api/system/mng/auth/saveauthmenu`,
                params,
            );

            if (result.data.success) {
                setRowData({
                    ...rowData,
                    menuRowData: result.data.data,
                });
            }
        } else {
            await meg({
                title: `선택된 권한이 없습니다.`,
                html: '',
            });
            return;
        }
    };

    const onCloseBtn = async (params) => {
        setModalOpen({
            ...modalOpen,
            btnModal: false,
        });

        if (params.result) {
            const param = {
                param1: params.result.data,
                param2: authGrid.getSelectedRows()[0],
            };

            const result = await API.request.post(
                `api/system/mng/auth/saveauthbtn`,
                param,
            );

            if (result.data.success) {
                setRowData({
                    ...rowData,
                    menuRowData: result.data.data,
                });
            }
        }
    };

    const menuColumnDefs = [
        {
            headerName: '권한여부',
            field: 'authYn',
            width: 80,
            align: 'center',
            type: 'checkbox',
            editable: false,
        },
        {
            headerName: '메뉴ID ',
            field: 'menuId',
            width: 80,
            align: 'center',
            editable: false,
        },
        {headerName: '메뉴명', field: 'menuNm', width: 500, editable: false},
        {headerName: '버튼명', field: 'btnList', width: 250, editable: false},
        {
            headerName: '권한버튼명',
            field: 'btnAuthList',
            width: 250,
            editable: false,
        },
        {
            headerName: '버튼권한',
            field: 'grpMenuId',
            width: 100,
            editable: false,
            align: 'center',
            type: 'button',
            // buttonName: this.l10n('M01032', '권한등록'),
            onButtonClick: handleAddAuthBtn,
            validation: true,
            tooltipDesc: '권한 페이지를 먼저 등록 해주십시오.',
        },
    ];

    const btnStyle = {
        position: 'absolute' as const,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 350,
        bgcolor: 'background.paper',
        boxShadow: 24,
        border: '0px solid',
    };

    return (
        <>
            <Modal
                open={modalOpen.btnModal}
                onClose={onCloseBtn}
                hideBackdrop={true}
                // keepMounted={true}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description">
                <Box sx={btnStyle} className={'popup'}>
                    <div className="popup_top">
                        <span className="close_icon" onClick={onCloseBtn}>
                            X
                        </span>
                    </div>
                    <div className="popup_contents">
                        <SysMngAuthBtnP
                            tit={'버튼권한 등록'}
                            width={400}
                            onClose={onCloseBtn}
                        />
                    </div>
                </Box>
            </Modal>
            <SGrid
                grid={'SysMngAuth_menuGrid'}
                rowData={rowData.menuRowData}
                // onModalOpen={onModalOpen}
                title={'메뉴 정보'}
                onClickSave={handleSaveMenu}
                // onClickDelete={programDelete}
                columnDefs={menuColumnDefs}
            />
        </>
    );
};

const SysMngAuth = () => {
    const {data} = Storage.getGridList();
    const authGrid = data?.get('SysMngAuth_authGrid');
    const [modalOpen, setModalOpen] = useState({
        btnModal: false,
        userModal: false,
    });

    const sizeObj = useMemo(() => {
        return {
            width: '750',
            height: '820',
            isResize: false,
        };
    }, []);

    const [grpRowData, setGrpRowData] = useState({
        grpRowData: [],
    });

    const [subRowData, setSubRowData] = useState({
        userRowData: [],
        menuRowData: [],
    });

    const [userModal, setUserModal] = useState({
        validationList: [],
        mode: 'multi',
    });

    const [treeObj, setTreeObj] = useState({
        treeData: [],
        expandedTreeKeys: [],
        checkedTreeKeys: [],
    });

    useEffect(() => {
        let asyncTaks = true;
        if (asyncTaks) {
            hendleSearch();
        }
        return () => {
            asyncTaks = false;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const hendleSearch = async () => {
        const searchObj = {
            grpNm: '',
            menuId: '',
            formId: '',
            grpMenuId: 0,
        };
        const result = await API.request.post(
            `api/system/mng/auth/getauth`,
            searchObj,
        );

        if (result.data.success) {
            setGrpRowData({
                ...grpRowData,
                grpRowData: result.data.data,
            });
        }
    };
    const handleAeuthSelect = async (param) => {
        const selectObj = param.api.getSelectedRows()[0];
        setTreeObj({
            ...treeObj,
            treeData: [],
            expandedTreeKeys: [],
            checkedTreeKeys: [],
        });
        if (selectObj) {
            if (selectObj.newRow) {
                setSubRowData({
                    ...subRowData,
                    userRowData: [],
                    menuRowData: [],
                });
                return;
            }
        }
        const authData = await API.request.post(
            `api/system/mng/auth/getauthall`,
            selectObj,
        );

        let authList = '';
        if (authData.data.success) {
            setSubRowData({
                ...subRowData,
                userRowData: authData.data.data[1],
                menuRowData: authData.data.data[2],
            });

            authData.data.data[0].forEach((e) => {
                if (e.authYn == 'Y') {
                    authList += e.deptCd + ',';
                    // ylist.push(e.deptCd);
                }
            });
            authList = authList.slice(0, authList.length - 1);
            const authCheck = authList.split(',');
            setTreeObj({
                ...treeObj,
                treeData: authData.data.data[0],
                expandedTreeKeys: authData.data.data[0][0].deptCd,
                checkedTreeKeys: authCheck,
            });
        }
    };

    const handleSaveAuth = async (param) => {
        if (param.data) {
            const params = {
                param1: param.data,
                param2: authGrid.getSelectedRows()[0],
            };
            const focusIndex = authGrid.getSelectedNodes()[0].rowIndex;
            const result = await API.request.post(
                `api/system/mng/auth/saveauth`,
                params,
            );

            if (result.data.success) {
                setGrpRowData({
                    ...grpRowData,
                    grpRowData: result.data.data,
                });
                SGridUtil.setFocusByRowIndex(focusIndex, authGrid);
            }
        }
    };

    const handleDelAuth = async (param) => {
        if (param.data) {
            const params = {
                param1: param.data,
                param2: authGrid.getSelectedRows()[0],
            };
            const result = await API.request.post(
                `api/system/mng/auth/deleteauth-list`,
                params,
            );

            if (result.data.success) {
                setGrpRowData({
                    ...grpRowData,
                    grpRowData: result.data.data,
                });
            }
        }
    };

    const handleDelAuthUser = async (params) => {
        const param = {
            param1: params.data,
            param2: authGrid.getSelectedRows()[0],
        };
        const result = await API.request.post(
            `api/system/mng/auth/saveauthuser`,
            param,
        );
        if (result.data.success) {
            setSubRowData({
                ...subRowData,
                userRowData: result.data.data,
            });
        }
    };

    const authColumnDefs = [
        {
            headerName: '',
            field: 'check',
            default: 'N',
            type: 'checkbox',
            width: 32,
            checkbox: true,
        },
        {
            headerName: '권한ID',
            field: 'grpId',
            width: 100,
            key: true,
            essential: true,
            editable: true,
        },
        {
            headerName: '권한명',
            field: 'grpNm',
            width: 120,
            essential: true,
            editable: true,
        },
        {headerName: '설명', field: 'grpDesc', editable: true, width: 200},
    ];

    const userColumnDefs = [
        {
            headerName: '',
            field: 'check',
            default: 'N',
            type: 'checkbox',
            width: 32,
            checkbox: true,
        },
        ,
        {
            headerName: '계정명',
            field: 'userId',
            width: 90,
            align: 'center',
            editable: false,
            key: true,
        },
        {headerName: '이름', field: 'userNm', width: 100, editable: false},
    ];

    const onCloseUser = async (param) => {
        setModalOpen({
            ...modalOpen,
            userModal: false,
        });

        if (param.data && param.data.length > 0) {
            param.data.map((item) => {
                item.newRow = true;
            });
            const params = {
                param1: param.data,
                param2: authGrid?.getSelectedRows()[0],
            };
            const result = await API.request.post(
                `api/system/mng/auth/saveauthuser`,
                params,
            );

            if (result.data.success) {
                setSubRowData({
                    ...subRowData,
                    userRowData: result.data.data,
                });
            }
        }
    };

    const onUserModalOpen = () => {
        const selectAuthObj = authGrid.getSelectedRows()[0];

        if (!selectAuthObj) {
            meg({
                title: `[권한정보]일람에서 권한을 먼저 선택 해 주세요.`,
                html: '',
            });
            return;
        }

        setUserModal({
            ...userModal,
            validationList: subRowData.userRowData,
        });
        setModalOpen({
            ...modalOpen,
            userModal: true,
        });
    };

    return (
        <>
            <div className="sys_mng_auth">
                <SModal
                    tit={'사용자 등록'}
                    param={userModal}
                    isOpen={modalOpen.userModal}
                    sizeObj={sizeObj}
                    path={'common/popup/user/User'}
                    onClose={onCloseUser}
                />
                <div className="contents_wrap_parents_w">
                    <div className="contents_wrap w40">
                        <SGrid
                            grid={'SysMngAuth_authGrid'}
                            rowData={grpRowData.grpRowData}
                            isAddBtnOpen={true}
                            title={'권한 정보'}
                            onClickSave={handleSaveAuth}
                            onClickDelete={handleDelAuth}
                            columnDefs={authColumnDefs}
                            onSelectionChanged={handleAeuthSelect}
                        />
                    </div>
                    <div className="contents_wrap w30">
                        <DeptTree treeObj={treeObj} authGrid={authGrid} />
                    </div>
                    <div
                        className="contents_wrap w30"
                        style={{marginBottom: '3px'}}>
                        <SGrid
                            grid={'SysMngAuth_userGrid'}
                            rowData={subRowData.userRowData}
                            isModalOpen={true}
                            onModalOpen={onUserModalOpen}
                            title={'사용자 정보'}
                            onClickDelete={handleDelAuthUser}
                            columnDefs={userColumnDefs}
                        />
                    </div>
                </div>
                <div className="contents_wrap_parents_w h100">
                    <div className="contents_wrap w100 h100">
                        <MenuGrid
                            menuRowData={subRowData.menuRowData}
                            authGrid={authGrid}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default SysMngAuth;
