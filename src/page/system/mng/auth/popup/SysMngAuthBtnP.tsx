import React, {useEffect, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import API from 'override/api/API';

const SysMngAuthBtnP = (param) => {
    const {data} = Storage.getGridList();
    const menuGrid = data?.get('SysMngAuthBtnP_btnGrid');
    const tit = param.tit || '';
    const [rowData, setRowData] = useState([]);
    useEffect(() => {
        getAuthBtn();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getAuthBtn = async () => {
        if (menuGrid) {
            const params = menuGrid.getSelectedRows()[0];
            const result = await API.request.post(
                'api/system/mng/auth/getauthbtn',
                params,
            );
            if (result.data.success) {
                setRowData(result.data.data);
            }
        }
    };

    const btnSave = async (saveList) => {
        if (param.onClose) {
            const result = {
                result: saveList,
            };
            param.onClose(result);
        }
    };

    const columnDefs = [
        {
            headerName: '',
            field: 'authYn',
            width: 50,
            align: 'center',
            type: 'checkbox',
            editable: false,
        },
        {headerName: '버튼ID', field: 'btnId', width: 100, editable: false},
        {headerName: '버튼명', field: 'btnNm', width: 150, editable: false},
    ];

    return (
        <>
            {tit ? <div className="popup_tit">{tit}</div> : null}
            <div className="contents_wrap h100">
                <SGrid
                    grid={'SysMngAuthBtnP_btnGrid'}
                    title={'버튼 정보'}
                    rowData={rowData}
                    height={300}
                    onClickSave={btnSave}
                    columnDefs={columnDefs}
                />
            </div>
        </>
    );
};
export default SysMngAuthBtnP;
