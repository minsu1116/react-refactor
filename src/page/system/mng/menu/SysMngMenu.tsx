import React, {useState, useEffect} from 'react';
import API from 'override/api/API';
import STree from 'components/atoms/tree/STree';
import SGrid from 'components/atoms/grid/SGrid';
import Storage from '@override/api/Storage';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import SysMngMenuPrgAddP from '@page/system/mng/menu/popup/SysMngMenuPrgAddP';
import {meg} from '@override/alert/Alert';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import './style.scss';
const SysMngMenu = () => {
    const [modalOpen, setModalOpen] = useState(false);
    const [treeObj, setTreeData] = useState({
        treeData: [],
        menuIdList: [],
        selectedTreeKey: '',
    });

    const [menuAddObj, setMenuAddObj] = useState({
        parentMenuId: '',
    });

    const [rowData, setRowData] = useState({
        menuRowData: [],
        prgRowData: [],
    });
    const [searchObj, setSearchObj] = useState({
        menuId: '',
        menuNm: '',
    });

    const {data} = Storage.getGridList();
    const menuGrid = data?.get('SysMngMenu_menuGrid');
    const prgGrid = data?.get('SysMngMenu_prgGrid');

    useEffect(() => {
        onClickSearch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleTreeSelect = async (key) => {
        const param = {
            menuId: key,
        };

        setTreeData({
            ...treeObj,
            selectedTreeKey: key,
        });

        const result = await API.request.post(
            `/api/system/mng/menu/getmenutree`,
            param,
        );

        if (result.data.success) {
            setRowData({
                ...rowData,
                menuRowData: result.data.data,
                prgRowData: [],
            });

            setMenuAddObj({
                ...menuAddObj,
                parentMenuId: key,
            });
        }
    };

    //메뉴 트리 데이터
    const onClickSearch = async () => {
        const menuIdList = [];

        const param = searchObj;
        const result = await API.request.post(
            `api/system/mng/menu/getmenutree`,
            param,
        );

        if (result.data.success) {
            const tempList = result.data.data;
            result.data.data.forEach((x) => {
                if (x.childYn == 'Y') {
                    menuIdList.push(x.menuId);
                }
            });
            setTreeData({
                ...treeObj,
                treeData: tempList,
                menuIdList: menuIdList,
            });
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await onClickSearch();
        }
    };

    const getPrgRowData = async () => {
        if (data) {
            if (menuGrid && menuGrid.getSelectedRows()) {
                const param = menuGrid.getSelectedRows()[0];
                if (!param.formId) {
                    setRowData({
                        ...rowData,
                        prgRowData: [],
                    });
                    return;
                }
                const result = await API.request.post(
                    `api/system/mng/menu/getmenuprg`,
                    param,
                );
                if (result.data.success) {
                    setRowData({
                        ...rowData,
                        prgRowData: result.data.data,
                    });
                }
            } else {
            }
        }
    };

    const handleDelMenu = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };

        const result = await API.request.post(
            `api/system/mng/menu/deletemenus`,
            params,
        );

        if (result) {
            //트리 재조회.
            onClickSearch();
            handleTreeSelect(treeObj.selectedTreeKey);
        }
    };

    const handleSaveMenu = async (param) => {
        const params = {
            param1: param.data,
            param2: searchObj,
        };

        const result = await API.request.post(
            `api/system/mng/menu/savemenu`,
            params,
        );

        if (result.data.success) {
            onClickSearch();
            handleTreeSelect(treeObj.selectedTreeKey);
        }
    };

    const onClose = async (params) => {
        setModalOpen(false);
        const selectObj = menuGrid.getSelectedRows()[0];

        if (params['formId']) {
            params.check = 'Y';
            params.menuId = selectObj.menuId;
            params.newRow = 'true';
            rowData.prgRowData.push(params);
            prgGrid.setRowData(rowData.prgRowData);
        }
    };

    const onModalOpen = async () => {
        const selectObj = menuGrid.getSelectedRows()[0];
        if (!selectObj) {
            meg({
                title: `매뉴정보 일람에서 먼저 매뉴를 선택 해 주십시오.`,
                html: '',
            });
            return;
        }
        setModalOpen(true);
    };

    const programSave = async (param) => {
        const validationList = rowData.prgRowData.filter(
            (x) => x.mainYn == 'Y',
        );

        if (validationList.length > 1) {
            meg({
                title: `메인여부는 하나만 체크할 수 있습니다.`,
                html: '',
            });
            return;
        }

        const params = {
            param1: param.data,
            param2: searchObj,
        };

        const result = await API.request.post(
            `api/system/mng/menu/savemenuprg`,
            params,
        );

        if (result.data.success) {
            getPrgRowData();
        }
    };

    const programDelete = async (param) => {
        const prgSearchObj = {
            menuId: param.data[0]?.menuId || '',
        };

        const params = {
            param1: param.data,
            param2: prgSearchObj,
        };

        const result = await API.request.post(
            `api/system/mng/menu/deletemenuprgs`,
            params,
        );

        if (result.data.success) {
            prgGrid.setRowData(result.data.data);
        }
    };

    const style = {
        position: 'absolute' as const,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 500,
        bgcolor: 'background.paper',
        boxShadow: 24,
        border: '0px solid',
    };

    const menuColumnDefs = [
        {
            headerName: '',
            field: 'check',
            default: 'N',
            type: 'checkbox',
            width: 32,
            checkbox: true,
        },
        {
            headerName: '메뉴ID',
            field: 'menuId',
            width: 80,
            align: 'center',
            editable: false,
        },
        {
            headerName: '메뉴명',
            field: 'menuNm',
            width: 180,
            essential: true,
            editable: true,
        },
        {
            headerName: '상위ID',
            field: 'parentMenuId',
            width: 85,
            align: 'center',
            editable: false,
            essential: true,
        },
        {
            headerName: '프로그램ID',
            field: 'formId',
            width: 200,
            editable: false,
            type: 'iconButton',
            buttonIcon: 'icon-search',
            // onButtonClick: buttonClick,
            // onClose: handleClose,
            suppressKeyboardEvent: keybordEvent,
            Modal: <SysMngMenuPrgAddP tit={'프로그램 추가'} width={500} />,
        },
        {
            headerName: '파라미터',
            field: 'param',
            width: 90,
            align: 'center',
            editable: true,
        },
        {
            headerName: '정렬순서',
            field: 'sort',
            type: 'number',
            width: 90,
            align: 'center',
            editable: true,
        },
        {
            headerName: '아이콘',
            field: 'iconNm',
            width: 100,
            align: 'center',
            editable: true,
        },
        {
            headerName: '하위여부',
            field: 'childYn',
            width: 90,
            align: 'center',
            default: 'N',
            codeGroup: 'USE_YN',
            editable: true,
        },
        {
            headerName: '메뉴여부',
            field: 'displayYn',
            width: 90,
            align: 'center',
            default: 'Y',
            codeGroup: 'USE_YN',
            editable: true,
        },
        {
            headerName: '비고',
            field: 'menuDesc',
            width: 150,
            align: 'center',
            editable: true,
        },
    ];

    const prgColumnDefs = [
        {
            headerName: '',
            field: 'check',
            default: 'N',
            type: 'checkbox',
            width: 32,
            checkbox: true,
        },
        {headerName: '프로그램ID', field: 'formId', width: 200},
        {headerName: '프로그램명', field: 'formNm', width: 200},
        {
            headerName: '메인여부',
            field: 'mainYn',
            width: 90,
            align: 'center',
            editable: false,
            cellRenderer: 'rowCellCheckbox',
        },
    ];

    return (
        <>
            <Modal
                open={modalOpen}
                onClose={onClose}
                hideBackdrop={true}
                // keepMounted={true}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description">
                <Box sx={style} className="popup">
                    <div className="popup_top">
                        <span className="close_icon" onClick={onClose}>
                            X
                        </span>
                    </div>
                    <div className="popup_contents">
                        <SysMngMenuPrgAddP
                            tit={'프로그램 추가'}
                            width={500}
                            prgList={rowData.prgRowData}
                            onClose={onClose}
                        />
                    </div>
                </Box>
            </Modal>
            <div className="sys_mng_menu">
                <div className="search_wrap">
                    <div className="search_contents">
                        <Box
                            component="form"
                            sx={{
                                '& > :not(style)': {m: 1, width: '25ch'},
                            }}
                            noValidate
                            autoComplete="off">
                            <TextField
                                id="menuNm"
                                onChange={onChange}
                                onKeyDown={handleKeyDown}
                                label={'메뉴 ID/명'}
                                value={searchObj.menuNm}
                                size="small"
                            />
                        </Box>
                    </div>
                    <div className="search_btns">
                        <Tooltip title="조회">
                            <IconButton
                                onClick={onClickSearch}
                                className="btn_group">
                                <ManageSearchIcon className="btn_icon" />
                            </IconButton>
                        </Tooltip>
                    </div>
                </div>
                <div className="contents_wrap_parents_w h100">
                    <div
                        className="contents_wrap w30"
                        style={{minWidth: '300px'}}>
                        <STree
                            treeData={treeObj.treeData}
                            title={'부서 정보'}
                            defaultExpandAll={true}
                            keyMember={'menuId'}
                            pIdMember={'parentMenuId'}
                            titleMember={'menuNm'}
                            onSelect={handleTreeSelect}
                            selectedKeyId={treeObj.selectedTreeKey}
                            expandedKeysId={treeObj.menuIdList}
                            border={true}
                        />
                    </div>
                    <div className="contents_wrap_parents_h w100">
                        <div className="contents_wrap">
                            <SGrid
                                grid={'SysMngMenu_menuGrid'}
                                rowData={rowData.menuRowData}
                                isAddBtnOpen={true}
                                newRowDefault={{
                                    sort: 'auto',
                                    parentMenuId: menuAddObj.parentMenuId,
                                }}
                                title={'메뉴 정보'}
                                onSelectionChanged={getPrgRowData}
                                onClickDelete={handleDelMenu}
                                onClickSave={handleSaveMenu}
                                columnDefs={menuColumnDefs}
                            />
                        </div>
                        <div className="contents_wrap">
                            <SGrid
                                grid={'SysMngMenu_prgGrid'}
                                rowData={rowData.prgRowData}
                                isModalOpen={true}
                                onModalOpen={onModalOpen}
                                title={'메뉴 정보'}
                                onClickSave={programSave}
                                onClickDelete={programDelete}
                                columnDefs={prgColumnDefs}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

const keybordEvent = (params) => {
    if (params.event.which == 46 || params.event.which == 8) {
        params.data.formId = '';
        params.api.redrawRows();
    }
};

export default SysMngMenu;
