import React, {useEffect, useState} from 'react';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import SGrid from 'components/atoms/grid/SGrid';
import API from 'override/api/API';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';

const SysMngMenuPrgAddP = (param) => {
    const tit = param.tit || '';
    const prgList = param.prgList || [];
    const [rowData, setRowData] = useState({
        prgAddRowData: [],
    });

    const [searchObj, setSearchObj] = useState({
        menuId: '',
        menuNm: '',
        formId: '',
        programArr: [],
        cnt: 0,
    });

    useEffect(() => {
        handleSearchClick();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    const handleSearchClick = async () => {
        const rowData = [];
        const params = searchObj;
        const result = await API.request.post(
            `api/system/mng/prg/getprg`,
            params,
        );

        //조회시 메뉴관리 그리드 데이터는 제외
        if (result.data.success) {
            result.data.data.forEach((item) => {
                if (!prgList.some((x) => x.formId == item.formId)) {
                    rowData.push(item);
                }
            });
            setRowData({
                ...rowData,
                prgAddRowData: rowData,
            });
        }
    };

    const handleChoiceClick = async (params) => {
        if (params.data) {
            if (param.onClose) {
                param.onClose(params.data);
            }
            return params.data;
        }
    };

    const prgAddcolumnDefs = [
        {
            headerName: '프로그램ID',
            field: 'formId',
            width: 200,
            editable: false,
        },
        {
            headerName: '프로그램명',
            field: 'formNm',
            width: 200,
            editable: false,
        },
    ];

    return (
        <>
            {tit ? <div className="popup_tit">{tit}</div> : null}
            <div className="search_wrap">
                <div className="search_contents">
                    <Box component="form" noValidate autoComplete="on">
                        <TextField
                            id="formId"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'메뉴 ID/명'}
                            value={searchObj.formId}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap h100">
                <SGrid
                    grid={'SysMngMenu_popup_prgAddGrid'}
                    rowData={rowData.prgAddRowData}
                    height={500}
                    rowDoubleClick={handleChoiceClick}
                    // onClickSave={programSave}
                    columnDefs={prgAddcolumnDefs}
                />
            </div>
        </>
    );
};

export default SysMngMenuPrgAddP;
