import React, {useEffect, useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import API from 'override/api/API';
import Box from '@mui/material/Box';
import Storage from '@override/api/Storage';
import TextField from '@mui/material/TextField';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@material-ui/core/IconButton';

interface SearchObj {
    formId?: string;
    formType?: string;
}

const SysMngPrg = () => {
    const [rowData, setRowData] = useState({
        prgRowData: [],
        btnRowData: [],
    });
    const [searchObj, setSearchObj] = useState<SearchObj>({
        formId: '',
        formType: '',
    });

    const [selectedObj, setSelectedObj] = useState({
        formId: '',
    });

    useEffect(() => {
        // onClickSearch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const {formId} = searchObj;
    const {data} = Storage.getGridList();
    const prgGrid = data?.get('SysMngPrg_prgGrid');
    // const btnGrid = data.get('btnGrid');

    async function onClickRowData() {
        const param = searchObj;
        const result = await API.request.post(
            `/api/system/mng/prg/getprg`,
            param,
        );
        if (result.data.success) {
            return result.data.data;
        }

        return [];
    }

    // const onClick = () => {
    //     const grid = data.get('prgGrid');
    //     grid.getSelectedRows();
    // };

    async function onClickSearch() {
        const result = await onClickRowData();
        setRowData({
            ...rowData,
            prgRowData: result,
            btnRowData: [],
        });
    }

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await onClickSearch();
        }
    };

    const getBtn = async () => {
        if (data) {
            if (prgGrid && prgGrid.getSelectedRows()) {
                const param = prgGrid.getSelectedRows()[0];
                setSelectedObj(param);
                if (param?.newRow) {
                    setRowData({
                        ...rowData,
                        btnRowData: [],
                    });
                    return;
                }

                const result = await API.request.post(
                    `api/system/mng/prg/getprgbtn`,
                    param,
                );

                if (result.data.success) {
                    setRowData({
                        ...rowData,
                        btnRowData: result.data.data,
                    });
                }
            } else {
            }
        }
    };

    const programSave = async (param) => {
        const params = JSON.stringify({param1: param.data, param2: searchObj});
        const result = await API.request.post(
            `api/system/mng/prg/saveprg`,
            params,
        );

        if (result.data.success) {
            setRowData({
                ...rowData,
                prgRowData: result.data.data,
            });
        }
    };

    const programDelete = async (param) => {
        const params = JSON.stringify({param1: param.data, param2: searchObj});
        const result = await API.request.post(
            `api/system/mng/prg/deleteprg/list`,
            params,
        );

        if (result.data.success) {
            setRowData({
                ...rowData,
                prgRowData: result.data.data,
            });
        }
    };

    const btnSave = async (param) => {
        const param2 = prgGrid.getSelectedRows()[0];

        const params = JSON.stringify({param1: param.data, param2: param2});
        const result = await API.request.post(
            `api/system/mng/prg/saveprgbtn`,
            params,
        );

        if (result.data.success) {
            setRowData({
                ...rowData,
                btnRowData: result.data.data,
            });
        }
    };

    const btnDelete = async (param) => {
        const param2 = prgGrid.getSelectedRows()[0];

        const params = JSON.stringify({param1: param.data, param2: param2});
        const result = await API.request.post(
            `api/system/mng/prg/deleteprgbtn/list`,
            params,
        );

        if (result.data.success) {
            setRowData({
                ...rowData,
                prgRowData: result.data.data,
            });
        }
    };

    return (
        <>
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <TextField
                            id="formId"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'프로그램 ID/명'}
                            value={formId}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={onClickSearch}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'SysMngPrg_prgGrid'}
                    rowData={rowData.prgRowData}
                    height={400}
                    isAddBtnOpen={true}
                    onClickSave={programSave}
                    onClickDelete={programDelete}
                    title={'프로그램 정보'}
                    onSelectionChanged={getBtn}
                    columnDefs={prgColumnDefs}
                />
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'SysMngPrg_btnGrid'}
                    height={320}
                    isAddBtnOpen={true}
                    newRowDefault={{formId: selectedObj.formId}}
                    onClickSave={btnSave}
                    onClickDelete={btnDelete}
                    title={'버튼 정보'}
                    rowData={rowData.btnRowData}
                    columnDefs={btnColumnDefs}
                />
            </div>
        </>
    );
};

const prgColumnDefs = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
    },
    {
        headerName: '프로그램ID',
        field: 'formId',
        width: 250,
        essential: true,
        key: true,
    },
    {
        headerName: '프로그램명',
        field: 'formNm',
        width: 250,
        editable: true,
        essential: true,
        key: true,
    },
    {
        headerName: '프로그램 설명',
        field: 'formDesc',
        width: 200,
        editable: true,
    },
    {
        headerName: '프로그램 주소',
        field: 'formUrl',
        width: 250,
        editable: true,
    },
    {
        headerName: '프로그램 타입',
        field: 'formType',
        width: 120,
        align: 'center',
        default: 'WEB',
        editable: true,
    },
    {
        headerName: '프로그램 클래스',
        field: 'formClass',
        width: 200,
        editable: true,
    },
];

const btnColumnDefs = [
    {
        headerName: '',
        field: 'check',
        default: 'N',
        type: 'checkbox',
        width: 32,
        checkbox: true,
    },
    {
        headerName: '프로그램ID',
        field: 'formId',
        width: 250,
        essential: true,
    },
    {
        headerName: '버튼ID',
        field: 'btnId',
        width: 150,
        key: true,
        editable: true,
    },
    {
        headerName: '버튼명',
        field: 'btnNm',
        width: 200,
        editable: true,
    },
    {headerName: '비고', field: 'remark', width: 200, editable: true},
];
export default SysMngPrg;
