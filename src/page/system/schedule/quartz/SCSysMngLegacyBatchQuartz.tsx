import React, {useState} from 'react';
import SGrid from 'components/atoms/grid/SGrid';
import TextField from '@mui/material/TextField';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';
import API from 'override/api/API';
import './style.scss';

const SCSysMngLegacyBatchQuartz = () => {
    const [searchObj, setSearchObj] = useState({
        jobName: '',
    });
    const [rowData, setRowData] = useState([]);
    const columnDefs = [
        {headerName: 'Job 이름', field: 'jobName', width: 110},
        {headerName: 'Job 그룹', field: 'jobGroup', width: 110},
        {
            headerName: 'Job Class 이름',
            field: 'jobClassName',
            width: 110,
            editable: true,
            essential: true,
        },
        {
            headerName: 'Cron Expression',
            field: 'cronExpression',
            width: 110,
            editable: true,
            essential: true,
        },
        {
            headerName: 'Trigger 상태',
            field: 'triggerState',
            width: 110,
            editable: true,
            essential: true,
        },
        {headerName: 'Time Zone', field: 'timeZoneId', width: 110},
        {headerName: 'Trigger 이름', field: 'triggerName', width: 110},
        {headerName: 'Trigger 그룹', field: 'triggerGroup', width: 110},
        {
            headerName: '시스템명',
            field: 'description',
            width: 110,
        },
        {
            headerName: '다음 시작 시간',
            field: 'nextFireTimeConvert',
            width: 110,
            type: 'dateTimeFull',
        },
        {
            headerName: '이전 시작 시간',
            field: 'prevFireTimeConvert',
            width: 110,
            type: 'dateTimeFull',
        },
        {
            headerName: '생성시간',
            field: 'startTimeConvert',
            width: 110,
            type: 'dateTimeFull',
        },
    ];

    const handleSearchClick = async () => {
        const param = searchObj;
        const result = await API.quartz.post(`/quartz/job/list`, param);

        if (result.data.success) {
            setRowData(result.data.data);
        }
    };

    const handleSave = async (param) => {
        if (param.data) {
            param.data.forEach((element) => {
                if (element.newRow) {
                    API.quartz.post(`/quartz/job/add`, {param: element});
                } else {
                    API.quartz.post(`/quartz/job/cron`, {param: element});
                }
            });
        }
    };

    const onChange = (e) => {
        const newValue = {
            ...searchObj, // 기존의 값 복사 (spread operator)
            [e.target.id]: e.target.value, // 덮어쓰기
        };
        setSearchObj(newValue);
    };

    const handleKeyDown = async (e) => {
        if (e.keyCode == 13) {
            // if (loginObj.loginId == '' || loginObj.loginPass == '') {
            // }
            e.preventDefault();
            await handleSearchClick();
        }
    };

    return (
        <div className="sys_mng_login_log">
            <div className="search_wrap">
                <div className="search_contents">
                    <Box
                        component="form"
                        sx={{
                            '& > :not(style)': {m: 1, width: '25ch'},
                        }}
                        noValidate
                        autoComplete="off">
                        <TextField
                            id="jobName"
                            onChange={onChange}
                            onKeyDown={handleKeyDown}
                            label={'배치명'}
                            value={searchObj.jobName}
                            size="small"
                        />
                    </Box>
                </div>
                <div className="search_btns">
                    <Tooltip title="조회">
                        <IconButton
                            onClick={handleSearchClick}
                            className="btn_group">
                            <ManageSearchIcon className="btn_icon" />
                        </IconButton>
                    </Tooltip>
                </div>
            </div>
            <div className="contents_wrap">
                <SGrid
                    grid={'sys_quartz_grid'}
                    rowData={rowData}
                    isAddBtnOpen={true}
                    newRowDefault={{
                        description: 'TAS',
                    }}
                    onClickSave={handleSave}
                    // onClickDelete={equipCtyMtDelete}
                    title={'배치 정보'}
                    columnDefs={columnDefs}
                />
            </div>
        </div>
    );
};
export default SCSysMngLegacyBatchQuartz;
