import path from 'path';
import webpack from 'webpack';
import {Configuration} from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';

const webpackConfig = (): Configuration => ({
    entry: './src/index.tsx',
    mode: 'production',
    resolve: {
        modules: [path.resolve(__dirname, './src'), 'node_modules'],
        extensions: ['.ts', '.tsx', '.js'],
        plugins: [new TsconfigPathsPlugin({configFile: './tsconfig.json'})],
        alias: {
            '@': path.resolve(__dirname, 'src/'),
            '@components': path.resolve(__dirname, 'src/components/'),
            '@override': path.resolve(__dirname, 'src/override/'),
            '@page': path.resolve(__dirname, 'src/page/'),
            '@system': path.resolve(__dirname, 'src/page/system/'),
            '@img': path.resolve(__dirname, 'src/style/img/'),
        },
        fallback: {
            crypto: require.resolve('crypto-browserify'),
            buffer: require.resolve('buffer/'),
            url: require.resolve('url'),
            fs: require.resolve('fs'),
            assert: require.resolve('assert'),
            http: require.resolve('stream-http'),
            https: require.resolve('https-browserify'),
            os: require.resolve('os-browserify/browser'),
            stream: require.resolve('stream-browserify'),
        },
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[fullhash].js',
    },
    // optimization: {
    //     runtimeChunk: 'single',
    //     minimize: true,
    //     minimizer: [new TerserPlugin()],
    //     splitChunks: {
    //         chunks: 'all',
    //         maxInitialRequests: Infinity,
    //         maxSize: 50000,
    //         cacheGroups: {
    //             vendor: {
    //                 test: /[\\/]node_modules[\\/]/,
    //                 name(module) {
    //                     // get the name. E.g. node_modules/packageName/not/this/part.js
    //                     // or node_modules/packageName
    //                     const packageName = module.context.match(
    //                         /[\\/]node_modules[\\/](.*?)([\\/]|$)/,
    //                     )[1];

    //                     // npm package names are URL-safe, but some servers don't like @ symbols
    //                     return `npm.${packageName.replace('@', '')}`;
    //                 },
    //             },
    //         },
    //     },
    // },
    module: {
        rules: [
            {
                test: /\.js$/,
                enforce: 'pre',
                use: ['source-map-loader'],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader',
                        options: {
                            // Prefer `dart-sass`
                            implementation: require('sass'),
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            // modules: true,
                            // camelCase: true,
                            // sourceMap: true
                        },
                    },
                ],
            },
            {
                test: /\.less$/,
                // loader: 'style!css!less!postcss'
                use: [
                    {
                        loader: 'style-loader', // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                    },
                    {
                        loader: 'less-loader', // compiles Less to CSS
                    },
                ],
            },
            {
                test: /\.rdlx-json$/,
                exclude: /(node_modules)/,
                loader: 'json-loader',
            },
            {
                test: /\.(png|jpg|woff|woff2|eot|ttf|svg|gif|otf|mp4|rdlx-json)$/,
                loader: 'file-loader',
                options: {
                    limit: 1024,
                    name: '[name].[ext]',
                    publicPath: './dist/',
                    outputPath: './dist/',
                },
            },
            {
                test: /\.(ts|tsx|js|jsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
        ],
    },
    devtool: false,
    plugins: [
        new HtmlWebpackPlugin({
            // HtmlWebpackPlugin simplifies creation of HTML files to serve your webpack bundles
            template: './public/index.html',
            favicon: 'public/favicon.ico',
        }),
        // DefinePlugin allows you to create global constants which can be configured at compile time
        new webpack.DefinePlugin({
            // WEBPACK_CONFIG_API_URL: JSON.stringify('http://wat.lscns.com'),
            WEBPACK_CONFIG_API_URL: JSON.stringify('http://192.168.0.169:8090'),
            WEBPACK_CONFIG_API_QUARTZ: JSON.stringify(
                'http://10.15.19.30:8070',
            ),
        }),
        new ForkTsCheckerWebpackPlugin({
            // Speeds up TypeScript type checking and ESLint linting (by moving each to a separate process)
            eslint: {
                files: './src/**/*.{ts,tsx,js,jsx}',
            },
        }),
        new CompressionPlugin({
            algorithm: 'gzip',
            test: /\.js$|\.tsx$|\.ts$|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0.8,
        }),
    ],
});

export default webpackConfig;
